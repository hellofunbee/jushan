//自定义-----
// var baseWlwUrl = 'http://127.0.0.1:8888'; //自动化的基本地址
var baseWlwUrl = 'http://192.168.10.160:8888'; //自动化的基本地址
// var baseWlwUrl = 'http://39.107.119.69:8888'; //自动化的基本地址
var topColor = ['#47aa57', '#9bc24d', '#f29140', '#eb4745', '#e32088', '#92398b', '#0090cb', '#fac736', '#ec4846']

function addBtns(btns) {
    // parent.$(".pull-left").html("")
    if (btns) {
        $(btns).each(function (i, e) {

            if (!e.hidden) {
                $(".bottom-btns").append('<button class="btn btn-xxs btn-success" id="' + e.id + '" type="button">' + e.name + '</button>');

                $("#" + e.id).click(function () {
                    $.modal.openTab(e.name, e.url);
                    /* parent.$('.menuTab').text(parent.$('.menuTab').text() + '/' + e.name);*/
                })
            }


        })
    }

}
;

/**************************************时间格式化处理************************************/
function dateFtt(date, fmt) { //author: meizz

    try {
        if (!date) {
            return '--'
        }
        if (!fmt) {
            fmt = 'yyyy-MM-dd'
        }
        date = new Date(date);
        var o = {
            "M+": date.getMonth() + 1,     //月份
            "d+": date.getDate(),     //日
            "h+": date.getHours(),     //小时
            "m+": date.getMinutes(),     //分
            "s+": date.getSeconds(),     //秒
            "q+": Math.floor((date.getMonth() + 3) / 3), //季度
            "S": date.getMilliseconds()    //毫秒
        };
        if (/(y+)/.test(fmt))
            fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (var k in o)
            if (new RegExp("(" + k + ")").test(fmt))
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        return fmt;
    } catch (e) {
        console.log(e);
        return '--';
    }
}

//录入的时候 表单确认公用方法

function myPost(url, param, callback, onError) {
    $.ajax({
        type: "post",
        url: url,
        data: param,
        dataType: 'JSON',
        success: function (data) {
            if (callback) {
                callback(data);
            } else {
                console.log(data);
            }
        },
        error: function (e) {

            if (onError) {
                onError(e);
            } else {
                layer.msg(e.message);
            }
        }
    });
}

/*过滤富文本标签，只保留文字*/
function getText(value, limit) {
    if (!value) {
        return '--';
    }
    value = value.replace(/(\n)/g, "");
    value = value.replace(/(\t)/g, "");
    value = value.replace(/(\r)/g, "");
    value = value.replace(/<\/?[^>]*>/g, "");
    value = value.replace(/\s*/g, "");
    if (limit) {
        if (value && value.length > limit) {
            return value.substring(0, limit) + "...";
        }
    }
    return value;
}

/**
 * 打印
 */
$("#print-btn").click(function () {

    var orderType = $(this).attr("orderType");
    var nohide = $(this).attr("nohide");
    var nbsp = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

    // $("#printArea").printArea();
    $("#printArea").printThis({
        debug: false,
        importCSS: true,
        importStyle: true,
        printContainer: true,
        loadCSS: "/Content/Themes/Default/style.css",
        pageTitle: null,
        header: '<h1>' + $('#ordercode').text() + '</h1>',
        removeInline: true,
        printDelay: 333,
        formValues: false,
        beforePrint: function (el) {
            console.log($(el).html())
            $(el).find("#toolbar").remove()
            $(el).find("#toolbar1").remove()
            $(el).find("#ordercode").remove()
            $(el).find("#print-btn").remove()
            $(el).find(".pagination").remove()

            if (orderType == 1) {
                $(el).find("#autograph").html("生产记录人:" + nbsp + "记录时间：")
                $(el).find("#autograph").show()
            }
            if (orderType == 2) {
                $(el).find("#autograph").html("育苗记录人:" + nbsp + "记录时间：")
                $(el).find("#autograph").show()
            }
            if (orderType == 3) {
                $(el).find("#autograph").html("订单记录人:" + nbsp + "记录时间：")
                $(el).find("#autograph").show()
            }
            if (orderType == 4) {
                $(el).find("#autograph").html("检测人员:" + nbsp + "检测时间：")
                $(el).find("#autograph").show()
            }
            if (orderType == 7) {
                $("#autograph").html("入库经手人:" + nbsp + "入库时间：")
                $("#autograph").show()
            }
            if (orderType == 8) {
                $(el).find("#autograph").html("  货车押运人员：" + nbsp + "司机人员：" + nbsp + "铅封人员：" + nbsp + "  走车时间：" + nbsp + " 出库经手人：  ")
                $(el).find("#autograph").show()
            }
            if (orderType == 9) {
                $(el).find("#autograph").html("施药日期：" + nbsp + "  施药人签字：" + nbsp + " 植保负责人签字：")
                $(el).find("#autograph").show()
            }
            if (orderType == 10) {
                $(el).find("#autograph").html("入库经手人：" + nbsp + " 入库时间：  ")
                $(el).find("#autograph").show()
            }
            if (orderType == 11) {
                $(el).find("#autograph").html("出库经手人员：" + nbsp + " 出库时间：" + nbsp + " 牛奶接收人： ")
                $(el).find("#autograph").show()
            }
            if (orderType == 12) {
                $(el).find("#autograph").html("入库经手人:" + nbsp + " 入库时间： ")
                $(el).find("#autograph").show()
            }
            if (orderType == 13) {
                $(el).find("#autograph").html("出栏经手人：" + nbsp + " 菜房接收人：")
                $(el).find("#autograph").show()
            }
            if (orderType == 14) {
                $(el).find("#autograph").html("牛舍诊疗人:" + nbsp + " 诊疗时间：")
                $(el).find("#autograph").show()
            }
            if (orderType == 15) {
                $(el).find(el).find("#autograph").html("免疫操作员：" + nbsp + " 免疫时间：")
                $(el).find(el).find("#autograph").show()
            }
            if (orderType == 16) {
                $(el).find("#autograph").html("入库经手人：" + nbsp + "  入库时间：")
                $(el).find("#autograph").show()
            }
            if (orderType == 17) {
                $(el).find(el).find("#autograph").html("鸡舍诊疗人员：" + nbsp + " 诊疗时间：")
                $(el).find(el).find("#autograph").show()
            }
            if (orderType == 18) {
                $(el).find("#autograph").html("免疫操作员：" + nbsp + " 免疫时间：")
                $(el).find("#autograph").show()
            }
            if (orderType == 19) {
                $(el).find("#autograph").html("入库经手人：" + nbsp + "  入库时间：")
                $(el).find("#autograph").show()
            }
            if (orderType == 20) {
                $(el).find("#autograph").html("报表记录人：" + nbsp + "  记录时间：")
                $(el).find("#autograph").show()
            }
            if (orderType == 21) {
                $(el).find("#autograph").html("报表记录人：" + nbsp + "  记录时间：")
                $(el).find("#autograph").show()
            }
            if (orderType == 22) {
                $(el).find("#autograph").html("出入库经手人：" + nbsp + "  出入库时间：")
                $(el).find("#autograph").show()
            }
            if (orderType == 23) {
                $(el).find("#autograph").html($('#today').val() + "出库汇总")
                $(el).find("#autograph").show()
            }

            $(el).find(".pagination-detail").remove()
            if (nohide != 1) {
                $(el).find("thead th:last").remove()

                $(el).find("tbody tr").each(function (i, e) {
                    $(e).find("td:last").remove()

                })
            }
            $(el).find("td").css('border', 'solid 1 px ')

        }
    });


});


function setTitle(name, isBack) {
    parent.$('.menuTab').text(name);
    if (isBack) {
        parent.$('.back').show();
    } else {
        parent.$('.back').hide();
    }

}

// setTitle(document.title,true);

function setFrontTimeShortcutBtns(cd,ed) {
    var d = new Date();
    var m = d.getMonth() + 1;
    var day = d.getDate();
    m = m < 10 ? '0' + m : m;
    console.log(cd);
    console.log(ed);
    if (cd && !ed) {
        var thisYear = new Date().getFullYear() + "-01-01";
        var thisMonth = new Date().getFullYear() + "-" + m + "-01";
        var thisDay = new Date().getFullYear() + "-" + m + "-" + day;
        if (cd == thisYear) {
            $('.thisYear').removeClass("btn-primary").addClass("btn-success")
        } else if (cd == thisMonth) {
            $('.thisMonth').removeClass("btn-primary").addClass("btn-success")
        } else if (cd == thisDay) {
            $('.thisDay').removeClass("btn-primary").addClass("btn-success")
        }
    }
}

