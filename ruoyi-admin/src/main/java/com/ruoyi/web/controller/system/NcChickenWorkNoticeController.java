package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.service.*;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 工作提醒 信息操作处理
 *
 * @author ruoyi
 * @date 2019-09-09
 */
@Controller
@RequestMapping("/system/ncChickenWorkNotice")
public class NcChickenWorkNoticeController extends BaseController {
    private String prefix = "system/ncChickenWorkNotice";
    private String feedPrefix="system/screen";

    @Autowired
    private INcWorkNoticeService ncChickenWorkNoticeService;
    @Autowired
    private INcYaoClassService yaoClassService;
    @Autowired
    private INcWorkNoticeService ncWorkNoticeService;
    @Autowired
    private INcCowsDisinfectionService ncCowsDisinfectionser;
    @Autowired
    private INcFeedService ncFeedService;
    @Autowired
    private INcFeedLogService ncFeedLogService;
    @Autowired
    private INcFeedService feedService;
    @Autowired
    private INcInputChickenService inputChickenService;
    @RequiresPermissions("system:ncChickenWorkNotice:view")
    @GetMapping()
    public String ncChickenWorkNotice(ModelMap map)
    {
        Integer zFfeed=feedService.stockAmout(1);

        map.put("zFfeed",zFfeed);

        Integer fFfeed=feedService.stockAmout(2);

        map.put("fFfeed",fFfeed);

        Integer chickStackAamount=inputChickenService.chickenStackAmount();

        map.put("chickStackAamount",chickStackAamount);
        return prefix + "/ncChickenWorkNotice";
    }

    /**
     * 查询工作提醒列表
     */
    @RequiresPermissions("system:ncChickenWorkNotice:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo    list(NcWorkNotice ncChickenWorkNotice,ModelMap map)
    {
        startPage();
        List<NcWorkNotice> list = ncChickenWorkNoticeService.selectNcWorkNoticeList(ncChickenWorkNotice);
        return getDataTable(list);
    }
    //工作提醒中的鸡舍
    @RequiresPermissions("system:ncChickenWorkNotice:ncadd")
    @GetMapping("/ncadd")
    public String ncWorkNotice()

    {
        return prefix + "/ncadd";
    }


    /**
     * 导出工作提醒列表
     */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(NcWorkNotice ncChickenWorkNotice)
    {
        List<NcWorkNotice> list = ncChickenWorkNoticeService.selectNcWorkNoticeList(ncChickenWorkNotice);
        ExcelUtil<NcWorkNotice> util = new ExcelUtil<NcWorkNotice>(NcWorkNotice.class);
        return util.exportExcel(list, "ncChickenWorkNotice");
    }

    /**
     * 新增工作提醒
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存工作提醒
     */
    @RequiresPermissions("system:ncChickenWorkNotice:add")
    @Log(title = "工作提醒", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(NcWorkNotice ncChickenWorkNotice)
    {
        return toAjax(ncChickenWorkNoticeService.insertNcWorkNotice(ncChickenWorkNotice));
    }

    /**
     * 修改工作提醒
     */
    @GetMapping("/edit/{workNoticeId}")
    public String edit(@PathVariable("workNoticeId") Long workNoticeId, ModelMap mmap)
    {
        NcWorkNotice ncChickenWorkNotice = ncChickenWorkNoticeService.selectNcWorkNoticeById(workNoticeId);
        mmap.put("ncChickenWorkNotice", ncChickenWorkNotice);
        return prefix + "/edit";
    }

    /**
     * 修改保存工作提醒
     */
    @RequiresPermissions("system:ncChickenWorkNotice:edit")
    @Log(title = "工作提醒", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(NcWorkNotice ncChickenWorkNotice)
    {
        return toAjax(ncChickenWorkNoticeService.updateNcWorkNotice(ncChickenWorkNotice));
    }

    /**
     * 删除工作提醒
     */
    @RequiresPermissions("system:ncChickenWorkNotice:remove")
    @Log(title = "工作提醒", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(ncChickenWorkNoticeService.deleteNcWorkNoticeByIds(ids));
    }
    /**
     * 消毒记录    system:ncGoods:disinfection
     * 消毒      disinfection
     */
    @RequiresPermissions("system:ncChickenWorkNotice:disinfection")
    @Log(title = "农事", businessType = BusinessType.INSERT)
    @GetMapping("/disinfection/{workNoticeId}")
    public String disinfection(@PathVariable("workNoticeId") Long workNoticeId,ModelMap mmap){
        NcWorkNotice disinfection = ncChickenWorkNoticeService.selectNcWorkNoticeById(workNoticeId);
        mmap.put("disinfection",disinfection);

        return  prefix +"/disinfection";
    }
    /**
     * 消毒记录保存
     */

    @RequiresPermissions("system:ncChickenWorkNotice:disinfection")
    @Log(title = "消毒管理",businessType = BusinessType.INSERT)
    @PostMapping("/disinfection")
    @ResponseBody
    public AjaxResult addSave(NcCowsDisinfection ncCowsDisinfection){
        SysUser sysUser = ShiroUtils.getSysUser();
        ncCowsDisinfection.setDfUser(sysUser.getLoginName());
        //设置管理类型 牛舍 为2
        ncCowsDisinfection.setManagerType(1);

        //根据yao_id查找药品批次 ncCowsDisinfection.getYaoId()
        NcYaoClass ncYaoClass = yaoClassService.selectYaoClassById(ncCowsDisinfection.getYaoId());

        ncCowsDisinfection.setDfCode(ncYaoClass.getCode());

        //消毒结束将消毒时间放到最后修改时间
        NcWorkNotice ncWorkNotice = new NcWorkNotice();
        ncWorkNotice.setLastTime(ncCowsDisinfection.getDfDate());
        ncWorkNotice.setWorkNoticeId(ncCowsDisinfection.getWorkNoticeId());
        ncWorkNoticeService.updateNcWorkNotice(ncWorkNotice);

        return toAjax(ncCowsDisinfectionser.insertNcCowsDisinfection(ncCowsDisinfection));
    }
    /**
     * 饲喂操作
     */
    @RequiresPermissions("system:ncChickenWorkNotice:feed")
    @Log(title = "农事", businessType = BusinessType.INSERT)
    @GetMapping("/feed/{workNoticeId}")
    public String feed(@PathVariable("workNoticeId") Long workNoticeId, ModelMap mmap) {
        mmap.put("feed", ncWorkNoticeService.selectNcWorkNoticeById(workNoticeId));
        NcFeed feed1=new NcFeed();
        feed1.setFeedType(1);
        feed1.setManagerType(1);
        mmap.put("meterial",ncFeedService.selectNcFeedList(feed1));
        NcFeed feed=new NcFeed();
        feed.setFeedType(2);
        feed.setManagerType(1);
        mmap.put("meterial1",ncFeedService.selectNcFeedList(feed));
        return prefix + "/feed";
    }

    /**
     * 饲喂提交
     */
    @RequiresPermissions("system:ncChickenWorkNotice:feed")
    @Log(title = "饲喂提交", businessType = BusinessType.INSERT)
    @PostMapping("/feed")
    @ResponseBody
    public AjaxResult addSaveWork(NcFeedLog feedLog) {
        Map feedes = feedLog.getFeedes();
        NcFeedLog ncFeedLog = new NcFeedLog();
        Timestamp d = new Timestamp(System.currentTimeMillis());
        if(feedes.get("z_feedName")!=null){
            //通过feedId来获取库存
            NcFeed feed=ncFeedService.selectNcFeedById(Long.parseLong(feedes.get("z_feedName")+""));
            if (feed.getStockAmout().compareTo(BigDecimal.valueOf(Double.parseDouble(feedes.get("z_num")+"")))>-1) {
                ncFeedLog.setFeedId(Long.parseLong(feedes.get("z_feedName")+"") );
                ncFeedLog.setOutputAmount(BigDecimal.valueOf(Double.parseDouble(feedes.get("z_num")+"")));
                ncFeedLog.setFeedType(1);
                ncFeedLog.setManagerType(1);
                ncFeedLog.setCreateTime(d);
                ncFeedLog.setCreateBy(ShiroUtils.getSysUser().getLoginName());
                ncFeedLog.setReceiver(ShiroUtils.getSysUser().getLoginName());
                ncFeedLogService.insertNcFeedLog(ncFeedLog);
                feed.setStockAmout(feed.getStockAmout().subtract(BigDecimal.valueOf(Double.parseDouble(feedes.get("z_num")+""))));
                ncFeedService.updateNcFeed(feed);
            }
            else{
                return AjaxResult.error("主饲料库存不足");
            }

        }

        if(feedes.get("f_feedName")!=null){
            NcFeed feed1 = ncFeedService.selectNcFeedById(Long.parseLong(feedes.get("z_feedName")+"") );
            NcFeed feed = ncFeedService.selectNcFeedById(Long.parseLong(feedes.get("f_feedName")+"") );
            if (feed.getStockAmout().compareTo(BigDecimal.valueOf(Double.parseDouble(feedes.get("f_num") + "")))>-1) {

                ncFeedLog.setFeedId(Long.parseLong(feedes.get("f_feedName")+""));
                ncFeedLog.setOutputAmount(BigDecimal.valueOf(Double.parseDouble(feedes.get("f_num") + "")));
                ncFeedLog.setFeedType(2);
                ncFeedLog.setCreateBy(ShiroUtils.getSysUser().getLoginName());
                ncFeedLog.setReceiver(ShiroUtils.getSysUser().getLoginName());
                ncFeedLogService.insertNcFeedLog(ncFeedLog);
                feed.setStockAmout(feed.getStockAmout().subtract(BigDecimal.valueOf(Double.parseDouble(feedes.get("f_num") + ""))));	//设置出库后库存
                ncFeedService.updateNcFeed(feed);
            } else {
                ncFeedLogService.deleteNcFeedLogByIds(String.valueOf(ncFeedLog.getFeedLogId()));
                feed1.setStockAmout(feed1.getStockAmout().add(BigDecimal.valueOf(Double.parseDouble(feedes.get("z_num") + ""))));	//设置出库后库存
                ncFeedService.updateNcFeed(feed1);
                return AjaxResult.error("辅饲料库存不足");
            }

        }
        //设置最后操作时间
        NcWorkNotice ncWorkNotice = new NcWorkNotice();
        ncWorkNotice.setLastTime(new Date());
        ncWorkNotice.setWorkNoticeId(feedLog.getWorkNoticeId());
        ncWorkNoticeService.updateNcWorkNotice(ncWorkNotice);
        return AjaxResult.success("成功");
    }
    /**
        牛
     * 统计喂养的饲料

     */
    @GetMapping("/feedAmountsCows")
    public String feedAmountsCows( ModelMap map,String beginTime,String endTime,NcFeedLog feedLog) {
        if (StringUtils.isNotEmpty(beginTime)) {
            feedLog.getParams().put("beginTime", beginTime);
        }
        if (StringUtils.isNotEmpty(endTime)) {
            feedLog.getParams().put("endTime", endTime);
        }
        map.put("beginTime", beginTime);
        map.put("endTime", endTime);
        //总饲料数
        double feedAmount=0;
        if(ncFeedLogService.selectNcFeedLogAmountCows(feedLog).getAmount() != 0){
            feedAmount=ncFeedLogService.selectNcFeedLogAmountCows(feedLog).getAmount();
            map.put("feedAmount",new DecimalFormat("0.00").format(Double.valueOf(feedAmount)));
        }
        else{
            map.put("feedAmount","0.00");
        }



        //主饲料数量
        feedLog.setType(1);
        if(ncFeedLogService.selectNcFeedLogAmountCows(feedLog).getAmount() != 0){
            double zAmount=ncFeedLogService.selectNcFeedLogAmountCows(feedLog).getAmount();
            map.put("zfeedAmount",new DecimalFormat("0.00").format(Double.valueOf(zAmount)));
            map.put("zfeedAmountRatio", new DecimalFormat("0.00").format(Double.valueOf(zAmount /  feedAmount * 100)));
        }
        else{
            map.put("zfeedAmount","0.00");
            map.put("zfeedAmountRatio","0.00");
        }

        //辅饲料数量
        feedLog.setType(2);
        if(ncFeedLogService.selectNcFeedLogAmountCows(feedLog).getAmount() != 0){
            double fAmount=ncFeedLogService.selectNcFeedLogAmountCows(feedLog).getAmount();
            map.put("ffeedAmount",new DecimalFormat("0.00").format(Double.valueOf(fAmount)));
            map.put("ffeedAmountRatio", new DecimalFormat("0.00").format(Double.valueOf(fAmount /  feedAmount * 100)));
        }
        else{
            map.put("ffeedAmount","0.00");
            map.put("ffeedAmountRatio","0.00");
        }
        return prefix + "/feedAmountCows";
    }
    /**牛舍
     * @param dataSpan 时间跨度 1:年 2：月 3：日
     * @return
     */
    @PostMapping("/cowFeedQuery")
    @ResponseBody
    public TableDataInfo cowFeedQuery(Integer dataSpan, String beginTime, String endTime) {
        //

        List<Map> list = ncFeedLogService.selectCowCountByTime(dataSpan, beginTime, endTime);
        return getDataTable(list);
    }
    /**
     * 鸡
     * 前端统计喂养饲料
     */
    @GetMapping("/feedAmounts")
    public String feedAmounts( ModelMap map,String beginTime,String endTime,NcFeedLog feedLog) {
        if (StringUtils.isNotEmpty(beginTime)) {
            feedLog.getParams().put("beginTime", beginTime);
        }
        if (StringUtils.isNotEmpty(endTime)) {
            feedLog.getParams().put("endTime", endTime);
        }
        map.put("beginTime", beginTime);
        map.put("endTime", endTime);
        //总饲料数
        double feedAmount=0;
        if(ncFeedLogService.selectNcFeedLogAmount(feedLog).getAmount() != 0){
            feedAmount=ncFeedLogService.selectNcFeedLogAmount(feedLog).getAmount();
            map.put("feedAmount",new DecimalFormat("0.00").format(Double.valueOf(feedAmount)));
        }
        else{
            map.put("feedAmount","0.00");
        }



        //主饲料数量
        feedLog.setType(1);
        if(ncFeedLogService.selectNcFeedLogAmount(feedLog).getAmount() != 0){
            double zAmount=ncFeedLogService.selectNcFeedLogAmount(feedLog).getAmount();
            map.put("zfeedAmount",new DecimalFormat("0.00").format(Double.valueOf(zAmount)));
            map.put("zfeedAmountRatio", new DecimalFormat("0.00").format(Double.valueOf(zAmount/feedAmount*100)));
        }
        else{
            map.put("zfeedAmount","0.00");
            map.put("zfeedAmountRatio","0.00");
        }

        //辅饲料数量
        feedLog.setType(2);
        if(ncFeedLogService.selectNcFeedLogAmount(feedLog).getAmount() != 0){
            double fAmount=ncFeedLogService.selectNcFeedLogAmount(feedLog).getAmount();
            map.put("ffeedAmount",new DecimalFormat("0.00").format(Double.valueOf(fAmount)));
            map.put("ffeedAmountRatio", new DecimalFormat("0.00").format(Double.valueOf(fAmount/feedAmount*100)));
        }
        else{
            map.put("ffeedAmount","0.00");
            map.put("ffeedAmountRatio","0.00");
        }
        return prefix + "/feedAmount";
    }
    /**
     * @param dataSpan 时间跨度 1:年 2：月 3：日
     * @return
     */
    @PostMapping("/chickenFeedQuery")
    @ResponseBody
    public TableDataInfo chickenFeedQuery(Integer dataSpan, String beginTime, String endTime) {
        //

        List<Map> list = ncFeedLogService.selectCountByTime(dataSpan, beginTime, endTime);
        return getDataTable(list);
    }
    //牛舍喂养饲料大屏
    @GetMapping("/niushe2")
    public String cowfeedAmounts( ModelMap map) {
        Calendar   cal  =   Calendar.getInstance();
        //获取最近7天的数据
        List<Double> chickenFeed=new ArrayList<Double>();
        NcFeedLog ncFeedLog=new NcFeedLog();
        ncFeedLog.getParams().put("siwei",2);
        String[] last7Day=last7Day(cal);
        for(int i=0;i<last7Day.length;i++) {
            ncFeedLog.getParams().put("day",last7Day[i]);
            double amount=ncFeedLogService.selectNcFeedLogScreen(ncFeedLog);
            chickenFeed.add(amount);
        }
        //饲料
        map.put("cowFeed", chickenFeed);
        map.put("last7Day", last7Day);
        //主饲料
        List<Double> chickenzFeed=new ArrayList<Double>();
        ncFeedLog.setType(1);
        for(int i=0;i<last7Day.length;i++) {
            ncFeedLog.getParams().put("day",last7Day[i]);
            double amount=ncFeedLogService.selectNcFeedLogScreen(ncFeedLog);
            chickenzFeed.add(amount);
        }
        map.put("cowzFeed", chickenzFeed);
        //辅饲料
        List<Double> chickenfFeed=new ArrayList<Double>();
        ncFeedLog.setType(2);
        for(int i=0;i<last7Day.length;i++) {
            ncFeedLog.getParams().put("day",last7Day[i]);
            double amount=ncFeedLogService.selectNcFeedLogScreen(ncFeedLog);
            chickenfFeed.add(amount);
        }
        map.put("cowfFeed", chickenfFeed);
        //今天以及昨天同比
        NcFeedLog feed1=new NcFeedLog();
        feed1.getParams().put("siwei",2);
        Calendar   cal1=Calendar.getInstance();
        SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
        String time2 = format2.format(Calendar.getInstance().getTime());
        feed1.getParams().put("day",time2);
        double todayAmount=ncFeedLogService.selectNcFeedLogScreen(feed1);
        map.put("day", todayAmount);
        cal1.add(Calendar.DATE,   -1);
        String yesterday =format2.format(cal1.getTime());
        feed1.getParams().put("day",yesterday);
        double yesterdayAmount=ncFeedLogService.selectNcFeedLogScreen(feed1);
        if(yesterdayAmount != 0){
            map.put("dayRatio", new DecimalFormat("0.00").format(Double.valueOf((todayAmount- yesterdayAmount)/  yesterdayAmount * 100)));
        }
        else{
            map.put("dayRatio", new DecimalFormat("0.00").format((double)Math.round(todayAmount*100)));
        }
        //本月以及上个月同比
        NcFeedLog feed2=new NcFeedLog();
        feed2.getParams().put("siwei",2);
        Calendar   cal2=Calendar.getInstance();
        String month=null;
        if (cal2.get(Calendar.MONTH) + 1 < 10) {
            month= cal2.get(Calendar.YEAR) + "-0" + (cal2.get(Calendar.MONTH) + 1);
        }
        else {
            month = cal2.get(Calendar.YEAR) + "-" + (cal2.get(Calendar.MONTH) + 1);
        }
        feed2.getParams().put("month",month);
        double monthAmount=ncFeedLogService.selectNcFeedLogScreen(feed2);
        //上个月
        String lastMonth=null;
        cal2.add(Calendar.MONTH,-1);
        if (cal2.get(Calendar.MONTH) + 1 < 10) {
            lastMonth= cal2.get(Calendar.YEAR) + "-0" + (cal2.get(Calendar.MONTH) + 1);
        }
        else {
            lastMonth = cal2.get(Calendar.YEAR) + "-" + (cal2.get(Calendar.MONTH) + 1);
        }

        feed2.getParams().put("month",lastMonth);
        double laMonth=ncFeedLogService.selectNcFeedLogScreen(feed2);
        map.put("month", monthAmount);
        if(laMonth != 0){
            map.put("monthRatio", new DecimalFormat("0.00").format(Double.valueOf((monthAmount- laMonth)/  laMonth * 100)));
        }
        else{
            map.put("monthRatio", new DecimalFormat("0.00").format((double)Math.round(monthAmount*100)));
        }
        //本年以及去年月同比
        NcFeedLog feed3=new NcFeedLog();
        feed3.getParams().put("siwei",2);
        Calendar   cal3=Calendar.getInstance();
        String year=cal3.get(Calendar.YEAR)+"";
        feed3.getParams().put("year",year);
        double thisYear=ncFeedLogService.selectNcFeedLogScreen(feed3);
        cal3.add(Calendar.YEAR,-1);
        String year1 = cal3.get(Calendar.YEAR) + "";
        feed3.getParams().put("year",year1);
        double lastYear=ncFeedLogService.selectNcFeedLogScreen(feed3);
        map.put("year", thisYear);
        if(lastYear != 0){
            map.put("yearRatio", new DecimalFormat("0.00").format(Double.valueOf((thisYear- lastYear)/  lastYear * 100)));
        }
        else{
            map.put("yearRatio", new DecimalFormat("0.00").format((double)Math.round(thisYear*100)));
        }
        return feedPrefix+"/cowFeed";
    }
    //鸡舍喂养饲料大屏
    @GetMapping("/jishe2")
    public String chickenfeedAmounts( ModelMap map) {
        Calendar   cal  =   Calendar.getInstance();
        //获取最近7天的数据
        List<Double> chickenFeed=new ArrayList<Double>();
        NcFeedLog ncFeedLog=new NcFeedLog();
        ncFeedLog.getParams().put("siwei",1);
        String[] last7Day=last7Day(cal);
        for(int i=0;i<last7Day.length;i++) {
            ncFeedLog.getParams().put("day",last7Day[i]);
            double amount=ncFeedLogService.selectNcFeedLogScreen(ncFeedLog);
            chickenFeed.add(amount);
        }
        //饲料
        map.put("chickenFeed", chickenFeed);
        map.put("last7Day", last7Day);
        //主饲料
        List<Double> chickenzFeed=new ArrayList<Double>();
       ncFeedLog.setType(1);
        for(int i=0;i<last7Day.length;i++) {
            ncFeedLog.getParams().put("day",last7Day[i]);
            double amount=ncFeedLogService.selectNcFeedLogScreen(ncFeedLog);
            chickenzFeed.add(amount);
        }
        map.put("chickenzFeed", chickenzFeed);
        //辅饲料
        List<Double> chickenfFeed=new ArrayList<Double>();
        ncFeedLog.setType(2);
        for(int i=0;i<last7Day.length;i++) {
            ncFeedLog.getParams().put("day",last7Day[i]);
            double amount=ncFeedLogService.selectNcFeedLogScreen(ncFeedLog);
            chickenfFeed.add(amount);
        }
        map.put("chickenfFeed", chickenfFeed);
        //今天以及昨天同比
        NcFeedLog feed1=new NcFeedLog();
        feed1.getParams().put("siwei",1);
        Calendar   cal1=Calendar.getInstance();
        SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
        String time2 = format2.format(Calendar.getInstance().getTime());
        feed1.getParams().put("day",time2);
        double todayAmount=ncFeedLogService.selectNcFeedLogScreen(feed1);
        map.put("day", todayAmount);
        cal1.add(Calendar.DATE,   -1);
        String yesterday =format2.format(cal1.getTime());
        feed1.getParams().put("day",yesterday);
        double yesterdayAmount=ncFeedLogService.selectNcFeedLogScreen(feed1);
        if(yesterdayAmount != 0){
            map.put("dayRatio", new DecimalFormat("0.00").format(Double.valueOf((todayAmount- yesterdayAmount)/  yesterdayAmount * 100)));
        }
        else{
            map.put("dayRatio", new DecimalFormat("0.00").format((double)Math.round(todayAmount*100)));
        }
        //本月以及上个月同比
        NcFeedLog feed2=new NcFeedLog();
        feed2.getParams().put("siwei",1);
        Calendar   cal2=Calendar.getInstance();
        String month=null;
        if (cal2.get(Calendar.MONTH) + 1 < 10) {
            month= cal2.get(Calendar.YEAR) + "-0" + (cal2.get(Calendar.MONTH) + 1);
        }
        else {
            month = cal2.get(Calendar.YEAR) + "-" + (cal2.get(Calendar.MONTH) + 1);
        }
        feed2.getParams().put("month",month);
        double monthAmount=ncFeedLogService.selectNcFeedLogScreen(feed2);
        //上个月
        String lastMonth=null;
        cal2.add(Calendar.MONTH,-1);
        if (cal2.get(Calendar.MONTH) + 1 < 10) {
            lastMonth= cal2.get(Calendar.YEAR) + "-0" + (cal2.get(Calendar.MONTH) + 1);
        }
        else {
            lastMonth = cal2.get(Calendar.YEAR) + "-" + (cal2.get(Calendar.MONTH) + 1);
        }

        feed2.getParams().put("month",lastMonth);
        double laMonth=ncFeedLogService.selectNcFeedLogScreen(feed2);
        map.put("month", monthAmount);
        if(laMonth != 0){
            map.put("monthRatio", new DecimalFormat("0.00").format(Double.valueOf((monthAmount- laMonth)/  laMonth * 100)));
        }
        else{
            map.put("monthRatio", new DecimalFormat("0.00").format((double)Math.round(monthAmount*100)));
        }
        //本年以及去年月同比
        NcFeedLog feed3=new NcFeedLog();
        feed3.getParams().put("siwei",1);
        Calendar   cal3=Calendar.getInstance();
        String year=cal3.get(Calendar.YEAR)+"";
        feed3.getParams().put("year",year);
        double thisYear=ncFeedLogService.selectNcFeedLogScreen(feed3);
        cal3.add(Calendar.YEAR,-1);
        String year1 = cal3.get(Calendar.YEAR) + "";
        feed3.getParams().put("year",year1);
        double lastYear=ncFeedLogService.selectNcFeedLogScreen(feed3);
        map.put("year", thisYear);
        if(lastYear != 0){
            map.put("yearRatio", new DecimalFormat("0.00").format(Double.valueOf((thisYear- lastYear)/  lastYear * 100)));
        }
        else{
            map.put("yearRatio", new DecimalFormat("0.00").format((double)Math.round(thisYear*100)));
        }
        return feedPrefix+"/chickenFeed";
    }
    public String[] getDate12( Calendar cal){
        String[] last12Months=new String[12];
        for (int i = 0; i < 12; i++) {
            if (cal.get(Calendar.MONTH) + 1 < 10) {
                last12Months[11 - i] = cal.get(Calendar.YEAR) + "-0" + (cal.get(Calendar.MONTH) + 1);
            } else {
                last12Months[11 - i] = cal.get(Calendar.YEAR) + "-" + (cal.get(Calendar.MONTH) + 1);
            }
            cal.set(Calendar.MONTH, cal.get(Calendar.MONTH) - 1); //逐次往前推1个月

        }
        return  last12Months;
    }
    //产生最近一周的时间
    public String[] last7Day(Calendar cal) {
        String[] last7Day = new String[7];
        for (int i = 0; i < 7; i++) {
            if (cal.get(Calendar.MONTH) + 1 < 10) {
                if (cal.get(Calendar.DATE) < 10) {
                    last7Day[6 - i] = cal.get(Calendar.YEAR) + "-0" + (cal.get(Calendar.MONTH) + 1) + "-0" + (cal.get(Calendar.DATE));
                } else {
                    last7Day[6 - i] = cal.get(Calendar.YEAR) + "-0" + (cal.get(Calendar.MONTH) + 1) + "-" + (cal.get(Calendar.DATE));
                }
            } else {
                if (cal.get(Calendar.DATE) < 10) {
                    last7Day[6 - i] = cal.get(Calendar.YEAR) + "-" + (cal.get(Calendar.MONTH) + 1) + "-0" + (cal.get(Calendar.DATE));
                } else {
                    last7Day[6 - i] = cal.get(Calendar.YEAR) + "-" + (cal.get(Calendar.MONTH) + 1) + "-" + (cal.get(Calendar.DATE));
                }

            }
            cal.set(Calendar.DATE, cal.get(Calendar.DATE) - 1); //逐次往前推1天*/

        }
        return last7Day;
    }

}
