package com.ruoyi.web.controller.tool;

import com.ruoyi.system.service.ISysUserOnlineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Configuration
@EnableScheduling
public class SessionTask {

    @Autowired
    ISysUserOnlineService onlineService;

    /*把进入当前阶段的计划的第一阶段状态改为-状态 3 -->2 */
    @Scheduled(cron = "0/30 * * * * ?")
    //@Scheduled(fixedRate=5000)
    private void startPlan() {

    }


}