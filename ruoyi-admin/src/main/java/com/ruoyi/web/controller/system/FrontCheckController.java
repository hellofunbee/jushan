package com.ruoyi.web.controller.system;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.NcCheck;
import com.ruoyi.system.domain.NcOrderCf;
import com.ruoyi.system.service.INcCheckService;
import com.ruoyi.system.service.INcOrderCfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * ********************检测前端接口*********************
 *
 * @author ruoyi
 * @date 2019-09-01
 */
@Controller
@RequestMapping("/system/front_check")
public class FrontCheckController extends BaseController {
    private String prefix = "system/front_check";

    @Autowired
    private INcCheckService checkService;
    @Autowired
    private INcOrderCfService orderCfService;



    /*检测*/
    @GetMapping("/testingManagement")
    public String ncZhibaoLogs(NcCheck check,String beginTime,String endTime, ModelMap mmap) {
        if (StringUtils.isNotEmpty(beginTime)) {
            check.getParams().put("beginTime", beginTime);
        }
        if (StringUtils.isNotEmpty(endTime)) {
            check.getParams().put("endTime", endTime);
        }
        //按时间查询育苗计划数量
        /*状态 1:待生成订单 2：待执行 3.在执行 4.已完成*/

        mmap.put("beginTime", beginTime);
        mmap.put("endTime", endTime);
//        mmap.put("caiClass", checkService.getBigCaiClass(check));

        NcOrderCf order  = new NcOrderCf();
//        order.setCforderStatus("5");//已完成的
        if (StringUtils.isNotEmpty(beginTime)) {
            order.getParams().put("beginTime", beginTime);
        }
        if (StringUtils.isNotEmpty(endTime)) {
            order.getParams().put("endTime", endTime);
        }

        orderCfService.setOrderTypes(mmap, order, "3,4,5,6,7,8,9",2);
        return prefix + "/orderManage04-2";
//        return prefix + "/testingManagement";
    }

    /*菜房订单*/
    @GetMapping("/frontCheck")
    public String frontOrderCf(NcCheck chcek, String beginTime, String endTime, String ancestors, ModelMap mmap) {

        mmap.put("ancestors", ancestors);
        mmap.put("beginTime", beginTime);
        mmap.put("endTime", endTime);
        return prefix + "/frontCheck";
    }

}
