package com.ruoyi.web.controller.system;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.TPoint;
import com.ruoyi.system.service.ITPointService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;

/**
 * 物联网节点 信息操作处理
 * 
 * @author ruoyi
 * @date 2019-09-19
 */
@Controller
@RequestMapping("/system/tPoint")
public class TPointController extends BaseController
{
    private String prefix = "system/tPoint";
	
	@Autowired
	private ITPointService tPointService;
	
	@RequiresPermissions("system:tPoint:view")
	@GetMapping()
	public String tPoint()
	{
	    return prefix + "/tPoint";
	}
	
	/**
	 * 查询物联网节点列表
	 */
	@RequiresPermissions("system:tPoint:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(TPoint tPoint)
	{
		startPage();
        List<TPoint> list = tPointService.selectTPointList(tPoint);
		return getDataTable(list);
	}
	
	
	/**
	 * 导出物联网节点列表
	 */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(TPoint tPoint)
    {
    	List<TPoint> list = tPointService.selectTPointList(tPoint);
        ExcelUtil<TPoint> util = new ExcelUtil<TPoint>(TPoint.class);
        return util.exportExcel(list, "tPoint");
    }
	
	/**
	 * 新增物联网节点
	 */
	@GetMapping("/add")
	public String add()
	{
	    return prefix + "/add";
	}
	
	/**
	 * 新增保存物联网节点
	 */
	@RequiresPermissions("system:tPoint:add")
	@Log(title = "物联网节点", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(TPoint tPoint)
	{		
		return toAjax(tPointService.insertTPoint(tPoint));
	}

	/**
	 * 修改物联网节点
	 */
	@GetMapping("/edit/{tpId}")
	public String edit(@PathVariable("tpId") Integer tpId, ModelMap mmap)
	{
		TPoint tPoint = tPointService.selectTPointById(tpId);
		mmap.put("tPoint", tPoint);
	    return prefix + "/edit";
	}
	
	/**
	 * 修改保存物联网节点
	 */
	@RequiresPermissions("system:tPoint:edit")
	@Log(title = "物联网节点", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(TPoint tPoint)
	{		
		return toAjax(tPointService.updateTPoint(tPoint));
	}
	
	/**
	 * 删除物联网节点
	 */
	@RequiresPermissions("system:tPoint:remove")
	@Log(title = "物联网节点", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public AjaxResult remove(String ids)
	{		
		return toAjax(tPointService.deleteTPointByIds(ids));
	}
	
}
