package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.NcSeedRate;
import com.ruoyi.system.service.INcSeedRateService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * 发芽率 信息操作处理
 * 
 * @author ruoyi
 * @date 2019-09-16
 */
@Controller
@RequestMapping("/system/ncSeedRate")
public class NcSeedRateController extends BaseController
{
    private String prefix = "system/ncSeedRate";
	
	@Autowired
	private INcSeedRateService ncSeedRateService;
	
	@RequiresPermissions("system:ncSeedRate:view")
	@GetMapping()
	public String ncSeedRate()
	{
	    return prefix + "/ncSeedRate";
	}
	
	/**
	 * 查询发芽率列表
	 */
	@RequiresPermissions("system:ncSeedRate:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(NcSeedRate ncSeedRate)
	{
		startPage();
        List<NcSeedRate> list = ncSeedRateService.selectNcSeedRateList(ncSeedRate);
		return getDataTable(list);
	}
	
	
	/**
	 * 导出发芽率列表
	 */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(NcSeedRate ncSeedRate)
    {
    	List<NcSeedRate> list = ncSeedRateService.selectNcSeedRateList(ncSeedRate);
        ExcelUtil<NcSeedRate> util = new ExcelUtil<NcSeedRate>(NcSeedRate.class);
        return util.exportExcel(list, "发芽率列表");
    }
	
	/**
	 * 新增发芽率
	 */
	@GetMapping("/add")
	public String add()
	{
	    return prefix + "/add";
	}
	
	/**
	 * 新增保存发芽率
	 */
	@RequiresPermissions("system:ncSeedRate:add")
	@Log(title = "发芽率", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(NcSeedRate ncSeedRate)
	{
		ncSeedRate.setCreateTime(new Date());
		ncSeedRate.setCreateBy(ShiroUtils.getSysUser().getUserName());
		ncSeedRate = ncSeedRateService.fillValues(ncSeedRate);
		if (ncSeedRate == null) {
			return AjaxResult.error("未找到此品种");
		}
		return toAjax(ncSeedRateService.insertNcSeedRate(ncSeedRate));
	}

	/**
	 * 修改发芽率
	 */
	@GetMapping("/edit/{seedRateId}")
	public String edit(@PathVariable("seedRateId") Long seedRateId, ModelMap mmap)
	{
		NcSeedRate ncSeedRate = ncSeedRateService.selectNcSeedRateById(seedRateId);
		mmap.put("ncSeedRate", ncSeedRate);
	    return prefix + "/edit";
	}
	
	/**
	 * 修改保存发芽率
	 */
	@RequiresPermissions("system:ncSeedRate:edit")
	@Log(title = "发芽率", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(NcSeedRate ncSeedRate)
	{
		ncSeedRate = ncSeedRateService.fillValues(ncSeedRate);
		if (ncSeedRate == null) {
			return AjaxResult.error("未找到此品种");
		}
		return toAjax(ncSeedRateService.updateNcSeedRate(ncSeedRate));
	}
	
	/**
	 * 删除发芽率
	 */
	@RequiresPermissions("system:ncSeedRate:remove")
	@Log(title = "发芽率", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public AjaxResult remove(String ids)
	{		
		return toAjax(ncSeedRateService.deleteNcSeedRateByIds(ids));
	}
	
}
