package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.NcZaisha;
import com.ruoyi.system.service.INcZaishaService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 宰杀 信息操作处理
 * 
 * @author ruoyi
 * @date 2019-09-04
 */
@Controller
@RequestMapping("/system/ncZaisha")
public class NcZaishaController extends BaseController
{
    private String prefix = "system/ncZaisha";
	
	@Autowired
	private INcZaishaService ncZaishaService;
	
	@RequiresPermissions("system:ncZaisha:view")
	@GetMapping()
	public String ncZaisha()
	{
	    return prefix + "/ncZaisha";
	}
	
	/**
	 * 查询宰杀列表
	 */
	@RequiresPermissions("system:ncZaisha:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(NcZaisha ncZaisha)
	{
		startPage();
        List<NcZaisha> list = ncZaishaService.selectNcZaishaList(ncZaisha);
		return getDataTable(list);
	}
	
	
	/**
	 * 导出宰杀列表
	 */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(NcZaisha ncZaisha)
    {
    	List<NcZaisha> list = ncZaishaService.selectNcZaishaList(ncZaisha);
        ExcelUtil<NcZaisha> util = new ExcelUtil<NcZaisha>(NcZaisha.class);
        return util.exportExcel(list, "宰杀列表");
    }
	
	/**
	 * 新增宰杀
	 */
	@GetMapping("/add")
	public String add(ModelMap map)
	{
		String loginName=ShiroUtils.getSysUser().getLoginName();
		map.put("userName",loginName);
		return prefix + "/add";
	}
	
	/**
	 * 新增保存宰杀
	 */
	@RequiresPermissions("system:ncZaisha:add")
	@Log(title = "宰杀", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(NcZaisha ncZaisha)
	{		
		return toAjax(ncZaishaService.insertNcZaisha(ncZaisha));
	}

	/**
	 * 修改宰杀
	 */
	@GetMapping("/edit/{zaishaId}")
	public String edit(@PathVariable("zaishaId") Long zaishaId, ModelMap mmap)
	{
		NcZaisha ncZaisha = ncZaishaService.selectNcZaishaById(zaishaId);
		mmap.put("ncZaisha", ncZaisha);
		String loginName=ShiroUtils.getSysUser().getLoginName();
		mmap.put("userName",loginName);
	    return prefix + "/edit";
	}
	
	/**
	 * 修改保存宰杀
	 */
	@RequiresPermissions("system:ncZaisha:edit")
	@Log(title = "宰杀", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(NcZaisha ncZaisha)
	{		
		return toAjax(ncZaishaService.updateNcZaisha(ncZaisha));
	}
	
	/**
	 * 删除宰杀
	 */
	@RequiresPermissions("system:ncZaisha:remove")
	@Log(title = "宰杀", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public AjaxResult remove(String ids)
	{		
		return toAjax(ncZaishaService.deleteNcZaishaByIds(ids));
	}
	
}
