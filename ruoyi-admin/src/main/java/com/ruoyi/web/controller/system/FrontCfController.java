package com.ruoyi.web.controller.system;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.NcOrderCf;
import com.ruoyi.system.domain.NcTransportInfo;
import com.ruoyi.system.service.INcOrderCfService;
import com.ruoyi.system.service.INcOuputCfService;
import com.ruoyi.system.service.INcPengInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * ********************菜房订单前端接口*********************
 *
 * @author ruoyi
 * @date 2019-09-01
 */
@Controller
@RequestMapping("/system/front_caifang")
public class FrontCfController extends BaseController {
    private String prefix = "system/front_caifang";

    @Autowired
    private INcOrderCfService orderCfService;
    @Autowired
    private INcPengInfoService pengInfoService;

    @Autowired
    private INcOuputCfService ouputCfService;

    /*菜房*/
    @GetMapping("/vegetableWarehouses")
    public String vegetableWarehouses(NcOrderCf order, String beginTime, String endTime, ModelMap mmap) {

//        order.setCforderStatus("5");//已完成的
        if (StringUtils.isNotEmpty(beginTime)) {
            order.getParams().put("beginTime", beginTime);
        }
        if (StringUtils.isNotEmpty(endTime)) {
            order.getParams().put("endTime", endTime);
        }
        //按时间查询出库数量
        Map outputCount = ouputCfService.getOutputCount(order);

        mmap.put("beginTime", beginTime);
        mmap.put("endTime", endTime);
        mmap.put("outputCount", outputCount);

//        mmap.put("caiClass", ouputCfService.getBigCaiClass(order));
        ouputCfService.setOutputTypes(mmap, order);
        return prefix + "/vegetableWarehouses-2";
    }

    /*运送登记*/
    @GetMapping("/yunsong")
    public String yunong(NcTransportInfo transportInfo, ModelMap mmap) {
        return prefix + "/yunsong";
    }


    /*菜房出库*/
    @GetMapping("/frontOutputCf")
    public String frontOrderCf(String cfOrderFor, String beginTime, String endTime, String ancestors, ModelMap mmap) {

        mmap.put("cfOrderFor", cfOrderFor);
        mmap.put("ancestors", ancestors);
        mmap.put("beginTime", beginTime);
        mmap.put("endTime", endTime);
        return prefix + "/frontOutputCf";
    }

    /**
     *
     * @param orderType 订单类型（字典）
     * @param dataSpan   时间跨度 1:年 2：月 3：日
     * @return
     */
    @PostMapping("/outputQuery")
    @ResponseBody
    public TableDataInfo orderQuery(Integer outputType, Integer dataSpan, String  beginTime, String endTime, int cf){
        //

        List<Map> list = ouputCfService.selectCountByTime(outputType,dataSpan,beginTime,endTime,cf);
        return getDataTable(list);
    }


}
