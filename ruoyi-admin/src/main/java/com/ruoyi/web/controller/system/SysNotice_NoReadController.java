package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.SysNotice;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.service.ISysNoticeService;
import com.ruoyi.system.service.ISysUserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 公告 信息操作处理
 *
 * @author ruoyi
 */
@Controller
@RequestMapping("/system/notice_Noread")
public class SysNotice_NoReadController extends BaseController
{
    private String prefix = "system/notice";

    @Autowired
    private ISysNoticeService noticeService;

    @Autowired
    private ISysUserService userService;

    @RequiresPermissions("system:notice:view")
    @GetMapping()
    public String notice()
    {
        return prefix + "/notice_noread";
    }

    /**
     * 查询公告列表
     */
    @RequiresPermissions("system:notice:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(SysNotice notice)
    {
        startPage();
        //如果deptid 为1  那么 所以用户都可以看见这条通知
        //如果deptid 为2 那么只有userid中有的用户可以看见

        SysUser sysUser = ShiroUtils.getSysUser();
        if(sysUser.getUserId()==1){
            List<SysNotice> sysNoticeAll = noticeService.selectNoticeList(notice);
            return getDataTable((sysNoticeAll));

        }else {
            notice.setUserId(String.valueOf(sysUser.getUserId()));
            List<SysNotice> sysNotices = noticeService.selectNoticeByDeptIdNotRead(notice);
            return getDataTable(sysNotices);
        }


    }

    /**
     * 新增公告
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存公告
     */
    @RequiresPermissions("system:notice:add")
    @Log(title = "通知公告", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(SysNotice notice)
    {
        notice.setCreateBy(ShiroUtils.getLoginName());

        notice.setReadType(2); //设置未读
        return toAjax(noticeService.insertNotice(notice));
    }

    /**
     * 修改公告
     */
    @GetMapping("/edit/{noticeId}")
    public String edit(@PathVariable("noticeId") Long noticeId, ModelMap mmap)
    {
        mmap.put("notice", noticeService.selectNoticeById(noticeId));
        return prefix + "/edit2";
    }

    /**
     * 修改保存公告
     */
    @RequiresPermissions("system:notice:edit")
    @Log(title = "通知公告", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(SysNotice notice)
    {
        notice.setUpdateBy(ShiroUtils.getLoginName());
        return toAjax(noticeService.updateNotice(notice));
    }

    /**
     * 删除公告
     */
    @RequiresPermissions("system:notice:remove")
    @Log(title = "通知公告", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(noticeService.deleteNoticeByIds(ids));
    }

    /**
     * 公告详情
     */
    @GetMapping("/info/{noticeId}")
    public String info(@PathVariable("noticeId") Long noticeId, ModelMap mmap)
    {
        mmap.put("notice", noticeService.selectNoticeById(noticeId));
        return prefix + "/info";
    }

    /**
     * 更多公告
     */
    @GetMapping("/moreInfo")
    public String moreInfo(ModelMap mmap)
    {
        mmap.put("notices", noticeService.selectNoticeList(new SysNotice()));
        return prefix + "/more-notice";
    }

    //已读
    @RequiresPermissions("system:notice:edit")
    @Log(title = "已读", businessType = BusinessType.INSERT)
    @PostMapping("/read/{noticeId}")
    @ResponseBody
    public AjaxResult read(@PathVariable("noticeId") Long noticeId){
        SysNotice sysNotice = noticeService.selectNoticeById(noticeId);

        SysNotice notice = new SysNotice();
        notice.setNoticeId(sysNotice.getNoticeId());
        notice.setReadType(1);
        return toAjax(noticeService.updateNotice(notice));
    }

    //全部已读
    @RequiresPermissions("system:notice:edit")
    @Log(title = "全读", businessType = BusinessType.INSERT)
    @PostMapping("/readAll")
    @ResponseBody
    public AjaxResult readAll(){
        List<SysNotice> sysNoticesList = noticeService.selectNoticeList(new SysNotice());

        SysNotice sysNotices = new SysNotice();
        for (SysNotice sysNotice : sysNoticesList) {
            sysNotices.setNoticeId(sysNotice.getNoticeId());
            sysNotices.setReadType(1);
            noticeService.updateNotice(sysNotices);
        }
        return AjaxResult.success("全部已读");

    }


}

