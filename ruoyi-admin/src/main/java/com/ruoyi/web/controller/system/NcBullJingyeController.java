package com.ruoyi.web.controller.system;

import java.util.List;

import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.NcCowsBirth;
import com.ruoyi.system.domain.NcCowsFiles;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.service.INcCowsBirthService;
import com.ruoyi.system.service.INcCowsFilesService;
import com.ruoyi.system.service.ISysUserService;
import com.ruoyi.system.util.ExcelUtil2;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.NcBullJingye;
import com.ruoyi.system.service.INcBullJingyeService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;

/**
 * 牛舍精液 信息操作处理
 *
 * @author ruoyi
 * @date 2019-08-30
 */
@Controller
@RequestMapping("/system/ncBullJingye")
public class
NcBullJingyeController extends BaseController {
    private String prefix = "system/ncBullJingye";

    @Autowired
    private INcBullJingyeService ncBullJingyeService;

    @Autowired
    private ISysUserService userService;

    @Autowired
    private INcCowsFilesService cowsFilesService;

    @Autowired
    private INcCowsBirthService ncCowsBirthService;





    @RequiresPermissions("system:ncBullJingye:view")
    @GetMapping()
    public String ncBullJingye(ModelMap mmap) {
        List<SysUser> sysUsers = userService.selectUserList(new SysUser());
        mmap.put("users", sysUsers);
        NcCowsBirth ncCowsBirth = new NcCowsBirth();
        ncCowsBirth.setChildGender(1);
        List<NcCowsBirth> cows = ncCowsBirthService.selectNcCowsBirthList(ncCowsBirth);
        mmap.put("cows", cows);

        return prefix + "/ncBullJingye";
    }

    /**
     * 查询牛舍精液列表
     */
    @RequiresPermissions("system:ncBullJingye:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(NcBullJingye ncBullJingye) {
        startPage();
        List<NcBullJingye> list = ncBullJingyeService.selectNcBullJingyeList(ncBullJingye);
        return getDataTable(list);
    }


    /**
     * 导出牛舍精液列表
     */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(NcBullJingye ncBullJingye) {
        List<NcBullJingye> list = ncBullJingyeService.selectNcBullJingyeList(ncBullJingye);
        ExcelUtil2<NcBullJingye> util = new ExcelUtil2<NcBullJingye>(NcBullJingye.class);
        return util.exportExcel(list, "ncBullJingye");
    }

    /**
     * 新增牛舍精液
     */
    @GetMapping("/add")
    public String add(ModelMap mmap) {
        List<SysUser> sysUsers = userService.selectUserList(new SysUser());
        mmap.put("users", sysUsers);

        NcCowsBirth ncCowsBirth = new NcCowsBirth();
        ncCowsBirth.setChildGender(1);
        List<NcCowsBirth> cows = ncCowsBirthService.selectNcCowsBirthList(ncCowsBirth);
        mmap.put("cows", cows);

        return prefix + "/add";
    }

    /**
     * 新增保存牛舍精液
     */
    @RequiresPermissions("system:ncBullJingye:add")
    @Log(title = "牛舍精液", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(NcBullJingye ncBullJingye) {

        SysUser sysUser = ShiroUtils.getSysUser();
        try {
            return toAjax(ncBullJingyeService.insertNcBullJingye(ncBullJingye,sysUser));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return toAjax(ncBullJingyeService.insertNcBullJingye(ncBullJingye,sysUser));

    }

    /**
     * 修改牛舍精液
     */
    @GetMapping("/edit/{jingyeId}")
    public String edit(@PathVariable("jingyeId") Integer jingyeId, ModelMap mmap) {
        NcBullJingye ncBullJingye = ncBullJingyeService.selectNcBullJingyeById(jingyeId);
        mmap.put("ncBullJingye", ncBullJingye);
        List<SysUser> sysUsers = userService.selectUserList(new SysUser());
        mmap.put("users", sysUsers);

        NcCowsBirth ncCowsBirth = new NcCowsBirth();
        ncCowsBirth.setChildGender(1);
        List<NcCowsBirth> cows = ncCowsBirthService.selectNcCowsBirthList(new NcCowsBirth());
        mmap.put("cows", cows);
        return prefix + "/edit";
    }

    /**
     * 修改保存牛舍精液
     */
    @RequiresPermissions("system:ncBullJingye:edit")
    @Log(title = "牛舍精液", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(NcBullJingye ncBullJingye) {
        return toAjax(ncBullJingyeService.updateNcBullJingye(ncBullJingye));
    }


    /**
     * 牛舍精液出库
     */
    @GetMapping("/checkOut/{jingyeId}")
    public String checkOut(@PathVariable("jingyeId") Integer jingyeId, ModelMap mmap) {
        NcBullJingye ncBullJingye = ncBullJingyeService.selectNcBullJingyeById(jingyeId);
        mmap.put("ncBullJingye", ncBullJingye);


        NcCowsBirth ncCowsBirth = new NcCowsBirth();
        ncCowsBirth.setChildGender(1);
        List<NcCowsBirth> cows = ncCowsBirthService.selectNcCowsBirthList(new NcCowsBirth());
        mmap.put("cows", cows);

        //用户信息
        List<SysUser> sysUsers = userService.selectUserList(new SysUser());
        mmap.put("users", sysUsers);
        return prefix + "/checkOut";
    }


    /**
     * 删除牛舍精液
     */
    @RequiresPermissions("system:ncBullJingye:remove")
    @Log(title = "牛舍精液", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(ncBullJingyeService.deleteNcBullJingyeByIds(ids));
    }


}
