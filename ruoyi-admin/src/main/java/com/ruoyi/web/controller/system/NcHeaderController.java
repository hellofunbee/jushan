package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.NcHeader;
import com.ruoyi.system.service.INcHeaderService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 轮播图 信息操作处理
 * 
 * @author ruoyi
 * @date 2019-09-15
 */
@Controller
@RequestMapping("/system/ncHeader")
public class NcHeaderController extends BaseController
{
    private String prefix = "system/ncHeader";
	
	@Autowired
	private INcHeaderService ncHeaderService;
	
	@RequiresPermissions("system:ncHeader:view")
	@GetMapping()
	public String ncHeader()
	{
	    return prefix + "/ncHeader";
	}
	
	/**
	 * 查询轮播图列表
	 */
	@RequiresPermissions("system:ncHeader:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(NcHeader ncHeader)
	{
		startPage();
        List<NcHeader> list = ncHeaderService.selectNcHeaderList(ncHeader);
		return getDataTable(list);
	}
	
	
	/**
	 * 导出轮播图列表
	 */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(NcHeader ncHeader)
    {
    	List<NcHeader> list = ncHeaderService.selectNcHeaderList(ncHeader);
        ExcelUtil<NcHeader> util = new ExcelUtil<NcHeader>(NcHeader.class);
        return util.exportExcel(list, "ncHeader");
    }
	
	/**
	 * 新增轮播图
	 */
	@GetMapping("/add")
	public String add()
	{
	    return prefix + "/add";
	}
	
	/**
	 * 新增保存轮播图
	 */
	@RequiresPermissions("system:ncHeader:add")
	@Log(title = "轮播图", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(NcHeader ncHeader)
	{		
		return toAjax(ncHeaderService.insertNcHeader(ncHeader));
	}

	/**
	 * 修改轮播图
	 */
	@GetMapping("/edit/{hId}")
	public String edit(@PathVariable("hId") Integer hId, ModelMap mmap)
	{
		NcHeader ncHeader = ncHeaderService.selectNcHeaderById(hId);
		mmap.put("ncHeader", ncHeader);
	    return prefix + "/edit";
	}
	
	/**
	 * 修改保存轮播图
	 */
	@RequiresPermissions("system:ncHeader:edit")
	@Log(title = "轮播图", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(NcHeader ncHeader)
	{		
		return toAjax(ncHeaderService.updateNcHeader(ncHeader));
	}
	
	/**
	 * 删除轮播图
	 */
	@RequiresPermissions("system:ncHeader:remove")
	@Log(title = "轮播图", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public AjaxResult remove(String ids)
	{		
		return toAjax(ncHeaderService.deleteNcHeaderByIds(ids));
	}

	/**
	 * 轮播图状态修改
	 */
	@PostMapping("/changeStatus")
	@ResponseBody
	public AjaxResult changeStatus(NcHeader h)
	{
		return toAjax(ncHeaderService.changeStatus(h));
	}

}
