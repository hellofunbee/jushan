package com.ruoyi.web.controller.system;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.NcOrderCf;
import com.ruoyi.system.domain.SysDictData;
import com.ruoyi.system.service.INcOrderCfService;
import com.ruoyi.system.service.ISysDictDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * ********************菜房订单前端接口*********************
 *
 * @author ruoyi
 * @date 2019-09-01
 */
@Controller
@RequestMapping("/system/front_ordercf")
public class FrontOrderCfController extends BaseController {
    private String prefix = "system/front_ordercf";

    @Autowired
    private INcOrderCfService orderCfService;

    @Autowired
    private ISysDictDataService dictDataService;

    /*菜房订单*/
    @GetMapping("/orderManage04")
    public String orderManage04(NcOrderCf order, String beginTime, String endTime, ModelMap mmap) {

//        order.setCforderStatus("5");//已完成的
        if (StringUtils.isNotEmpty(beginTime)) {
            order.getParams().put("beginTime", beginTime);
        }
        if (StringUtils.isNotEmpty(endTime)) {
            order.getParams().put("endTime", endTime);
        }

        mmap.put("beginTime", beginTime);
        mmap.put("endTime", endTime);
        orderCfService.setOrderTypes(mmap, order, null,1);
        return prefix + "/orderManage04-2";
    }

    /*菜房订单*/
    @GetMapping("/frontOrderCf")
    public String frontOrderCf(NcOrderCf order, String beginTime, String endTime, String type, ModelMap mmap) {
        List<SysDictData> cforderType = dictDataService.selectDictDataByType("cforderType");
        for (SysDictData d : cforderType) {
            if (d.getDictValue().equals(type)) {
                d.setFlag(true);
            }
        }

        mmap.put("dicts", cforderType);
        mmap.put("type", type);
        mmap.put("beginTime", beginTime);
        mmap.put("endTime", endTime);
        return prefix + "/frontOrderCf";
    }

    /**
     *
     * @param orderType 订单类型（字典）
     * @param dataSpan   时间跨度 1:年 2：月 3：日
     * @return
     */
    @PostMapping("/orderQuery")
    @ResponseBody
    public TableDataInfo orderQuery(Integer orderType,Integer dataSpan,String  beginTime, String endTime,int type){
        //

        List<Map> list = orderCfService.selectCountByTime(orderType,dataSpan,beginTime,endTime,type);
        return getDataTable(list);
    }


}
