package com.ruoyi.web.controller.system;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.NcZhibaoDrugs;
import com.ruoyi.system.service.INcZhibaoDrugsService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;

/**
 * 植保用药 信息操作处理
 * 
 * @author ruoyi
 * @date 2019-08-29
 */
@Controller
@RequestMapping("/system/ncZhibaoDrugs")
public class NcZhibaoDrugsController extends BaseController
{
    private String prefix = "system/ncZhibaoDrugs";
	
	@Autowired
	private INcZhibaoDrugsService ncZhibaoDrugsService;
	
	@RequiresPermissions("system:ncZhibaoDrugs:view")
	@GetMapping()
	public String ncZhibaoDrugs()
	{
	    return prefix + "/ncZhibaoDrugs";
	}
	
	/**
	 * 查询植保用药列表
	 */
	@RequiresPermissions("system:ncZhibaoDrugs:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(NcZhibaoDrugs ncZhibaoDrugs)
	{
		startPage();
        List<NcZhibaoDrugs> list = ncZhibaoDrugsService.selectNcZhibaoDrugsList(ncZhibaoDrugs);
		return getDataTable(list);
	}
	
	
	/**
	 * 导出植保用药列表
	 */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(NcZhibaoDrugs ncZhibaoDrugs)
    {
    	List<NcZhibaoDrugs> list = ncZhibaoDrugsService.selectNcZhibaoDrugsList(ncZhibaoDrugs);
        ExcelUtil<NcZhibaoDrugs> util = new ExcelUtil<NcZhibaoDrugs>(NcZhibaoDrugs.class);
        return util.exportExcel(list, "ncZhibaoDrugs");
    }
	
	/**
	 * 新增植保用药
	 */
	@GetMapping("/add")
	public String add()
	{
	    return prefix + "/add";
	}
	
	/**
	 * 新增保存植保用药
	 */
	@RequiresPermissions("system:ncZhibaoDrugs:add")
	@Log(title = "植保用药", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(NcZhibaoDrugs ncZhibaoDrugs)
	{		
		return toAjax(ncZhibaoDrugsService.insertNcZhibaoDrugs(ncZhibaoDrugs));
	}

	/**
	 * 修改植保用药
	 */
	@GetMapping("/edit/{zhibaoId}")
	public String edit(@PathVariable("zhibaoId") Long zhibaoId, ModelMap mmap)
	{
		NcZhibaoDrugs ncZhibaoDrugs = ncZhibaoDrugsService.selectNcZhibaoDrugsById(zhibaoId);
		mmap.put("ncZhibaoDrugs", ncZhibaoDrugs);
	    return prefix + "/edit";
	}
	
	/**
	 * 修改保存植保用药
	 */
	@RequiresPermissions("system:ncZhibaoDrugs:edit")
	@Log(title = "植保用药", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(NcZhibaoDrugs ncZhibaoDrugs)
	{		
		return toAjax(ncZhibaoDrugsService.updateNcZhibaoDrugs(ncZhibaoDrugs));
	}
	
	/**
	 * 删除植保用药
	 */
	@RequiresPermissions("system:ncZhibaoDrugs:remove")
	@Log(title = "植保用药", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public AjaxResult remove(String ids)
	{		
		return toAjax(ncZhibaoDrugsService.deleteNcZhibaoDrugsByIds(ids));
	}
	
}
