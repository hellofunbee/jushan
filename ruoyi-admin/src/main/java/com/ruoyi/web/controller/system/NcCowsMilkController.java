package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.NcCowsFiles;
import com.ruoyi.system.domain.NcCowsMilk;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.service.INcCowsFilesService;
import com.ruoyi.system.service.INcCowsMilkService;
import com.ruoyi.system.service.ISysUserService;
import com.ruoyi.system.util.ExcelUtil2;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 产奶记录 信息操作处理
 *
 * @author ruoyi
 * @date 2019-09-06
 */
@Controller
@RequestMapping("/system/ncCowsMilk")
public class NcCowsMilkController extends BaseController
{
    private String prefix = "system/ncCowsMilk";
	private String prontPrefix = "system/ncCowsMilk/front";

	@Autowired
	private INcCowsMilkService ncCowsMilkService;

	@Autowired
	private ISysUserService iSysUserService;

	@Autowired
	private INcCowsFilesService ncCowsFilesService;
	@RequiresPermissions("system:ncCowsMilk:view")
	@GetMapping()
	public String ncCowsMilk()
	{
	    return prefix + "/ncCowsMilk";
	}

	@GetMapping("/pront")
	public String prontNcCowsMilk(){
		return prontPrefix + "/ncCowsMilk";
	}

	@GetMapping("/pront/createMilk")
	public String prontCreateNcCowsMilk(){
		return prontPrefix + "/ncCreateCowsMilk";
	}

	/**
	 * 查询产奶记录列表
	 */
	@RequiresPermissions("system:ncCowsMilk:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(NcCowsMilk ncCowsMilk)
	{
		startPage();
        List<NcCowsMilk> list = ncCowsMilkService.selectNcCowsMilkList(ncCowsMilk);
		return getDataTable(list);
	}

	/**
	 * 查询产奶记录列表
	 */
	@PostMapping("/front/list")
	@ResponseBody
	public TableDataInfo frontList(NcCowsMilk ncCowsMilk)
	{
		startPage();
		List<NcCowsMilk> list = ncCowsMilkService.selectNcCowsMilkList(ncCowsMilk);
		return getDataTable(list);
	}


	/**
	 * 导出产奶记录列表
	 */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(NcCowsMilk ncCowsMilk)
    {
    	List<NcCowsMilk> list = ncCowsMilkService.selectNcCowsMilkList(ncCowsMilk);
//        ExcelUtil<NcCowsMilk> util = new ExcelUtil<NcCowsMilk>(NcCowsMilk.class);
		ExcelUtil2<NcCowsMilk> util = new ExcelUtil2<>(NcCowsMilk.class);
		return util.exportExcel(list, "产奶记录单");
    }

	/**
	 * 新增产奶记录
	 */
	@GetMapping("/add")
	public String add(ModelMap mmap) {
		mmap.put("milk",ncCowsFilesService.selectNcCowsFilesList(new NcCowsFiles()));
		mmap.put("user",iSysUserService.selectUserList(new SysUser()));
	    return prefix + "/add";
	}

	/**
	 * 新增保存产奶记录
	 */
	@RequiresPermissions("system:ncCowsMilk:add")
	@Log(title = "产奶记录", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(NcCowsMilk ncCowsMilk)
	{
		return toAjax(ncCowsMilkService.insertNcCowsMilk(ncCowsMilk));
	}

	/**
	 * 修改产奶记录
	 */
	@GetMapping("/edit/{cowsMilkId}")
	public String edit(@PathVariable("cowsMilkId") Integer cowsMilkId, ModelMap mmap)
	{
		NcCowsMilk ncCowsMilk = ncCowsMilkService.selectNcCowsMilkById(cowsMilkId);
		mmap.put("ncCowsMilk", ncCowsMilk);

		List<SysUser> sysUsers = iSysUserService.selectUserList(new SysUser());
		if(ncCowsMilk != null && StringUtils.isNotNull(ncCowsMilk.getCreateBy())){
			for (SysUser sysUser : sysUsers) {
				if((""+sysUser.getUserId()).equals(ncCowsMilk.getCreateBy())){
					sysUser.setFlag(true);
					break;

				}
			}
		}
		List<NcCowsFiles> ncCowsFiles = ncCowsFilesService.selectNcCowsFilesList(new NcCowsFiles());
		if(ncCowsMilk != null && StringUtils.isNotNull(ncCowsMilk.getCowId())){
			for (NcCowsFiles ncCowsFile : ncCowsFiles) {
				if(ncCowsFile.getCowId().equals(ncCowsMilk.getCowId())){
					ncCowsFile.setFlag(true);
					break;
				}
			}

		}
		mmap.put("createBy",sysUsers);
		mmap.put("milk",ncCowsFiles);
	    return prefix + "/edit";
	}

	/**
	 * 修改保存产奶记录
	 */
	@RequiresPermissions("system:ncCowsMilk:edit")
	@Log(title = "产奶记录", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(NcCowsMilk ncCowsMilk)
	{
		return toAjax(ncCowsMilkService.updateNcCowsMilk(ncCowsMilk));
	}

	/**
	 * 删除产奶记录
	 */
	@RequiresPermissions("system:ncCowsMilk:remove")
	@Log(title = "产奶记录", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public AjaxResult remove(String ids)
	{
		return toAjax(ncCowsMilkService.deleteNcCowsMilkByIds(ids));
	}


}
