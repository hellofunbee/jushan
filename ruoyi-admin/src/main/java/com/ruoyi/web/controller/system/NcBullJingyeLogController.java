package com.ruoyi.web.controller.system;

import java.util.List;

import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.NcCowsBirth;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.service.INcCowsBirthService;
import com.ruoyi.system.service.ISysUserService;
import com.ruoyi.system.util.ExcelUtil2;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.NcBullJingyeLog;
import com.ruoyi.system.service.INcBullJingyeLogService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;

/**
 * 牛舍精液 信息操作处理
 *
 * @author ruoyi
 * @date 2019-08-30
 */
@Controller
@RequestMapping("/system/ncBullJingyeLog")
public class NcBullJingyeLogController extends BaseController {
    private String prefix = "system/ncBullJingyeLog";

    @Autowired
    private INcBullJingyeLogService ncBullJingyeLogService;


    @Autowired
    private ISysUserService userService;


    @Autowired
    private INcCowsBirthService ncCowsBirthService;



    @RequiresPermissions("system:ncBullJingyeLog:view")
    @GetMapping()
    public String ncBullJingyeLog(ModelMap mmap) {
        List<SysUser> sysUsers = userService.selectUserList(new SysUser());
        mmap.put("users", sysUsers);
        NcCowsBirth ncCowsBirth = new NcCowsBirth();
        ncCowsBirth.setChildGender(1);
        List<NcCowsBirth> cows = ncCowsBirthService.selectNcCowsBirthList(ncCowsBirth);
        mmap.put("cows", cows);


        return prefix + "/ncBullJingyeLog";
    }

    /**
     * 查询牛舍精液列表
     */
    @RequiresPermissions("system:ncBullJingyeLog:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(NcBullJingyeLog ncBullJingyeLog) {
        startPage();
        List<NcBullJingyeLog> list = ncBullJingyeLogService.selectNcBullJingyeLogList(ncBullJingyeLog);
        return getDataTable(list);
    }


    /**
     * 导出牛舍精液列表
     */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(NcBullJingyeLog ncBullJingyeLog) {
        List<NcBullJingyeLog> list = ncBullJingyeLogService.selectNcBullJingyeLogList(ncBullJingyeLog);
        ExcelUtil2<NcBullJingyeLog> util = new ExcelUtil2<NcBullJingyeLog>(NcBullJingyeLog.class);
        return util.exportExcel(list, "ncBullJingyeLog");
    }

    /**
     * 新增牛舍精液
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

   	/**
	 * 新增保存牛舍精液
	 */
	@RequiresPermissions("system:ncBullJingyeLog:add")
	@Log(title = "牛舍精液", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(NcBullJingyeLog ncBullJingyeLog) {


        SysUser sysUser = ShiroUtils.getSysUser();

        //出库业务
		boolean result = ncBullJingyeLogService.checkOutBullJingye( ncBullJingyeLog,sysUser);

		return toAjax(result);
	}

    /**
     * 修改牛舍精液
     */
    @GetMapping("/edit/{jingyeLogId}")
    public String edit(@PathVariable("jingyeLogId") Long jingyeLogId, ModelMap mmap) {
        NcBullJingyeLog ncBullJingyeLog = ncBullJingyeLogService.selectNcBullJingyeLogById(jingyeLogId);
        mmap.put("ncBullJingyeLog", ncBullJingyeLog);
        List<SysUser> sysUsers = userService.selectUserList(new SysUser());
        mmap.put("users", sysUsers);
        return prefix + "/edit";
    }

    /**
     * 修改保存牛舍精液
     */
    @RequiresPermissions("system:ncBullJingyeLog:edit")
    @Log(title = "牛舍精液", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(NcBullJingyeLog ncBullJingyeLog) {
        return toAjax(ncBullJingyeLogService.updateNcBullJingyeLog(ncBullJingyeLog));
    }

    /**
     * 删除牛舍精液
     */
    @RequiresPermissions("system:ncBullJingyeLog:remove")
    @Log(title = "牛舍精液", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(ncBullJingyeLogService.deleteNcBullJingyeLogByIds(ids));
    }

}
