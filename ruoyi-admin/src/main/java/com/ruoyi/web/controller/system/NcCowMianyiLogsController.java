package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.service.INcCowMianyiLogsService;
import com.ruoyi.system.service.INcDrugsService;
import com.ruoyi.system.service.ISysUserService;
import com.ruoyi.system.util.ExcelUtil2;
import com.ruoyi.system.util.ExcelUtil2Element;
import com.ruoyi.system.util.WorkbookExportUtil;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 免疫记录 信息操作处理
 *
 * @author ruoyi
 * @date 2019-08-29
 */
@Controller
@RequestMapping("/system/ncMianyiLogs/cow")
public class NcCowMianyiLogsController extends BaseController
{
    private String prefix = "system/ncMianyiLogs/cow";


    @Autowired
    private ISysUserService userService;

    @Autowired
    private INcDrugsService iNcDrugsService;


    @Autowired
    private INcCowMianyiLogsService ncMianyiLogsService;

    @RequiresPermissions("system:ncMianyiLogs:view")
    @GetMapping()
    public String ncMianyiLogs(ModelMap mmap)
    {
        List<SysUser> sysUsers = userService.selectUserList(new SysUser());
        mmap.put("users", sysUsers);
        return prefix + "/ncMianyiLogs";
    }

    /**
     * 查询免疫记录列表
     */
    @RequiresPermissions("system:ncMianyiLogs:list")
    @PostMapping("/list/{myType}/{myStatus}")
    @ResponseBody
    public TableDataInfo list(@PathVariable("myType") Integer myType ,@PathVariable("myStatus") Integer myStatus ,NcMianyiLogs ncMianyiLogs)
    {
        startPage();
        ncMianyiLogs.setMyType(myType);
        ncMianyiLogs.setMyStatus(myStatus);
        List<NcMianyiLogs> list = ncMianyiLogsService.selectNcMianyiLogsList(ncMianyiLogs);
        return getDataTable(list);
    }


    /**
     * 导出免疫记录列表
     */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(NcMianyiLogs ncMianyiLogs)
    {
        WorkbookExportUtil wu = new WorkbookExportUtil(new XSSFWorkbook(), "牛舍免疫数据");

        List<NcMianyiLogs> mianyiList = ncMianyiLogsService.selectNcMianyiLogsList(ncMianyiLogs);
        ExcelUtil2Element<NcMianyiLogs> m_mianyi = new ExcelUtil2Element<>(NcMianyiLogs.class, wu);
        m_mianyi.exportExcel(mianyiList,"免疫数据");

        //查询兽药库存中的疫苗信息
        List<NcDrugs> ncDrugsYiMiaoList = iNcDrugsService.selectNcDrugsChildList(235L);
        ExcelUtil2Element<NcDrugs> m_YiMiao= new ExcelUtil2Element<>(NcDrugs.class, wu);
        m_YiMiao.exportExcel(ncDrugsYiMiaoList,"疫苗数据");

        return wu.exportExcel();
    }

    /**
     * 新增免疫记录
     */
    @GetMapping("/add")
    public String add(ModelMap mmap)
    {

        //用户信息
        List<SysUser> sysUsers = userService.selectUserList(new SysUser());
        mmap.put("users", sysUsers);

        //查询兽药库存中的疫苗信息
        List<NcDrugs> ncDrugsYiMiaoList = iNcDrugsService.selectNcDrugsChildList(235L);
        mmap.put("yimiaoList", ncDrugsYiMiaoList);



        return prefix + "/add";
    }

    /**
     * 新增保存免疫记录
     */
    @RequiresPermissions("system:ncMianyiLogs:add")
    @Log(title = "免疫记录", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(NcMianyiLogs ncMianyiLogs)
    {
        return ncMianyiLogsService.insertNcMianyiLogs(ncMianyiLogs);
    }

    /**
     * 修改免疫记录
     */
    @GetMapping("/edit/{myId}")
    public String edit(@PathVariable("myId") Long myId, ModelMap mmap)
    {
        NcMianyiLogs ncMianyiLogs = ncMianyiLogsService.selectNcMianyiLogsById(myId);
        mmap.put("ncMianyiLogs", ncMianyiLogs);

        List<SysUser> sysUsers = userService.selectUserList(new SysUser());
        mmap.put("users", sysUsers);
        return prefix + "/edit";
    }

    /**
     * 修改保存免疫记录
     */
    @RequiresPermissions("system:ncMianyiLogs:edit")
    @Log(title = "免疫记录", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(NcMianyiLogs ncMianyiLogs)
    {
        return toAjax(ncMianyiLogsService.updateNcMianyiLogs(ncMianyiLogs));
    }



    /**
     * 批量确认生成检测单
     */
    @Log(title = "确认生成检测单", businessType = BusinessType.OTHER)
    @RequiresPermissions("system:ncMianyiLogs:confirm")
    @PostMapping("/confirmTable")
    @ResponseBody
    public AjaxResult confirmTable(String ids) {
        SysUser user = ShiroUtils.getSysUser();
        return toAjax(ncMianyiLogsService.createZlOrderByIds(ids,user));
    }


    /**
     * 删除免疫记录
     */
    @RequiresPermissions("system:ncMianyiLogs:remove")
    @Log(title = "免疫记录", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(ncMianyiLogsService.deleteNcMianyiLogsByIds(ids));
    }

}
