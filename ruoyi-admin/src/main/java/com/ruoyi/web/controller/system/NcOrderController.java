package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.NcOrder;
import com.ruoyi.system.service.INcOrderService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 订单主（系统中的所有订单共用此） 信息操作处理
 *
 * @author ruoyi
 * @date 2019-07-31
 */
@Controller
@RequestMapping("/system/ncOrder")
public class NcOrderController extends BaseController {
    private String prefix = "system/ncOrder";

    @Autowired
    private INcOrderService ncOrderService;

    @RequiresPermissions("system:ncOrder:view")
    @GetMapping()
    public String ncOrder(Integer orderType, ModelMap mmap,String beginTime,String endTime) {
        mmap.put("orderType", orderType);
        mmap.put("beginTime", beginTime);
        mmap.put("endTime", endTime);
        return prefix + "/ncOrder";
    }
    //植保订单
    @GetMapping("/zhibaoOrder")
    public String nczhibaoOrder(Integer orderType, ModelMap mmap) {
        mmap.put("orderType", orderType);
        mmap.put("outputType",1);
        return prefix + "/front/ncOrder";
    }
    //查询植保记录单详情
    @GetMapping("/zhibaoOrderDetail")
    public String zhibaoOrderDetail(NcOrder ncOrder, ModelMap mmap) {
        NcOrder o = ncOrderService.selectNcOrderById(ncOrder.getOrderId());


        mmap.put("orderId", ncOrder.getOrderId());

        return "system/ncZhibao/front/ncZhibao-orderList";

    }

    /*菜房1的订单记录单*/
    @GetMapping("caifang1")
    public String ncOrder_caiFang1(Integer orderType, ModelMap mmap) {
        mmap.put("orderType", orderType);
        return prefix + "/ncOrder-caifang1";
    }

    /**
     * 查询订单主（系统中的所有订单共用此）列表
     */
    @RequiresPermissions("system:ncOrder:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(NcOrder ncOrder) {
        startPage();
        List<NcOrder> list = ncOrderService.selectNcOrderList(ncOrder);
        return getDataTable(list);
    }


    /**
     * 导出订单主（系统中的所有订单共用此）列表
     */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(NcOrder ncOrder) {
        List<NcOrder> list = ncOrderService.selectNcOrderList(ncOrder);
        ExcelUtil<NcOrder> util = new ExcelUtil<NcOrder>(NcOrder.class);
        return util.exportExcel(list, "ncOrder");
    }

    /**
     * 新增订单主（系统中的所有订单共用此）
     */
    @GetMapping("/add")
    public String add(Integer orderType, ModelMap mmap) {
        mmap.put("orderType", orderType);
        return prefix + "/add";
    }

    /**
     * 新增保存订单主（系统中的所有订单共用此）
     */
    @RequiresPermissions("system:ncOrder:add")
    @Log(title = "订单主（系统中的所有订单共用此）", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(NcOrder ncOrder) {
        return toAjax(ncOrderService.insertNcOrder(ncOrder));
    }

    /**
     * 修改订单主（系统中的所有订单共用此）
     */
    @GetMapping("/edit/{orderId}")
    public String edit(@PathVariable("orderId") Long orderId, ModelMap mmap) {
        NcOrder ncOrder = ncOrderService.selectNcOrderById(orderId);
        mmap.put("ncOrder", ncOrder);
        return prefix + "/edit";
    }

    /**
     * 修改保存订单主（系统中的所有订单共用此）
     */
    @RequiresPermissions("system:ncOrder:edit")
    @Log(title = "订单主（系统中的所有订单共用此）", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(NcOrder ncOrder) {
        return toAjax(ncOrderService.updateNcOrder(ncOrder));
    }

    /**
     * 删除订单主（系统中的所有订单共用此）
     */
    @RequiresPermissions("system:ncOrder:remove")
    @Log(title = "订单主（系统中的所有订单共用此）", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(ncOrderService.deleteNcOrderByIds(ids));
    }


    @GetMapping("orderList")
    public String orderList(NcOrder ncOrder, ModelMap mmap) {
        NcOrder o = ncOrderService.selectNcOrderById(ncOrder.getOrderId());

        /**
         * 单子类型： 1：生产计划单 2：育苗计划单 3：订单记录单 4：检测记录单 5：菜房1：入库单
         * 6：菜房1：出库单 7：菜房2：入库单 8：菜房2：出库单
         * 9：植保记录单 10：牛舍入库单 11：牛舍：出库单
         * 12：鸡舍入库单 13：鸡舍出库单 14：牛舍诊疗单 15：牛舍免疫单 ：
         * 16：牛舍巡视单  17：鸡舍诊疗单 18：鸡舍免疫单 ：19：鸡舍巡视单
         */
        switch (o.getOrderType()) {
            case 1:
                mmap.put("orderId", ncOrder.getOrderId());
                return "system/ncPlan2/ncPlan-orderList";
            case 2:
                mmap.put("orderId", ncOrder.getOrderId());
                return "system/ncPlan/ncPlan-orderList";
            case 3:
                mmap.put("orderId", ncOrder.getOrderId());
                return "system/ncOrderCf/ncOrderCf-orderList";
            case 4:
                mmap.put("orderId", ncOrder.getOrderId());
                return "system/ncCheck/ncCheck-orderList";
            case 5:
                mmap.put("orderId", ncOrder.getOrderId());
                return "system/ncInputCf/ncInputCf-orderList";
            case 6:
                mmap.put("orderId", ncOrder.getOrderId());
                return "system/ncOuputCf/ncOuputCf-orderList";
            case 7:
                mmap.put("orderId", ncOrder.getOrderId());
                return "system/ncInputCf/ncInputCf-orderList";
            case 8:
                mmap.put("orderId", ncOrder.getOrderId());
                return "system/ncOuputCf/ncOuputCf-orderList";
            case 9:
                mmap.put("orderId", ncOrder.getOrderId());
                mmap.put("order", o);
                return "system/ncZhibao/ncZhibao-orderList";
            case 10:
                mmap.put("orderId", ncOrder.getOrderId());
                mmap.put("order",o);
                return "system/ncInputMilk/ncInputMilk-orderlist";
            case 11:
                mmap.put("orderId", ncOrder.getOrderId());
                mmap.put("order",o);
                return "system/ncOutputMilk/ncOutputMilk-orderlist";
            case 12:
                mmap.put("orderId", ncOrder.getOrderId());
                mmap.put("order",o);
                return "system/ncInputChicken/ncInputChicken-orderlist";
            case 13:
                mmap.put("orderId", ncOrder.getOrderId());
                mmap.put("order",o);
                return "system/ncOutputChicken/ncOutputChicken-orderlist";
            case 14:
                mmap.put("orderId", ncOrder.getOrderId());
                return "system/ncZhenliaoLogs/cow/ncZhenliao-orderList";
            case 15:
                mmap.put("orderId", ncOrder.getOrderId());
                return "system/ncMianyiLogs/cow/ncMianyi-orderList";
            case 16:
                mmap.put("orderId", ncOrder.getOrderId());
                return "system/ncXunshiLogs/cow/ncXunShi-orderList";
            case 17:
                mmap.put("orderId", ncOrder.getOrderId());
                return "system/ncZhenliaoLogs/chicken/ncZhenliao-orderList";
            case 18:
                mmap.put("orderId", ncOrder.getOrderId());
                return "system/ncMianyiLogs/chicken/ncMianyi-orderList";
            case 19:
                mmap.put("orderId", ncOrder.getOrderId());
                return "system/ncXunshiLogs/chicken/ncXunShi-orderList";
        }
        return "index";
    }
}
