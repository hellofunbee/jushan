package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.NcYedan;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.service.INcYedanService;
import com.ruoyi.system.service.ISysUserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 液氮 信息操作处理
 *
 * @author ruoyi
 * @date 2019-09-02
 */
@Controller
@RequestMapping("/system/ncYedan")
public class NcYedanController extends BaseController {
    private String prefix = "system/ncYedan";

    @Autowired
    private INcYedanService ncYedanService;

    @Autowired
    private ISysUserService userService;

    @RequiresPermissions("system:ncYedan:view")
    @GetMapping()
    public String ncYedan(ModelMap mmap) {

        List<SysUser> sysUsers = userService.selectUserList(new SysUser());
        mmap.put("users", sysUsers);
        return prefix + "/ncYedan";
    }

    /**
     * e
     * 查询液氮列表
     */
    @RequiresPermissions("system:ncYedan:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(NcYedan ncYedan) {
        startPage();
        List<NcYedan> list = ncYedanService.selectNcYedanList(ncYedan);
        return getDataTable(list);
    }


    /**
     * 新增液氮
     */
    @GetMapping("/add")
    public String add(ModelMap mmap) {
        List<SysUser> sysUsers = userService.selectUserList(new SysUser());
        mmap.put("users", sysUsers);
        return prefix + "/add";
    }

    /**
     * 新增保存液氮
     */
    @RequiresPermissions("system:ncYedan:add")
    @Log(title = "液氮", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(NcYedan ncYedan) {
        SysUser sysUser = ShiroUtils.getSysUser();
        try {
            return toAjax(ncYedanService.insertNcYedan(ncYedan, sysUser));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return toAjax(ncYedanService.insertNcYedan(ncYedan, sysUser));
    }

    /**
     * 修改液氮
     */
    @GetMapping("/edit/{yedanId}")
    public String edit(@PathVariable("yedanId") Long yedanId, ModelMap mmap) {
        NcYedan ncYedan = ncYedanService.selectNcYedanById(yedanId);
        mmap.put("ncYedan", ncYedan);
        List<SysUser> sysUsers = userService.selectUserList(new SysUser());
        mmap.put("users", sysUsers);
        return prefix + "/edit";
    }

    /**
     * 修改保存液氮
     */
    @RequiresPermissions("system:ncYedan:edit")
    @Log(title = "液氮", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(NcYedan ncYedan) {
        return toAjax(ncYedanService.updateNcYedan(ncYedan));
    }


    /**
     * 液氮出库
     */
    @GetMapping("/checkOut/{yedanId}")
    public String checkOut(@PathVariable("yedanId") Long yedanId, ModelMap mmap) {
        //液氮库存信息
        NcYedan ncYedan = ncYedanService.selectNcYedanById(yedanId);
        mmap.put("ncYedan", ncYedan);
        List<SysUser> sysUsers = userService.selectUserList(new SysUser());
        mmap.put("users", sysUsers);

        return prefix + "/checkOut";
    }

    /**
     * 删除液氮
     */
    @RequiresPermissions("system:ncYedan:remove")
    @Log(title = "液氮", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(ncYedanService.deleteNcYedanByIds(ids));
    }

    @RequiresPermissions("system:ncYedan:view")
    @GetMapping("/importTemplate")
    @ResponseBody
    public AjaxResult importTemplate() {
        ExcelUtil<NcYedan> util = new ExcelUtil<NcYedan>(NcYedan.class);
        return util.importTemplateExcel("液氮导入模板");
    }

    @Log(title = "液氮", businessType = BusinessType.IMPORT)
    @RequiresPermissions("system:ncYedan:import")
    @PostMapping("/importData")
    @ResponseBody
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<NcYedan> util = new ExcelUtil<NcYedan>(NcYedan.class);
        List<NcYedan> yedanList = util.importExcel(file.getInputStream());
        String message = ncYedanService.importYeDan(yedanList, updateSupport, ShiroUtils.getSysUser());
        return AjaxResult.success(message);
    }


    /**
     * 导出液氮列表
     */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(NcYedan ncYedan) {
        List<NcYedan> list = ncYedanService.selectNcYedanList(ncYedan);
        ExcelUtil<NcYedan> util = new ExcelUtil<NcYedan>(NcYedan.class);
        return util.exportExcel(list, "液氮数据");
    }


}
