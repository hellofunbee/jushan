package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.Ztree;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.service.*;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 诊疗单 信息操作处理
 *
 * @author ruoyi
 * @date 2019-08-28
 */

@Controller
@RequestMapping("/system/ncZhenliaoLogs/chicken")
public class NcChickenZhenliaoLogsController extends BaseController
{
	private String prefix = "system/ncZhenliaoLogs/chicken";

	@Autowired
	private INcChickenZhenliaoLogsService ncZhenliaoLogsService;

	@Autowired
	private ISysUserService userService;
	@Autowired
	private INcDrugsService iNcDrugsService;

	@Autowired
	private INcYaoClassService yaoClassService;

	@Autowired
	private  ISysDictDataService dictDataService;
	@Autowired
	private INcDrugsService ncDrugsService;
	@Autowired
	private INcZhenliaoDrugsService zhenliaoDrugsService;


	@RequiresPermissions("system:ncZhenliaoLogs:view")
	@GetMapping()
	public String ncZhenliaoLogs()
	{
		return prefix + "/ncZhenliaoLogs";
	}


	/**
	 * 查询诊疗单列表
	 */
	@RequiresPermissions("system:ncZhenliaoLogs:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(NcZhenliaoLogs ncZhenliaoLogs)
	{
		startPage();
		List<NcZhenliaoLogs> list = ncZhenliaoLogsService.selectNcZhenliaoLogsList(ncZhenliaoLogs);
		return getDataTable(list);
	}
	/**
	 * 查询诊疗单列表
	 */
	@RequiresPermissions("system:ncZhenliaoLogs:list")
	@PostMapping("/list/{zlType}/{zlStatus}")
	@ResponseBody
	public TableDataInfo list(@PathVariable("zlType") Integer zlType ,@PathVariable("zlStatus") String zlStatus , NcZhenliaoLogs ncZhenliaoLogs)
	{
		startPage();
		ncZhenliaoLogs.setZlType(zlType);
		ncZhenliaoLogs.setZlStatus(zlStatus);
		List<NcZhenliaoLogs> list = ncZhenliaoLogsService.selectNcZhenliaoLogsList(ncZhenliaoLogs);
		return getDataTable(list);
	}
	/**
	 * 查询诊疗单列表
	 */
	@RequiresPermissions("system:ncZhenliaoLogs:list")
	@PostMapping("/listConfirm/{zlType}/{zlStatus}")
	@ResponseBody
	public TableDataInfo listConfirm(@PathVariable("zlType") Integer zlType ,@PathVariable("zlStatus") String zlStatus , NcZhenliaoLogs ncZhenliaoLogs)
	{
		startPage();
		ncZhenliaoLogs.setZlType(zlType);
		ncZhenliaoLogs.setZlStatus(zlStatus);
		List<NcZhenliaoLogs> list = ncZhenliaoLogsService.selectNcZhenliaoLogsListConfirm(ncZhenliaoLogs);
		return getDataTable(list);
	}


	/**
	 * 导出诊疗单列表
	 */
	@PostMapping("/export")
	@ResponseBody
	public AjaxResult export(NcZhenliaoLogs ncZhenliaoLogs)
	{
		List<NcZhenliaoLogs> list = ncZhenliaoLogsService.selectNcZhenliaoLogsList(ncZhenliaoLogs);
		ExcelUtil<NcZhenliaoLogs> util = new ExcelUtil<NcZhenliaoLogs>(NcZhenliaoLogs.class);
		return util.exportExcel(list, "ncZhenliaoLogs");
	}

	/**
	 * 新增诊疗单
	 */
	@GetMapping("/add")
	public String add(ModelMap mmap) {
		//用户信息
		List<SysUser> sysUsers = userService.selectUserList(new SysUser());
		mmap.put("users", sysUsers);

		//查询兽药库存中的药品信息
		List<NcDrugs> ncDrugsList = ncDrugsService.selectNcDrugsList(new NcDrugs());
		mmap.put("drugs", ncDrugsList);

		//用药方法
		List<SysDictData> eType = dictDataService.selectDictDataByType("animalExecuteType");
		mmap.put("eType", eType);
		//用药单位
		List<SysDictData> unit = dictDataService.selectDictDataByType("unitType");
		mmap.put("unit", unit);

		return prefix + "/add";
	}

	/**
	 * 新增保存诊疗单
	 */
	@RequiresPermissions("system:ncZhenliaoLogs:add")
	@Log(title = "诊疗单", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(NcZhenliaoLogs ncZhenliaoLogs)
	{
		return ncZhenliaoLogsService.insertNcZhenliaoLogs(ncZhenliaoLogs,ShiroUtils.getSysUser());
	}

	/**
	 * 修改诊疗单
	 */
	@GetMapping("/edit/{zlId}")
	public String edit(@PathVariable("zlId") Long zlId, ModelMap mmap)
	{
		NcZhenliaoLogs ncZhenliaoLogs = ncZhenliaoLogsService.selectNcZhenliaoLogsById(zlId);
		mmap.put("ncZhenliaoLogs", ncZhenliaoLogs);

		//用户信息
		List<SysUser> sysUsers = userService.selectUserList(new SysUser());
		mmap.put("users", sysUsers);


		//查询兽药库存中的药品信息
		List<NcDrugs> ncDrugsList = ncDrugsService.selectNcDrugsList(new NcDrugs());
		mmap.put("drugs", ncDrugsList);


		//用药方法
		List<SysDictData> eType = dictDataService.selectDictDataByType("animalExecuteType");
		mmap.put("eType", eType);
		//用药单位
		List<SysDictData> unit = dictDataService.selectDictDataByType("unitType");
		mmap.put("unit", unit);


		//诊疗单对应用药
		NcZhenliaoDrugs ncZhenliaoDrugs = new NcZhenliaoDrugs();
		ncZhenliaoDrugs.setZhenliaoId(ncZhenliaoLogs.getZlId());
		List<NcZhenliaoDrugs> ncZhenliaoDrugs1 = zhenliaoDrugsService.selectNcZhenliaoDrugsList(ncZhenliaoDrugs);
		mmap.put("zlDrugs", ncZhenliaoDrugs1);

		return prefix + "/edit";
	}


	/**
	 * 修改保存检测
	 */
	@RequiresPermissions("system:ncZhenliaoLogs:edit")
	@Log(title = "检测", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(NcZhenliaoLogs ncZhenliaoLogs)
	{
		return ncZhenliaoLogsService.updateNcZhenliaoLogs(ncZhenliaoLogs);
	}

	/**
	 * 删除诊疗单
	 */
	@RequiresPermissions("system:ncZhenliaoLogs:remove")
	@Log(title = "诊疗单", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public AjaxResult remove(String ids)
	{
		return toAjax(ncZhenliaoLogsService.deleteNcZhenliaoLogsByIds(ids));
	}





	/**
	 * 确认诊疗单
	 */
	@GetMapping("/confirmCheck/{zlId}")
	public String confirmCheck(@PathVariable("zlId") Long zlId, ModelMap mmap)
	{
		NcZhenliaoLogs ncZhenliaoLogs = ncZhenliaoLogsService.selectNcZhenliaoLogsById(zlId);
		mmap.put("ncZhenliaoLogs", ncZhenliaoLogs);
		//药品信息
		List<NcDrugs> ncDrugs = ncDrugsService.selectNcDrugsList(new NcDrugs());
		mmap.put("ncDrugs", ncDrugs);

		//用户信息
		List<SysUser> sysUsers = userService.selectUserList(new SysUser());
		mmap.put("users", sysUsers);

		//查询兽药库存中的药品信息
		List<NcDrugs> ncDrugsList = ncDrugsService.selectNcDrugsList(new NcDrugs());
		mmap.put("drugs", ncDrugsList);

		//用药方法
		List<SysDictData> eType = dictDataService.selectDictDataByType("animalExecuteType");
		mmap.put("eType", eType);
		//用药单位
		List<SysDictData> unit = dictDataService.selectDictDataByType("unitType");
		mmap.put("unit", unit);
		return prefix + "/edit-confirm";
	}

	/**
	 * 确认诊疗单
	 */

	@PostMapping("/confirm")
	@ResponseBody
	public AjaxResult confirm(NcZhenliaoLogs ncZhenliaoLogs) {

		SysUser user = ShiroUtils.getSysUser();
		return ncZhenliaoLogsService.confirmNcZhenliaoLogs(ncZhenliaoLogs,user);

	}

	/**
	 * 批量确认诊疗单生成检测单
	 */
	@Log(title = "确认生成巡视测单", businessType = BusinessType.OTHER)
	@PostMapping("/confirmTable")
	@ResponseBody
	public AjaxResult confirmTable(String ids,ModelMap mmap) {
		SysUser user = ShiroUtils.getSysUser();
		return toAjax(ncZhenliaoLogsService.createZlOrderByIds(ids,user));
	}
	/**
	 * 请求诊疗列表
	 */
	@GetMapping("/askZhenLiaoList")
	public String askZhenLiaoList(ModelMap mmap) {
		//用户信息
		List<SysUser> sysUsers = userService.selectUserList(new SysUser());
		mmap.put("users", sysUsers);
		//当前用户
		SysUser sysUser = ShiroUtils.getSysUser();
		mmap.put("sysUser", sysUser);
		return prefix + "/askZhenLiaoList";
	}


	/**
	 * 请求诊疗
	 */
	@GetMapping("/askZhenLiao")
	public String askZhenLiao(ModelMap mmap) {
		//用户信息
		List<SysUser> sysUsers = userService.selectUserList(new SysUser());
		mmap.put("users", sysUsers);
		SysUser sysUser = ShiroUtils.getSysUser();
		mmap.put("sysUser", sysUser);
		return prefix + "/askZhenLiao";
	}

	/**
	 * 新增保存诊疗单
	 */
	@RequiresPermissions("system:ncZhenliaoLogs:add")
	@Log(title = "诊疗单", businessType = BusinessType.INSERT)
	@PostMapping("/addZhenLiao")
	@ResponseBody
	public AjaxResult addZhenLiao(NcZhenliaoLogs ncZhenliaoLogs)
	{
		return toAjax(ncZhenliaoLogsService.askNcZhenliaoLogs(ncZhenliaoLogs,ShiroUtils.getSysUser()));
	}


}
