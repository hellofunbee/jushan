package com.ruoyi.web.controller.system;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.NcWorkMirror;
import com.ruoyi.system.service.INcWorkMirrorService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;

/**
 * 标准农事镜像 信息操作处理
 * 
 * @author ruoyi
 * @date 2019-09-02
 */
@Controller
@RequestMapping("/system/ncWorkMirror")
public class NcWorkMirrorController extends BaseController
{
    private String prefix = "system/ncWorkMirror";
	
	@Autowired
	private INcWorkMirrorService ncWorkMirrorService;
	
	@RequiresPermissions("system:ncWorkMirror:view")
	@GetMapping()
	public String ncWorkMirror()
	{
	    return prefix + "/ncWorkMirror";
	}
	
	/**
	 * 查询标准农事镜像列表
	 */
	@RequiresPermissions("system:ncWorkMirror:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(NcWorkMirror ncWorkMirror)
	{
		startPage();
        List<NcWorkMirror> list = ncWorkMirrorService.selectNcWorkMirrorList(ncWorkMirror);
		return getDataTable(list);
	}
	
	
	/**
	 * 导出标准农事镜像列表
	 */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(NcWorkMirror ncWorkMirror)
    {
    	List<NcWorkMirror> list = ncWorkMirrorService.selectNcWorkMirrorList(ncWorkMirror);
        ExcelUtil<NcWorkMirror> util = new ExcelUtil<NcWorkMirror>(NcWorkMirror.class);
        return util.exportExcel(list, "ncWorkMirror");
    }
	
	/**
	 * 新增标准农事镜像
	 */
	@GetMapping("/add")
	public String add()
	{
	    return prefix + "/add";
	}
	
	/**
	 * 新增保存标准农事镜像
	 */
	@RequiresPermissions("system:ncWorkMirror:add")
	@Log(title = "标准农事镜像", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(NcWorkMirror ncWorkMirror)
	{		
		return toAjax(ncWorkMirrorService.insertNcWorkMirror(ncWorkMirror));
	}

	/**
	 * 修改标准农事镜像
	 */
	@GetMapping("/edit/{workId}")
	public String edit(@PathVariable("workId") Long workId, ModelMap mmap)
	{
		NcWorkMirror ncWorkMirror = ncWorkMirrorService.selectNcWorkMirrorById(workId);
		mmap.put("ncWorkMirror", ncWorkMirror);
	    return prefix + "/edit";
	}
	
	/**
	 * 修改保存标准农事镜像
	 */
	@RequiresPermissions("system:ncWorkMirror:edit")
	@Log(title = "标准农事镜像", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(NcWorkMirror ncWorkMirror)
	{		
		return toAjax(ncWorkMirrorService.updateNcWorkMirror(ncWorkMirror));
	}
	
	/**
	 * 删除标准农事镜像
	 */
	@RequiresPermissions("system:ncWorkMirror:remove")
	@Log(title = "标准农事镜像", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public AjaxResult remove(String ids)
	{		
		return toAjax(ncWorkMirrorService.deleteNcWorkMirrorByIds(ids));
	}
	
}
