package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.Ztree;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.NcXiaoduLogs;
import com.ruoyi.system.domain.SysRole;
import com.ruoyi.system.service.INcXiaoduLogsService;
import com.ruoyi.system.service.ISysDeptService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * 消毒记录（植保） 信息操作处理
 *
 * @author ruoyi
 * @date 2019-08-30
 */
@Controller
@RequestMapping("/system/ncXiaoduLogs")
public class NcXiaoduLogsController extends BaseController {
    private String prefix = "system/ncXiaoduLogs";

    @Autowired
    private INcXiaoduLogsService ncXiaoduLogsService;
    @Autowired
    private ISysDeptService deptService;

    @RequiresPermissions("system:ncXiaoduLogs:view")
    @GetMapping()
    public String ncXiaoduLogs(String workType, String beginTime, String endTime, ModelMap map) {
        map.put("workType", workType);
        map.put("beginxdTime", beginTime);
        map.put("endxdTime", endTime);
        return prefix + "/ncXiaoduLogs";
    }

    /**
     * 查询消毒记录（植保）列表
     */
    @RequiresPermissions("system:ncXiaoduLogs:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(NcXiaoduLogs ncXiaoduLogs) {
        startPage();
        List<NcXiaoduLogs> list = ncXiaoduLogsService.selectNcXiaoduLogsList(ncXiaoduLogs);
        return getDataTable(list);
    }


    /**
     * 导出消毒记录（植保）列表
     */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(NcXiaoduLogs ncXiaoduLogs) {
        List<NcXiaoduLogs> list = ncXiaoduLogsService.selectNcXiaoduLogsList(ncXiaoduLogs);
        ExcelUtil<NcXiaoduLogs> util = new ExcelUtil<NcXiaoduLogs>(NcXiaoduLogs.class);
        return util.exportExcel(list, "消毒记录表");
    }

    /**
     * 新增消毒记录（植保）
     */
    @GetMapping("/add")
    public String add(ModelMap map) {
        map.put("dept", deptService.selectDeptById(Long.valueOf(100)));
        map.put("loginName", ShiroUtils.getLoginName());
        return prefix + "/add";
    }

    /**
     * 新增保存消毒记录（植保）
     */
    @RequiresPermissions("system:ncXiaoduLogs:add")
    @Log(title = "消毒记录", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(NcXiaoduLogs ncXiaoduLogs) {
        ncXiaoduLogs.setCreateTime(new Date());
        ncXiaoduLogs.setCreateBy("" + ShiroUtils.getSysUser().getUserId());
        return toAjax(ncXiaoduLogsService.insertNcXiaoduLogs(ncXiaoduLogs));
    }

    /**
     * 修改消毒记录（植保）
     */
    @GetMapping("/edit/{xdId}")
    public String edit(@PathVariable("xdId") Long xdId, ModelMap mmap) {
        NcXiaoduLogs ncXiaoduLogs = ncXiaoduLogsService.selectNcXiaoduLogsById(xdId);
        mmap.put("ncXiaoduLogs", ncXiaoduLogs);
        mmap.put("loginName", ShiroUtils.getLoginName());
        mmap.put("dept", deptService.selectDeptById(Long.valueOf(100)));
        return prefix + "/edit";
    }

    /**
     * 修改保存消毒记录（植保）
     */
    @RequiresPermissions("system:ncXiaoduLogs:edit")
    @Log(title = "消毒记录（植保）", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(NcXiaoduLogs ncXiaoduLogs) {
        ncXiaoduLogs.setCreateTime(new Date());
        return toAjax(ncXiaoduLogsService.updateNcXiaoduLogs(ncXiaoduLogs));
    }

    /**
     * 删除消毒记录（植保）
     */
    @RequiresPermissions("system:ncXiaoduLogs:remove")
    @Log(title = "消毒记录（植保）", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(ncXiaoduLogsService.deleteNcXiaoduLogsByIds(ids));
    }

    /**
     * 加载角色部门（数据权限）列表树
     */
    @GetMapping("/roleDeptTreeData")
    @ResponseBody
    public List<Ztree> deptTreeData(SysRole role) {
        List<Ztree> ztrees = deptService.roleDeptTreeData(role);
        return ztrees;
    }

    @RequiresPermissions("system:ncXiaoduLogs:view")
    @GetMapping("/zhibaoXiaodu")
    public String ncXiaodu(String workType, String beginTime, String endTime, ModelMap map) {
        map.put("workType", workType);
        map.put("beginxdTime", beginTime);
        map.put("endxdTime", endTime);
        return prefix + "/front/ncXiaoduLogs";
    }

}
