package com.ruoyi.web.controller.system;

import com.ruoyi.WebSocketTemplate;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.service.*;
import com.ruoyi.system.util.ExcelUtil2Element;
import com.ruoyi.system.util.WorkbookExportUtil;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 诊疗单 信息操作处理
 *
 * @author ruoyi
 * @date 2019-08-28
 */
@Controller
@RequestMapping("/system/ncZhenliaoLogs/cow")
public class NcCowZhenliaoLogsController extends BaseController {
    private String prefix = "system/ncZhenliaoLogs/cow";

    @Autowired
    private INcCowZhenliaoLogsService ncZhenliaoLogsService;

    @Autowired
    private ISysUserService userService;

    @Autowired
    private INcCowsFilesService cowsFilesService;

    @Autowired
    private INcCowsBirthService ncCowsBirthService;

    @Autowired
    private INcYaoClassService yaoClassService;

    @Autowired
    private INcDrugsService ncDrugsService;

    @Autowired
    private ISysDictDataService dictDataService;

    @Autowired
    private INcZhenliaoDrugsService zhenliaoDrugsService;


    @Autowired
    private ISysNoticeService noticeService;
    @Autowired
    private WebSocketTemplate socketTemplate;

    @RequiresPermissions("system:ncZhenliaoLogs:view")
    @GetMapping()
    public String ncZhenliaoLogs() {
        return prefix + "/ncZhenliaoLogs";
    }
    @RequiresPermissions("system:ncZhenliaoLogs:view")
    @GetMapping("/allZhenliao")
    public String ncAllZhenliaoLogs() {
        return prefix + "/allZhenliaologs";
    }


    /**
     * 查询诊疗单列表
     */
    @RequiresPermissions("system:ncZhenliaoLogs:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(NcZhenliaoLogs ncZhenliaoLogs) {
        startPage();
        List<NcZhenliaoLogs> list = ncZhenliaoLogsService.selectNcZhenliaoLogsList(ncZhenliaoLogs);
        return getDataTable(list);
    }


    /**
     * 查询诊疗单列表
     */
    @RequiresPermissions("system:ncZhenliaoLogs:list")
    @PostMapping("/list/{zlType}/{zlStatus}")
    @ResponseBody
    public TableDataInfo list(@PathVariable("zlType") Integer zlType, @PathVariable("zlStatus") String zlStatus, NcZhenliaoLogs ncZhenliaoLogs) {
        startPage();
        ncZhenliaoLogs.setZlType(zlType);
        ncZhenliaoLogs.setZlStatus(zlStatus);
        List<NcZhenliaoLogs> list = ncZhenliaoLogsService.selectNcZhenliaoLogsList(ncZhenliaoLogs);
        return getDataTable(list);
    }

    /**
     * 查询诊疗单列表
     */
    @RequiresPermissions("system:ncZhenliaoLogs:list")
    @PostMapping("/listConfirm/{zlType}/{zlStatus}")
    @ResponseBody
    public TableDataInfo listConfirm(@PathVariable("zlType") Integer zlType, @PathVariable("zlStatus") String zlStatus, NcZhenliaoLogs ncZhenliaoLogs) {
        startPage();
        ncZhenliaoLogs.setZlType(zlType);
        ncZhenliaoLogs.setZlStatus(zlStatus);
        List<NcZhenliaoLogs> list = ncZhenliaoLogsService.selectNcZhenliaoLogsListConfirm(ncZhenliaoLogs);
        return getDataTable(list);
    }


    /**
     * 新增诊疗单
     */
    @GetMapping("/add")
    @RequiresPermissions("system:ncZhenliaoLogs:add")
    public String add(ModelMap mmap) {

        //用户信息
        List<SysUser> sysUsers = userService.selectUserList(new SysUser());
        mmap.put("users", sysUsers);

        SysUser sysUser = ShiroUtils.getSysUser();
        mmap.put("sysUser", sysUser);

        //成年奶牛耳号信息
        NcCowsFiles ncCowsFiles = new NcCowsFiles();
        List<String> cowsStr1 = cowsFilesService.selectAllCowCode();

        //幼年犊牛信息
        NcCowsBirth ncCowsBirth = new NcCowsBirth();
        List<String> cowsStr2 = ncCowsBirthService.selectAllChildCode();
        cowsStr1.addAll(cowsStr2);
        mmap.put("cows", cowsStr1);

        //查询兽药库存中的药品信息
        List<NcDrugs> ncDrugsList = ncDrugsService.selectNcDrugsList(new NcDrugs());
        mmap.put("drugs", ncDrugsList);

        //用药方法
        List<SysDictData> eType = dictDataService.selectDictDataByType("animalExecuteType");
        mmap.put("eType", eType);
        //用药单位
        List<SysDictData> unit = dictDataService.selectDictDataByType("unitType");
        mmap.put("unit", unit);

        return prefix + "/add";
    }

    /**
     * 新增保存诊疗单
     */
    @RequiresPermissions("system:ncZhenliaoLogs2:add")
    @Log(title = "诊疗单", businessType = BusinessType.INSERT)
    @PostMapping("/addZhenLiao")
    @ResponseBody
    public AjaxResult addZhenLiao(NcZhenliaoLogs ncZhenliaoLogs) {
        ncZhenliaoLogs.setZlStatus("5");
        if (ncZhenliaoLogs.getIllMuns() >= 0) {
            return toAjax(ncZhenliaoLogsService.askNcZhenliaoLogs(ncZhenliaoLogs, ShiroUtils.getSysUser()));
        } else {
            return AjaxResult.error("不能有负数");
        }

    }


    /**
     * 新增保存诊疗单
     */
    @RequiresPermissions("system:ncZhenliaoLogs:add")
    @Log(title = "诊疗单", businessType = BusinessType.INSERT)
    @PostMapping("/addZhenLiao1")
    @ResponseBody
    public AjaxResult addZhenLiao1(NcZhenliaoLogs ncZhenliaoLogs) {
        if (ncZhenliaoLogs.getIllMuns() >= 0) {
            return ncZhenliaoLogsService.insertNcZhenliaoLogs(ncZhenliaoLogs, ShiroUtils.getSysUser());
        } else {
            return AjaxResult.error("不能有负数");
        }

    }


    /**
     * 修改诊疗单
     */
    @GetMapping("/edit/{zlId}")
    public String edit(@PathVariable("zlId") Long zlId, ModelMap mmap) {
        NcZhenliaoLogs ncZhenliaoLogs = ncZhenliaoLogsService.selectNcZhenliaoLogsById(zlId);
        mmap.put("ncZhenliaoLogs", ncZhenliaoLogs);

        //用户信息
        List<SysUser> sysUsers = userService.selectUserList(new SysUser());
        mmap.put("users", sysUsers);


        //成年奶牛耳号信息
        NcCowsFiles ncCowsFiles = new NcCowsFiles();
        List<String> cowsStr1 = cowsFilesService.selectAllCowCode();

        //幼年犊牛信息
        NcCowsBirth ncCowsBirth = new NcCowsBirth();
        List<String> cowsStr2 = ncCowsBirthService.selectAllChildCode();
        cowsStr1.addAll(cowsStr2);
        mmap.put("cows", cowsStr1);

        //查询兽药库存中的药品信息
        List<NcDrugs> ncDrugsList = ncDrugsService.selectNcDrugsList(new NcDrugs());
        mmap.put("drugs", ncDrugsList);

        //用药方法
        List<SysDictData> eType = dictDataService.selectDictDataByType("animalExecuteType");
        mmap.put("eType", eType);
        //用药单位
        List<SysDictData> unit = dictDataService.selectDictDataByType("unitType");
        mmap.put("unit", unit);


        //诊疗单对应用药
        NcZhenliaoDrugs ncZhenliaoDrugs = new NcZhenliaoDrugs();
        ncZhenliaoDrugs.setZhenliaoId(ncZhenliaoLogs.getZlId());
        List<NcZhenliaoDrugs> ncZhenliaoDrugs1 = zhenliaoDrugsService.selectNcZhenliaoDrugsList(ncZhenliaoDrugs);
        mmap.put("zlDrugs", ncZhenliaoDrugs1);
        return prefix + "/edit";
    }


    /**
     * 修改保存检测
     */
    @RequiresPermissions("system:ncZhenliaoLogs:edit")
    @Log(title = "检测", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(NcZhenliaoLogs ncZhenliaoLogs) {
        return ncZhenliaoLogsService.updateNcZhenliaoLogs(ncZhenliaoLogs);
    }

    /**
     * 转 诊疗单 确认页面
     */
    @GetMapping("/confirmCheck/{zlId}")
    public String confirmCheck(@PathVariable("zlId") Long zlId, ModelMap mmap) {

        NcZhenliaoLogs ncZhenliaoLogs = ncZhenliaoLogsService.selectNcZhenliaoLogsById(zlId);
        mmap.put("ncZhenliaoLogs", ncZhenliaoLogs);

        //用户信息
        List<SysUser> sysUsers = userService.selectUserList(new SysUser());
        mmap.put("users", sysUsers);


        //成年奶牛耳号信息
        NcCowsFiles ncCowsFiles = new NcCowsFiles();
        List<String> cowsStr1 = cowsFilesService.selectAllCowCode();

        //幼年犊牛信息
        NcCowsBirth ncCowsBirth = new NcCowsBirth();
        List<String> cowsStr2 = ncCowsBirthService.selectAllChildCode();
        cowsStr1.addAll(cowsStr2);
        mmap.put("cows", cowsStr1);

        //查询兽药库存中的药品信息
        List<NcDrugs> ncDrugsList = ncDrugsService.selectNcDrugsList(new NcDrugs());
        mmap.put("drugs", ncDrugsList);


        //用药方法
        List<SysDictData> eType = dictDataService.selectDictDataByType("animalExecuteType");
        mmap.put("eType", eType);
        //用药单位
        List<SysDictData> unit = dictDataService.selectDictDataByType("unitType");
        mmap.put("unit", unit);
        return prefix + "/edit-confirm";
    }

    /**
     * 确认诊疗单
     *
     * @param ncZhenliaoLogs 诊疗单对象
     * @return
     */
    @PostMapping("/confirm")
    @ResponseBody
    public AjaxResult confirm(NcZhenliaoLogs ncZhenliaoLogs) {
        SysUser user = ShiroUtils.getSysUser();
        return ncZhenliaoLogsService.confirmNcZhenliaoLogs(ncZhenliaoLogs, user);
    }

    /**
     * 确认生成检测单
     */
    @Log(title = "确认牛舍诊疗单", businessType = BusinessType.OTHER)
    @RequiresPermissions("system:ncZhenliaoLogs:confirm")
    @PostMapping("/confirmTable")
    @ResponseBody
    public AjaxResult confirmTable(String ids, ModelMap mmap) {
        SysUser user = ShiroUtils.getSysUser();
        return toAjax(ncZhenliaoLogsService.createZlOrderByIds(ids, user));
    }


    /**
     * 删除诊疗单
     */
    @RequiresPermissions("system:ncZhenliaoLogs:remove")
    @Log(title = "诊疗单", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(ncZhenliaoLogsService.deleteNcZhenliaoLogsByIds(ids));
    }


    /**
     * 请求诊疗
     */
    @GetMapping("/askZhenLiao")
    public String askZhenLiao(ModelMap mmap) {
        //用户信息
        List<SysUser> sysUsers = userService.selectUserList(new SysUser());
        mmap.put("users", sysUsers);

        SysUser sysUser = ShiroUtils.getSysUser();
        mmap.put("sysUser", sysUser);

        //成年奶牛耳号信息
        NcCowsFiles ncCowsFiles = new NcCowsFiles();
        List<String> cowsStr1 = cowsFilesService.selectAllCowCode();

        //幼年犊牛信息
        NcCowsBirth ncCowsBirth = new NcCowsBirth();
        List<String> cowsStr2 = ncCowsBirthService.selectAllChildCode();
        cowsStr1.addAll(cowsStr2);
        mmap.put("cows", cowsStr1);
        return prefix + "/askZhenLiao";
    }


    /**
     * 请求诊疗列表
     */
    @GetMapping("/askZhenLiaoList")
    public String askZhenLiaoList(ModelMap mmap) {

        //用户信息
        List<SysUser> sysUsers = userService.selectUserList(new SysUser());
        mmap.put("users", sysUsers);

        SysUser sysUser = ShiroUtils.getSysUser();
        mmap.put("sysUser", sysUser);

        //成年奶牛耳号信息
        NcCowsFiles ncCowsFiles = new NcCowsFiles();
        List<String> cowsStr1 = cowsFilesService.selectAllCowCode();

        //幼年犊牛信息
        NcCowsBirth ncCowsBirth = new NcCowsBirth();
        List<String> cowsStr2 = ncCowsBirthService.selectAllChildCode();
        cowsStr1.addAll(cowsStr2);
        mmap.put("cows", cowsStr1);

        return prefix + "/askZhenLiaoList";
    }

    /**
     * 发送兽医请求
     */
    @Log(title = "发送兽医请求", businessType = BusinessType.OTHER)
    @RequiresPermissions("system:ncZhenliaoLogs:edit")
    @PostMapping("/sendZlRequest")
    @ResponseBody
    public AjaxResult sendZlRequest(String ids, ModelMap mmap) {

        //发送工作通知

        String[] zlIds = ids.split(",");

        SysNotice notice = null;
        SysUser user = new SysUser();
        List<SysUser> users = new ArrayList<>();


        if (zlIds.length > 0) {
            for (String zlId : zlIds) {
                NcZhenliaoLogs ncZhenliaoLogs = ncZhenliaoLogsService.selectNcZhenliaoLogsById(Long.parseLong(zlId));

                notice = new SysNotice();
                notice.setNoticeTitle("牛舍诊疗请求");
                notice.setNoticeContent("耳号【" + ncZhenliaoLogs.getCowCode() + "】") ;
                notice.setNoticeType("3");
                notice.setStatus("0");
                notice.setDeptId("2"); //设置选择用户
                notice.setReadType(2); //设置未读
                notice.setCreateTime(new Date());
                notice.setCreateBy(ShiroUtils.getLoginName());

                //兽医部门
                user.setDeptId(Long.parseLong("250"));
                List<SysUser> sysUsers1 = userService.selectUserList(user);
                users.addAll(sysUsers1);

                String userId = "";
                if (!users.isEmpty()) {
                    for (SysUser sysUser : users) {
                        userId += sysUser.getUserId() + ",";
                    }
                }
                notice.setUserId(userId);
                int no = noticeService.insertNotice(notice);
                if (no > 0) {
                    socketTemplate.sendMessage(notice);
                }
            }
        }


        return toAjax(ncZhenliaoLogsService.updateZhenliao(ids));
    }


    /**
     * 导出诊疗单列表
     */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(NcZhenliaoLogs ncZhenliaoLogs) {

        WorkbookExportUtil wu = new WorkbookExportUtil(new XSSFWorkbook(), "牛舍诊疗数据");

        List<NcZhenliaoLogs> zhenliaoLogsList = ncZhenliaoLogsService.selectNcZhenliaoLogsList(ncZhenliaoLogs);
        ExcelUtil2Element<NcZhenliaoLogs> m_zhenliao = new ExcelUtil2Element<>(NcZhenliaoLogs.class, wu);
        m_zhenliao.exportExcel(zhenliaoLogsList, "诊疗数据");


        List<NcZhenliaoDrugs> drugList = new ArrayList<>();

        if (zhenliaoLogsList != null && zhenliaoLogsList.size() > 0) {
            for (NcZhenliaoLogs zhenliaoLogs : zhenliaoLogsList) {
                NcZhenliaoDrugs drugs = new NcZhenliaoDrugs();
                drugs.setZhenliaoId(zhenliaoLogs.getZlId());
                List<NcZhenliaoDrugs> zhenliaoDrugs = zhenliaoDrugsService.selectNcZhenliaoDrugsList(drugs);
                drugList.addAll(zhenliaoDrugs);

            }

            ExcelUtil2Element<NcZhenliaoDrugs> m_zhenliaoDrugs = new ExcelUtil2Element<>(NcZhenliaoDrugs.class, wu);
            m_zhenliaoDrugs.exportExcel(drugList, "诊疗用药数据");

        }


        return wu.exportExcel();
    }


    /**
     * 查询诊疗单详情
     */
    @RequiresPermissions("system:ncCowsZhenliaoLogs:detail")
    @GetMapping("/detail/{id}")
    public String detail(@PathVariable("id") Long zlId, ModelMap mmap) {
        NcZhenliaoLogs ncZhenliaoLogs = ncZhenliaoLogsService.selectNcZhenliaoLogsById(zlId);
        mmap.put("name", "job");
        mmap.put("zl", ncZhenliaoLogs);
        NcZhenliaoDrugs ncZhenliaoDrugs = new NcZhenliaoDrugs();
        ncZhenliaoDrugs.setZhenliaoId(ncZhenliaoLogs.getZlId());
        List<NcZhenliaoDrugs> ncZhenliaoDrugs1 = zhenliaoDrugsService.selectNcZhenliaoDrugsList(ncZhenliaoDrugs);
        mmap.put("zlDrugs", ncZhenliaoDrugs1);
        return prefix + "/detail";
    }

    /**
     * 修改诊疗单2
     */
    @RequiresPermissions("system:ncZhenliaoLogs2:edit")
    @GetMapping("/edit2/{zlId}")
    public String edit2(@PathVariable("zlId") Long zlId, ModelMap mmap) {
        NcZhenliaoLogs ncZhenliaoLogs = ncZhenliaoLogsService.selectNcZhenliaoLogsById(zlId);
        mmap.put("ncZhenliaoLogs", ncZhenliaoLogs);

        //用户信息
        List<SysUser> sysUsers = userService.selectUserList(new SysUser());
        mmap.put("users", sysUsers);


        //成年奶牛耳号信息
        NcCowsFiles ncCowsFiles = new NcCowsFiles();
        List<String> cowsStr1 = cowsFilesService.selectAllCowCode();

        //幼年犊牛信息
        NcCowsBirth ncCowsBirth = new NcCowsBirth();
        List<String> cowsStr2 = ncCowsBirthService.selectAllChildCode();
        cowsStr1.addAll(cowsStr2);
        mmap.put("cows", cowsStr1);

        //药品信息
        List<NcDrugs> ncDrugs = ncDrugsService.selectNcDrugsList(new NcDrugs());
        mmap.put("ncDrugs", ncDrugs);

        return prefix + "/edit2";
    }
}
