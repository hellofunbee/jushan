package com.ruoyi.web.controller.tool;

import com.ruoyi.common.config.Global;
import com.ruoyi.common.utils.DateUtils;
import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.File;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @version V1.0
 * @desc AES 加密工具类
 */

public class AESUtil {

    private static final String KEY_ALGORITHM = "AES";
    private static final String DEFAULT_CIPHER_ALGORITHM = "AES/ECB/PKCS5Padding";//默认的加密算法

    /**
     * AES 加密操作
     *
     * @param content  待加密内容
     * @param password 加密密码
     * @return 返回Base64转码后的加密数据
     */
    public static String encrypt(String content, String password) {
        try {
            Cipher cipher = Cipher.getInstance(DEFAULT_CIPHER_ALGORITHM);// 创建密码器

            byte[] byteContent = content.getBytes("utf-8");

            cipher.init(Cipher.ENCRYPT_MODE, getSecretKey(password));// 初始化为加密模式的密码器

            byte[] result = cipher.doFinal(byteContent);// 加密

            return Base64.encodeBase64String(result);//通过Base64转码返回
        } catch (Exception ex) {
            Logger.getLogger(AESUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    /**
     * AES 解密操作
     *
     * @param content
     * @param password
     * @return
     */
    public static String decrypt(String content, String password) {
        try {
            //实例化
            Cipher cipher = Cipher.getInstance(DEFAULT_CIPHER_ALGORITHM);

            //使用密钥初始化，设置为解密模式
            cipher.init(Cipher.DECRYPT_MODE, getSecretKey(password));

            //执行操作
            byte[] result = cipher.doFinal(Base64.decodeBase64(content));

            return new String(result, "utf-8");
        } catch (Exception ex) {
            Logger.getLogger(AESUtil.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    /**
     * 生成加密秘钥
     *
     * @return
     */
    private static SecretKeySpec getSecretKey(final String password) {
        //返回生成指定算法密钥生成器的 KeyGenerator 对象
        KeyGenerator kg = null;


        try {
            kg = KeyGenerator.getInstance(KEY_ALGORITHM);
            SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
            random.setSeed(password.getBytes());
            kg.init(128, random);//AES 要求密钥长度为 128

            //生成一个密钥
            SecretKey secretKey = kg.generateKey();
            return new SecretKeySpec(secretKey.getEncoded(), KEY_ALGORITHM);// 转换为AES专用密钥
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(AESUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }


    public static void main(String[] args) {

        /*Scanner in = new Scanner(System.in);
        String str = in.nextLine();
        System.out.println(str);*/
        String s = "尊敬%%%的aa客户gg您好，您的r使ttT用C有效33T期是&&2020-01-24&&谢谢您3023233UU的使用-+==。";

        System.out.println("s0:" + s);
        String s1 = AESUtil.encrypt(s, "xSFDLhRftM6Ozav7Zj0Z4A");
        System.out.println("s1:" + s1);
        String z4A = AESUtil.decrypt(s1, "xSFDLhRftM6Ozav7Zj0Z4A");
        System.out.println("s2:" + z4A);

        System.out.println(z4A.split("&&")[1]);
//        System.out.println(getTime());


        String test = decrypt("3Lq6kClG3CtCq4sQULzATHij8TBEPdYWy0VRBgPXzdqGVgeINgZZzul/3iyp2QDiE4ctORbUfY6wYsYYpRhjcmNIdH0FoxmMeP5eGrGcxqsABn6+HSidY4tunctdtlPV6kTUK5yzLpMuwQgFWsXxag==","xSFDLhRftM6Ozav7Zj0Z4A");
        System.out.println("test:"+test);
        System.out.println(test.split("&&")[1]);
    }

    public static int getTime() {
        try {
            String f = new File(Global.getProfile()).getParent();

            String code = ReadTest.txt2String(new File(f + File.separator + "authorization.txt"));
            String z4A = AESUtil.decrypt(code, "xSFDLhRftM6Ozav7Zj0Z4A");

            String s = z4A.split("&&")[1];

            int l = DateUtils.getDiffDays(DateUtils.parseDate(DateUtils.dateTime2()), DateUtils.dateTime("yyyy-MM-dd", s));
            return l;
        } catch (Exception e) {

        }

        return 0;
    }

}


