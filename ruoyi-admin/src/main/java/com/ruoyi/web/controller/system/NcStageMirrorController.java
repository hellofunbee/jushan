package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.NcStageMirror;
import com.ruoyi.system.service.INcStageMirrorService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 生产（育苗）阶段 镜像 信息操作处理
 * 
 * @author ruoyi
 * @date 2019-09-02
 */
@Controller
@RequestMapping("/system/ncStageMirror")
public class NcStageMirrorController extends BaseController
{
    private String prefix = "system/ncStageMirror";
	
	@Autowired
	private INcStageMirrorService ncStageMirrorService;
	
	@RequiresPermissions("system:ncStageMirror:view")
	@GetMapping()
	public String ncStageMirror()
	{
	    return prefix + "/ncStageMirror";
	}
	
	/**
	 * 查询生产（育苗）阶段 镜像列表
	 */
	@RequiresPermissions("system:ncStageMirror:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(NcStageMirror ncStageMirror)
	{
		startPage();
        List<NcStageMirror> list = ncStageMirrorService.selectNcStageMirrorList(ncStageMirror);
		return getDataTable(list);
	}
	
	
	/**
	 * 导出生产（育苗）阶段 镜像列表
	 */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(NcStageMirror ncStageMirror)
    {
    	List<NcStageMirror> list = ncStageMirrorService.selectNcStageMirrorList(ncStageMirror);
        ExcelUtil<NcStageMirror> util = new ExcelUtil<NcStageMirror>(NcStageMirror.class);
        return util.exportExcel(list, "ncStageMirror");
    }
	
	/**
	 * 新增生产（育苗）阶段 镜像
	 */
	@GetMapping("/add")
	public String add()
	{
	    return prefix + "/add";
	}
	
	/**
	 * 新增保存生产（育苗）阶段 镜像
	 */
	@RequiresPermissions("system:ncStageMirror:add")
	@Log(title = "生产（育苗）阶段 镜像", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(NcStageMirror ncStageMirror)
	{		
		return toAjax(ncStageMirrorService.insertNcStageMirror(ncStageMirror));
	}

	/**
	 * 修改生产（育苗）阶段 镜像
	 */
	@GetMapping("/edit/{stageId}")
	public String edit(@PathVariable("stageId") Long stageId, ModelMap mmap)
	{
		NcStageMirror ncStageMirror = ncStageMirrorService.selectNcStageMirrorById(stageId);
		mmap.put("ncStageMirror", ncStageMirror);
	    return prefix + "/edit";
	}
	
	/**
	 * 修改保存生产（育苗）阶段 镜像
	 */
	@RequiresPermissions("system:ncStageMirror:edit")
	@Log(title = "生产（育苗）阶段 镜像", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(NcStageMirror ncStageMirror)
	{		
		return toAjax(ncStageMirrorService.updateNcStageMirror(ncStageMirror));
	}
	
	/**
	 * 删除生产（育苗）阶段 镜像
	 */
	@RequiresPermissions("system:ncStageMirror:remove")
	@Log(title = "生产（育苗）阶段 镜像", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public AjaxResult remove(String ids)
	{		
		return toAjax(ncStageMirrorService.deleteNcStageMirrorByIds(ids));
	}
	
}
