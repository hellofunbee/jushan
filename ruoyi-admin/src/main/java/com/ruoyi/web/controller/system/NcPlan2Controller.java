package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.NcUtils;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.service.*;
import com.ruoyi.system.util.ExcelUtil2Element;
import com.ruoyi.system.util.WorkbookExportUtil;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 计划 信息操作处理
 *
 * @author ruoyi
 * @date 2019-07-31
 */
@Controller
@RequestMapping("/system/ncPlan2")
public class NcPlan2Controller extends BaseController {
    private String prefix = "system/ncPlan2";

    @Autowired
    private INcPlanService ncPlanService;
    @Autowired
    private INcWorkMirrorService workMirrorService;
    @Autowired
    private INcStageMirrorService stageMirrorService;
    @Autowired
    private INcStandardService standardService;

    @Autowired
    private INcOuputCfService ncOuputCfService;
    @Autowired
    private INcCaiService ncCaiService;

    @Autowired
    private ISysUserService userService;

    @Autowired
    private ISysDeptService deptService;
    @Autowired
    private INcOrderCfService orderService;

    @RequiresPermissions("system:ncPlan:view")
    @GetMapping()
    public String ncPlan() {
        return prefix + "/ncPlan";
    }


    @RequiresPermissions("system:ncPlan:view")
    @GetMapping("/ncPlanFront")
    public String ncPlanFront() {
        return prefix + "/ncPlanFront";
    }

    @RequiresPermissions("system:ncPlan:view")
    @GetMapping("/ncPlanCaiId")
    public String ncPlan(long caiId, ModelMap mmap) {
        mmap.put("ancestors", caiId);
        return prefix + "/ncPlanCaiId";

    }


    @RequiresPermissions("system:ncPlan:add")
    @GetMapping("input")
    public String input() {
        return prefix + "/ncPlan-input";
    }

    @GetMapping("allPlan2")
    public String allPlan2() {
        return prefix + "/ncPlan-all";
    }

    /**
     * 查询计划列表
     */
    @RequiresPermissions("system:ncPlan:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(NcPlan ncPlan) {
        startPage();
        List<NcPlan> list = ncPlanService.selectNcPlanList(ncPlan);
        return getDataTable(list);
    }

    /**
     * 查询计划列表
     */
    @RequiresPermissions("system:ncPlan:list")
    @PostMapping("/selectNcPlanListByDeptId")
    @ResponseBody
    public TableDataInfo selectNcPlanListByDeptId(NcPlan ncPlan) {
        startPage();
        List<NcPlan> list = ncPlanService.selectNcPlanListByDeptId(ncPlan);
        return getDataTable(list);
    }


    /**
     * 新增计划
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存计划
     */
    @RequiresPermissions("system:ncPlan:add")
    @Log(title = "计划", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(NcPlan ncPlan) {
        ncPlan.setStatus("1");
        ncPlan.setPlanCode(NcUtils.getPlanCode());
        ncPlan.setCreateTime(new Date());
        ncPlan.setCreateBy(ShiroUtils.getLoginName());
        ncPlan = ncPlanService.fillValues(ncPlan);
        if (ncPlan == null) {
            return AjaxResult.error("未找到此品种");
        }
        return toAjax(ncPlanService.insertNcPlan(ncPlan));
    }

    /**
     * 修改计划
     */
    @GetMapping("/edit/{planId}")
    public String edit(@PathVariable("planId") Long planId, ModelMap mmap) {
        NcPlan ncPlan = ncPlanService.selectNcPlanById(planId);
        mmap.put("ncPlan", ncPlan);
        return prefix + "/edit";
    }

    /**
     * 修改保存计划
     */
    @RequiresPermissions("system:ncPlan:edit")
    @Log(title = "计划", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(NcPlan ncPlan) {
        //如果蔬菜品种有变化 则重新 赋值
        NcPlan ncPlanOld = ncPlanService.selectNcPlanById(ncPlan.getPlanId());
        if (ncPlanOld != null && ncPlanOld.getCropVarieties() == ncPlan.getCropVarieties()) {
            return toAjax(ncPlanService.updateNcPlan(ncPlan));
        } else {
            ncPlan = ncPlanService.fillValues(ncPlan);
            if (ncPlan == null) {
                return AjaxResult.error("未找到此品种");
            }
            return toAjax(ncPlanService.updateNcPlan(ncPlan));
        }

    }

    /**
     * 删除计划
     */
    @RequiresPermissions("system:ncPlan:remove")
    @Log(title = "计划", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(ncPlanService.deleteNcPlanByIds(ids));
    }

    /**
     * 确认生成育苗计划单
     */
    @Log(title = "确认生成育苗计划单", businessType = BusinessType.DELETE)
    @PostMapping("/confirmTable")
    @ResponseBody
    public AjaxResult confirmTable(String ids) {
        SysUser user = ShiroUtils.getSysUser();
        //TODO 生成育苗计划单
        return toAjax(ncPlanService.createNcPlanByIds(ids, user, Constants.ORDER_PRODUCE));
    }


    /*育苗计划*/
    @GetMapping("/frontPlanList")
    public String frontPlanList(String status, String beginTime, String endTime, String ancestors, ModelMap mmap) {
        mmap.put("status", status);
        mmap.put("ancestors", ancestors);
        mmap.put("beginTime", beginTime);
        mmap.put("endTime", endTime);
        return prefix + "/ncPlanCaiId";
    }


    /*计划详情*/
    @GetMapping("/planDetail")
    public String planDetail(Long planId, String planType, ModelMap mmap) {
        ncPlanService.planDetail(planId, planType, mmap);
        return prefix + "/planDetail1";
    }


    /*阶段 或者 工作 详情*/
    @GetMapping("/detail")
    public String detail(Integer type, Long id, String workDate, ModelMap mmap) {
        //1 :工作详情（镜像） 2：阶段详情（镜像）
        if (type == 2) {
            NcStageMirror stage = stageMirrorService.selectNcStageMirrorById(id);
            mmap.put("stage", stage);
            return prefix + "/detail-stage";
        } else if (type == 1) {
            NcWorkMirror work = workMirrorService.selectNcWorkMirrorById(id);
            mmap.put("work", work);
            mmap.put("workDate", workDate);
            return prefix + "/detail-work";
        } else {
            return "main";
        }

    }

    /*生产计划汇总表*/

    @GetMapping("/sumProductionPlanUI")
    public String sumProductionPlanUI(ModelMap mmap) {


        //查询计划所有作物类别
        List<NcPlan> cropNameCns = ncPlanService.queryCropNameCnInPlan(new NcPlan());
        //过滤鸡蛋牛奶相关计划
        cropNameCns.removeIf(plan -> ncCaiService.selectCaiById(plan.getCropVarieties()).getAncestors().contains("210"));
        mmap.put("cropNameCns", cropNameCns);


        List<NcCai> ncCais = ncCaiService.selectCaiListHasParent2(new NcCai());
        ncCais.removeIf(cai -> cai.getAncestors().split(",").length != 3);
        mmap.put("ncCais", ncCais);

        //用户信息
        List<SysUser> sysUsers = userService.selectUserList(new SysUser());
        mmap.put("users", sysUsers);

        return prefix + "/sumProductionPlan";
    }

    @RequiresPermissions("system:ncPlan:remove")
    @PostMapping("/sumProductionPlan")
    @ResponseBody
    public TableDataInfo sumProductionPlan(ModelMap mmap, NcPlan ncPlanQUery) {

        startPage();
        //查询生产计划所有作物类别
        ncPlanQUery.setPlanType(2);
        List<NcPlan> ncPlanCropS = ncPlanService.queryCropNameCnInPlan(ncPlanQUery);

        //过滤鸡蛋牛奶相关计划
        ncPlanCropS.removeIf(plan -> ncCaiService.selectCaiById(plan.getCropVarieties()).getAncestors().contains("210"));


        //根据作物类别查询计划集合
        for (NcPlan ncPlan : ncPlanCropS) {

            //总茬次
            int chaci = 0;

            //总面积
            BigDecimal sumArea = new BigDecimal(0L);

            //该班组生产计划的产量
            BigDecimal realAmout = new BigDecimal(0L);

            //根据作物类别查询 物类别下的所有生产计划
            List<NcPlan> ncPlans = ncPlanService.queryPlanByCropNameCn(ncPlan.getCropNameCn());

            // 循坏该作物类别 下的所有生产计划
            for (NcPlan plan : ncPlans) {

                BigDecimal sumAreaVO = new BigDecimal(0L);
                BigDecimal realAmoutVO = new BigDecimal(0L);

                //班组生产计划作物类别
                chaci += plan.getChaci();
                if (plan.getSumArea() != null)
                    sumArea = sumAreaVO.add(plan.getSumArea());


                //根据计划id查找菜房订单
                NcOrderCf order = new NcOrderCf();
                Integer strPlanId = Integer.parseInt(plan.getPlanId() + "");
                order.setPlanId(strPlanId);
                List<NcOrderCf> ncOrderCfs = orderService.selectNcOrderCfList(order);


                if (ncOrderCfs != null && ncOrderCfs.size() > 0) {
                    for (NcOrderCf ncOrderCf : ncOrderCfs) {
                        //该班组生产计划的实际产量
                        BigDecimal bigDecimal1 = ncOuputCfService.queryOutAmoutByPlanId(ncOrderCf.getCforderId());
                        if (bigDecimal1 != null) {
                            realAmout = realAmoutVO.add(bigDecimal1);
                        }
                    }
                }

            }
            ncPlan.setRealAmout(realAmout);
            ncPlan.setSumArea(sumArea);
            ncPlan.setChaci(chaci);
        }
        return getDataTable(ncPlanCropS);

    }

    /**
     * 下载导入模板
     *
     * @return
     */
    @RequiresPermissions("system:ncPlan:view")
    @GetMapping("/importTemplate")
    @ResponseBody
    public AjaxResult importTemplate() {

        WorkbookExportUtil wu = new WorkbookExportUtil(new XSSFWorkbook(), "生产计划数据模板");

        /*订单模板*/
        ExcelUtil2Element<NcPlanTemplate2> m_order = new ExcelUtil2Element<>(NcPlanTemplate2.class, wu);
        m_order.importTemplateExcel("生产计划数据");


        /*菜分类基础数据*/
        List<NcCai> cais = ncCaiService.selectCaiListHasParent2(new NcCai());
        //过滤鸡蛋牛奶与菜单类
        cais.removeIf(cai -> cai.getAncestors().contains("210") | cai.getAncestors().split(",").length != 3);


        ExcelUtil2Element<NcCai> m_cai = new ExcelUtil2Element<>(NcCai.class, wu);
        m_cai.exportExcel(cais, "菜分类基础数据");


        List<SysDept> sysDepts = deptService.selectDeptList(new SysDept());
        sysDepts.removeIf(dept -> dept.getType() != 2 || dept.getAncestors().split(",").length != 3);
        ExcelUtil2Element<SysDept> m_dept = new ExcelUtil2Element<>(SysDept.class, wu);
        m_dept.exportExcel(sysDepts, "班组基础数据");

        return wu.exportExcel();
    }


    @Log(title = "计划管理", businessType = BusinessType.IMPORT)
    @RequiresPermissions("system:ncPlan:import")
    @PostMapping("/importData")
    @ResponseBody
    public AjaxResult importData1(MultipartFile file, boolean updateSupport) throws Exception {
        SysUser sysUser = ShiroUtils.getSysUser();
        AjaxResult messsage = ncPlanService.importNcPlan(sysUser, 2, file, updateSupport);
        return messsage;
    }


    /**
     * 导出计划列表
     */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(NcPlan ncPlan) {
        WorkbookExportUtil wu = new WorkbookExportUtil(new XSSFWorkbook(), "生产计划数据");


        List<NcPlanExprot2> planList = ncPlanService.selectNcPlanExprot2(ncPlan);
        ExcelUtil2Element<NcPlanExprot2> m_planList = new ExcelUtil2Element<>(NcPlanExprot2.class, wu);
        m_planList.exportExcel(planList, "生产计划数据");


        NcStandard standard = new NcStandard();
        standard.setStandardType("2");
        List<NcStandard> standardList = standardService.selectNcStandardList(standard);
        ExcelUtil2Element<NcStandard> m_standard = new ExcelUtil2Element<>(NcStandard.class, wu);
        m_standard.exportExcel(standardList, "生产标准数据");



        /*菜分类基础数据*/
        List<NcCai> cais = ncCaiService.selectCaiListHasParent2(new NcCai());
        //过滤鸡蛋牛奶与菜单类
        cais.removeIf(cai -> cai.getAncestors().contains("210") | cai.getAncestors().split(",").length != 3);
        ExcelUtil2Element<NcCai> m_cai = new ExcelUtil2Element<>(NcCai.class, wu);
        m_cai.exportExcel(cais, "菜分类基础数据");


        List<SysDept> sysDepts = deptService.selectDeptList(new SysDept());
        sysDepts.removeIf(dept -> dept.getType() != 2 || dept.getAncestors().split(",").length != 3);
        ExcelUtil2Element<SysDept> m_dept = new ExcelUtil2Element<>(SysDept.class, wu);
        m_dept.exportExcel(sysDepts, "班组基础数据");

        return wu.exportExcel();
    }

}
