package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.NcTransportInfo;
import com.ruoyi.system.service.INcTransportInfoService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 菜房出库 信息操作处理
 * 
 * @author ruoyi
 * @date 2019-09-05
 */
@Controller
@RequestMapping("/system/ncTransportInfo")
public class NcTransportInfoController extends BaseController
{
    private String prefix = "system/ncTransportInfo";
	
	@Autowired
	private INcTransportInfoService ncTransportInfoService;
	
	@RequiresPermissions("system:ncTransportInfo:view")
	@GetMapping()
	public String ncTransportInfo()
	{
	    return prefix + "/ncTransportInfo";
	}
	
	/**
	 * 查询菜房出库列表
	 */
	@RequiresPermissions("system:ncTransportInfo:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(NcTransportInfo ncTransportInfo)
	{
		startPage();
        List<NcTransportInfo> list = ncTransportInfoService.selectNcTransportInfoList(ncTransportInfo);
		return getDataTable(list);
	}
	
	
	/**
	 * 导出菜房出库列表
	 */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(NcTransportInfo ncTransportInfo)
    {
    	List<NcTransportInfo> list = ncTransportInfoService.selectNcTransportInfoList(ncTransportInfo);
        ExcelUtil<NcTransportInfo> util = new ExcelUtil<NcTransportInfo>(NcTransportInfo.class);
        return util.exportExcel(list, "运送登记");
    }
	
	/**
	 * 新增菜房出库
	 */
	@GetMapping("/add")
	public String add(ModelMap map)
	{
		map.put("user", ShiroUtils.getSysUser().getLoginName());
	    return prefix + "/add";
	}
	
	/**
	 * 新增保存菜房出库
	 */
	@RequiresPermissions("system:ncTransportInfo:add")
	@Log(title = "菜房出库", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(NcTransportInfo ncTransportInfo)
	{

		return toAjax(ncTransportInfoService.insertNcTransportInfo(ncTransportInfo));
	}

	/**
	 * 修改菜房出库
	 */
	@GetMapping("/edit/{transId}")
	public String edit(@PathVariable("transId") Integer transId, ModelMap mmap)
	{
		NcTransportInfo ncTransportInfo = ncTransportInfoService.selectNcTransportInfoById(transId);
		mmap.put("ncTransportInfo", ncTransportInfo);
	    return prefix + "/edit";
	}
	
	/**
	 * 修改保存菜房出库
	 */
	@RequiresPermissions("system:ncTransportInfo:edit")
	@Log(title = "菜房出库", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(NcTransportInfo ncTransportInfo)
	{		
		return toAjax(ncTransportInfoService.updateNcTransportInfo(ncTransportInfo));
	}
	
	/**
	 * 删除菜房出库
	 */
	@RequiresPermissions("system:ncTransportInfo:remove")
	@Log(title = "菜房出库", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public AjaxResult remove(String ids)
	{		
		return toAjax(ncTransportInfoService.deleteNcTransportInfoByIds(ids));
	}
	
}
