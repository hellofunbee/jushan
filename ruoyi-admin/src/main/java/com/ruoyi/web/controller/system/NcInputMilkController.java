package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.NcUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.NcInputMilk;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.service.INcInputMilkService;
import com.ruoyi.system.service.ISysUserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 牛奶入库 信息操作处理
 *
 * @author ruoyi
 * @date 2019-09-11
 */
@Controller
@RequestMapping("/system/ncInputMilk")
public class NcInputMilkController extends BaseController
{
    private String prefix = "system/ncInputMilk";

	@Autowired
	private INcInputMilkService ncInputMilkService;

	@Autowired
	private ISysUserService userService;

	@RequiresPermissions("system:ncInputMilk:view")
	@GetMapping()
	public String ncInputMilk()
	{
	    return prefix + "/ncInputMilk";
	}

	/**
	 * 查询牛奶入库列表
	 */
	@RequiresPermissions("system:ncInputMilk:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(NcInputMilk ncInputMilk)
	{
		startPage();
        List<NcInputMilk> list = ncInputMilkService.selectNcInputMilkList(ncInputMilk);
		return getDataTable(list);
	}


	/**
	 * 导出牛奶入库列表
	 */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(NcInputMilk ncInputMilk)
    {
    	List<NcInputMilk> list = ncInputMilkService.selectNcInputMilkList(ncInputMilk);
        ExcelUtil<NcInputMilk> util = new ExcelUtil<NcInputMilk>(NcInputMilk.class);
        return util.exportExcel(list, "牛舍入库单");
    }

	/**
	 * 新增牛奶入库
	 */
	@GetMapping("/add")
	public String add(ModelMap mmap)
	{
		List<SysUser> sysUsers = userService.selectUserList(new SysUser());
		mmap.put("users", sysUsers);
	    return prefix + "/add";
	}

	/**
	 * 新增保存牛奶入库
	 */
	@RequiresPermissions("system:ncInputMilk:add")
	@Log(title = "牛奶入库", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(NcInputMilk ncInputMilk)
	{
		ncInputMilk.setInputType(1);
		ncInputMilk.setInputCode(NcUtils.getMilkCode());
		return toAjax(ncInputMilkService.insertNcInputMilk(ncInputMilk));
	}

	/**
	 * 修改牛奶入库
	 */
	@GetMapping("/edit/{inputId}")
	public String edit(@PathVariable("inputId") Integer inputId, ModelMap mmap)
	{
		NcInputMilk ncInputMilk = ncInputMilkService.selectNcInputMilkById(inputId);
		mmap.put("ncInputMilk", ncInputMilk);

		//挤奶人
		List<SysUser> sysUsers = userService.selectUserList(new SysUser());
		if (ncInputMilk !=null && StringUtils.isNotNull(ncInputMilk)) {
			for (SysUser sysUser : sysUsers) {
				if((""+sysUser.getUserId()) .equals(ncInputMilk.getMilkman())){
					sysUser.setFlag(true);
					break;
				}
			}
		}
		mmap.put("milkman",sysUsers);

		//消毒人
		List<SysUser> sysUserss = userService.selectUserList(new SysUser());
		if (ncInputMilk !=null && StringUtils.isNotNull(ncInputMilk)) {
			for (SysUser sysUser : sysUserss) {
				if((""+sysUser.getUserId()) .equals(ncInputMilk.getDisinfector())){
					sysUser.setFlag(true);
					break;
				}
			}
		}
		mmap.put("disinfector",sysUserss);

		//入库经手人
		List<SysUser> sysUsersss = userService.selectUserList(new SysUser());
		if (ncInputMilk !=null && StringUtils.isNotNull(ncInputMilk)) {
			for (SysUser sysUser : sysUsersss) {
				if((""+sysUser.getUserId()) .equals(ncInputMilk.getInputUser())){
					sysUser.setFlag(true);
					break;
				}
			}
		}
		mmap.put("inputUser",sysUsersss);

		return prefix + "/edit";
	}

	/**
	 * 修改保存牛奶入库
	 */
	@RequiresPermissions("system:ncInputMilk:edit")
	@Log(title = "牛奶入库", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(NcInputMilk ncInputMilk)
	{
		return toAjax(ncInputMilkService.updateNcInputMilk(ncInputMilk));
	}

	/**
	 * 删除牛奶入库
	 */
	@RequiresPermissions("system:ncInputMilk:remove")
	@Log(title = "牛奶入库", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public AjaxResult remove(String ids)
	{
		return toAjax(ncInputMilkService.deleteNcInputMilkByIds(ids));
	}

	/**
	 * 确定生成入库单
	 */
	@Log(title = "确认生成入库单", businessType = BusinessType.OTHER)
	@PostMapping("/confirmTable")
	@ResponseBody
	public AjaxResult confirmTable(String ids){
		String[] split = ids.split(",");
		System.out.println(split);
		for (String s : split) {
			NcInputMilk ncInputMilk = ncInputMilkService.selectNcInputMilkById(Integer.parseInt(s));
			ncInputMilk.setLeftAmout(ncInputMilk.getInputAmout());
			ncInputMilkService.updateNcInputMilk(ncInputMilk);
		}

		SysUser user = ShiroUtils.getSysUser();
		return toAjax(ncInputMilkService.createOrderByIds(ids,user));
	}

}
