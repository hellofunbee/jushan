package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.NcGoodsLog;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.service.INcGoodsLogService;
import com.ruoyi.system.service.INcMeterialService;
import com.ruoyi.system.service.ISysUserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 物资仓储出入记录 信息操作处理
 *
 * @author ruoyi
 * @date 2019-08-06
 */
@Controller
@RequestMapping("/system/ncGoodsLog")
public class NcGoodsLogController extends BaseController
{
    private String prefix = "system/ncGoodsLog";

	@Autowired
	private INcGoodsLogService ncGoodsLogService;

	@Autowired
	private INcMeterialService ncMeterialService;

	@Autowired
	private ISysUserService iSysUserService;

	@RequiresPermissions("system:ncGoodsLog:view")
	@GetMapping()
	public String ncGoodsLog()
	{
	    return prefix + "/ncGoodsLog";
	}

	/**
	 * 查询物资仓储出入记录列表
	 */
	@RequiresPermissions("system:ncGoodsLog:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(NcGoodsLog ncGoodsLog)
	{
		startPage();
        List<NcGoodsLog> list = ncGoodsLogService.selectNcGoodsLogList(ncGoodsLog);
		return getDataTable(list);
	}


	/**
	 * 导出物资仓储出入记录列表
	 */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(NcGoodsLog ncGoodsLog)
    {
    	List<NcGoodsLog> list = ncGoodsLogService.selectNcGoodsLogList(ncGoodsLog);
        ExcelUtil<NcGoodsLog> util = new ExcelUtil<NcGoodsLog>(NcGoodsLog.class);
        return util.exportExcel(list, "出入库记录单");
    }

	/**
	 * 新增物资仓储出入记录
	 */
	@GetMapping("/add")
	public String add()
	{
	    return prefix + "/add";
	}

	/**
	 * 新增保存物资仓储出入记录
	 */
	@RequiresPermissions("system:ncGoodsLog:add")
	@Log(title = "物资仓储出入记录", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(NcGoodsLog ncGoodsLog)
	{
		return toAjax(ncGoodsLogService.insertNcGoodsLog(ncGoodsLog));
	}

	/**
	 * 修改物资仓储出入记录
	 */
	@GetMapping("/edit/{goodsLogId}")
	public String edit(@PathVariable("goodsLogId") Long goodsLogId, ModelMap mmap)
	{
		NcGoodsLog ncGoodsLog = ncGoodsLogService.selectNcGoodsLogById(goodsLogId);
		mmap.put("ncGoodsLog", ncGoodsLog);
		//采购人
		List<SysUser> sysUsers = iSysUserService.selectUserList(new SysUser());
		if(ncGoodsLog!=null && StringUtils.isNotNull(ncGoodsLog.getPurchaser())){
			for (SysUser sysUser : sysUsers) {
				if((""+sysUser.getUserId()) .equals(ncGoodsLog.getPurchaser())){
					sysUser.setFlag(true);
					break;
				}
			}

		}
		mmap.put("purchaser",sysUsers);
		//入库人
		List<SysUser> sysUseres = iSysUserService.selectUserList(new SysUser());
		if(ncGoodsLog!=null && StringUtils.isNotNull(ncGoodsLog.getCreateBy())){
			for (SysUser sysUsere : sysUseres) {
				if((""+sysUsere.getUserId()) .equals(ncGoodsLog.getCreateBy())){
					sysUsere.setFlag(true);
					break;
				}
			}
		}
		mmap.put("createBy",sysUseres);


		//领用人
		List<SysUser> sysUseress = iSysUserService.selectUserList(new SysUser());
		if(ncGoodsLog!=null && StringUtils.isNotNull(ncGoodsLog.getReceiver())){
			for (SysUser useress : sysUseress) {
				if((""+useress.getUserId()) .equals(ncGoodsLog.getReceiver())){
					useress.setFlag(true);
					break;
				}
			}
		}
		mmap.put("receiver",sysUseress);


	    return prefix + "/edit";
	}

	/**
	 * 修改保存物资仓储出入记录
	 */
	@RequiresPermissions("system:ncGoodsLog:edit")
	@Log(title = "物资仓储出入记录", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(NcGoodsLog ncGoodsLog)
	{
/*		List<NcMeterial> ncMeterials = ncMeterialService.selectMeterialIdByName(ncGoodsLog.getMeterialName());
		for (NcMeterial ncMeterial : ncMeterials) {
			ncGoodsLog.setMeterialId(ncMeterial.getMeterialId());
		}*/

		return toAjax(ncGoodsLogService.updateNcGoodsLog(ncGoodsLog));
	}

	/**
	 * 删除物资仓储出入记录
	 */
	@RequiresPermissions("system:ncGoodsLog:remove")
	@Log(title = "物资仓储出入记录", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public AjaxResult remove(String ids)
	{
		return toAjax(ncGoodsLogService.deleteNcGoodsLogByIds(ids));
	}

}
