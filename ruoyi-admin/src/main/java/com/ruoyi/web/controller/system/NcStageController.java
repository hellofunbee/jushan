package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.NcStage;
import com.ruoyi.system.domain.NcStandard;
import com.ruoyi.system.service.INcStageService;
import com.ruoyi.system.service.INcStandardService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 生产（育苗）阶段 信息操作处理
 *
 * @author ruoyi
 * @date 2019-07-28
 */
@Controller
@RequestMapping("/system/ncStage")
public class NcStageController extends BaseController {
    private String prefix = "system/ncStage";

    @Autowired
    private INcStageService ncStageService;
    @Autowired
    private INcStandardService standardService;

    @RequiresPermissions("system:ncStage:view")
    @GetMapping()
    public String ncStage() {
        return prefix + "/ncStage";
    }

    /**
     * 查询生产（育苗）阶段列表
     */
    @RequiresPermissions("system:ncStage:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(NcStage ncStage) {
        startPage();
        List<NcStage> list = ncStageService.selectNcStageList(ncStage);
        return getDataTable(list);
    }


    /**
     * 导出生产（育苗）阶段列表
     */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(NcStage ncStage) {
        List<NcStage> list = ncStageService.selectNcStageList(ncStage);
        ExcelUtil<NcStage> util = new ExcelUtil<NcStage>(NcStage.class);
        return util.exportExcel(list, "ncStage");
    }

    /**
     * 新增生产（育苗）阶段
     */
    @GetMapping("/add")
    public String add(Long standardId, ModelMap mmap) {
        NcStandard standard = standardService.selectNcStandardById(standardId);
        mmap.put("ncStandard", standard);
        return prefix + "/add";
    }

    /**
     * 新增保存生产（育苗）阶段
     */
    @RequiresPermissions("system:ncStage:add")
    @Log(title = "生产（育苗）阶段", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(NcStage ncStage) {
        return toAjax(ncStageService.insertNcStage(ncStage));
    }

    /**
     * 修改生产（育苗）阶段
     */
    @GetMapping("/edit/{stageId}")
    public String edit(@PathVariable("stageId") Long stageId, ModelMap mmap) {
        NcStage ncStage = ncStageService.selectNcStageById(stageId);
        mmap.put("ncStage", ncStage);
        return prefix + "/edit";
    }

    /**
     * 修改保存生产（育苗）阶段
     */
    @RequiresPermissions("system:ncStage:edit")
    @Log(title = "生产（育苗）阶段", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(NcStage ncStage) {
        return toAjax(ncStageService.updateNcStage(ncStage));
    }

    /**
     * 删除生产（育苗）阶段
     */
    @RequiresPermissions("system:ncStage:remove")
    @Log(title = "生产（育苗）阶段", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(ncStageService.deleteNcStageByIds(ids));
    }

}
