package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.NcUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.NcFeed;
import com.ruoyi.system.domain.NcFeedLog;
import com.ruoyi.system.domain.NcMeterial;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.service.INcFeedLogService;
import com.ruoyi.system.service.INcFeedService;
import com.ruoyi.system.service.INcMeterialService;
import com.ruoyi.system.service.ISysUserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * 饲料库存 信息操作处理
 *
 * @author ruoyi
 * @date 2019-09-10
 */
@Controller
@RequestMapping("/system/ncFeed2")
public class NcFeed2Controller extends BaseController {
    private String prefix = "system/ncFeed2";

	@Autowired
	private INcFeedService ncFeedService;

	@Autowired
	private ISysUserService iSysUserService;

	@Autowired
	private INcFeedLogService ncFeedLogService;

	@Autowired
	private INcMeterialService ncMeterialService;

	@Autowired
	private ISysUserService  userService;

	@RequiresPermissions("system:ncFeed:view")
	@GetMapping()
	public String ncFeed()
	{
	    return prefix + "/ncFeed";
	}

	@GetMapping("/chicken")
	public String ncChicken()
	{

		return prefix + "/front/ncFeed";
	}

	/**
	 * 查询饲料库存列表
	 */
	@RequiresPermissions("system:ncFeed:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(NcFeed ncFeed)
	{
		startPage();
        List<NcFeed> list = ncFeedService.selectNcFeedList(ncFeed);
		return getDataTable(list);
	}


	/**
	 * 导出饲料库存列表
	 */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(NcFeed ncFeed)
    {
		ncFeed.setFeedType(2);
		ncFeed.setManagerType(2);
    	List<NcFeed> list = ncFeedService.selectNcFeedList(ncFeed);
//        ExcelUtil<NcFeed> util = new ExcelUtil<NcFeed>(NcFeed.class);
		ExcelUtil<NcFeed> util = new ExcelUtil<>(NcFeed.class);
		return util.exportExcel(list, "辅饲料单");
    }

	/**
	 * 新增饲料库存
	 */
	@GetMapping("/add")
	public String add(ModelMap mmap)
	{
		List<SysUser> sysUsers = userService.selectUserList(new SysUser());
		mmap.put("users", sysUsers);
	    return prefix + "/add";
	}

	/**
	 * 新增保存饲料库存
	 */
	@RequiresPermissions("system:ncFeed:add")
	@Log(title = "饲料库存", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(NcFeed ncFeed)
	{
		ncFeed.setFeedCode(NcUtils.getFeedCode());//设置饲料批次
		ncFeed.setManagerType(2); ncFeed.setFeedType(2);   //设置俩个状态码
		ncFeed.setFeedName(ncFeed.getMeterialName());
		ncFeed.setStockAmout(ncFeed.getInputAmount());
		return toAjax(ncFeedService.insertNcFeed(ncFeed));
	}

	/**
	 * 修改饲料库存
	 */
	@RequiresPermissions("system:ncFeed:edit")
	@GetMapping("/edit/{feedId}")
	public String edit(@PathVariable("feedId") Long feedId, ModelMap mmap)
	{
		NcFeed ncFeed = ncFeedService.selectNcFeedById(feedId);
		mmap.put("ncFeed", ncFeed);

		//采购人
		List<SysUser> sysUsers = userService.selectUserList(new SysUser());
		if (ncFeed !=null && StringUtils.isNotNull(ncFeed)) {
			for (SysUser sysUser : sysUsers) {
				if((""+sysUser.getUserId()) .equals(ncFeed.getPurchaser())){
					sysUser.setFlag(true);
					break;
				}
			}
		}
		mmap.put("purchaser",sysUsers);

		//经手人
		List<SysUser> sysUserss = userService.selectUserList(new SysUser());
		if (ncFeed !=null && StringUtils.isNotNull(ncFeed)) {
			for (SysUser sysUser : sysUserss) {
				if((""+sysUser.getUserId()) .equals(ncFeed.getCreateBy())){
					sysUser.setFlag(true);
					break;
				}
			}
		}
		mmap.put("createBy",sysUserss);

	    return prefix + "/edit";
	}

	/**
	 * 修改保存饲料库存
	 */
	@RequiresPermissions("system:ncFeed:edit")
	@Log(title = "饲料库存", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(NcFeed ncFeed)
	{
		List<NcMeterial> ncMeterials = ncMeterialService.selectMeterialIdByName(ncFeed.getMeterialName());
		for (NcMeterial ncMeterial : ncMeterials) {
			ncFeed.setMeterialId(ncMeterial.getMeterialId());
			//通过id找到name赋值给feedName
			NcMeterial meterial = ncMeterialService.selectMeterialById(ncMeterial.getMeterialId());
			ncFeed.setFeedName(meterial.getMeterialName());
		}
		return toAjax(ncFeedService.updateNcFeed(ncFeed));
	}

	/**
	 * 删除饲料库存
	 */
	@RequiresPermissions("system:ncFeed:remove")
	@Log(title = "饲料库存", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public AjaxResult remove(String ids)
	{
		return toAjax(ncFeedService.deleteNcFeedByIds(ids));
	}


	/**
	 * 出库操作
	 */
	@RequiresPermissions("system:ncFeed:depot")
	@Log(title = "出库", businessType = BusinessType.INSERT)
	@GetMapping("/depot/{feedId}")
	public String depot(@PathVariable("feedId")Long feedId , ModelMap mmap){
		mmap.put("depot",ncFeedService.selectNcFeedById(feedId));
		mmap.put("users",iSysUserService.selectUserList(new SysUser()));
		return prefix + "/depot";
	}

	/**
	 * 出库操作提交
	 */
	@RequiresPermissions("system:ncFeed:depot")
	@Log(title = "出库提交", businessType = BusinessType.INSERT)
	@PostMapping("/depot")
	@ResponseBody
	public AjaxResult addSave(NcFeedLog ncFeedLog){
		NcFeed ncFeed = ncFeedService.selectNcFeedById(ncFeedLog.getFeedId());
		if (ncFeed.getStockAmout()!=null) {
			if (ncFeed.getStockAmout().compareTo(ncFeedLog.getOutputAmount())>-1) {
				ncFeed.setStockAmout(ncFeed.getStockAmout().subtract(ncFeedLog.getOutputAmount()));
				ncFeedService.updateNcFeed(ncFeed);

				return toAjax(ncFeedLogService.insertNcFeedLog(ncFeedLog));
			}
			return AjaxResult.error("库存不足");
		}
		return AjaxResult.error("库存为空");
	}

	@GetMapping("ncFeedCows2")
	public String ncFeedCows2()
	{
		return prefix + "/ncFeedCows2";
	}
}
