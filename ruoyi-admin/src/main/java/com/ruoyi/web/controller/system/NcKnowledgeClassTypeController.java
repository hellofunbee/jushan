package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.Ztree;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.NcKnowledgeClassType;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.service.INcKnowledgeClassTypeService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 知识课堂类别 信息操作处理
 * 
 * @author ruoyi
 * @date 2019-09-10
 */
@Controller
@RequestMapping("/system/ncKnowledgeClassType")
public class NcKnowledgeClassTypeController extends BaseController
{
    private String prefix = "system/ncKnowledgeClassType";
	
	@Autowired
	private INcKnowledgeClassTypeService ncKnowledgeClassTypeService;
	
	@RequiresPermissions("system:ncKnowledgeClassType:view")
	@GetMapping()
	public String ncKnowledgeClassType()
	{
	    return prefix + "/ncKnowledgeClassType";
	}
	
	/**
	 * 查询知识课堂类别列表
	 */
	@RequiresPermissions("system:ncKnowledgeClassType:list")
	@PostMapping("/list")
	@ResponseBody
	public List<NcKnowledgeClassType> list(NcKnowledgeClassType ncKnowledgeClassType)
	{
        List<NcKnowledgeClassType> list = ncKnowledgeClassTypeService.selectNcKnowledgeClassTypeList(ncKnowledgeClassType);
		return list;
	}
	
	
	/**
	 * 导出知识课堂类别列表
	 */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(NcKnowledgeClassType ncKnowledgeClassType)
    {
    	List<NcKnowledgeClassType> list = ncKnowledgeClassTypeService.selectNcKnowledgeClassTypeList(ncKnowledgeClassType);
        ExcelUtil<NcKnowledgeClassType> util = new ExcelUtil<NcKnowledgeClassType>(NcKnowledgeClassType.class);
        return util.exportExcel(list, "ncKnowledgeClassType");
    }
	/**
	 * 新增知识课堂类别
	 */
	@GetMapping("/add/{parentId}")
	public String add(@PathVariable("parentId") Long parentId, ModelMap mmap)
	{

		NcKnowledgeClassType ncKnowledgeClassType = ncKnowledgeClassTypeService.selectNcKnowledgeClassTypeById(parentId);
		mmap.put("kc", ncKnowledgeClassType);
		if (ncKnowledgeClassType == null){
			NcKnowledgeClassType ncKnowledgeClassType1 = new NcKnowledgeClassType();
			ncKnowledgeClassType1.setKctypeId(244L);
			mmap.put("kc", ncKnowledgeClassType1);
		}
	    return prefix + "/add";
	}
	
	/**
	 * 新增保存知识课堂类别
	 */
	@RequiresPermissions("system:ncKnowledgeClassType:add")
	@Log(title = "知识课堂类别", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(NcKnowledgeClassType ncKnowledgeClassType)
	{
		SysUser sysUser = ShiroUtils.getSysUser();
		return toAjax(ncKnowledgeClassTypeService.insertNcKnowledgeClassType(ncKnowledgeClassType,sysUser));
	}

	/**
	 * 修改知识课堂类别
	 */
	@GetMapping("/edit/{kctypeId}")
	public String edit(@PathVariable("kctypeId") Long kctypeId, ModelMap mmap)
	{
		NcKnowledgeClassType ncKnowledgeClassType = ncKnowledgeClassTypeService.selectNcKnowledgeClassTypeById(kctypeId);


		if (ncKnowledgeClassType.getParentId() != 0){
			NcKnowledgeClassType ncKnowledgeClassTypeParent = ncKnowledgeClassTypeService.selectNcKnowledgeClassTypeById(ncKnowledgeClassType.getParentId());
			ncKnowledgeClassType.setParentName(ncKnowledgeClassTypeParent.getKcTypeName());
		}
		mmap.put("ncKnowledgeClassType", ncKnowledgeClassType);
	    return prefix + "/edit";
	}
	
	/**
	 * 修改保存知识课堂类别
	 */
	@RequiresPermissions("system:ncKnowledgeClassType:edit")
	@Log(title = "知识课堂类别", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(NcKnowledgeClassType ncKnowledgeClassType)
	{
		SysUser sysUser = ShiroUtils.getSysUser();
		return toAjax(ncKnowledgeClassTypeService.updateNcKnowledgeClassType(ncKnowledgeClassType,sysUser));
	}
	
	/**
	 * 删除知识课堂类别
	 */
	@RequiresPermissions("system:ncKnowledgeClassType:remove")
	@Log(title = "知识课堂类别", businessType = BusinessType.DELETE)
	@GetMapping( "/remove/{id}")
	@ResponseBody
	public AjaxResult remove(@PathVariable("id") Long kctypeId)
	{
		if (ncKnowledgeClassTypeService.selectKCTypeClassCount(kctypeId) > 0)
		{
			return AjaxResult.warn("存在下级知识课堂,不允许删除");
		}
	/*	if (ncKnowledgeClassTypeService.checkKCClassExistKnowClass(kctypeId))
		{
			return AjaxResult.warn("知识课堂存在用户,不允许删除");
		}*/
//		return AjaxResult.success("1111");	}
		return toAjax(ncKnowledgeClassTypeService.deleteNcKnowledgeClassTypeByIds(kctypeId));	}


	/**
	 * 选择知识课堂
	 */
	@GetMapping("/selectKCTypeTree/{kctypeId}")
	public String selectKCTypeTree(@PathVariable("kctypeId") Long kctypeId, ModelMap mmap)
	{
		mmap.put("kc", ncKnowledgeClassTypeService.selectNcKnowledgeClassTypeById(kctypeId));
		return prefix + "/tree";
	}


	/**
	 * 加载知识课堂列表树
	 */
	@GetMapping("/treeData")
	@ResponseBody
	public List<Ztree> treeData()
	{
		List<Ztree> ztrees = ncKnowledgeClassTypeService.selectKCTypeClassTree(new NcKnowledgeClassType());
		return ztrees;
	}
}
