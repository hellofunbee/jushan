package com.ruoyi.web.controller.system;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.system.domain.NcOuputCf;
import com.ruoyi.system.domain.NcOutputChicken;
import com.ruoyi.system.domain.NcOutputMilk;
import com.ruoyi.system.service.INcOuputCfService;
import com.ruoyi.system.service.INcOutputChickenService;
import com.ruoyi.system.service.INcOutputMilkService;
import com.sun.scenario.effect.impl.prism.PrDrawable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Description: 内部信息页面
 * @Author: csk
 * @Date: 2019/9/18 13:39
 **/
@Controller
@RequestMapping("/front/innerInfo")
public class FrontInnerInfoController extends BaseController {
    String prefix = "system/front_innerInfo";

    @Autowired
    private INcOutputMilkService ncOutputMilkService;
    @Autowired
    private INcOutputChickenService ncOutputChickenService;
    @Autowired
    private INcOuputCfService ncOuputCfService;

    @RequestMapping("/info")
    public String info(String beginTime, String endTime, ModelMap mmap,String dataType,String output){
        ncOutputMilkService.innoInfo(beginTime,endTime,mmap,dataType,output);
        ncOutputChickenService.innoInfo(beginTime,endTime,mmap,dataType,output);
        ncOuputCfService.innoInfo(beginTime,endTime,mmap,dataType,output);
        mmap.put("startTime",beginTime);
        mmap.put("endTime",endTime);
        return prefix+"/innerInfo";
    }
    /**
     * @param dataSpan 时间跨度 1:年 2：月 3：日
     * @return
     */
    @PostMapping("/infoQuery")
    @ResponseBody
    public TableDataInfo chickenFeedQuery( String beginTime, String endTime,String outputType,Integer dataSpan) {

        List<Map> chickenList = ncOutputChickenService.selectInfoByTime(dataSpan, beginTime, endTime,outputType);
        List<Map> milkList =ncOutputMilkService.selectInfoByTime(dataSpan, beginTime, endTime,outputType);
        List<Map> cfList =ncOuputCfService.selectInfoByTime(dataSpan, beginTime, endTime,outputType);
        List list=new ArrayList();
        list.add(chickenList);
        list.add(milkList);
        list.add(cfList);
        return getDataTable(list);
    }

    @RequestMapping("/infoPage")
    public String infoPage(String type, ModelMap mmap,String outputType){
        mmap.put("outputType","all".equals(outputType) ? "" : outputType);
        if("chk".equals(type)){
            return prefix+"/chkInfo";
        }else if("milk".equals(type)){
            return prefix+"/milkInfo";
        }else if("cf".equals(type)){
            return prefix+"/cfInfo";
        }else{}
        return prefix+"/innerInfo";
    }

    @RequestMapping("/infoTotal")
    @ResponseBody
    public TableDataInfo infoTotal(String type, Integer outputType ){
        if("chk".equals(type)){
            startPage();
            NcOutputChicken ncOutputChicken = new NcOutputChicken();
            ncOutputChicken.setOutputType(outputType);
            List<NcOutputChicken> list = ncOutputChickenService.selectNcOutputChickenList(ncOutputChicken);
            return getDataTable(list);
        }else if("milk".equals(type)){
            startPage();
            List<NcOutputMilk> list = ncOutputMilkService.selectNcoutputMilkOrderInfoByOutputType(outputType);
            return getDataTable(list);
        }else if("cf".equals(type)){
            startPage();
            NcOuputCf ncOuputCf = new NcOuputCf();
            ncOuputCf.setOutputType(outputType);
            List<NcOuputCf> list = ncOuputCfService.selectNcOuputCfList(ncOuputCf);
            return getDataTable(list);
        }else{}
        return  null;
    }









}
