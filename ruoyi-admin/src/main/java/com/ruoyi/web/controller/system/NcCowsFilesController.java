package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.NcCowsFiles;
import com.ruoyi.system.service.INcCowsFilesService;
import com.ruoyi.system.util.ExcelUtil2;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 牛档案 信息操作处理
 *
 * @author ruoyi
 * @date 2019-09-02
 */
@Controller
@RequestMapping("/system/ncCowsFiles")
public class NcCowsFilesController extends BaseController
{
    private String prefix = "system/ncCowsFiles";
    private String frontPrefix = "system/ncCowsFiles/front";

	@Autowired
	private INcCowsFilesService ncCowsFilesService;

	@RequiresPermissions("system:ncCowsFiles:view")
	@GetMapping()
	public String ncCowsFiles()
	{
	    return prefix + "/ncCowsFiles";
	}

	@GetMapping("/front")
	public String frontNcCowsFiles()
	{
		return frontPrefix + "/ncCowsFiles";
	}

	/**
	 * 查询牛档案列表
	 */
	@RequiresPermissions("system:ncCowsFiles:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(NcCowsFiles ncCowsFiles)
	{
		startPage();
        List<NcCowsFiles> list = ncCowsFilesService.selectNcCowsFilesList(ncCowsFiles);
		return getDataTable(list);
	}
	/**
	 * 前端查询牛档案列表
	 */
	@PostMapping("/frontList")
	@ResponseBody
	public TableDataInfo frontList(NcCowsFiles ncCowsFiles)
	{
		startPage();
		List<NcCowsFiles> list = ncCowsFilesService.selectNcCowsFilesList(ncCowsFiles);
		return getDataTable(list);
	}


	/**
	 * 导出牛档案列表
	 */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(NcCowsFiles ncCowsFiles)
    {
    	List<NcCowsFiles> list = ncCowsFilesService.selectNcCowsFilesList(ncCowsFiles);
//        ExcelUtil<NcCowsFiles> util = new ExcelUtil<NcCowsFiles>(NcCowsFiles.class);
		ExcelUtil2<NcCowsFiles> util = new ExcelUtil2<>(NcCowsFiles.class);
		return util.exportExcel(list, "牛档案单");

    }

	/**
	 * 新增牛档案
	 */
	@GetMapping("/add")
	public String add()
	{
	    return prefix + "/add";
	}

	/**
	 * 新增保存牛档案
	 */
	@RequiresPermissions("system:ncCowsFiles:add")
	@Log(title = "牛档案", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(NcCowsFiles ncCowsFiles)
	{
		try {
			//耳号不能重复
			Integer one = ncCowsFilesService.selectCowCodeOne(ncCowsFiles.getCowCode());
			if (one < 1) {
				if (Integer.parseInt(ncCowsFiles.getCowCode()) >=0) {
					ncCowsFiles.setUpdateTime(new Date());
					return toAjax(ncCowsFilesService.insertNcCowsFiles(ncCowsFiles));
				}else {
					return AjaxResult.error("不能有负数");
				}
			}
			else {
				return AjaxResult.error("耳号重复");
			}


		}catch (Exception e){
			  return AjaxResult.error("耳号应为整数");
		}
	}

	/**
	 * 修改牛档案
	 */
	@GetMapping("/edit/{cowId}")
	public String edit(@PathVariable("cowId") Integer cowId, ModelMap mmap)
	{
		NcCowsFiles ncCowsFiles = ncCowsFilesService.selectNcCowsFilesById(cowId);
		mmap.put("ncCowsFiles", ncCowsFiles);
	    return prefix + "/edit";
	}

	/**
	 * 修改保存牛档案
	 */
	@RequiresPermissions("system:ncCowsFiles:edit")
	@Log(title = "牛档案", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(NcCowsFiles ncCowsFiles)
	{
		try {
		if (Integer.parseInt(ncCowsFiles.getCowCode()) >=0 && Integer.parseInt(ncCowsFiles.getChilds()) >=0) {

			ncCowsFiles.setUpdateTime(new Date());
			return toAjax(ncCowsFilesService.updateNcCowsFiles(ncCowsFiles));
		}else {
			return AjaxResult.error("不能有负数");
		}
		}catch (Exception e){
			return AjaxResult.error("只能为整数");
		}

	}

	/**
	 * 删除牛档案
	 */
	@RequiresPermissions("system:ncCowsFiles:remove")
	@Log(title = "牛档案", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public AjaxResult remove(String ids)
	{
		return toAjax(ncCowsFilesService.deleteNcCowsFilesByIds(ids));
	}


	/**
	 * 产犊信息
	 */
	@GetMapping("/birth/{cowId}")
	public String config(@PathVariable("cowId") Integer cowId, ModelMap mmap) {
		NcCowsFiles ncCowsFiles = ncCowsFilesService.selectNcCowsFilesById(cowId);

		//获取当前系统时间 放入cowAge
		SimpleDateFormat df = new SimpleDateFormat("yyyy");//设置日期格式
		String newdate = df.format(new Date());
		ncCowsFiles.setCowAge(newdate);
		System.out.println("cowId:  "+ncCowsFiles.getCowId());
		mmap.put("ncCowsFiles", ncCowsFiles);
		return "system/ncCowsBirth/ncCowsBirth";
	}






}
