package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.NcChickenMonthCensus;
import com.ruoyi.system.service.INcChickenMonthCensusService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 鸡舍月报 信息操作处理
 * 
 * @author ruoyi
 * @date 2019-09-19
 */
@Controller
@RequestMapping("/system/ncChickenMonthCensus")
public class NcChickenMonthCensusController extends BaseController
{
    private String prefix = "system/ncChickenMonthCensus";
	
	@Autowired
	private INcChickenMonthCensusService ncChickenMonthCensusService;
	
	@RequiresPermissions("system:ncChickenMonthCensus:view")
	@GetMapping()
	public String ncChickenMonthCensus()
	{
	    return prefix + "/ncChickenMonthCensus";
	}
	
	/**
	 * 查询鸡舍月报列表
	 */
	@RequiresPermissions("system:ncChickenMonthCensus:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(NcChickenMonthCensus ncChickenMonthCensus)
	{
		startPage();
        List<NcChickenMonthCensus> list = ncChickenMonthCensusService.selectNcChickenMonthCensusList(ncChickenMonthCensus);
		return getDataTable(list);
	}
	
	
	/**
	 * 导出鸡舍月报列表
	 */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(NcChickenMonthCensus ncChickenMonthCensus)
    {
    	List<NcChickenMonthCensus> list = ncChickenMonthCensusService.selectNcChickenMonthCensusList(ncChickenMonthCensus);
        ExcelUtil<NcChickenMonthCensus> util = new ExcelUtil<NcChickenMonthCensus>(NcChickenMonthCensus.class);
        return util.exportExcel(list, "鸡舍月报表");
    }
	
	/**
	 * 新增鸡舍月报
	 */
	@GetMapping("/add")
	public String add()
	{
	    return prefix + "/add";
	}
	
	/**
	 * 新增保存鸡舍月报
	 */
	@RequiresPermissions("system:ncChickenMonthCensus:add")
	@Log(title = "鸡舍月报", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(NcChickenMonthCensus ncChickenMonthCensus)
	{		
		return toAjax(ncChickenMonthCensusService.insertNcChickenMonthCensus(ncChickenMonthCensus));
	}

	/**
	 * 修改鸡舍月报
	 */
	@GetMapping("/edit/{cenusId}")
	public String edit(@PathVariable("cenusId") Long cenusId, ModelMap mmap)
	{
		NcChickenMonthCensus ncChickenMonthCensus = ncChickenMonthCensusService.selectNcChickenMonthCensusById(cenusId);
		mmap.put("ncChickenMonthCensus", ncChickenMonthCensus);
	    return prefix + "/edit";
	}
	
	/**
	 * 修改保存鸡舍月报
	 */
	@RequiresPermissions("system:ncChickenMonthCensus:edit")
	@Log(title = "鸡舍月报", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(NcChickenMonthCensus ncChickenMonthCensus)
	{		
		return toAjax(ncChickenMonthCensusService.updateNcChickenMonthCensus(ncChickenMonthCensus));
	}
	
	/**
	 * 删除鸡舍月报
	 */
	@RequiresPermissions("system:ncChickenMonthCensus:remove")
	@Log(title = "鸡舍月报", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public AjaxResult remove(String ids)
	{		
		return toAjax(ncChickenMonthCensusService.deleteNcChickenMonthCensusByIds(ids));
	}
	
}
