package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.NcCheck;
import com.ruoyi.system.domain.NcInputCf;
import com.ruoyi.system.domain.NcOrderCf;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.service.INcCheckService;
import com.ruoyi.system.service.INcInputCfService;
import com.ruoyi.system.service.INcOrderCfService;
import com.ruoyi.system.util.ExcelUtil2;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * 菜房入库 信息操作处理
 *
 * @author ruoyi
 * @date 2019-08-30
 */
@Controller
@RequestMapping("/system/ncInputCf")
public class NcInputCfController extends BaseController {
    private String prefix = "system/ncInputCf";

    @Autowired
    private INcInputCfService ncInputCfService;
    @Autowired
    private INcCheckService checkService;

    @Autowired
    private INcOrderCfService orderCfService;

    @RequiresPermissions("system:ncInputCf:view")
    @GetMapping()
    public String ncInputCf(Integer type, ModelMap mmap) {
        mmap.put("type", type);
        return prefix + "/ncInputCf";
    }

    @GetMapping("ncInputCf-stock")
    public String ncStockCf(Integer type, ModelMap mmap) {
        mmap.put("type", type);
        return prefix + "/ncInputCf-stock";
    }

    /**
     * 查询菜房入库列表
     */
    @RequiresPermissions("system:ncInputCf:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(NcInputCf ncInputCf) {
        startPage();
        List<NcInputCf> list = ncInputCfService.selectNcInputCfList(ncInputCf);
        return getDataTable(list);
    }


    /**
     * 导出菜房入库列表
     */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(NcInputCf ncInputCf) {
        List<NcInputCf> list = ncInputCfService.selectNcInputCfList(ncInputCf);
        ExcelUtil2<NcInputCf> util = new ExcelUtil2<NcInputCf>(NcInputCf.class);
        return util.exportExcel(list, "菜房入库单");
    }

    /**
     * 新增菜房入库
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存菜房入库
     */
    @RequiresPermissions("system:ncInputCf:add")
    @Log(title = "菜房入库", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(NcInputCf ncInputCf) {
        return toAjax(ncInputCfService.insertNcInputCf(ncInputCf));
    }

    /**
     * 修改菜房入库
     */
    @GetMapping("/edit/{inputId}")
    public String edit(@PathVariable("inputId") Integer inputId, ModelMap mmap) {
        NcInputCf ncInputCf = ncInputCfService.selectNcInputCfById(inputId);
        mmap.put("ncInputCf", ncInputCf);
        return prefix + "/edit";
    }


    /**
     * 修改保存菜房入库
     */
    @RequiresPermissions("system:ncInputCf:edit")
    @Log(title = "菜房入库", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(NcInputCf ncInputCf) {
        return toAjax(ncInputCfService.updateNcInputCf(ncInputCf));
    }

    /**
     * 删除菜房入库
     */
    @RequiresPermissions("system:ncInputCf:remove")
    @Log(title = "菜房入库", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(ncInputCfService.deleteNcInputCfByIds(ids));
    }


    /**
     * 新增保存菜房入库
     */
    @Log(title = "菜房入库", businessType = BusinessType.INSERT)
    @PostMapping("/saveInput")
    @ResponseBody
    public AjaxResult saveInput(NcInputCf ncInputCf) {
        /*更改订单状态 为已入库*/
        NcCheck check = checkService.selectNcCheckById(ncInputCf.getCheckId());
        if (check != null && check.getCforderId() != null) {
            NcOrderCf orderCf = orderCfService.selectNcOrderCfById(check.getCforderId());
            orderCf.setCforderStatus("4");//待出库
            orderCfService.updateNcOrderCf(orderCf);
        }

        ncInputCf.setLeftAmount(ncInputCf.getInputAmout());//库存
        ncInputCf.setCreateTime(new Date());
        ncInputCf.setInputUser(ShiroUtils.getLoginName());
        ncInputCf.setInputTime(new Date());
        ncInputCf.setStatus("1");
        return toAjax(ncInputCfService.insertNcInputCf(ncInputCf));
    }

    /**
     * 查询菜房出库列表
     */
    @PostMapping("/inStocks")
    @ResponseBody
    public TableDataInfo inStocks(String cropVarieties) {
        NcInputCf inputCf = new NcInputCf();
        inputCf.getParams().put("cropVarieties", cropVarieties);//品种
        List<NcInputCf> list = ncInputCfService.selectNcInputCfList(inputCf);
        return getDataTable(list);
    }

    /**
     * 确认生成出库单
     */
    @Log(title = "确认生成出库单", businessType = BusinessType.OTHER)
    @PostMapping("/confirmTable")
    @ResponseBody
    public AjaxResult confirmTable(String ids,Integer caifangType) {
        SysUser user = ShiroUtils.getSysUser();
        return toAjax(ncInputCfService.createInputOrderByIds(ids, user,caifangType));
    }


}
