package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.NcOutPut;
import com.ruoyi.system.service.INcOutPutService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 出库登记 信息操作处理
 * 
 * @author ruoyi
 * @date 2019-09-16
 */
@Controller
@RequestMapping("/system/ncOutPut")
public class NcOutPutController extends BaseController
{
    private String prefix = "system/ncOutPut";
	
	@Autowired
	private INcOutPutService ncOutPutService;
	
	@RequiresPermissions("system:ncOutPut:view")
	@GetMapping()
	public String ncOutPut()
	{
	    return prefix + "/ncOutPut";
	}
	
	/**
	 * 查询出库登记列表
	 */
	@RequiresPermissions("system:ncOutPut:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(NcOutPut ncOutPut)
	{
		startPage();
        List<NcOutPut> list = ncOutPutService.selectNcOutPutList(ncOutPut);
		return getDataTable(list);
	}
	
	
	/**
	 * 导出出库登记列表
	 */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(NcOutPut ncOutPut)
    {
    	List<NcOutPut> list = ncOutPutService.selectNcOutPutList(ncOutPut);
        ExcelUtil<NcOutPut> util = new ExcelUtil<NcOutPut>(NcOutPut.class);
        return util.exportExcel(list, "育苗出库登记");
    }
	
	/**
	 * 新增出库登记
	 */
	@GetMapping("/add")
	public String add()
	{
	    return prefix + "/add";
	}
	
	/**
	 * 新增保存出库登记
	 */
	@RequiresPermissions("system:ncOutPut:add")
	@Log(title = "出库登记", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(NcOutPut ncOutPut)
	{		
		return toAjax(ncOutPutService.insertNcOutPut(ncOutPut));
	}

	/**
	 * 修改出库登记
	 */
	@GetMapping("/edit/{outPutId}")
	public String edit(@PathVariable("outPutId") Long outPutId, ModelMap mmap)
	{
		NcOutPut ncOutPut = ncOutPutService.selectNcOutPutById(outPutId);
		mmap.put("ncOutPut", ncOutPut);
	    return prefix + "/edit";
	}
	
	/**
	 * 修改保存出库登记
	 */
	@RequiresPermissions("system:ncOutPut:edit")
	@Log(title = "出库登记", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(NcOutPut ncOutPut)
	{		
		return toAjax(ncOutPutService.updateNcOutPut(ncOutPut));
	}
	
	/**
	 * 删除出库登记
	 */
	@RequiresPermissions("system:ncOutPut:remove")
	@Log(title = "出库登记", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public AjaxResult remove(String ids)
	{		
		return toAjax(ncOutPutService.deleteNcOutPutByIds(ids));
	}
	
}
