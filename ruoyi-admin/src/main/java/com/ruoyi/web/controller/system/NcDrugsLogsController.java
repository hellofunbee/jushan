package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.Ztree;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.NcDrugsLogs;
import com.ruoyi.system.domain.NcYaoClass;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.service.INcDrugsLogsService;
import com.ruoyi.system.service.INcYaoClassService;
import com.ruoyi.system.service.ISysUserService;
import com.ruoyi.system.util.ExcelUtil2;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 兽药出库记录 信息操作处理
 *
 * @author ruoyi
 * @date 2019-09-02
 */
@Controller
@RequestMapping("/system/ncDrugsLogs")
public class NcDrugsLogsController extends BaseController {
    private String prefix = "system/ncDrugsLogs";

    @Autowired
    private INcDrugsLogsService ncDrugsLogsService;

    @Autowired
    private INcYaoClassService ncYaoClassService;


    @Autowired
    private ISysUserService userService;


    @RequiresPermissions("system:ncDrugsLogs:view")
    @GetMapping()
    public String ncDrugsLogs(ModelMap mmap) {


        List<SysUser> sysUsers = userService.selectUserList(new SysUser());
        mmap.put("users", sysUsers);
        return prefix + "/ncDrugsLogs";
    }

    /**
     * 查询兽药出库记录列表
     */
    @RequiresPermissions("system:ncDrugsLogs:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(NcDrugsLogs ncDrugsLogs) {
        startPage();
        List<NcDrugsLogs> list = ncDrugsLogsService.selectNcDrugsLogsList(ncDrugsLogs);
        return getDataTable(list);
    }


    /**
     * 导出兽药出库记录列表
     */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(NcDrugsLogs ncDrugsLogs) {
        List<NcDrugsLogs> list = ncDrugsLogsService.selectNcDrugsLogsList(ncDrugsLogs);
        ExcelUtil2<NcDrugsLogs> util = new ExcelUtil2<NcDrugsLogs>(NcDrugsLogs.class);
        return util.exportExcel(list, "ncDrugsLogs");
    }

    /**
     * 新增兽药出库记录
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存兽药出库记录
     */
    @RequiresPermissions("system:ncDrugsLogs:add")
    @Log(title = "兽药出库记录", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(NcDrugsLogs ncDrugsLogs) {

        SysUser sysUser = ShiroUtils.getSysUser();
        //出库业务
        AjaxResult result = ncDrugsLogsService.checkOutDrugs(ncDrugsLogs, sysUser);

        return result;
    }

    /**
     * 修改兽药出库记录
     */
    @GetMapping("/edit/{drugLogId}")
    public String edit(@PathVariable("drugLogId") Long drugLogId, ModelMap mmap) {
        NcDrugsLogs ncDrugsLogs = ncDrugsLogsService.selectNcDrugsLogsById(drugLogId);
        mmap.put("ncDrugsLogs", ncDrugsLogs);

        List<SysUser> sysUsers = userService.selectUserList(new SysUser());
        mmap.put("users", sysUsers);
        return prefix + "/edit";
    }

    /**
     * 修改保存兽药出库记录
     */
    @RequiresPermissions("system:ncDrugsLogs:edit")
    @Log(title = "兽药出库记录", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(NcDrugsLogs ncDrugsLogs) {
        return toAjax(ncDrugsLogsService.updateNcDrugsLogs(ncDrugsLogs));
    }

    /**
     * 删除兽药出库记录
     */
    @RequiresPermissions("system:ncDrugsLogs:remove")
    @Log(title = "兽药出库记录", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(ncDrugsLogsService.deleteNcDrugsLogsByIds(ids));
    }

    /**
     * 选择药品树
     */
    @GetMapping("/selectYaoTree/{yaoClassId}")
    public String selectYaoClassTree(@PathVariable("yaoClassId") Long yaoClassId, ModelMap mmap) {

        mmap.put("yao", ncYaoClassService.selectYaoClassById(yaoClassId));
        return prefix + "/tree";
    }

    /**
     * 加载药品列表树
     */
    @GetMapping("/treeData")
    @ResponseBody
    public List<Ztree> treeData() {
        List<Ztree> ztrees = ncYaoClassService.selectYaoClassTree(new NcYaoClass());
        return ztrees;
    }

}
