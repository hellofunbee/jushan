package com.ruoyi.web.controller.system;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.SysDept;
import com.ruoyi.system.service.ISysDeptService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * ********************菜房订单前端接口*********************
 *
 * @author ruoyi
 * @date 2019-09-01
 */
@Controller
@RequestMapping("/system/iot")
public class FrontIOTController extends BaseController {
    private String prefix = "system/iot";


    @Autowired
    private ISysDeptService deptService;

    /*温室详情*/
    @GetMapping("/data")
    public String data(ModelMap mmap, Integer tp_id) {
        mmap.put("tp_id", tp_id);
        mmap.put("depts", getDepts());
        return prefix + "/data";
    }

    /*菜房*/
    @GetMapping("/ctrl")
    public String ctrl(ModelMap mmap, Integer tp_id) {
        mmap.put("tp_id", tp_id);
        mmap.put("depts", getDepts());
        return prefix + "/ctrl";
    }
    /*获取部门uuid字符串*/
    private String getDepts() {
        SysDept dept = new SysDept();
        dept.setAncestors("101");
        dept.setType(2);
        List<SysDept> list = deptService.selectDeptList(dept);
        StringBuffer sb = new StringBuffer();
        if(ShiroUtils.getSysUser().isAdmin()){
            sb.append("abeb937e031a1be89d47e2099d7fc733,");//室外气象站的uuid
        }
        for (SysDept d : list) {
            sb.append(d.getUuid());
            sb.append(",");
        }

        return sb.toString();
    }


    /*设备管理*/
    @RequiresPermissions("system:iot:iotConfig")
    @GetMapping("/iotConfig")
    public String iotConfig(ModelMap mmap) {
        mmap.put("depts", getDepts());
        return prefix + "/iotConfig";
    }


}
