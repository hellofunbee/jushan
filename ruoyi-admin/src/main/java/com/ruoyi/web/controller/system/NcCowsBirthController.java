package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.NcCowsBirth;
import com.ruoyi.system.service.INcCowsBirthService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * 产犊记录 信息操作处理
 *
 * @author ruoyi
 * @date 2019-09-02
 */
@Controller
@RequestMapping("/system/ncCowsBirth")
public class NcCowsBirthController extends BaseController
{
    private String prefix = "system/ncCowsBirth";

	@Autowired
	private INcCowsBirthService ncCowsBirthService;

	@RequiresPermissions("system:ncCowsBirth:view")
	@GetMapping()
	public String ncCowsBirth()
	{
	    return prefix + "/ncCowsBirth";
	}

	/**
	 * 查询产犊记录列表
	 */
	@RequiresPermissions("system:ncCowsBirth:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(NcCowsBirth ncCowsBirth)
	{
		startPage();
        List<NcCowsBirth> list = ncCowsBirthService.selectNcCowsBirthList(ncCowsBirth);
		return getDataTable(list);
	}


	/**
	 * 导出产犊记录列表
	 */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(NcCowsBirth ncCowsBirth)
    {
    	List<NcCowsBirth> list = ncCowsBirthService.selectNcCowsBirthList(ncCowsBirth);
        ExcelUtil<NcCowsBirth> util = new ExcelUtil<NcCowsBirth>(NcCowsBirth.class);
        return util.exportExcel(list, "产犊信息单");
    }

	/**
	 * 新增产犊记录
	 */
	@GetMapping("/add")
	public String add(Integer cowId,ModelMap mmap)
	{
		mmap.put("cowId",cowId);
	    return prefix + "/add";
	}

	/**
	 * 新增保存产犊记录
	 */
	@RequiresPermissions("system:ncCowsBirth:add")
	@Log(title = "产犊记录", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(NcCowsBirth ncCowsBirth)
	{
		try {
			Integer one = ncCowsBirthService.selectCowCodeOne(ncCowsBirth.getChildCode());
			if (one < 1) {
				if (Integer.parseInt(ncCowsBirth.getChildCode()) >=0) {
					ncCowsBirth.setUpdateTime(new Date());
					return toAjax(ncCowsBirthService.insertNcCowsBirth(ncCowsBirth));
				}else {
					return AjaxResult.error("不能有负数");
				}
			}else {
				return AjaxResult.error("耳号重复");
			}

		}catch (Exception e){
			return AjaxResult.error("不能为小数");
		}


	}

	/**
	 * 修改产犊记录
	 */
	@GetMapping("/edit/{birthId}")
	public String edit(@PathVariable("birthId") Integer birthId, ModelMap mmap)
	{
		NcCowsBirth ncCowsBirth = ncCowsBirthService.selectNcCowsBirthByBId(birthId);
		mmap.put("ncCowsBirth", ncCowsBirth);
	    return prefix + "/edit";
	}

	/**
	 * 修改保存产犊记录
	 */
	@RequiresPermissions("system:ncCowsBirth:edit")
	@Log(title = "产犊记录", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(NcCowsBirth ncCowsBirth)
	{
		try {
			if (Integer.parseInt(ncCowsBirth.getChildCode()) >=0) {
				ncCowsBirth.setUpdateTime(new Date());
				return toAjax(ncCowsBirthService.updateNcCowsBirth(ncCowsBirth));
			}else {
				return AjaxResult.error("不能有负数");
			}
		}catch (Exception e){
			return AjaxResult.error("不能为小数");
		}
	}

	/**
	 * 删除产犊记录
	 */
	@RequiresPermissions("system:ncCowsBirth:remove")
	@Log(title = "产犊记录", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public AjaxResult remove(String ids)
	{
		return toAjax(ncCowsBirthService.deleteNcCowsBirthByIds(ids));
	}

}
