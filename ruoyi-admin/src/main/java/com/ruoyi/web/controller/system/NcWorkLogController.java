package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.service.*;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 农事记录 信息操作处理
 *
 * @author ruoyi
 * @date 2019-09-01
 */
@Controller
@RequestMapping("/system/ncWorkLog")
public class NcWorkLogController extends BaseController {
    private String prefix = "system/ncWorkLog";
    private String frontPrefix = "system/ncWorkLog/front";

    @Autowired
    private INcWorkLogService ncWorkLogService;

    @Autowired
    private INcWorkService ncWorkService;

    @Autowired
    private INcWorkMirrorService workMirrorService;

    @Autowired
    private INcStageMirrorService stageMirrorService;

    @Autowired
    private INcPlanService planService;

    @Autowired
    private INcPengInfoService pengInfoService;

    @Autowired
    private ITPointService pointService;

    @Autowired
    private ISysDeptService sysDeptService;

    @RequiresPermissions("system:ncWorkLog:view")
    @GetMapping()
    public String ncWorkLog(Integer type, ModelMap mmap) {
        mmap.put("type", type);
        return prefix + "/ncWorkLog";
    }

    @GetMapping("/HotHouse")
    public String prontNcWorkHotHouseLog(ModelMap mmap){
        //获取温室信息
        SysDept dept = new SysDept();
        dept.setType(3);
        dept.setAncestors("101");//温室
        List<SysDept> list = sysDeptService.selectDeptList(dept);
        List<SysDept> result = new ArrayList<>();
        //大棚
        for (SysDept d : list) {
            if (d.getAncestors().split(",").length == 4) {
                result.add(d);
            }
        }
        //根据大棚 去找传感信息
        for (SysDept d : result) {
            //查找每个温室的传感数据
            d.setParams(pointService.listSensorChartInfo(d));

        }

        try {
            //对结果进行处理
            pointService.calcFailData(result);
        } catch (Exception e) {

        }

        mmap.put("pengs", result);
        return frontPrefix + "/ncWorkHotHouseLog";
    }



    /**
     * 查询农事记录
     */
    @RequiresPermissions("system:ncWorkLog:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(NcWorkLog ncWorkLog) {
        startPage();
        List<NcWorkLog> list = ncWorkLogService.selectNcWorkLogList(ncWorkLog);
        return getDataTable(list);
    }


    /**
     * 导出农事记录
     */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(NcWorkLog ncWorkLog) {
        List<NcWorkLog> list = ncWorkLogService.selectNcWorkLogList(ncWorkLog);
        ExcelUtil<NcWorkLog> util = new ExcelUtil<NcWorkLog>(NcWorkLog.class);
        return util.exportExcel(list, "ncWorkLog");
    }

    /**
     * 新增农事记录
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存农事记录
     */
    @RequiresPermissions("system:ncWorkLog:add")
    @Log(title = "农事记录", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(NcWorkLog ncWorkLog) {
        return toAjax(ncWorkLogService.insertNcWorkLog(ncWorkLog));
    }

    /**
     * 修改农事记录
     */
    @GetMapping("/edit/{zaipeiId}")
    public String edit(@PathVariable("zaipeiId") Long zaipeiId, ModelMap mmap) {
        NcWorkLog ncWorkLog = ncWorkLogService.selectNcWorkLogById(zaipeiId);
        mmap.put("ncWorkLog", ncWorkLog);
        return prefix + "/edit";
    }

    /**
     * 修改保存农事记录
     */
    @RequiresPermissions("system:ncWorkLog:edit")
    @Log(title = "农事记录", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(NcWorkLog ncWorkLog) {
        return toAjax(ncWorkLogService.updateNcWorkLog(ncWorkLog));
    }

    /**
     * 删除农事记录
     */
    @RequiresPermissions("system:ncWorkLog:remove")
    @Log(title = "农事记录", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(ncWorkLogService.deleteNcWorkLogByIds(ids));
    }

    /**************************自己的业务逻辑***********************************/


    /*在执行页面*/
    @GetMapping("/ncWorkLogWorking")
    public String ncWorkLogWorking(Integer type, ModelMap mmap) {
        mmap.put("type", type);
        return prefix + "/ncWorkLog-working";
    }

    /*待执行页面*/
    @GetMapping("/ncWorkLogPreWorking")
    public String ncWorkLogPreWorking(Integer type, ModelMap mmap) {
        mmap.put("type", type);
        return prefix + "/ncWorkLog-preworking";
    }

    /**
     * 确认农事-页面 记录
     */
    @GetMapping("/confirmWork/{workMirrorId}")
    public String confirmWork(@PathVariable("workMirrorId") Long workMirrorId, ModelMap mmap) {
        NcWorkMirror ncWork = workMirrorService.selectNcWorkMirrorById(workMirrorId);
        mmap.put("ncWorkMirror", ncWork);
        return prefix + "/edit-confirm";
    }
    /**
     * 确认农事-页面 农资 记录
     */
    @GetMapping("/confirmWork2/{workMirrorId}")
    public String confirmWork2(@PathVariable("workMirrorId") Long workMirrorId, ModelMap mmap) {
        NcWorkMirror ncWork = workMirrorService.selectNcWorkMirrorById(workMirrorId);
        mmap.put("ncWorkMirror", ncWork);
        return prefix + "/edit-confirm2";
    }

    /**
     * 确认农事-页面 记录
     */
    @GetMapping("/confirmStage/{stageMirrorId}")
    public String confirmStage(@PathVariable("stageMirrorId") Long stageMirrorId, ModelMap mmap) {
        NcStageMirror stage = stageMirrorService.selectNcStageMirrorById(stageMirrorId);
        mmap.put("ncStageMirror", stage);
        return prefix + "/edit-confirm-stage";
    }

    /**
     * 确认农事-页面 记录
     */
    @GetMapping("/delayStage/{stageMirrorId}")
    public String delayStage(@PathVariable("stageMirrorId") Long stageMirrorId, ModelMap mmap) {
        NcStageMirror stage = stageMirrorService.selectNcStageMirrorById(stageMirrorId);
        mmap.put("ncStageMirror", stage);
        return prefix + "/edit-delayDays";
    }

    /**
     * 确认农事-保存 记录
     */
    @Log(title = "确认农事-保存 记录", businessType = BusinessType.INSERT)
    @PostMapping("/confirmWork")
    @ResponseBody
    public AjaxResult confirmWork(NcWorkLog ncWorkLog) {
        //TODO 后续会根据确认的时间推断出 新的标准
        NcWork w = ncWorkService.selectNcWorkById(ncWorkLog.getWorkId());

        ncWorkLog.setCycleDays(w.getCycleDays());
        ncWorkLog.setInput(w.getInput());
        ncWorkLog.setInputCn(w.getInputCn());
        ncWorkLog.setInputCount(w.getInputCount());
        ncWorkLog.setInputUnit(w.getInputUnit());
        ncWorkLog.setIsDynamic(w.getIsDynamic());
        ncWorkLog.setStageId(w.getStageId());
        ncWorkLog.setStandardId(w.getStandardId());
        ncWorkLog.setWorkDetail(w.getWorkDetail());
        ncWorkLog.setWorkEnd(w.getWorkEnd());
        ncWorkLog.setWorkName(w.getWorkName());
        ncWorkLog.setWorkStart(w.getWorkStart());
        ncWorkLog.setWorkType(w.getWorkType());

        ncWorkLog.setCreateBy(ShiroUtils.getLoginName());
        ncWorkLog.setCreateTime(new Date());
        return toAjax(ncWorkLogService.insertNcWorkLog(ncWorkLog));
    }

    /*所有农事*/
    @GetMapping("/allWorkLogs")
    public String allWorkLogs(ModelMap mmap,Integer planId) {
        mmap.put("planId",planId);
        return prefix + "/ncWorkLog-all";
    }

    /*采摘任务*/
    @GetMapping("/harvestWork")
    public String harvestWork(ModelMap mmap) {
        SysUser sysUser = ShiroUtils.getSysUser();
        mmap.put("userId",sysUser.getUserId());
        return prefix + "/harvest-work";
    }


    /*添加农资*/
    @GetMapping("/addNongZi")
    public String addNongZi(Long planId, Long stageId, ModelMap mmap) {
        mmap.put("planId", planId);
        mmap.put("stageId", stageId);
        return prefix + "/add-nongzi";
    }


    /*添加农资保存*/
    @PostMapping("/addNongZiSave")
    @ResponseBody
    public AjaxResult addNongZiSave(NcWorkLog log, ModelMap mmap) {
        NcPlan plan = planService.selectNcPlanById(log.getPlanId());
        log.setWorkType("2");
        log.setStandardId(plan.getStandardId());
        log.setSource(2);//新增
        log.setCreateTime(new Date());
        log.setCreateBy(ShiroUtils.getLoginName());
        return AjaxResult.success(ncWorkLogService.insertNcWorkLog(log));
    }

    /*添加农资*/
    @GetMapping("/addNongShi")
    public String addNongShi(Long planId, Long stageId, ModelMap mmap) {
        mmap.put("planId", planId);
        mmap.put("stageId", stageId);
        return prefix + "/add-nongshi";
    }


    /*添加农事*/
    @PostMapping("/addNongShiSave")
    @ResponseBody
    public AjaxResult addNongShiSave(NcWorkLog log, ModelMap mmap) {
        NcPlan plan = planService.selectNcPlanById(log.getPlanId());
        log.setWorkType("1");
        log.setStandardId(plan.getStandardId());
        log.setSource(2);//新增
        log.setCreateBy(ShiroUtils.getLoginName());
        return AjaxResult.success(ncWorkLogService.insertNcWorkLog(log));
    }

    /**********************温室前端接口*********************/

    private String prefix_WenShi = "system/front_wenshi";

    /*温室管理*/
    @GetMapping("/greenhouseManagement")
    public String greenhouseManagement(Integer type, ModelMap mmap) {
        return prefix_WenShi + "/greenhouseManagement";
    }

    private String prefix_screen = "system/screen";

    //大屏锅炉房1
    @GetMapping("/houseScreen")
    public String houseScreen(ModelMap mmap){
        //获取温室信息
        SysDept dept = new SysDept();
        dept.setType(3);
        dept.setAncestors("101");//温室
        List<SysDept> list = sysDeptService.selectDeptList(dept);
        List<SysDept> result = new ArrayList<>();
        //大棚
        for (SysDept d : list) {
            if (d.getAncestors().split(",").length == 4) {
                result.add(d);
            }
        }
        //根据大棚 去找传感信息
        for (SysDept d : result) {
            //查找每个温室的传感数据
            d.setParams(pointService.listSensorChartInfo(d));

        }

        mmap.put("pengs", result);
        mmap.put("user", ShiroUtils.getSysUser());
        return prefix_screen + "/guolu-1";
    }


}
