package com.ruoyi.web.controller.system;

import com.ruoyi.WebSocketTemplate;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.NcZhenliaoDrugs;
import com.ruoyi.system.domain.NcZhenliaoLogs;
import com.ruoyi.system.domain.SysNotice;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.service.INcZhenliaoDrugsService;
import com.ruoyi.system.service.INcZhenliaoLogsService;
import com.ruoyi.system.service.ISysNoticeService;
import com.ruoyi.system.service.ISysUserService;
import com.ruoyi.system.util.ExcelUtil2Element;
import com.ruoyi.system.util.WorkbookExportUtil;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * 诊疗单 信息操作处理
 *
 * @author ruoyi
 * @date 2019-09-11
 */
@Controller
@RequestMapping("/system/ncChickenZhenliaoLogs")
public class NcChickenZhenliaoController extends BaseController {
    private String prefix = "system/ncChickenZhenliaoLogs";

    @Autowired
    private INcZhenliaoLogsService ncZhenliaoLogsService;
    @Autowired
    private ISysUserService userService;
    @Autowired
    private INcZhenliaoDrugsService zhenliaoDrugsService;
    @Autowired
    private ISysNoticeService noticeService;
    @Autowired
    private WebSocketTemplate socketTemplate;
    @RequiresPermissions("system:ncChickenZhenliaoLogs:view")
    @GetMapping()
    public String ncZhenliaoLogs()
    {
        return prefix + "/ncZhenliaoLogs";
    }

    @RequiresPermissions("system:ncChickenZhenliaoLogs:view")
    @GetMapping("/allZhenliao")
    public String ncallZhenliaoLogs()
    {
        return prefix + "/allzhenliaoLogs";
    }

    /**
     * 查询诊疗单列表
     */
    @RequiresPermissions("system:ncChickenZhenliaoLogs:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(NcZhenliaoLogs ncZhenliaoLogs)
    {
        startPage();
        List<NcZhenliaoLogs> list = ncZhenliaoLogsService.selectNcZhenliaoLogsList(ncZhenliaoLogs);
        return getDataTable(list);
    }


    /**
     * 导出诊疗单列表
     */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(NcZhenliaoLogs ncZhenliaoLogs)
    {
        WorkbookExportUtil wu = new WorkbookExportUtil(new XSSFWorkbook(), "鸡舍诊疗数据");

        List<NcZhenliaoLogs> zhenliaoLogsList = ncZhenliaoLogsService.selectNcZhenliaoLogsList(ncZhenliaoLogs);
        ExcelUtil2Element<NcZhenliaoLogs> m_zhenliao = new ExcelUtil2Element<>(NcZhenliaoLogs.class, wu);
        m_zhenliao.exportExcel(zhenliaoLogsList, "诊疗数据");


        List<NcZhenliaoDrugs> drugList = new ArrayList<>();

        if (zhenliaoLogsList != null && zhenliaoLogsList.size() > 0) {
            for (NcZhenliaoLogs zhenliaoLogs : zhenliaoLogsList) {
                NcZhenliaoDrugs drugs = new NcZhenliaoDrugs();
                drugs.setZhenliaoId(zhenliaoLogs.getZlId());
                List<NcZhenliaoDrugs> zhenliaoDrugs = zhenliaoDrugsService.selectNcZhenliaoDrugsList(drugs);
                drugList.addAll(zhenliaoDrugs);

            }
            ExcelUtil2Element<NcZhenliaoDrugs> m_zhenliaoDrugs = new ExcelUtil2Element<>(NcZhenliaoDrugs.class, wu);
            m_zhenliaoDrugs.exportExcel(drugList, "诊疗用药数据");

        }
        return wu.exportExcel();
    }

    /**
     * 新增诊疗单
     */
    @GetMapping("/add")
    public String add(ModelMap mmp)
    {
        //用户信息
        List<SysUser> sysUsers = userService.selectUserList(new SysUser());
        mmp.put("users", sysUsers);
        return prefix + "/add";
    }

    /**
     * 新增保存诊疗单
     */
    @RequiresPermissions("system:ncChickenZhenliaoLogs:add")
    @Log(title = "诊疗单", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(NcZhenliaoLogs ncZhenliaoLogs)
    {
        ncZhenliaoLogs.setZlStatus("5");
        ncZhenliaoLogs.setZlTime(new Date());
        ncZhenliaoLogs.setCreateTime(new Date());
        ncZhenliaoLogs.setCreateBy(ShiroUtils.getLoginName());
        ncZhenliaoLogs.setReportUser(ShiroUtils.getUserId());
    return toAjax(ncZhenliaoLogsService.insertNcZhenliaoLogs(ncZhenliaoLogs));
    }






    /**
     * 修改诊疗单
     */
    @GetMapping("/edit/{zlId}")
    public String edit(@PathVariable("zlId") Long zlId, ModelMap mmap)
    {
        NcZhenliaoLogs ncZhenliaoLogs = ncZhenliaoLogsService.selectNcZhenliaoLogsById(zlId);
        mmap.put("ncZhenliaoLogs", ncZhenliaoLogs);
        return prefix + "/edit";
    }

    /**
     * 修改保存诊疗单
     */
    @RequiresPermissions("system:ncChickenZhenliaoLogs:edit")
    @Log(title = "诊疗单", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(NcZhenliaoLogs ncZhenliaoLogs)
    {
        return toAjax(ncZhenliaoLogsService.updateNcZhenliaoLogs(ncZhenliaoLogs));
    }

    /**
     * 删除诊疗单
     */
    @RequiresPermissions("system:ncChickenZhenliaoLogs:remove")
    @Log(title = "诊疗单", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(ncZhenliaoLogsService.deleteNcZhenliaoLogsByIds(ids));
    }
//    /**
//     * z诊疗单详情
//     */
//    @GetMapping("/detail/{zlId}")
//    public String orderDetail(@PathVariable("zlId") Long zlId,ModelMap map)
//    {
//        map.put("zlId",zlId);
//        return prefix + "/details";
//    }

    /**
     * 查询诊疗单详情
     */
    @RequiresPermissions("system:ncChickenZhenliaoLogs:detail")
     @GetMapping("/detail/{id}")
    public String detail(@PathVariable("id") Long zlId,ModelMap mmap)
    {
        NcZhenliaoLogs ncZhenliaoLogs = ncZhenliaoLogsService.selectNcZhenliaoLogsById(zlId);
        mmap.put("name", "job");
        mmap.put("zl", ncZhenliaoLogs);
        NcZhenliaoDrugs ncZhenliaoDrugs = new NcZhenliaoDrugs();
        ncZhenliaoDrugs.setZhenliaoId(ncZhenliaoLogs.getZlId());
        List<NcZhenliaoDrugs> ncZhenliaoDrugs1 = zhenliaoDrugsService.selectNcZhenliaoDrugsList(ncZhenliaoDrugs);
        mmap.put("zlDrugs", ncZhenliaoDrugs1);
        return prefix + "/detail";
    }


    @Log(title = "发送诊疗请求", businessType = BusinessType.OTHER)
    @PostMapping("/confirmTable")
    @ResponseBody
    public AjaxResult confirmTable(String ids) {

        //发送工作通知

        String[] zlIds = ids.split(",");

        SysNotice notice = null;
        SysUser user = new SysUser();
        List<SysUser> users = new ArrayList<>();


        if (zlIds.length > 0) {
            for (String zlId : zlIds) {
                NcZhenliaoLogs ncZhenliaoLogs = ncZhenliaoLogsService.selectNcZhenliaoLogsById(Long.parseLong(zlId));

                notice = new SysNotice();
                notice.setNoticeTitle("鸡舍诊疗请求");
                notice.setNoticeContent("鸡舍号【" + ncZhenliaoLogs.getRoomNum() + "】") ;
                notice.setNoticeType("3");
                notice.setStatus("0");
                notice.setDeptId("2"); //设置选择用户
                notice.setReadType(2); //设置未读
                notice.setCreateTime(new Date());
                notice.setCreateBy(ShiroUtils.getLoginName());

                //兽医部门
                user.setDeptId(Long.parseLong("250"));
                List<SysUser> sysUsers1 = userService.selectUserList(user);
                users.addAll(sysUsers1);

                String userId = "";
                if (!users.isEmpty()) {
                    for (SysUser sysUser : users) {
                        userId += sysUser.getUserId() + ",";
                    }
                }
                notice.setUserId(userId);
                int no = noticeService.insertNotice(notice);
                if (no > 0) {
                    socketTemplate.sendMessage(notice);
                }
            }
        }
        return toAjax(ncZhenliaoLogsService.updateZhenliao(ids));
    }
}
