package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.NcInputChicken;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.service.INcFeedService;
import com.ruoyi.system.service.INcInputChickenService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * 鸡入库 信息操作处理
 * 
 * @author ruoyi
 * @date 2019-09-09
 */
@Controller
@RequestMapping("/system/ncInputChicken")
public class NcInputChickenController extends BaseController
{
    private String prefix = "system/ncInputChicken";
	
	@Autowired
	private INcInputChickenService ncInputChickenService;
	@Autowired
	private INcFeedService feedService;
	@Autowired
	private INcInputChickenService inputChickenService;
	@RequiresPermissions("system:ncInputChicken:view")
	@GetMapping()
	public String ncInputChicken(ModelMap map)
	{
		Integer zFfeed=feedService.stockAmout(1);

		map.put("zFfeed",zFfeed);

		Integer fFfeed=feedService.stockAmout(2);

		map.put("fFfeed",fFfeed);

		Integer chickStackAamount=inputChickenService.chickenStackAmount();

		map.put("chickStackAamount",chickStackAamount);

		return prefix + "/ncInputChicken";
	}
	
	/**
	 * 查询鸡入库列表
	 */
	@RequiresPermissions("system:ncInputChicken:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(NcInputChicken ncInputChicken,ModelMap map)
	{
		startPage();
        List<NcInputChicken> list = ncInputChickenService.selectNcInputChickenList(ncInputChicken);
		return getDataTable(list);
	}
	
	
	/**
	 * 导出鸡入库列表
	 */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(NcInputChicken ncInputChicken)
    {
    	List<NcInputChicken> list = ncInputChickenService.selectNcInputChickenList(ncInputChicken);
        ExcelUtil<NcInputChicken> util = new ExcelUtil<NcInputChicken>(NcInputChicken.class);
        return util.exportExcel(list, "鸡入库表");
    }
	
	/**
	 * 新增鸡入库
	 */
	@GetMapping("/add")
	public String add(ModelMap map)
	{
	    map.put("user",ShiroUtils.getLoginName());
		return prefix + "/add";
	}
	
	/**
	 * 新增保存鸡入库
	 */
	@RequiresPermissions("system:ncInputChicken:add")
	@Log(title = "鸡入库", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(NcInputChicken ncInputChicken)
	{
		ncInputChicken.setInputCode(DateUtils.parseDateToStr("yyyyMMddHHmmss", new Date()));
		return toAjax(ncInputChickenService.insertNcInputChicken(ncInputChicken));
	}

	/**
	 * 修改鸡入库
	 */
	@GetMapping("/edit/{inputId}")
	public String edit(@PathVariable("inputId") Integer inputId, ModelMap mmap)
	{
		NcInputChicken ncInputChicken = ncInputChickenService.selectNcInputChickenById(inputId);
		mmap.put("ncInputChicken", ncInputChicken);
	    return prefix + "/edit";
	}
	
	/**
	 * 修改保存鸡入库
	 */
	@RequiresPermissions("system:ncInputChicken:edit")
	@Log(title = "鸡入库", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(NcInputChicken ncInputChicken)
	{		
		return toAjax(ncInputChickenService.updateNcInputChicken(ncInputChicken));
	}
	
	/**
	 * 删除鸡入库
	 */
	@RequiresPermissions("system:ncInputChicken:remove")
	@Log(title = "鸡入库", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public AjaxResult remove(String ids)
	{		
		return toAjax(ncInputChickenService.deleteNcInputChickenByIds(ids));
	}
	/**
	 * 确认生成入库单
	 */
	@Log(title = "确认生成入库单", businessType = BusinessType.OTHER)
	@PostMapping("/confirmTable")
	@ResponseBody
	public AjaxResult confirmTable(String ids) {
		SysUser user = ShiroUtils.getSysUser();
		return toAjax(ncInputChickenService.createOrderByIds(ids,user));
	}
	
}
