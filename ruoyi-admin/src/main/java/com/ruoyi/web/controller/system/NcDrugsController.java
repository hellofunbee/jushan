package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.NcDrugs;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.service.INcDrugsService;
import com.ruoyi.system.service.ISysUserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 兽药库存 信息操作处理
 *
 * @author ruoyi
 * @date 2019-09-02
 */
@Controller
@RequestMapping("/system/ncDrugs")
public class NcDrugsController extends BaseController {
    private String prefix = "system/ncDrugs";

    @Autowired
    private INcDrugsService ncDrugsService;

    @Autowired
    private ISysUserService userService;

    @RequiresPermissions("system:ncDrugs:view")
    @GetMapping()
    public String ncDrugs(ModelMap mmap) {

            

        List<SysUser> sysUsers = userService.selectUserList(new SysUser());
        mmap.put("users", sysUsers);

        return prefix + "/ncDrugs";
    }

    /**
     * 查询兽药库存列表
     */
    @RequiresPermissions("system:ncDrugs:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(NcDrugs ncDrugs) {
        startPage();
        List<NcDrugs> list = ncDrugsService.selectNcDrugsList(ncDrugs);
        return getDataTable(list);
    }


    /**
     * 导出兽药库存列表
     */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(NcDrugs ncDrugs) {
        List<NcDrugs> list = ncDrugsService.selectNcDrugsList(ncDrugs);
        ExcelUtil<NcDrugs> util = new ExcelUtil<NcDrugs>(NcDrugs.class);
        return util.exportExcel(list, "ncDrugs");
    }

    /**
     * 新增兽药库存
     */
    @GetMapping("/add")
    public String add(ModelMap mmap) {
        List<SysUser> sysUsers = userService.selectUserList(new SysUser());
        mmap.put("users", sysUsers);
        return prefix + "/add";
    }

    /**
     * 新增保存兽药库存
     */
    @RequiresPermissions("system:ncDrugs:add")
    @Log(title = "兽药库存", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(NcDrugs ncDrugs) {
        SysUser sysUser = ShiroUtils.getSysUser();
        return toAjax(ncDrugsService.insertNcDrugs(ncDrugs, sysUser));
    }

    /**
     * 修改兽药库存
     */
    @GetMapping("/edit/{drugId}")
    public String edit(@PathVariable("drugId") Long drugId, ModelMap mmap) {
        NcDrugs ncDrugs = ncDrugsService.selectNcDrugsById(drugId);
        mmap.put("ncDrugs", ncDrugs);
        List<SysUser> sysUsers = userService.selectUserList(new SysUser());
        mmap.put("users", sysUsers);
        return prefix + "/edit";
    }

    /**
     * 修改保存兽药库存
     */
    @RequiresPermissions("system:ncDrugs:edit")
    @Log(title = "兽药库存", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(NcDrugs ncDrugs) {
        return toAjax(ncDrugsService.updateNcDrugs(ncDrugs));
    }

    /**
     * 兽药出库
     */
    @GetMapping("/checkOut/{drugId}")
    public String checkOut(@PathVariable("drugId") Long drugId, ModelMap mmap) {
        NcDrugs ncDrugs = ncDrugsService.selectNcDrugsById(drugId);
        mmap.put("ncDrugs", ncDrugs);
        List<SysUser> sysUsers = userService.selectUserList(new SysUser());
        mmap.put("users", sysUsers);
        return prefix + "/checkOut";
    }


    /**
     * 删除兽药库存
     */
    @RequiresPermissions("system:ncDrugs:remove")
    @Log(title = "兽药库存", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(ncDrugsService.deleteNcDrugsByIds(ids));
    }

}
