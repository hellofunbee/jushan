package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.NcUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.NcCai;
import com.ruoyi.system.domain.NcCheck;
import com.ruoyi.system.domain.NcOrderCf;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.service.INcCaiService;
import com.ruoyi.system.service.INcCheckService;
import com.ruoyi.system.service.INcOrderCfService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * 检测 信息操作处理
 *
 * @author ruoyi
 * @date 2019-08-29
 */
@Controller
@RequestMapping("/system/ncCheck")
public class NcCheckController extends BaseController {
    private String prefix = "system/ncCheck";

    @Autowired
    private INcCheckService ncCheckService;

    @Autowired
    private INcCaiService caiService;

    @Autowired
    private INcOrderCfService cfService;

    @RequiresPermissions("system:ncCheck:view")
    @GetMapping()
    public String ncCheck(ModelMap mmap) {
        SysUser sysUser = ShiroUtils.getSysUser();
        mmap.put("userId",sysUser.getUserId());
        return prefix + "/ncCheck";
    }

    /**
     * 查询检测列表
     */
    @RequiresPermissions("system:ncCheck:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(NcCheck ncCheck) {
        startPage();
        List<NcCheck> list = ncCheckService.selectNcCheckList(ncCheck);
        return getDataTable(list);
    }


    /**
     * 导出检测列表
     */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(NcCheck ncCheck) {
        List<NcCheck> list = ncCheckService.selectNcCheckList(ncCheck);
        ExcelUtil<NcCheck> util = new ExcelUtil<NcCheck>(NcCheck.class);
        return util.exportExcel(list, "检测记录单");
    }

    /**
     * 新增检测
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存检测
     */
    @RequiresPermissions("system:ncCheck:add")
    @Log(title = "检测", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(NcCheck ncCheck) {

        /*把订单状态更新为已检测*/
        if (ncCheck.getCforderId() != null && ncCheck.getCforderId() > 0) {
            NcOrderCf orderCf = cfService.selectNcOrderCfById(ncCheck.getCforderId());
            orderCf.setCforderStatus("3");//待入库
            cfService.updateNcOrderCf(orderCf);
        }
        return toAjax(ncCheckService.insertNcCheck(ncCheck));
    }

    /**
     * 修改检测
     */
    @GetMapping("/edit/{checkId}")
    public String edit(@PathVariable("checkId") Long checkId, ModelMap mmap) {
        NcCheck ncCheck = ncCheckService.selectNcCheckById(checkId);
        mmap.put("ncCheck", ncCheck);
        return prefix + "/edit";
    }

    /**
     * 新增检测
     */
    @GetMapping("/confirmCheck/{cforderId}")
    public String confirmCheck(@PathVariable("cforderId") Long cforderId, ModelMap mmap) {
        NcOrderCf ncOrderCf = cfService.selectNcOrderCfById(cforderId);
        NcCheck ck = new NcCheck();
        ck.setStatus("1");//待生成订单
        ck.setCheckResult(1);
        ck.setCheckTime(new Date());
        ck.setCheckUser(ShiroUtils.getLoginName());
        ck.setPiciCode(NcUtils.getOrderCode());
        ck.setCreateBy(ShiroUtils.getLoginName());
        ck.setCreateTime(new Date());
        ck.setCforderId(cforderId);
        mmap.put("ncCheck", ck);
        return prefix + "/edit-confirm";
    }

    /**
     * 修改保存检测
     */
    @RequiresPermissions("system:ncCheck:edit")
    @Log(title = "检测", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(NcCheck ncCheck) {
        return toAjax(ncCheckService.updateNcCheck(ncCheck));
    }

    /**
     * 删除检测
     */
    @RequiresPermissions("system:ncCheck:remove")
    @Log(title = "检测", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(ncCheckService.deleteNcCheckByIds(ids));
    }

    /**
     * 确认生成检测单
     */
    @Log(title = "确认生成检测单", businessType = BusinessType.OTHER)
    @PostMapping("/confirmTable")
    @ResponseBody
    public AjaxResult confirmTable(String ids) {
        SysUser user = ShiroUtils.getSysUser();
        //TODO 生成育苗计划单
        return toAjax(ncCheckService.createOrderByIds(ids, user));
    }

    /**
     * 确认入库 修改数量信息
     */
    @GetMapping("/confirmInput/{checkId}")
    public String confirmInput(@PathVariable("checkId") Long checkId, ModelMap mmap) {
        NcCheck check = ncCheckService.selectNcCheckById(checkId);
        mmap.put("ncCheck", check);
        return prefix + "/edit-input";
    }

    /**
     * 挂起订单
     */
    @Log(title = "挂起订单", businessType = BusinessType.UPDATE)
    @PostMapping("/guaQi")
    @ResponseBody
    public AjaxResult editSave(NcOrderCf orderCf) {
        NcOrderCf o = new NcOrderCf();
        o.setCforderId(orderCf.getCforderId());
        o.setCforderStatus("10");
        return toAjax(cfService.updateNcOrderCf(o));
    }

    /**
     * 新增保存检测
     */
    @RequiresPermissions("system:ncCheck:add")
    @Log(title = "检测", businessType = BusinessType.INSERT)
    @PostMapping("/addCheck")
    @ResponseBody
    public AjaxResult addNewCheckSave(NcCheck ncCheck, Long cropVarieties, String cropVarietiesCn, Integer cforderFor, Integer planId,Integer cforderType) {
        NcCai cai = caiService.selectCaiById(cropVarieties);
        if (cai != null && cai.getParentId() != null) {
            cai = caiService.selectCaiById(cai.getParentId());
        }

        NcOrderCf cf = new NcOrderCf();
        cf.setIsCheck(1);
        cf.setCforderType(cforderType);
        cf.setCforderFor(cforderFor);
        cf.setCreateTime(new Date());
        cf.setCreateBy(ShiroUtils.getLoginName());
        cf.setCforderStatus("5");
        cf.setCropVarietiesCn(cropVarietiesCn);
        cf.setCropVarieties(cropVarieties);
        if (planId != null && planId > 0) {
            cf.setPlanId(planId);
            cf.setHasPlan("1");
        }else {
            cf.setHasPlan("2");
        }
        cf.setStatus("2");
        if (cai != null) {
            cf.setCropNameCn(cai.getCaiName());
        }
        cf.setCforderStatus("3");

        cfService.insertNcOrderCf(cf);
        ncCheck.setCforderId(cf.getCforderId());
        ncCheck.setStatus("3");
        ncCheck.setPiciCode(NcUtils.getOrderCode());
        ncCheck.setCreateTime(new Date());
        ncCheck.setCreateBy(ShiroUtils.getLoginName());
        ncCheck.setCheckUser(ShiroUtils.getLoginName());
        ncCheck.setPiciCode(NcUtils.getOrderCode());

        return toAjax(ncCheckService.insertNcCheck(ncCheck));
    }


}
