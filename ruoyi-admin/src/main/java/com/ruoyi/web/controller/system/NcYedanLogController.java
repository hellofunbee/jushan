package com.ruoyi.web.controller.system;

import java.util.List;

import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.NcYedan;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.service.ISysUserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.NcYedanLog;
import com.ruoyi.system.service.INcYedanLogService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.web.multipart.MultipartFile;

/**
 * 液氮出库 信息操作处理
 *
 * @author ruoyi
 * @date 2019-09-02
 */
@Controller
@RequestMapping("/system/ncYedanLog")
public class NcYedanLogController extends BaseController {
    private String prefix = "system/ncYedanLog";

    @Autowired
    private INcYedanLogService ncYedanLogService;


    @Autowired
    private ISysUserService userService;


    @RequiresPermissions("system:ncYedanLog:view")
    @GetMapping()
    public String ncYedanLog(ModelMap mmap) {

        List<SysUser> sysUsers = userService.selectUserList(new SysUser());
        mmap.put("users", sysUsers);
        return prefix + "/ncYedanLog";
    }

    /**
     * 查询液氮出库列表
     */
    @RequiresPermissions("system:ncYedanLog:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(NcYedanLog ncYedanLog) {
        startPage();
        List<NcYedanLog> list = ncYedanLogService.selectNcYedanLogList(ncYedanLog);
        return getDataTable(list);
    }



    /**
     * 新增液氮出库
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存液氮出库
     */
    @RequiresPermissions("system:ncYedanLog:add")
    @Log(title = "液氮出库", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(NcYedanLog ncYedanLog) {


        SysUser sysUser = ShiroUtils.getSysUser();

        //出库业务
        boolean result = ncYedanLogService.checkOutYeDan(ncYedanLog,sysUser);

        return toAjax(result);
    }

    /**
     * 修改液氮出库
     */
    @GetMapping("/edit/{yedanLogId}")
    public String edit(@PathVariable("yedanLogId") Long yedanLogId, ModelMap mmap) {
        NcYedanLog ncYedanLog = ncYedanLogService.selectNcYedanLogById(yedanLogId);
        mmap.put("ncYedanLog", ncYedanLog);
        List<SysUser> sysUsers = userService.selectUserList(new SysUser());
        mmap.put("users", sysUsers);
        return prefix + "/edit";
    }

    /**
     * 修改保存液氮出库
     */
    @RequiresPermissions("system:ncYedanLog:edit")
    @Log(title = "液氮出库", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(NcYedanLog ncYedanLog) {
        return toAjax(ncYedanLogService.updateNcYedanLog(ncYedanLog));
    }

    /**
     * 删除液氮出库
     */
    @RequiresPermissions("system:ncYedanLog:remove")
    @Log(title = "液氮出库", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(ncYedanLogService.deleteNcYedanLogByIds(ids));
    }

/*
    @RequiresPermissions("system:ncYedanLog:view")
    @GetMapping("/importTemplate")
    @ResponseBody
    public AjaxResult importTemplate() {
        ExcelUtil<NcYedan> util = new ExcelUtil<NcYedan>(NcYedan.class);
        return util.importTemplateExcel("液氮出库导入模板");
    }

    @Log(title = "液氮", businessType = BusinessType.IMPORT)
    @RequiresPermissions("system:ncYedanLog:import")
    @PostMapping("/importData")
    @ResponseBody
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<NcYedan> util = new ExcelUtil<NcYedan>(NcYedan.class);
        List<NcYedan> yedanList = util.importExcel(file.getInputStream());
        String message = ncYedanService.importYeDan(yedanList, updateSupport, ShiroUtils.getSysUser());
        return AjaxResult.success(message);
    }*/



    /**
     * 导出液氮出库列表
     */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(NcYedanLog ncYedanLog) {
        List<NcYedanLog> list = ncYedanLogService.selectNcYedanLogList(ncYedanLog);
        ExcelUtil<NcYedanLog> util = new ExcelUtil<NcYedanLog>(NcYedanLog.class);
        return util.exportExcel(list, "ncYedanLog");
    }

}
