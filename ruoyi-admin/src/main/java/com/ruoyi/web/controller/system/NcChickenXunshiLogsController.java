package com.ruoyi.web.controller.system;

import java.util.List;

import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.service.ISysUserService;
import com.ruoyi.system.util.ExcelUtil2;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.NcXunshiLogs;
import com.ruoyi.system.service.INcChickenXunshiLogsService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;

/**
 *  巡视记录 信息操作处理
 *
 * @author ruoyi
 * @date 2019-09-02
 */
@Controller
@RequestMapping("/system/ncXunshiLogs/chicken")
public class NcChickenXunshiLogsController extends BaseController {
    private String prefix = "system/ncXunshiLogs/chicken";

    @Autowired
    private INcChickenXunshiLogsService ncXunshiLogsService;



    @Autowired
    private ISysUserService userService;


    @RequiresPermissions("system:ncXunshiLogs:view")
    @GetMapping()
    public String ncXunshiLogs(ModelMap mmap) {



        List<SysUser> sysUsers = userService.selectUserList(new SysUser());
        mmap.put("users", sysUsers);

        return prefix + "/ncXunshiLogs";
    }

    /**
     * 查询巡视记录列表
     */
    @RequiresPermissions("system:ncXunshiLogs:list")
    @PostMapping("/list/{xsType}/{xsStatus}")
    @ResponseBody
    public TableDataInfo list(@PathVariable("xsType") Integer xsType, @PathVariable("xsStatus") Integer xsStatus, NcXunshiLogs ncXunshiLogs) {
        startPage();
        ncXunshiLogs.setXsStatus(xsStatus);
        ncXunshiLogs.setXsType(xsType);
        List<NcXunshiLogs> list = ncXunshiLogsService.selectNcXunshiLogsList(ncXunshiLogs);
        return getDataTable(list);
    }


    /**
     * 导出巡视记录列表
     */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(NcXunshiLogs ncXunshiLogs) {
        List<NcXunshiLogs> list = ncXunshiLogsService.selectNcXunshiLogsList(ncXunshiLogs);
        ExcelUtil2<NcXunshiLogs> util = new ExcelUtil2<NcXunshiLogs>(NcXunshiLogs.class);
        return util.exportExcel(list, "ncXunshiLogs");
    }

    /**
     * 新增巡视记录
     */
    @GetMapping("/add")
    public String add(ModelMap mmap) {
        List<SysUser> sysUsers = userService.selectUserList(new SysUser());
        mmap.put("users", sysUsers);
        return prefix + "/add";
    }

    /**
     * 新增保存巡视记录
     */
    @RequiresPermissions("system:ncXunshiLogs:add")
    @Log(title = "巡视记录", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(NcXunshiLogs ncXunshiLogs) {
        return toAjax(ncXunshiLogsService.insertNcXunshiLogs(ncXunshiLogs));
    }

    /**
     * 修改巡视记录
     */
    @GetMapping("/edit/{xsId}")
    public String edit(@PathVariable("xsId") Long xsId, ModelMap mmap) {
        NcXunshiLogs ncXunshiLogs = ncXunshiLogsService.selectNcXunshiLogsById(xsId);
        mmap.put("ncXunshiLogs", ncXunshiLogs);
        List<SysUser> sysUsers = userService.selectUserList(new SysUser());
        mmap.put("users", sysUsers);
        return prefix + "/edit";
    }

    /**
     * 修改保存巡视记录
     */
    @RequiresPermissions("system:ncXunshiLogs:edit")
    @Log(title = "巡视记录", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(NcXunshiLogs ncXunshiLogs) {
        return toAjax(ncXunshiLogsService.updateNcXunshiLogs(ncXunshiLogs));
    }


    /**
     * 批量确认生成检测单
     *
     */
    @Log(title = "确认生成检测单", businessType = BusinessType.OTHER)
    @PostMapping("/confirmTable")
    @ResponseBody
    public AjaxResult confirmTable(String ids) {

        SysUser user = ShiroUtils.getSysUser();
        return toAjax(ncXunshiLogsService.createZlOrderByIds(ids, user));
    }


    /**
     * 删除巡视记录
     */
    @RequiresPermissions("system:ncXunshiLogs:remove")
    @Log(title = "巡视记录", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(ncXunshiLogsService.deleteNcXunshiLogsByIds(ids));
    }

}
