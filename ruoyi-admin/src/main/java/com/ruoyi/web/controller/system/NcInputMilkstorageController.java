package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.NcInputMilk;
import com.ruoyi.system.domain.NcOrderCf;
import com.ruoyi.system.domain.NcOutputMilk;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.service.INcInputMilkService;
import com.ruoyi.system.service.INcOrderCfService;
import com.ruoyi.system.service.INcOutputMilkService;
import com.ruoyi.system.service.ISysUserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * 牛奶入库 信息操作处理
 *
 * @author ruoyi
 * @date 2019-09-11
 */
@Controller
@RequestMapping("/system/ncInputMilkStorage")
public class NcInputMilkstorageController extends BaseController
{
    private String prefix = "system/ncInputMilk";

	@Autowired
	private INcInputMilkService ncInputMilkService;

	@Autowired
	private INcOutputMilkService ncOutputMilkService;

	@Autowired
	private INcOrderCfService ncOrderCfService;

	@Autowired
	private ISysUserService iSysUserService;

	@RequiresPermissions("system:ncInputMilk:view")
	@GetMapping()
	public String ncInputMilk()
	{
	    return prefix + "/ncInputMilkStorage";
	}

	/**
	 * 查询牛奶入库列表
	 */
	@RequiresPermissions("system:ncInputMilk:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(NcInputMilk ncInputMilk,int stock)
	{
		startPage();
		if(stock > 0){
			ncInputMilk.getParams().put("stock",1);
		}
        List<NcInputMilk> list = ncInputMilkService.selectNcInputMilkList(ncInputMilk);
		return getDataTable(list);
	}


	/**
	 * 导出牛奶入库列表
	 */
	@Log(title = "牛奶仓储", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(NcInputMilk ncInputMilk)
    {
    	List<NcInputMilk> list = ncInputMilkService.selectNcInputMilkList(ncInputMilk);
        ExcelUtil<NcInputMilk> util = new ExcelUtil<NcInputMilk>(NcInputMilk.class);
        return util.exportExcel(list, "牛奶仓储");
    }

	/**
	 * 新增牛奶入库
	 */
	@GetMapping("/add")
	public String add()
	{
	    return prefix + "/add";
	}

	/**
	 * 新增保存牛奶入库
	 */
	@RequiresPermissions("system:ncInputMilk:add")
	@Log(title = "牛奶入库", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(NcInputMilk ncInputMilk)
	{
		ncInputMilk.setInputType(1);
		return toAjax(ncInputMilkService.insertNcInputMilk(ncInputMilk));
	}

	/**
	 * 修改牛奶入库
	 */
	@GetMapping("/edit/{inputId}")
	public String edit(@PathVariable("inputId") Integer inputId, ModelMap mmap)
	{
		NcInputMilk ncInputMilk = ncInputMilkService.selectNcInputMilkById(inputId);
		mmap.put("ncInputMilk", ncInputMilk);
	    return prefix + "/edit";
	}

	/**
	 * 修改保存牛奶入库
	 */
	@RequiresPermissions("system:ncInputMilk:edit")
	@Log(title = "牛奶入库", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(NcInputMilk ncInputMilk)
	{
		return toAjax(ncInputMilkService.updateNcInputMilk(ncInputMilk));
	}

	/**
	 * 删除牛奶入库
	 */
	@RequiresPermissions("system:ncInputMilk:remove")
	@Log(title = "牛奶入库", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public AjaxResult remove(String ids)
	{
		return toAjax(ncInputMilkService.deleteNcInputMilkByIds(ids));
	}

	/**
	 * 确定生成入库单
	 */
	@Log(title = "确认生成入库单", businessType = BusinessType.OTHER)
	@PostMapping("/confirmTable")
	@ResponseBody
	public AjaxResult confirmTable(String ids){
		SysUser user = ShiroUtils.getSysUser();
		return toAjax(ncInputMilkService.createOrderByIds(ids,user));
	}

	/**
	 * 出入库记录
	 */
	@Log(title = "出库", businessType = BusinessType.INSERT)
	@GetMapping("/depot/{inputId}")
	public String depot(@PathVariable("inputId")Integer inputId , ModelMap mmap){

		mmap.put("depot",ncInputMilkService.selectNcInputMilkById(inputId));
		mmap.put("users",iSysUserService.selectUserList(new SysUser()));


		return prefix + "/depot";
	}

	/**
	 * 出入库记录保存
	 */
	@Log(title = "出入库保存", businessType = BusinessType.INSERT)
	@PostMapping("/depot")
	@ResponseBody
	public AjaxResult addSave(NcOutputMilk ncOutputMilk){
		NcInputMilk inputMilk = ncInputMilkService.selectNcInputMilkById(Integer.parseInt(ncOutputMilk.getInputId() + ""));
		if(inputMilk.getLeftAmout() != null){
//			if (Integer.parseInt(inputMilk.getLeftAmout() + "") >= Integer.parseInt(ncOutputMilk.getOutputAmout() + "")) {
				if (inputMilk.getLeftAmout().compareTo(ncOutputMilk.getOutputAmout()) > -1) {

				ncOutputMilk.setOutputType(2);

				NcOrderCf ncOrderCf = new NcOrderCf();
				ncOrderCf.setCropNameCn("牛奶");     //插入名字为牛奶
				//根据inputid获取到编号放入订单
				ncOrderCf.setCforderCode(inputMilk.getInputCode());
				//插入类型
				ncOrderCf.setCforderType(ncOutputMilk.getCforderType());

				ncOrderCfService.insertNcOrderCf(ncOrderCf);
				ncOutputMilk.setCforderId(Integer.parseInt(ncOrderCf.getCforderId()+"")); //设置订单id

				//设置消毒人和挤奶人
				ncOutputMilk.setMilkman(inputMilk.getMilkman());
				ncOutputMilk.setDisinfector(inputMilk.getDisinfector());

				//库存减少
				BigDecimal a = inputMilk.getLeftAmout().subtract(ncOutputMilk.getOutputAmout());

//				int amout = (Integer.parseInt(inputMilk.getLeftAmout() + "") - Integer.parseInt(ncOutputMilk.getOutputAmout() + ""));
				inputMilk.setLeftAmout(a);

				ncInputMilkService.updateNcInputMilk(inputMilk);

				return toAjax(ncOutputMilkService.insertNcOutputMilk(ncOutputMilk));
			}
			return AjaxResult.error("库存不足");
			}
		return AjaxResult.error("库存为空");



	}



}
