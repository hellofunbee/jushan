package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.NcChickenDayCensus;
import com.ruoyi.system.service.INcChickenDayCensusService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 鸡日报 信息操作处理
 * 
 * @author ruoyi
 * @date 2019-09-19
 */
@Controller
@RequestMapping("/system/ncChickenDayCensus")
public class NcChickenDayCensusController extends BaseController
{
    private String prefix = "system/ncChickenDayCensus";
	
	@Autowired
	private INcChickenDayCensusService ncChickenDayCensusService;
	
	@RequiresPermissions("system:ncChickenDayCensus:view")
	@GetMapping()
	public String ncChickenDayCensus(String beginTime,String endTime,ModelMap map )
	{
		map.put("beginTime", beginTime);
		map.put("endTime", endTime);
	    return prefix + "/ncChickenDayCensus";
	}
	
	/**
	 * 查询鸡日报列表
	 */
	@RequiresPermissions("system:ncChickenDayCensus:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(NcChickenDayCensus ncChickenDayCensus)
	{
		startPage();
        List<NcChickenDayCensus> list = ncChickenDayCensusService.selectNcChickenDayCensusList(ncChickenDayCensus);
		return getDataTable(list);
	}
	
	
	/**
	 * 导出鸡日报列表
	 */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(NcChickenDayCensus ncChickenDayCensus)
    {
    	List<NcChickenDayCensus> list = ncChickenDayCensusService.selectNcChickenDayCensusList(ncChickenDayCensus);
        ExcelUtil<NcChickenDayCensus> util = new ExcelUtil<NcChickenDayCensus>(NcChickenDayCensus.class);
        return util.exportExcel(list, "鸡舍日报表");
    }
	
	/**
	 * 新增鸡日报
	 */
	@GetMapping("/add")
	public String add()
	{
	    return prefix + "/add";
	}
	
	/**
	 * 新增保存鸡日报
	 */
	@RequiresPermissions("system:ncChickenDayCensus:add")
	@Log(title = "鸡日报", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(NcChickenDayCensus ncChickenDayCensus)
	{		
		return toAjax(ncChickenDayCensusService.insertNcChickenDayCensus(ncChickenDayCensus));
	}

	/**
	 * 修改鸡日报
	 */
	@GetMapping("/edit/{censusId}")
	public String edit(@PathVariable("censusId") Long censusId, ModelMap mmap)
	{
		NcChickenDayCensus ncChickenDayCensus = ncChickenDayCensusService.selectNcChickenDayCensusById(censusId);
		mmap.put("ncChickenDayCensus", ncChickenDayCensus);
	    return prefix + "/edit";
	}
	
	/**
	 * 修改保存鸡日报
	 */
	@RequiresPermissions("system:ncChickenDayCensus:edit")
	@Log(title = "鸡日报", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(NcChickenDayCensus ncChickenDayCensus)
	{		
		return toAjax(ncChickenDayCensusService.updateNcChickenDayCensus(ncChickenDayCensus));
	}
	
	/**
	 * 删除鸡日报
	 */
	@RequiresPermissions("system:ncChickenDayCensus:remove")
	@Log(title = "鸡日报", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public AjaxResult remove(String ids)
	{		
		return toAjax(ncChickenDayCensusService.deleteNcChickenDayCensusByIds(ids));
	}
	
}
