package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.Ztree;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.NcYaoClass;
import com.ruoyi.system.domain.SysRole;
import com.ruoyi.system.service.INcYaoClassService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 药品信息
 *
 * @author ruoyi
 */
@Controller
@RequestMapping("/system/ncYaoClass")
public class NcYaoClassController extends BaseController {
    private String prefix = "system/ncYaoClass";

    @Autowired
    private INcYaoClassService yaoClassService;

    @RequiresPermissions("system:yaoClass:view")
    @GetMapping()
    public String yaoClass() {

        return prefix + "/ncYaoClass";
    }

    @RequiresPermissions("system:yaoClass:list")
    @PostMapping("/list")
    @ResponseBody
    public List<NcYaoClass> list(NcYaoClass yaoClass) {
        List<NcYaoClass> yaoClassList = yaoClassService.selectYaoClassList(yaoClass);
        return yaoClassList;
    }

    /**
     * 新增药品
     */
    @GetMapping("/add/{parentId}")
    public String add(@PathVariable("parentId") Long parentId, ModelMap mmap) {
        mmap.put("yao", yaoClassService.selectYaoClassById(parentId));
        return prefix + "/add";
    }

    /**
     * 新增保存药品
     */
    @Log(title = "药品管理", businessType = BusinessType.INSERT)
    @RequiresPermissions("system:yaoClass:add")
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(@Validated NcYaoClass yaoClass) {
        /*if (UserConstants.DEPT_NAME_NOT_UNIQUE.equals(yaoClassService.checkYaoClassNameUnique(yaoClass))) {
            return error("新增药品'" + yaoClass.getYaoName() + "'失败，药品名称已存在");
        }*/
        yaoClass.setCreateBy(ShiroUtils.getLoginName());
        return toAjax(yaoClassService.insertYaoClass(yaoClass));
    }

    /**
     * 修改
     */
    @GetMapping("/edit/{yaoClassId}")
    public String edit(@PathVariable("yaoClassId") Long yaoClassId, ModelMap mmap) {
        NcYaoClass yaoClass = yaoClassService.selectYaoClassById(yaoClassId);
        if (StringUtils.isNotNull(yaoClass) && 100L == yaoClassId) {
//			yaoClass.setParentName("无");
        }
        mmap.put("yao", yaoClass);
        return prefix + "/edit";
    }

    /**
     * 保存
     */
    @Log(title = "药品管理", businessType = BusinessType.UPDATE)
    @RequiresPermissions("system:yaoClass:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(@Validated NcYaoClass yaoClass) {
        /*if (UserConstants.DEPT_NAME_NOT_UNIQUE.equals(yaoClassService.checkYaoClassNameUnique(yaoClass))) {
            return error("修改药品" + yaoClass.getYaoName() + "'失败，药品名称已存在");
        } else */
        if (yaoClass.getParentId().equals(yaoClass.getYaoId())) {
            return error("修改药品'" + yaoClass.getYaoName() + "'失败，上级药品分类不能是自己");
        }
        yaoClass.setUpdateBy(ShiroUtils.getLoginName());
        return toAjax(yaoClassService.updateYaoClass(yaoClass));
    }

    /**
     * 删除
     */
    @Log(title = "药品管理", businessType = BusinessType.DELETE)
    @RequiresPermissions("system:yaoClass:remove")
    @GetMapping("/remove/{id}")
    @ResponseBody
    public AjaxResult remove(@PathVariable("id") Long yaoClassId) {
        System.out.println(yaoClassService.selectYaoClassCount(yaoClassId));
        if (yaoClassService.selectYaoClassCount(yaoClassId) > 0) {
            return AjaxResult.warn("存在下级药品,不允许删除");
        }
        if (yaoClassService.checkYaoClassExistUser(yaoClassId)) {
            return AjaxResult.warn("药品存在用户,不允许删除");
        }
        return toAjax(yaoClassService.deleteYaoClassById(yaoClassId));
    }

    /**
     * 校验药品名称
     */
    @PostMapping("/checkYaoClassNameUnique")
    @ResponseBody
    public String checkYaoClassNameUnique(NcYaoClass yaoClass) {
        return yaoClassService.checkYaoClassNameUnique(yaoClass);
    }

    /**
     * 选择药品树
     */
    @GetMapping("/selectYaoTree/{yaoClassId}")
    public String selectYaoClassTree(@PathVariable("yaoClassId") Long yaoClassId, ModelMap mmap) {

        mmap.put("yao", yaoClassService.selectYaoClassById(yaoClassId));
        return prefix + "/tree";
    }

    /**
     * 加载药品列表树
     */
    @GetMapping("/treeData")
    @ResponseBody
    public List<Ztree> treeData() {
        List<Ztree> ztrees = yaoClassService.selectYaoClassTree(new NcYaoClass());
        return ztrees;
    }

    /**
     * 加载角色药品（数据权限）列表树
     */
    @GetMapping("/roleYaoClassTreeData")
    @ResponseBody
    public List<Ztree> yaoClassTreeData(SysRole role) {
        List<Ztree> ztrees = yaoClassService.roleYaoClassTreeData(role);
        return ztrees;
    }
}
