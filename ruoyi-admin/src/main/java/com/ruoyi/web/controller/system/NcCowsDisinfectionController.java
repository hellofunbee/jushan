package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.NcCowsDisinfection;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.service.INcCowsDisinfectionService;
import com.ruoyi.system.service.ISysUserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 消毒记录（牛舍） 信息操作处理
 *
 * @author ruoyi
 * @date 2019-09-04
 */
@Controller
@RequestMapping("/system/ncCowsDisinfection")
public class NcCowsDisinfectionController extends BaseController
{
    private String prefix = "system/ncCowsDisinfection";
    private String frontPrefix = "system/ncCowsDisinfection/front";

	@Autowired
	private INcCowsDisinfectionService ncCowsDisinfectionService;

	@Autowired
	private ISysUserService userService;

	@RequiresPermissions("system:ncCowsDisinfection:view")
	@GetMapping()
	public String ncCowsDisinfection()
	{

		return prefix + "/ncCowsDisinfection";
	}

	@GetMapping("/front")
	public String frontNcCowsDisinfection()
	{
		return frontPrefix + "/ncCowsDisinfection";
	}

	/**
	 * 查询消毒记录（牛舍）列表
	 */
	@RequiresPermissions("system:ncCowsDisinfection:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(NcCowsDisinfection ncCowsDisinfection)
	{
		startPage();
        List<NcCowsDisinfection> list = ncCowsDisinfectionService.selectNcCowsDisinfectionList(ncCowsDisinfection);
		return getDataTable(list);
	}
	@PostMapping("/front/list")
	@ResponseBody
	public TableDataInfo frontList(NcCowsDisinfection ncCowsDisinfection)
	{
		startPage();
		List<NcCowsDisinfection> list = ncCowsDisinfectionService.selectNcCowsDisinfectionList(ncCowsDisinfection);
		return getDataTable(list);
	}


	/**
	 * 导出消毒记录（牛舍）列表
	 */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(NcCowsDisinfection ncCowsDisinfection)
    {
		ncCowsDisinfection.setManagerType(2);
    	List<NcCowsDisinfection> list = ncCowsDisinfectionService.selectNcCowsDisinfectionList(ncCowsDisinfection);
        ExcelUtil<NcCowsDisinfection> util = new ExcelUtil<NcCowsDisinfection>(NcCowsDisinfection.class);
        return util.exportExcel(list, "牛舍消毒单");
    }

	/**
	 * 新增消毒记录（牛舍）
	 */
	@GetMapping("/add")
	public String add()
	{
	    return prefix + "/add";
	}

	/**
	 * 新增保存消毒记录（牛舍）
	 */
	@RequiresPermissions("system:ncCowsDisinfection:add")
	@Log(title = "消毒记录（牛舍）", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(NcCowsDisinfection ncCowsDisinfection)
	{
		ncCowsDisinfection.setManagerType(2);
		return toAjax(ncCowsDisinfectionService.insertNcCowsDisinfection(ncCowsDisinfection));
	}

	/**
	 * 修改消毒记录（牛舍）
	 */
	@GetMapping("/edit/{dfId}")
	public String edit(@PathVariable("dfId") Long dfId, ModelMap mmap)
	{
		NcCowsDisinfection ncCowsDisinfection = ncCowsDisinfectionService.selectNcCowsDisinfectionById(dfId);
		mmap.put("ncCowsDisinfection", ncCowsDisinfection);


		List<SysUser> sysUsers = userService.selectUserList(new SysUser());
		if (ncCowsDisinfection !=null && StringUtils.isNotNull(ncCowsDisinfection)) {
			for (SysUser sysUser : sysUsers) {
				if((""+sysUser.getUserId()) .equals(ncCowsDisinfection.getDfUser())){
					sysUser.setFlag(true);
					break;
				}
			}
		}
		mmap.put("dfUser",sysUsers);
	    return prefix + "/edit";
	}

	/**
	 * 修改保存消毒记录（牛舍）
	 */
	@RequiresPermissions("system:ncCowsDisinfection:edit")
	@Log(title = "消毒记录（牛舍）", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(NcCowsDisinfection ncCowsDisinfection)
	{
		return toAjax(ncCowsDisinfectionService.updateNcCowsDisinfection(ncCowsDisinfection));
	}

	/**
	 * 删除消毒记录（牛舍）
	 */
	@RequiresPermissions("system:ncCowsDisinfection:remove")
	@Log(title = "消毒记录（牛舍）", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public AjaxResult remove(String ids)
	{
		return toAjax(ncCowsDisinfectionService.deleteNcCowsDisinfectionByIds(ids));
	}

}
