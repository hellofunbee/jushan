package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.Ztree;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.NcKnowledgeClass;
import com.ruoyi.system.domain.NcKnowledgeClassType;
import com.ruoyi.system.service.INcKnowledgeClassService;
import com.ruoyi.system.service.INcKnowledgeClassTypeService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 知识课堂 信息操作处理
 *
 * @author ruoyi
 * @date 2019-09-10
 */
@Controller
@RequestMapping("/system/ncKnowledgeClass")
public class NcKnowledgeClassController extends BaseController {
    private String prefix = "system/ncKnowledgeClass";

    @Autowired
    private INcKnowledgeClassService ncKnowledgeClassService;


    @Autowired
    private INcKnowledgeClassTypeService ncKnowledgeClassTypeService;


    @RequiresPermissions("system:ncKnowledgeClass:view")
    @GetMapping()
    public String ncKnowledgeClass() {
        return prefix + "/ncKnowledgeClass";
    }


    @RequiresPermissions("system:ncKnowledgeClass:view")
    @GetMapping("/listClassRoom")
    public String listClassRoom(Long kctypeId,ModelMap mmap) {
        mmap.put("kctypeId",kctypeId);
        return prefix + "/knowledgeClassroom";
    }


    /**
     * 查询知识课堂列表
     */
    @RequiresPermissions("system:ncKnowledgeClass:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(NcKnowledgeClass ncKnowledgeClass) {
        startPage();
        List<NcKnowledgeClass> list = ncKnowledgeClassService.selectNcKnowledgeClassList(ncKnowledgeClass);
        return getDataTable(list);
    }




    /*根据分类分页查找列表*/
    @RequiresPermissions("system:ncKnowledgeClass:list")
    @PostMapping("/listByKCTypeId")
    @ResponseBody
    public TableDataInfo listByKCTypeId(NcKnowledgeClass ncKnowledgeClass,ModelMap mmap) {
        startPage();
        ncKnowledgeClass.setTitle("%" + ncKnowledgeClass.getTitle() + "%");
        List<NcKnowledgeClass> list = ncKnowledgeClassService.selectNcKnowledgeClassList(ncKnowledgeClass);
        return getDataTable(list);

    }

    /**
     * 新增知识课堂
     */
    @GetMapping("/showKC/{kcId}")
    public String showKC(@PathVariable("kcId") Long kcId, ModelMap mmap) {
        NcKnowledgeClass thisKC = ncKnowledgeClassService.selectNcKnowledgeClassById(kcId);
        mmap.put("kc", thisKC);
        return prefix + "/showKC";
    }


    /**
     * 导出知识课堂列表
     */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(NcKnowledgeClass ncKnowledgeClass) {
        List<NcKnowledgeClass> list = ncKnowledgeClassService.selectNcKnowledgeClassList(ncKnowledgeClass);
        ExcelUtil<NcKnowledgeClass> util = new ExcelUtil<NcKnowledgeClass>(NcKnowledgeClass.class);
        return util.exportExcel(list, "ncKnowledgeClass");
    }

    /**
     * 新增知识课堂
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存知识课堂
     */
    @RequiresPermissions("system:ncKnowledgeClass:add")
    @Log(title = "知识课堂", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(NcKnowledgeClass ncKnowledgeClass) {
        ncKnowledgeClass.setCreateBy(ShiroUtils.getSysUser().getUserName());
        ncKnowledgeClass.setCreateTime(new Date());

        return toAjax(ncKnowledgeClassService.insertNcKnowledgeClass(ncKnowledgeClass, ShiroUtils.getSysUser()));
    }

    /**
     * 修改知识课堂
     */
    @GetMapping("/edit/{kcId}")
    public String edit(@PathVariable("kcId") Long kcId, ModelMap mmap) {
        NcKnowledgeClass ncKnowledgeClass = ncKnowledgeClassService.selectNcKnowledgeClassById(kcId);

        NcKnowledgeClassType ncKnowledgeClassType = ncKnowledgeClassTypeService.selectNcKnowledgeClassTypeById(ncKnowledgeClass.getKctypeId());


        mmap.put("typeName", ncKnowledgeClassType.getKcTypeName());
        mmap.put("ncKnowledgeClass", ncKnowledgeClass);
        return prefix + "/edit";
    }

    /**
     * 修改保存知识课堂
     */
    @RequiresPermissions("system:ncKnowledgeClass:edit")
    @Log(title = "知识课堂", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(NcKnowledgeClass ncKnowledgeClass) {
        return toAjax(ncKnowledgeClassService.updateNcKnowledgeClass(ncKnowledgeClass, ShiroUtils.getSysUser()));
    }

    /**
     * 删除知识课堂
     */
    @RequiresPermissions("system:ncKnowledgeClass:remove")
    @Log(title = "知识课堂", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(ncKnowledgeClassService.deleteNcKnowledgeClassByIds(ids));
    }


    /**
     * 选择知识课堂
     */
    @GetMapping("/selectKCTypeTree/{kctypeId}")
    public String selectKCTypeTree(@PathVariable("kctypeId") Long kctypeId, ModelMap mmap) {
        mmap.put("kc", ncKnowledgeClassTypeService.selectNcKnowledgeClassTypeById(kctypeId));
        return prefix + "/tree";
    }


    /**
     * 加载知识课堂列表树
     */
    @GetMapping("/treeData")
    @ResponseBody
    public List<Ztree> treeData() {
        List<Ztree> ztrees = ncKnowledgeClassTypeService.selectKCTypeClassTree(new NcKnowledgeClassType());
        return ztrees;
    }


}
