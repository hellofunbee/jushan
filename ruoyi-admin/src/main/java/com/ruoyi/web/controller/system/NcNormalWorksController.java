package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.NcNormalWorks;
import com.ruoyi.system.service.INcNormalWorksService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * 农事记录 信息操作处理
 *
 * @author ruoyi
 * @date 2019-07-31
 */
@Controller
@RequestMapping("/system/ncNormalWorks")
public class NcNormalWorksController extends BaseController {
    private String prefix = "system/ncNormalWorks";

    private String frontPrefix = "system/ncNormalWorks/front";
    @Autowired
    private INcNormalWorksService ncNormalWorksService;

    @RequiresPermissions("system:ncNormalWorks:view")
    @GetMapping()
    public String ncNormalWorks(Integer workType, ModelMap mmap) {
        mmap.put("workType", workType);
        return prefix + "/ncNormalWorks";
    }
    @GetMapping("/front")
    public String frontNcNormalWorks(Integer workType, ModelMap mmap) {
        mmap.put("workType", workType);
        return frontPrefix + "/ncNormalWorks-front";
    }





    /**
     * 查询农事记录列表
     */
    @RequiresPermissions("system:ncNormalWorks:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(NcNormalWorks ncNormalWorks) {
        startPage();
        List<NcNormalWorks> list = ncNormalWorksService.selectNcNormalWorksList(ncNormalWorks);
        return getDataTable(list);
    }


    /**
     * 导出农事记录列表
     */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(NcNormalWorks ncNormalWorks) {
        List<NcNormalWorks> list = ncNormalWorksService.selectNcNormalWorksList(ncNormalWorks);
        ExcelUtil<NcNormalWorks> util = new ExcelUtil<NcNormalWorks>(NcNormalWorks.class);
        return util.exportExcel(list, "工作记录表");
    }

    /**
     * 新增农事记录
     */
    @GetMapping("/add")
    public String add(Integer workType, ModelMap mmap) {
        mmap.put("workType", workType);
        mmap.put("userName", ShiroUtils.getLoginName());
        return prefix + "/add";
    }

    /**
     * 新增保存农事记录
     */
    @RequiresPermissions("system:ncNormalWorks:add")
    @Log(title = "农事记录", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(NcNormalWorks ncNormalWorks) {
        ncNormalWorks.setCreateTime(new Date());
        return toAjax(ncNormalWorksService.insertNcNormalWorks(ncNormalWorks));
    }

    /**
     * 修改农事记录
     */
    @GetMapping("/edit/{nWorkId}")
    public String edit(@PathVariable("nWorkId") Long nWorkId, ModelMap mmap) {
        NcNormalWorks ncNormalWorks = ncNormalWorksService.selectNcNormalWorksById(nWorkId);
        mmap.put("ncNormalWorks", ncNormalWorks);
        return prefix + "/edit";
    }

    /**
     * 修改保存农事记录
     */
    @RequiresPermissions("system:ncNormalWorks:edit")
    @Log(title = "农事记录", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(NcNormalWorks ncNormalWorks) {
        return toAjax(ncNormalWorksService.updateNcNormalWorks(ncNormalWorks));
    }

    /**
     * 删除农事记录
     */
    @RequiresPermissions("system:ncNormalWorks:remove")
    @Log(title = "农事记录", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(ncNormalWorksService.deleteNcNormalWorksByIds(ids));
    }

}
