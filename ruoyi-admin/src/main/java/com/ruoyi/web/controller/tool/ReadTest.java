package com.ruoyi.web.controller.tool;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

/**
 * 进制转换工具类
 */
public class ReadTest {
    /**
     * @return 返回文件内容
     * @author 戴尔电脑   * @date 2018-1-19 下午4:02:38
     * 读取txt文件的内容 * @param file 想要读取的文件对象
     * <p>
     * course.txt
     * 1,数据库
     * 2,数学
     * 3,信息系统
     * 4,操作系统
     * 5,数据结构
     * 6,数据处理
     */
    public static String txt2String(File file) {
        StringBuilder result = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));//构造一个BufferedReader类来读取文件
            String s = null;
            while ((s = br.readLine()) != null) {//使用readLine方法，一次读一行
                result.append(System.lineSeparator() + s);
            }
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result.toString();
    }

    public static void main(String[] args) {
        File file = new File("D:/course.txt");
        System.out.println(txt2String(file));
    }

}

