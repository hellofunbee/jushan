package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.NcPengInfo;
import com.ruoyi.system.service.INcPengInfoService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 计划大棚 信息操作处理
 * 
 * @author ruoyi
 * @date 2019-09-08
 */
@Controller
@RequestMapping("/system/ncPengInfo")
public class NcPengInfoController extends BaseController
{
    private String prefix = "system/ncPengInfo";
	
	@Autowired
	private INcPengInfoService ncPengInfoService;
	
	@RequiresPermissions("system:ncPengInfo:view")
	@GetMapping()
	public String ncPengInfo()
	{
	    return prefix + "/ncPengInfo";
	}
	
	/**
	 * 查询计划大棚列表
	 */
	@RequiresPermissions("system:ncPengInfo:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(NcPengInfo ncPengInfo)
	{
		startPage();
        List<NcPengInfo> list = ncPengInfoService.selectNcPengInfoList(ncPengInfo);
		return getDataTable(list);
	}
	
	
	/**
	 * 导出计划大棚列表
	 */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(NcPengInfo ncPengInfo)
    {
    	List<NcPengInfo> list = ncPengInfoService.selectNcPengInfoList(ncPengInfo);
        ExcelUtil<NcPengInfo> util = new ExcelUtil<NcPengInfo>(NcPengInfo.class);
        return util.exportExcel(list, "ncPengInfo");
    }
	
	/**
	 * 新增计划大棚
	 */
	@GetMapping("/add")
	public String add()
	{
	    return prefix + "/add";
	}
	
	/**
	 * 新增保存计划大棚
	 */
	@RequiresPermissions("system:ncPengInfo:add")
	@Log(title = "计划大棚", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(NcPengInfo ncPengInfo)
	{		
		return toAjax(ncPengInfoService.insertNcPengInfo(ncPengInfo));
	}

	/**
	 * 修改计划大棚
	 */
	@GetMapping("/edit/{pengInfoId}")
	public String edit(@PathVariable("pengInfoId") Long pengInfoId, ModelMap mmap)
	{
		NcPengInfo ncPengInfo = ncPengInfoService.selectNcPengInfoById(pengInfoId);
		mmap.put("ncPengInfo", ncPengInfo);
	    return prefix + "/edit";
	}
	
	/**
	 * 修改保存计划大棚
	 */
	@RequiresPermissions("system:ncPengInfo:edit")
	@Log(title = "计划大棚", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(NcPengInfo ncPengInfo)
	{		
		return toAjax(ncPengInfoService.updateNcPengInfo(ncPengInfo));
	}
	
	/**
	 * 删除计划大棚
	 */
	@RequiresPermissions("system:ncPengInfo:remove")
	@Log(title = "计划大棚", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public AjaxResult remove(String ids)
	{		
		return toAjax(ncPengInfoService.deleteNcPengInfoByIds(ids));
	}
	
}
