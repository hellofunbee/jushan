package com.ruoyi.web.controller.system;

import com.ruoyi.common.config.Global;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 首页 业务处理
 *
 * @author ruoyi
 */
@Controller
public class SysIndexController extends BaseController {
    @Autowired
    private ISysMenuService menuService;

    @Autowired
    private ITPointService pointService;

    //知识课堂
    @Autowired
    private INcKnowledgeClassService knowledgeClassService;

    //知识课堂类别
    @Autowired
    private INcKnowledgeClassTypeService knowledgeClassTypeService;

    @Autowired
    private ISysNoticeService noticeService;

    @Autowired
    private INcHeaderService headerService;


    // 系统首页
    @GetMapping("/index")
    public String index(ModelMap mmap) {
        // 取身份信息
        SysUser user = ShiroUtils.getSysUser();
        // 根据用户id取出菜单
        List<SysMenu> menus = menuService.selectMenusByUser(user);
        //对特定菜单进行处理
        menuService.addSpecialMenu(menus);
        mmap.put("menus", menus);
        mmap.put("user", user);
        mmap.put("copyrightYear", Global.getCopyrightYear());
        mmap.put("demoEnabled", Global.isDemoEnabled());
        return "index";
    }

    // 系统介绍
    @GetMapping("/system/main")
    public String main(ModelMap mmap) {


        ///////////////////////////     首页知识课堂 四大类 start ////////////////////
        //查询知识课堂类别
        NcKnowledgeClassType ncKnowledgeClassType = new NcKnowledgeClassType();
        ncKnowledgeClassType.setParentId(0L);
        List<NcKnowledgeClassType> KCClassTypes = knowledgeClassTypeService.selectNcKnowledgeClassTypeList(ncKnowledgeClassType);
        Map<NcKnowledgeClassType, List<NcKnowledgeClass>> map = new HashMap<>(16);
        for (NcKnowledgeClassType kcClassType : KCClassTypes) {
            //查询对应类别的知识课堂
            NcKnowledgeClass ncKnowledgeClass = new NcKnowledgeClass();
            ncKnowledgeClass.setKctypeId(kcClassType.getKctypeId());
            List<NcKnowledgeClass> ncKnowledgeClasses = knowledgeClassService.selectFourNcKnowledgeClassList(kcClassType.getKctypeId());
            map.put(kcClassType, ncKnowledgeClasses);

        }
        mmap.put("map", map);
        ///////////////////////////     首页知识课堂 四大类 end ////////////////////
        //轮播图
        NcHeader header = new NcHeader();
        header.sethState("Y");
        List<NcHeader> ncHeaders = headerService.selectNcHeaderList(header);

        //通知公告
        SysNotice sysNotice = new SysNotice();
        sysNotice.getParams().put("homenotice", 1);
        List<SysNotice> notices = noticeService.selectHomePageNotice(sysNotice);

        mmap.put("notices", notices.size() > 5 ? notices.subList(0, 5) : notices);
        mmap.put("headers", ncHeaders);
        mmap.put("version", Global.getVersion());
        mmap.put("userId", ShiroUtils.getSysUser().getUserId());

        mmap.put("sensor", pointService.getWeatherInfo());

        return "main";
    }

    // 系统介绍
    @GetMapping("/system/socket")
    public String socket(ModelMap mmap) {
        return "test";
    }

    //消息角标
    @GetMapping("/system/getMsgCount")
    @ResponseBody
    public Integer getMsgCount() {
        SysUser user = ShiroUtils.getSysUser();

        //获取有多少个未读消息
        List<SysNotice> sysNotices = noticeService.selectNoticeList(new SysNotice());
        ArrayList<SysNotice> tempList = new ArrayList<>();
        SysNotice notice = new SysNotice();
        SysUser sysUser = ShiroUtils.getSysUser();
        for (SysNotice sysNotice : sysNotices) {
            if (sysNotice.getReadType() == 2) {
                if (("1").equals(sysNotice.getDeptId())) {
                    tempList.add(sysNotice);
                }
            }
            if (("2").equals(sysNotice.getDeptId())) {
                notice.setReadType(2);
                notice.setUserId(String.valueOf(sysUser.getUserId()));
            }
        }
        List<SysNotice> noticeList = noticeService.selectNoticeList(notice);
        tempList.addAll(noticeList);

        int noreadCount = tempList.size();


        return noreadCount;
    }


    @GetMapping("/my_logout")
    @ResponseBody
    public Integer my_logout(ModelMap mmap, int type) {
        ShiroUtils.logout();
        return 1;
    }

    /**
     * 触屏端退出登录
     * 1：鸡舍 2：牛舍 3：温室 4：菜房1 5：菜房2
     */
    @GetMapping("/screenLogout")
    public String screen(ModelMap mmap, int type) {
        /*try {
            ShiroUtils.logout();
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        if (type == 1) {
            return "system/screen/chicken/login";
        }
        else if (type == 2) {
            return "system/screen/cow/login";
        }
        else if (type == 4) {
            return "system/screen/caifang1/login";
        }
        else if (type == 5) {
            return "system/screen/caifang2/login";
        }
        else if (type == 6) {
            return "system/screen/guolu/login";
        }
        else if (type == 7) {
            return "system/screen/wenshi/login";
        }
        else {
            return "login";
        }
    }


    // 测试
    @GetMapping("/system/screen/test")
    public String screen(ModelMap mmap) {
        return "system/screen/iframe";
    }

    /**
     * 锅炉登录
     *
     * @param mmap
     * @return
     */
    @GetMapping("/guolu")
    public String guoluScreen(ModelMap mmap) {
        mmap.put("copyrightYear", Global.getCopyrightYear());
        return "system/screen/guolu/login";
    }


    /**
     * 锅炉主页
     *
     * @param mmap
     * @return
     */
    @GetMapping("/system/screen/guolu")
    public String guoluScreenIndex(ModelMap mmap) {
        SysUser user = ShiroUtils.getSysUser();
        mmap.put("user", user);
        return "system/screen/guolu/iframe";
    }


    /**
     * 温室登录
     *
     * @param mmap
     * @return
     */
    @GetMapping("/wenshi")
    public String wenshiScreen(ModelMap mmap) {
        mmap.put("copyrightYear", Global.getCopyrightYear());
        return "system/screen/wenshi/login";
    }


    /**
     * 温室主页
     *
     * @param mmap
     * @return
     */
    @GetMapping("/system/screen/wenshi")
    public String wenshiScreenIndex(ModelMap mmap) {
        SysUser user = ShiroUtils.getSysUser();
        mmap.put("user", user);
        return "system/screen/wenshi/iframe";
    }

    /**
     * 鸡舍登录
     *
     * @param mmap
     * @return
     */
    @GetMapping("/jishe")
    public String chickenScreen(ModelMap mmap) {
        mmap.put("copyrightYear", Global.getCopyrightYear());
        return "system/screen/chicken/login";
    }

    /**
     * 鸡舍主页
     *
     * @param mmap
     * @return
     */
    @GetMapping("/system/screen/jishe")
    public String chickenScreenIndex(ModelMap mmap) {
        SysUser user = ShiroUtils.getSysUser();
        mmap.put("user", user);
        return "system/screen/chicken/iframe";
    }
    /**
     * 牛舍登录
     *
     * @param mmap
     * @return
     */

    @GetMapping("/niushe")
    public String cowScreen(ModelMap mmap) {
        mmap.put("copyrightYear", Global.getCopyrightYear());
        return "system/screen/cow/login";
    }
    /**
     * 牛舍主页
     *
     * @param mmap
     * @return
     */
    @GetMapping("/system/screen/cow")
    public String cowScreenIndex(ModelMap mmap) {
        SysUser user = ShiroUtils.getSysUser();
        mmap.put("user", user);
        return "system/screen/cow/iframe";
    }
    /**
     * 菜房1登录
     *
     * @param mmap
     * @return
     */

    @GetMapping("/caifang1")
    public String caifang1(ModelMap mmap) {
        mmap.put("copyrightYear", Global.getCopyrightYear());
        return "system/screen/caifang1/login";
    }
    /**
     * 菜房1主页
     *
     * @param mmap
     * @return
     */
    @GetMapping("/system/screen/caifang1")
    public String caifang1ScreenIndex(ModelMap mmap) {
        SysUser user = ShiroUtils.getSysUser();
        mmap.put("user", user);
        return "system/screen/caifang1/iframe";
    }
    /**
     * 菜房2登录
     *
     * @param mmap
     * @return
     */

    @GetMapping("/caifang2")
    public String caifang2(ModelMap mmap) {
        mmap.put("copyrightYear", Global.getCopyrightYear());
        return "system/screen/caifang2/login";
    }
    /**
     * 菜房2主页
     *
     * @param mmap
     * @return
     */
    @GetMapping("/system/screen/caifang2")
    public String caifang2ScreenIndex(ModelMap mmap) {
        SysUser user = ShiroUtils.getSysUser();
        mmap.put("user", user);
        return "system/screen/caifang2/iframe";
    }

}
