package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.NcFeedLog;
import com.ruoyi.system.service.INcFeedLogService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 饲料出入记录 信息操作处理
 *
 * @author ruoyi
 * @date 2019-09-10
 */
@Controller
@RequestMapping("/system/ncFeedLog2")
public class NcFeedLog2Controller extends BaseController
{
    private String prefix = "system/ncFeedLog2";

	@Autowired
	private INcFeedLogService ncFeedLogService;

	@RequiresPermissions("system:ncFeedLog:view")
	@GetMapping()
	public String ncFeedLog()
	{
	    return prefix + "/ncFeedLog";
	}

	/**
	 * 查询饲料出入记录列表
	 */
	@RequiresPermissions("system:ncFeedLog:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(NcFeedLog ncFeedLog)
	{
		startPage();
        List<NcFeedLog> list = ncFeedLogService.selectNcFeedLogList(ncFeedLog);
		return getDataTable(list);
	}
	@GetMapping("/chicken")
	public String ncChicken()
	{

		return prefix + "/front/ncFeedLog";
	}


	/**
	 * 导出饲料出入记录列表
	 */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(NcFeedLog ncFeedLog)
    {
    	List<NcFeedLog> list = ncFeedLogService.selectNcFeedLogList(ncFeedLog);
//        ExcelUtil<NcFeedLog> util = new ExcelUtil<NcFeedLog>(NcFeedLog.class);
		ExcelUtil<NcFeedLog> util = new ExcelUtil<>(NcFeedLog.class);
        return util.exportExcel(list, "辅饲料记录单");

    }

	/**
	 * 新增饲料出入记录
	 */
	@GetMapping("/add")
	public String add()
	{
	    return prefix + "/add";
	}

	/**
	 * 新增保存饲料出入记录
	 */
	@RequiresPermissions("system:ncFeedLog:add")
	@Log(title = "饲料出入记录", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(NcFeedLog ncFeedLog)
	{
		return toAjax(ncFeedLogService.insertNcFeedLog(ncFeedLog));
	}

	/**
	 * 修改饲料出入记录
	 */
	@GetMapping("/edit/{feedLogId}")
	public String edit(@PathVariable("feedLogId") Long feedLogId, ModelMap mmap)
	{
		NcFeedLog ncFeedLog = ncFeedLogService.selectNcFeedLogById(feedLogId);
		mmap.put("ncFeedLog", ncFeedLog);
	    return prefix + "/edit";
	}

	/**
	 * 修改保存饲料出入记录
	 */
	@RequiresPermissions("system:ncFeedLog:edit")
	@Log(title = "饲料出入记录", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(NcFeedLog ncFeedLog)
	{
		return toAjax(ncFeedLogService.updateNcFeedLog(ncFeedLog));
	}

	/**
	 * 删除饲料出入记录
	 */
	@RequiresPermissions("system:ncFeedLog:remove")
	@Log(title = "饲料出入记录", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public AjaxResult remove(String ids)
	{
		return toAjax(ncFeedLogService.deleteNcFeedLogByIds(ids));
	}

	@GetMapping("ncFeedLogCows2")
	public String ncFeedLogCows()
	{
		return prefix + "/ncFeedLogCows2";
	}

}
