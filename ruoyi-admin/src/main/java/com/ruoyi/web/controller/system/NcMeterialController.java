package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.Ztree;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.NcMeterial;
import com.ruoyi.system.domain.SysRole;
import com.ruoyi.system.service.INcMeterialService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 物资信息
 *
 * @author ruoyi
 */
@Controller
@RequestMapping("/system/meterial")
public class NcMeterialController extends BaseController {
    private String prefix = "system/meterial";

    @Autowired
    private INcMeterialService meterialService;

    @RequiresPermissions("system:meterial:view")
    @GetMapping()
    public String meterial() {
        return prefix + "/meterial";
    }

    @RequiresPermissions("system:meterial:list")
    @PostMapping("/list")
    @ResponseBody
    public List<NcMeterial> list(NcMeterial meterial) {
        List<NcMeterial> meterialList = meterialService.selectMeterialList(meterial);
        return meterialList;
    }

    /**
     * 新增物资
     */
    @GetMapping("/add/{parentId}")
    public String add(@PathVariable("parentId") Long parentId, ModelMap mmap) {
        mmap.put("meterial", meterialService.selectMeterialById(parentId));
        return prefix + "/add";
    }

    /**
     * 新增保存物资
     */
    @Log(title = "物资管理", businessType = BusinessType.INSERT)
    @RequiresPermissions("system:meterial:add")
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(@Validated NcMeterial meterial) {
        /*if (UserConstants.DEPT_NAME_NOT_UNIQUE.equals(meterialService.checkMeterialNameUnique(meterial))) {
            return error("新增物资'" + meterial.getMeterialName() + "'失败，物资名称已存在");
        }*/
        meterial.setCreateBy(ShiroUtils.getLoginName());
        return toAjax(meterialService.insertMeterial(meterial));
    }

    /**
     * 修改
     */
    @GetMapping("/edit/{meterialId}")
    public String edit(@PathVariable("meterialId") Long meterialId, ModelMap mmap) {
        NcMeterial meterial = meterialService.selectMeterialById(meterialId);
        if (StringUtils.isNotNull(meterial) && 100L == meterialId) {
            meterial.setParentName("无");
        }
        mmap.put("meterial", meterial);
        return prefix + "/edit";
    }

    /**
     * 保存
     */
    @Log(title = "物资管理", businessType = BusinessType.UPDATE)
    @RequiresPermissions("system:meterial:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(@Validated NcMeterial meterial) {
        /*if (UserConstants.DEPT_NAME_NOT_UNIQUE.equals(meterialService.checkMeterialNameUnique(meterial))) {
            return error("修改物资'" + meterial.getMeterialName() + "'失败，物资名称已存在");
        } else */
        if (meterial.getParentId().equals(meterial.getMeterialId())) {
            return error("修改物资'" + meterial.getMeterialName() + "'失败，上级物资分类不能是自己");
        }
        meterial.setUpdateBy(ShiroUtils.getLoginName());
        return toAjax(meterialService.updateMeterial(meterial));
    }

    /**
     * 删除
     */
    @Log(title = "物资管理", businessType = BusinessType.DELETE)
    @RequiresPermissions("system:meterial:remove")
    @GetMapping("/remove/{meterialId}")
    @ResponseBody
    public AjaxResult remove(@PathVariable("meterialId") Long meterialId) {
        if (meterialService.selectMeterialCount(meterialId) > 0) {
            return AjaxResult.warn("存在下级物资,不允许删除");
        }
        if (meterialService.checkMeterialExistUser(meterialId)) {
            return AjaxResult.warn("物资存在用户,不允许删除");
        }
        return toAjax(meterialService.deleteMeterialById(meterialId));
    }

    /**
     * 校验物资名称
     */
    @PostMapping("/checkMeterialNameUnique")
    @ResponseBody
    public String checkMeterialNameUnique(NcMeterial meterial) {
        return meterialService.checkMeterialNameUnique(meterial);
    }

    /**
     * 选择物资树
     */
    @GetMapping("/selectMeterialTree/{meterialId}")
    public String selectMeterialTree(@PathVariable("meterialId") Long meterialId, ModelMap mmap) {
        mmap.put("meterial", meterialService.selectMeterialById(meterialId));
        return prefix + "/tree";
    }

    /**
     * 加载物资列表树
     */
    @GetMapping("/treeData")
    @ResponseBody
    public List<Ztree> treeData() {
        List<Ztree> ztrees = meterialService.selectMeterialTree(new NcMeterial());
        return ztrees;
    }

    /**
     * 加载角色物资（数据权限）列表树
     */
    @GetMapping("/roleMeterialTreeData")
    @ResponseBody
    public List<Ztree> meterialTreeData(SysRole role) {
        List<Ztree> ztrees = meterialService.roleMeterialTreeData(role);
        return ztrees;
    }
}
