package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.NcGasDuty;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.service.INcGasDutyService;
import com.ruoyi.system.service.ISysUserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * 锅炉房值班记录 信息操作处理
 *
 * @author ruoyi
 * @date 2019-07-31
 */
@Controller
@RequestMapping("/system/ncGasDuty")
public class NcGasDutyController extends BaseController
{
    private String prefix = "system/ncGasDuty";

    private String frontPrefix = "system/ncGasDuty/front";

	@Autowired
	private INcGasDutyService ncGasDutyService;

	@Autowired
	private ISysUserService iSysUserService;
	@RequiresPermissions("system:ncGasDuty:view")
	@GetMapping()
	public String ncGasDuty()
	{
	    return prefix + "/ncGasDuty";
	}

	@GetMapping("/front")
	public String ncGasDutyFront(){
		return frontPrefix+"/ncGasDuty";
	}



	/**
	 * 查询锅炉房值班记录列表
	 */
	@RequiresPermissions("system:ncGasDuty:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(NcGasDuty ncGasDuty)
	{
		startPage();
        List<NcGasDuty> list = ncGasDutyService.selectNcGasDutyList(ncGasDuty);
		return getDataTable(list);
	}


	/**
	 * 导出锅炉房值班记录列表
	 */
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(NcGasDuty ncGasDuty)
    {
    	List<NcGasDuty> list = ncGasDutyService.selectNcGasDutyList(ncGasDuty);
        ExcelUtil<NcGasDuty> util = new ExcelUtil<NcGasDuty>(NcGasDuty.class);
        return util.exportExcel(list, "值班记录单");
    }

	/**
	 * 新增锅炉房值班记录
	 */
	@GetMapping("/add")
	public String add(ModelMap mmap)
	{
		mmap.put("loginName",ShiroUtils.getLoginName());
		mmap.put("users",iSysUserService.selectUserList(new SysUser()));
	    return prefix + "/add";
	}

	/**
	 * 新增保存锅炉房值班记录
	 */
	@RequiresPermissions("system:ncGasDuty:add")
	@Log(title = "锅炉房值班记录", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public AjaxResult addSave(NcGasDuty ncGasDuty)
	{
		SysUser sysUser = ShiroUtils.getSysUser();
		ncGasDuty.setCreateBy(sysUser.getLoginName()); //设置查表人员
		//时间如果为空自动填充当前系统时间
		Date nowdata = ncGasDuty.getCreateTime();
		if(StringUtils.isNull(nowdata)){
			ncGasDuty.setCreateTime(new Date());
		}
		return toAjax(ncGasDutyService.insertNcGasDuty(ncGasDuty));
	}

	/**
	 * 修改锅炉房值班记录
	 */
	@GetMapping("/edit/{dutyId}")
	public String edit(@PathVariable("dutyId") Long dutyId, ModelMap mmap)
	{
		NcGasDuty ncGasDuty = ncGasDutyService.selectNcGasDutyById(dutyId);
		List<SysUser> sysUsers = iSysUserService.selectUserList(new SysUser());

		if(ncGasDuty != null && StringUtils.isNotNull(ncGasDuty.getNextDutyUser())){
			for (SysUser sysUser : sysUsers) {
				 if((""+sysUser.getUserId()) .equals(ncGasDuty.getNextDutyUser())){
				 	sysUser.setFlag(true);
				 	break;
				 }
			}
		}

		List<SysUser> sysUseres = iSysUserService.selectUserList(new SysUser());
		if(ncGasDuty != null && StringUtils.isNotNull(ncGasDuty.getOnDutyUser())){
			for (SysUser sysUser : sysUseres) {
				if((""+sysUser.getUserId()) .equals(ncGasDuty.getOnDutyUser())){
					sysUser.setFlag(true);
					break;
				}
			}
		}

		mmap.put("ncGasDuty", ncGasDuty);
        mmap.put("nextDutyUsers",sysUsers);
		mmap.put("onDutyUsers",sysUseres);


	    return prefix + "/edit";
	}

	/**
	 * 修改保存锅炉房值班记录
	 */
	@RequiresPermissions("system:ncGasDuty:edit")
	@Log(title = "锅炉房值班记录", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public AjaxResult editSave(NcGasDuty ncGasDuty)
	{
		return toAjax(ncGasDutyService.updateNcGasDuty(ncGasDuty));
	}

	/**
	 * 删除锅炉房值班记录
	 */
	@RequiresPermissions("system:ncGasDuty:remove")
	@Log(title = "锅炉房值班记录", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public AjaxResult remove(String ids)
	{
		return toAjax(ncGasDutyService.deleteNcGasDutyByIds(ids));
	}

}
