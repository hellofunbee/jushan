package com.ruoyi.web.controller.system;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.NcOrder;
import com.ruoyi.system.domain.NcOutputMilk;
import com.ruoyi.system.service.INcOrderService;
import com.ruoyi.system.service.INcOutputMilkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/system/ncCowsMilk/")
public class FrontMilkOutController extends BaseController {

    private String prontPrefix = "system/ncCowsMilk/front";
    private String prefix =  "system/ncCowsMilk";
    @Autowired
    private INcOutputMilkService ncOutputMilkService;

    @Autowired
    private INcOrderService ncOrderService;

    /**
     * 新牛舍管理
     */
    @GetMapping("/selectMilkOutNew")
    public String selectMilkOutNew(String beginTime, String endTime, ModelMap mmap, NcOutputMilk ncOutputMilk){
        if (StringUtils.isNotEmpty(beginTime)) {
            ncOutputMilk.getParams().put("beginTime", beginTime);
        }
        if (StringUtils.isNotEmpty(endTime)) {
            ncOutputMilk.getParams().put("endTime", endTime);
        }
        mmap.put("beginTime", beginTime);
        mmap.put("endTime", endTime);
        ncOutputMilkService.SelectNcOutMilkTotal(mmap,ncOutputMilk);
        return prontPrefix + "/ncCowsMilkNew";
    }

    /**
     * 牛舍管理
     * @param startTime
     * @param endTime
     * @param mmap
     * @param ncOutputMilk
     * @return
     */
    @GetMapping("/selectMilkOut")
    public String selectMilkOut(@RequestParam(value = "beginTime",required = false)String startTime, @RequestParam(value = "endTime",required = false)String endTime, ModelMap mmap, NcOutputMilk ncOutputMilk){

        if (StringUtils.isNotEmpty(startTime)) {
            ncOutputMilk.getParams().put("beginTime", startTime);
        }
        if (StringUtils.isNotEmpty(endTime)) {
            ncOutputMilk.getParams().put("endTime", endTime);
        }
        mmap.put("beginTime", startTime);
        mmap.put("endTime", endTime);
        ncOutputMilkService.SelectNcOutMilkTotal(mmap,ncOutputMilk);
        return prontPrefix + "/ncCowsMilk";
    }
    /**
     * 查询牛奶出库列表
     */


    @GetMapping("/milkOrder")
    public String ncOutputMilk(String type,ModelMap mmap)
    {
        mmap.put("outputType",type);
        mmap.put("orderType", 11);
        return prontPrefix + "/ncCowsMilkSort";
    }

    @PostMapping("/frontOrdeList")
    @ResponseBody
    public TableDataInfo list(NcOrder ncOrder) {
        startPage();
        List<NcOrder> list = ncOrderService.selectNcOrderList(ncOrder);
        return getDataTable(list);
    }

    @GetMapping("/frontMilkOrderInfo")
    public String frontMilkOrderInfo(String orderId,ModelMap mmap,String orderCode,String orderTime,String createBy )
    {
        mmap.put("orderId",orderId);
        mmap.put("orderCode",orderCode);
        mmap.put("orderTime",orderTime);
        mmap.put("createBy",createBy);
        return prontPrefix + "/ncCowsMilkOrderInfo";
    }

    @PostMapping("/frontOrdeListInfo")
    @ResponseBody
    public TableDataInfo orderlist(NcOrder ncOrder) {
        startPage();
        List<NcOutputMilk> list = ncOutputMilkService.selectNcoutputMilkOrderInfoByOrderId(ncOrder.getOrderId());
        return getDataTable(list);
    }


    @PostMapping("/milkQuery")
    @ResponseBody
    public TableDataInfo milkQuery(Integer dataSpan,String  beginTime, String endTime ,Integer cforderType){
        //

        List<Map> list = ncOutputMilkService.selectCountByTime(dataSpan,beginTime,endTime,cforderType);
        return getDataTable(list);
    }





}
