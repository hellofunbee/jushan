package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.Ztree;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.NcCai;
import com.ruoyi.system.domain.SysRole;
import com.ruoyi.system.service.INcCaiService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 蔬菜信息
 *
 * @author ruoyi
 */
@Controller
@RequestMapping("/system/cai")
public class NcCaiController extends BaseController {
    private String prefix = "system/cai";

    @Autowired
    private INcCaiService caiService;

    @RequiresPermissions("system:cai:view")
    @GetMapping()
    public String cai() {
        return prefix + "/cai";
    }

    @RequiresPermissions("system:cai:list")
    @PostMapping("/list")
    @ResponseBody
    public List<NcCai> list(NcCai cai) {
        List<NcCai> caiList = caiService.selectCaiList(cai);
        return caiList;
    }

    /**
     * 新增蔬菜
     */
    @GetMapping("/add/{parentId}")
    public String add(@PathVariable("parentId") Long parentId, ModelMap mmap) {
        mmap.put("cai", caiService.selectCaiById(parentId));
        return prefix + "/add";
    }

    /**
     * 新增保存蔬菜
     */
    @Log(title = "蔬菜管理", businessType = BusinessType.INSERT)
    @RequiresPermissions("system:cai:add")
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(@Validated NcCai cai) {
        /*if (UserConstants.DEPT_NAME_NOT_UNIQUE.equals(caiService.checkCaiNameUnique(cai)))
        {
            return error("新增蔬菜'" + cai.getCaiName() + "'失败，蔬菜名称已存在");
        }*/
        cai.setCreateBy(ShiroUtils.getLoginName());
        return toAjax(caiService.insertCai(cai));
    }

    /**
     * 修改
     */
    @GetMapping("/edit/{caiId}")
    public String edit(@PathVariable("caiId") Long caiId, ModelMap mmap) {
        NcCai cai = caiService.selectCaiById(caiId);
        if (StringUtils.isNotNull(cai) && 100L == caiId) {
            cai.setParentName("无");
        }
        mmap.put("cai", cai);
        return prefix + "/edit";
    }

    /**
     * 保存
     */
    @Log(title = "蔬菜管理", businessType = BusinessType.UPDATE)
    @RequiresPermissions("system:cai:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(@Validated NcCai cai) {
        /*if (UserConstants.DEPT_NAME_NOT_UNIQUE.equals(caiService.checkCaiNameUnique(cai)))
        {
            return error("修改蔬菜'" + cai.getCaiName() + "'失败，蔬菜名称已存在");
        }
        else*/
        if (cai.getParentId().equals(cai.getCaiId())) {
            return error("修改蔬菜'" + cai.getCaiName() + "'失败，上级蔬菜分类不能是自己");
        }
        cai.setUpdateBy(ShiroUtils.getLoginName());
        return toAjax(caiService.updateCai(cai));
    }

    /**
     * 删除
     */
    @Log(title = "蔬菜管理", businessType = BusinessType.DELETE)
    @RequiresPermissions("system:cai:remove")
    @GetMapping("/remove/{caiId}")
    @ResponseBody
    public AjaxResult remove(@PathVariable("caiId") Long caiId) {
        if (caiService.selectCaiCount(caiId) > 0) {
            return AjaxResult.warn("存在下级蔬菜,不允许删除");
        }
        if (caiService.checkCaiExistUser(caiId)) {
            return AjaxResult.warn("蔬菜存在用户,不允许删除");
        }
        return toAjax(caiService.deleteCaiById(caiId));
    }

    /**
     * 校验蔬菜名称
     */
    @PostMapping("/checkCaiNameUnique")
    @ResponseBody
    public String checkCaiNameUnique(NcCai cai) {
        return caiService.checkCaiNameUnique(cai);
    }

    /**
     * 选择蔬菜树
     */
    @GetMapping("/selectCaiTree/{caiId}")
    public String selectCaiTree(@PathVariable("caiId") Long caiId, ModelMap mmap, String parentIds) {
        mmap.put("cai", caiService.selectCaiById(caiId));
        mmap.put("parentIds", parentIds);
        return prefix + "/tree";
    }

    /**
     * 加载蔬菜列表树
     */
    @GetMapping("/treeData")
    @ResponseBody
    public List<Ztree> treeData(String caiIds) {
        NcCai cai = new NcCai();
        cai.getParams().put("caiIds", caiIds);
        List<Ztree> ztrees = caiService.selectCaiTree(cai);
        return ztrees;
    }

    /**
     * 加载角色蔬菜（数据权限）列表树
     */
    @GetMapping("/roleCaiTreeData")
    @ResponseBody
    public List<Ztree> caiTreeData(SysRole role) {
        List<Ztree> ztrees = caiService.roleCaiTreeData(role);
        return ztrees;
    }
}
