package com.ruoyi.common.utils;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

public class NcUtils {
    static int oneDay = 1000 * 3600 * 24;

    /*标准的code计算*/
    public static String getStandardCode(int size) {
        size = size + 1;
        if (size < 10) {
            return "00" + size;
        } else if (size < 100) {
            return "0" + size;
        } else {
            return "" + size;
        }

    }

    /*计划编号*/

    public static String getPlanCode() {

        return DateUtils.parseDateToStr("yyyyMMddHHmm", new Date()) + get4();
    }

    /*订单编号*/
    public static String getOrderCode() {
        return DateUtils.parseDateToStr("yyyyMMddHHmm", new Date()) + get4();
    }

    /*物资批号*/
    public static String getmeterialCode() {
        return DateUtils.parseDateToStr("yyyyMMddHHmm", new Date()) + get4();
    }

    /*饲料批次*/
    public static String getFeedCode() {
        return DateUtils.parseDateToStr("yyyyMMddHHmm", new Date()) + get4();
    }

    /*牛奶批次*/
    public static String getMilkCode() {
        return DateUtils.parseDateToStr("yyyyMMddHHmm", new Date()) + get4();
    }

    public static int differentDaysByDate(Date today, Date planDay) {
        int days = 1000;
        try {
            String d1 = DateUtils.parseDateToStr("yyyy-MM-dd", today);
            String d2 = DateUtils.parseDateToStr("yyyy-MM-dd", planDay);
            days = (int) ((parseDate(d1).getTime() - parseDate(d2).getTime()) / (1000 * 3600 * 24));

        } catch (Exception e) {
            e.printStackTrace();
        }
//        System.out.println("****************:" + days);
        return days;
    }


    public static Date parseDate(String date) throws ParseException {
        if (date.isEmpty()) {
            return null;
        }
        return new SimpleDateFormat("yyyy-MM-dd").parse(date);
    }

    /*获取map里面的int数值*/
    public static Integer getCount(Map m, String key) {
        if (m.get(key) != null) {
            Double d = Double.parseDouble("" + m.get(key));
            return d.intValue();
        } else {
            return 0;
        }
    }

    /*int 和 map 的 int 计算比例*/
    public static String getRate(Integer total, Map counts, String key) {
        Double t = new Double(total);
        Double c = new Double(NcUtils.getCount(counts, key));
        if (t == 0) {
            return "0.00";
        } else {
            DecimalFormat df = new DecimalFormat("0.00");//格式化，区小数后两位
            return df.format(c / t * 100);
        }

    }

    /*获取map里面的int数值*/
    public static Double getCountD(Map m, String key) {
        if (m.get(key) != null) {
            Double d = Double.parseDouble("" + m.get(key));
            return d;
        } else {
            return 0.0;
        }
    }

    private static String get4() {
        String a = "0123456789";
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < 5; i++) {
            int rand = (int) (Math.random() * a.length());
            sb.append(a.charAt(rand));
        }
        return "0" + sb.toString()+" ";
    }

    /*对象转为string*/
    public static String getStr(Object o) {
        if (o != null) {
            return o.toString();
        } else {
            return null;
        }
    }

    /*对象转为string*/
    public static Integer getInteger(Object o) {
        if (o == null) {
            return null;
        } else {
            try {
                Double d = Double.parseDouble(o.toString());
                return d.intValue();
            } catch (Exception e) {

            }
            return null;
        }
    }


    public static Double getDouble(Object o) {
        if (o == null) {
            return null;
        } else {
            try {
                return Double.parseDouble(o.toString());
            } catch (Exception e) {

            }
            return null;
        }
    }

    public static BigDecimal getBigDecimal(Object o) {
        if (o == null) {
            return null;
        } else {
            try {
                return new BigDecimal(o.toString());
            } catch (Exception e) {

            }
            return null;
        }
    }

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++)
            System.out.println(getOrderCode());
    }
}
