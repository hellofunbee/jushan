package com.ruoyi.common.constant;

/**
 * 通用常量信息
 *
 * @author ruoyi
 */
public class Constants {
    /**
     * UTF-8 字符集
     */
    public static final String UTF8 = "UTF-8";

    /**
     * 通用成功标识
     */
    public static final String SUCCESS = "0";

    /**
     * 通用失败标识
     */
    public static final String FAIL = "1";

    /**
     * 登录成功
     */
    public static final String LOGIN_SUCCESS = "Success";

    /**
     * 注销
     */
    public static final String LOGOUT = "Logout";

    /**
     * 登录失败
     */
    public static final String LOGIN_FAIL = "Error";

    /**
     * 自动去除表前缀
     */
    public static final String AUTO_REOMVE_PRE = "true";

    /**
     * 当前记录起始索引
     */
    public static final String PAGE_NUM = "pageNum";

    /**
     * 每页显示记录数
     */
    public static final String PAGE_SIZE = "pageSize";

    /**
     * 排序列
     */
    public static final String ORDER_BY_COLUMN = "orderByColumn";

    /**
     * 排序的方向 "desc" 或者 "asc".
     */
    public static final String IS_ASC = "isAsc";

    /*订单类型*/

    /**
     * 单子类型： 1：生产计划单 2：育苗计划单 3：订单记录单 4：检测记录单 5：菜房1：入库单 6：菜房1：出库单 7：菜房2：入库单 8：菜房2：出库单 9：植保记录单 10：牛舍入库单 11：牛舍：出库单
     * 12：鸡舍入库单 13：鸡舍出库单 14：牛舍诊疗单 15：牛舍免疫单 ：16：牛舍巡视单  17：鸡舍诊疗单 18：鸡舍免疫单 ：19：鸡舍巡视单
     */


    public static final Integer ORDER_PRODUCE = 1;
    public static final Integer ORDER_YUMIAO = 2;
    public static final Integer ORDER = 3;
    public static final Integer ORDER_CHECK = 4;
    public static final Integer ORDER_INPUT1 = 5;
    public static final Integer ORDER_OUTPUT1 = 6;

    public static final Integer ORDER_INPUT2 = 7;
    public static final Integer ORDER_OUTPUT3 = 8;
    public static final Integer ORDER_ZHIBAO = 9;
    public static final Integer ORDER_MILK_INPUT = 10;
    public static final Integer ORDER_MILK_OUTPUT = 11;
    public static final Integer ORDER_CHICKEN_INPUT = 12;
    public static final Integer ORDER_CHICKEN_OUTPUT = 13;
    public static final Integer ORDER_COW_ZHENLIAO = 14;
    public static final Integer ORDER_COW_MIANYI = 15;
    public static final Integer ORDER_COW_XUNSHI = 16;

    public static final Integer ORDER_CHICKEN_ZHENLIAO = 17;
    public static final Integer ORDER_CHICKEN_MIANYI = 18;
    public static final Integer ORDER_CHICKEN_XUNSHI = 19;

    public static final String cksid = "5b208367-5ff9-455d-9eaa-03dc9e44e533";
    public static final String ckuid = "37";

    /*物联网相关请求地址*/
//    public static final String WLW_BASE_URL = "http://39.107.119.69:8888";
//    public static final String WLW_BASE_URL = "http://127.0.0.1:8888";
    public static final String WLW_BASE_URL = "http://192.168.10.160:8888";
    public static final String listSensorChartInfo = "/listSensorChartInfo";


}
