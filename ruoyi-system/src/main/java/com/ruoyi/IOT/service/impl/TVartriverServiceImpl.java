package com.ruoyi.IOT.service.impl;

import com.ruoyi.IOT.service.ITVartriverService;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.system.domain.TVartriver;
import com.ruoyi.system.mapper.TVartriverMapper;
import com.ruoyi.system.service.ISysDeptService;
import com.ruoyi.system.service.ITPointService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

/**
 * 传感器数数据 服务层实现
 *
 * @author ruoyi
 * @date 2019-10-28
 */
@Service
public class TVartriverServiceImpl implements ITVartriverService {
    @Autowired
    private TVartriverMapper tVartriverMapper;

    @Autowired
    private ITPointService pointService;

    @Autowired
    private ISysDeptService sysDeptService;


    @Resource
    @Qualifier("primaryJdbcTemplate")
    private JdbcTemplate jdbcTemplate;


    /**
     * 查询传感器数数据信息
     *
     * @param id 传感器数数据ID
     * @return 传感器数数据信息
     */
    @Override
    @DataSource(value = DataSourceType.SLAVE)
    public TVartriver selectTVartriverById(Integer id) {
        return tVartriverMapper.selectTVartriverById(id);
    }

    @Override
    public List<TVartriver> selectTVartriverList(TVartriver tVartriver) {
        return tVartriverMapper.selectTVartriverList(tVartriver);
    }

    /**
     * 手动管理传感器数数据列表
     *
     * @param device 传感器数数据信息
     * @return 传感器数数据集合
     */
    @Override
    @DataSource(value = DataSourceType.SLAVE)
    public List<TVartriver> queryTVartriverList(TVartriver device) {
        List<TVartriver> deviceList = new ArrayList<>();

        //查找所有温室下的设备
        String sql = "SELECT p1.tp_name as parentName,p.tp_name ,p.deviceId FROM t_point p LEFT JOIN t_point p1 on p.tp_pid = p1.tp_id WHERE  p.tp_pid in (SELECT tp_id  FROM t_point  WHERE tp_type = 2 ) order by p.tp_order";
        List<Map<String, Object>> maps = jdbcTemplate.queryForList(sql);

        for (Map<String, Object> map : maps) {
            TVartriver theDevice = null;

            //设备所属温室
            String parentName = (String) map.get("parentName");
            //设备id
            String deviceId = (String) map.get("deviceId");
            //设备数据表后缀
            String replace = deviceId.replace(".", "");
            //设备数据所在表
            String table = "t_vartriver_" + replace;

            //查询设备对应数据表的首条数据 时间倒排
            String sql1 = " select id,deviceId, infoDataTime, channel1, channel2, channel3, channel4, channel5, channel6, channel7, channel8,channel17,createTime from " + table + " ORDER BY infoDataTime desc  LIMIT 0,1";

            try {
                //表数据为空时 jdbc查询抛出异常 无碍
                theDevice = jdbcTemplate.queryForObject(sql1, new BeanPropertyRowMapper<>(TVartriver.class));
            } catch (DataAccessException e) {
            }

            if (theDevice != null) {
                //查到的设备最新一条数据
                //设置对应温室
                theDevice.setParentName(parentName);
                //设置设备id
                theDevice.setDeviceId(deviceId);
                theDevice.setManualControl("0");
                deviceList.add(theDevice);

            } else {

                //该设备无数据 添加
                TVartriver vartriver = new TVartriver();
                vartriver.setDeviceId(deviceId);
                vartriver.setInfoDataTime(new Date());
                vartriver.setChannel1("");
                vartriver.setChannel2("");
                vartriver.setChannel3("");
                vartriver.setChannel4("");
                vartriver.setChannel5("");
                vartriver.setChannel6("");
                vartriver.setChannel7("");
                vartriver.setChannel8("");
                vartriver.setChannel17("");
                vartriver.setZuhe("1245");
                vartriver.setManualControl("0");
                vartriver.setInfoDataTime(new Date());
                vartriver.setParentName(parentName);
                vartriver.setCreateTime(new Date());
                deviceList.add(vartriver);

            }

        }

        //查询手动管理数据的所有记录
        List<TVartriver> tVartriverListDB = tVartriverMapper.selectTVartriverList(new TVartriver());


        for (TVartriver vartriver : deviceList) {
              vartriver.setId(-1);
              vartriver.setChannel1(vartriver.getChannel1() + "  =>  ");
              vartriver.setChannel2(vartriver.getChannel2() + "  =>  ");
              vartriver.setChannel3(vartriver.getChannel3() + "  =>  ");
              vartriver.setChannel4(vartriver.getChannel4() + "  =>  ");
              vartriver.setChannel5(vartriver.getChannel5() + "  =>  ");
              vartriver.setChannel6(vartriver.getChannel6() + "  =>  ");
              vartriver.setChannel7(vartriver.getChannel7() + "  =>  ");
              vartriver.setChannel8(vartriver.getChannel8() + "  =>  ");
              vartriver.setChannel17(vartriver.getChannel17() + "  =>  ");

            for (TVartriver vartriverDB : tVartriverListDB) {
                if ((vartriver.getDeviceId()).equals(vartriverDB.getDeviceId())){
                    vartriver.setId(vartriverDB.getId());
                    vartriver.setManualControl(vartriverDB.getManualControl());
                    vartriver.setZuhe(vartriverDB.getZuhe());
                    vartriver.setChannel1(vartriver.getChannel1() + vartriverDB.getChannel1());
                    vartriver.setChannel2(vartriver.getChannel2() + vartriverDB.getChannel2());
                    vartriver.setChannel3(vartriver.getChannel3() + vartriverDB.getChannel3());
                    vartriver.setChannel4(vartriver.getChannel4() + vartriverDB.getChannel4());
                    vartriver.setChannel5(vartriver.getChannel5() + vartriverDB.getChannel5());
                    vartriver.setChannel6(vartriver.getChannel6() + vartriverDB.getChannel6());
                    vartriver.setChannel7(vartriver.getChannel7() + vartriverDB.getChannel7());
                    vartriver.setChannel8(vartriver.getChannel8() + vartriverDB.getChannel8());
                    vartriver.setChannel17(vartriver.getChannel17() + vartriverDB.getChannel17());
                }
            }

        }
        return deviceList;
    }

    /**
     * 新增传感器数数据
     *
     * @param tVartriver 传感器数数据信息
     * @return 结果
     */
    @Override
    @DataSource(value = DataSourceType.SLAVE)
    public int insertTVartriver(TVartriver tVartriver) {
        return tVartriverMapper.insertTVartriver(tVartriver);
    }

    /**
     * 修改传感器数数据
     *
     * @param tVartriver 传感器数数据信息
     * @return 结果
     */
    @Override
    @DataSource(value = DataSourceType.SLAVE)
    public int updateTVartriver(TVartriver tVartriver) {

        TVartriver vartriver = tVartriverMapper.selectTVartriverById(tVartriver.getId());


        if (vartriver == null){
            tVartriver.setId(null);
            return tVartriverMapper.insertTVartriver(tVartriver);
        }else {
            return  tVartriverMapper.updateTVartriver(tVartriver);
        }
   }

    /**
     * 删除传感器数数据对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    @DataSource(value = DataSourceType.SLAVE)
    public int deleteTVartriverByIds(String ids) {
        return tVartriverMapper.deleteTVartriverByIds(Convert.toStrArray(ids));
    }

}
