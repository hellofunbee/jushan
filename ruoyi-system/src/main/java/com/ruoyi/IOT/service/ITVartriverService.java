package com.ruoyi.IOT.service;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.system.domain.TVartriver;
import java.util.List;

/**
 * 传感器数数据 服务层
 * 
 * @author ruoyi
 * @date 2019-10-28
 */
public interface ITVartriverService 
{
	/**
     * 查询传感器数数据信息
     * 
     * @param id 传感器数数据ID
     * @return 传感器数数据信息
     */
	@DataSource(value = DataSourceType.SLAVE)
	public TVartriver selectTVartriverById(Integer id);
	
	/**
     * 查询传感器数数据列表
     * 
     * @param tVartriver 传感器数数据信息
     * @return 传感器数数据集合
     */
	@DataSource(value = DataSourceType.SLAVE)
	public List<TVartriver> selectTVartriverList(TVartriver tVartriver);

	/**
	 * 手动管理传感器数数据列表
	 *
	 * @param tVartriver 传感器数数据信息
	 * @return 传感器数数据集合
	 */
	@DataSource(value = DataSourceType.SLAVE)
	public List<TVartriver> queryTVartriverList(TVartriver tVartriver);
	
	/**
     * 新增传感器数数据
     * 
     * @param tVartriver 传感器数数据信息
     * @return 结果
     */
	@DataSource(value = DataSourceType.SLAVE)
	public int insertTVartriver(TVartriver tVartriver);
	
	/**
     * 修改传感器数数据
     * 
     * @param tVartriver 传感器数数据信息
     * @return 结果
     */
	@DataSource(value = DataSourceType.SLAVE)
	public int updateTVartriver(TVartriver tVartriver);
		
	/**
     * 删除传感器数数据信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@DataSource(value = DataSourceType.SLAVE)
	public int deleteTVartriverByIds(String ids);
	
}
