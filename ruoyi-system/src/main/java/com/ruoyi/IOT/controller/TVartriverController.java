package com.ruoyi.IOT.controller;

import com.ruoyi.IOT.service.ITVartriverService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TVartriver;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 传感器数数据 信息操作处理
 *
 * @author ruoyi
 * @date 2019-10-28
 */
@Controller
@RequestMapping("/system/tVartriver")
public class TVartriverController extends BaseController {
    private String prefix = "system/tVartriver";

    @Autowired
    private ITVartriverService tVartriverService;

    @RequiresPermissions("system:tVartriver:view")
    @GetMapping()
    public String tVartriver() {
        return prefix + "/tVartriver";
    }

    /**
     * 查询传感器数数据列表
     */
    @RequiresPermissions("system:tVartriver:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(TVartriver tVartriver) {
        startPage();
        List<TVartriver> list = tVartriverService.selectTVartriverList(tVartriver);
        return getDataTable(list);
    }

    /**
     * 查询传感器数数据列表
     */
    @RequiresPermissions("system:tVartriver:list")
    @PostMapping("/query")
    @ResponseBody
    public TableDataInfo query(TVartriver tVartriver) {
        startPage();
        List<TVartriver> list = tVartriverService.queryTVartriverList(tVartriver);
        return getDataTable(list);
    }



    /**
     * 导出传感器数数据列表
     */
    @RequiresPermissions("system:tVartriver:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(TVartriver tVartriver) {
        List<TVartriver> list = tVartriverService.selectTVartriverList(tVartriver);
        ExcelUtil<TVartriver> util = new ExcelUtil<TVartriver>(TVartriver.class);
        return util.exportExcel(list, "tVartriver");
    }

    /**
     * 新增传感器数数据
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存传感器数数据
     */
    @RequiresPermissions("system:tVartriver:add")
    @Log(title = "传感器数数据", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(TVartriver tVartriver) {
        return toAjax(tVartriverService.insertTVartriver(tVartriver));
    }

    /**
     * 修改传感器数数据
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap) {

        String[] split = id.split(",");

        TVartriver tVartriver = tVartriverService.selectTVartriverById(Integer.parseInt(split[0]));

        if (tVartriver == null) {

            mmap.put("tVartriver", new TVartriver());
        } else {

            mmap.put("tVartriver", tVartriver);
        }
        mmap.put("id",split[0]);
        mmap.put("deviceId",split[1]);
        return prefix + "/edit";
    }

    /**
     * 修改保存传感器数数据
     */
    @RequiresPermissions("system:tVartriver:edit")
    @Log(title = "传感器数数据", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(TVartriver tVartriver) {
        return toAjax(tVartriverService.updateTVartriver(tVartriver));
    }

    /**
     * 删除传感器数数据
     */
    @RequiresPermissions("system:tVartriver:remove")
    @Log(title = "传感器数数据", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(tVartriverService.deleteTVartriverByIds(ids));
    }

}
