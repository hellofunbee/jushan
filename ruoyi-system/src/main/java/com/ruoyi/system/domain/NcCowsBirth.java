package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 产犊记录表 nc_cows_birth
 *
 * @author ruoyi
 * @date 2019-09-02
 */
public class NcCowsBirth extends BaseEntity
{
	private static final long serialVersionUID = 1L;

	/** 牛id */
	private Integer cowId;
	/** 发情时间 */
	@Excel(name="发情时间",width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
	private Date fqDate;
	/** 配种时间 */
	@Excel(name="配种时间",width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
	private Date pzDate;
	/** 产犊时间 */
	@Excel(name="产犊时间",width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
	private Date givebirthTime;
	/** 犊牛性别 */
	@Excel(name="来源",readConverterExp = "1=公,2=母")
	private Integer childGender;
	/** 耳号 */
	@Excel(name="耳号")
	private String childCode;
	/** 犊牛体重 */
	@Excel(name="犊牛体重")
	private BigDecimal childWeight;
	/** 淘汰时间 */
	@Excel(name="淘汰时间",width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
	private Date eliminateTime;
	/** 淘汰原因 */
	@Excel(name="淘汰原因")
	private String eliminateReason;
	/** 牛犊子id （主键） */
	private Integer birthId;


	public void setCowId(Integer cowId)
	{
		this.cowId = cowId;
	}

	public Integer getCowId()
	{
		return cowId;
	}
	public void setFqDate(Date fqDate)
	{
		this.fqDate = fqDate;
	}

	public Date getFqDate()
	{
		return fqDate;
	}
	public void setPzDate(Date pzDate)
	{
		this.pzDate = pzDate;
	}

	public Date getPzDate()
	{
		return pzDate;
	}
	public void setGivebirthTime(Date givebirthTime)
	{
		this.givebirthTime = givebirthTime;
	}

	public Date getGivebirthTime()
	{
		return givebirthTime;
	}
	public void setChildGender(Integer childGender)
	{
		this.childGender = childGender;
	}

	public Integer getChildGender()
	{
		return childGender;
	}
	public void setChildCode(String childCode)
	{
		this.childCode = childCode;
	}

	public String getChildCode()
	{
		return childCode;
	}
	public void setChildWeight(BigDecimal childWeight)
	{
		this.childWeight = childWeight;
	}

	public BigDecimal getChildWeight()
	{
		return childWeight;
	}
	public void setEliminateTime(Date eliminateTime)
	{
		this.eliminateTime = eliminateTime;
	}

	public Date getEliminateTime()
	{
		return eliminateTime;
	}
	public void setEliminateReason(String eliminateReason)
	{
		this.eliminateReason = eliminateReason;
	}

	public String getEliminateReason()
	{
		return eliminateReason;
	}
	public void setBirthId(Integer birthId)
	{
		this.birthId = birthId;
	}

	public Integer getBirthId()
	{
		return birthId;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("cowId", getCowId())
            .append("fqDate", getFqDate())
            .append("pzDate", getPzDate())
            .append("givebirthTime", getGivebirthTime())
            .append("childGender", getChildGender())
            .append("childCode", getChildCode())
            .append("childWeight", getChildWeight())
            .append("updateTime", getUpdateTime())
            .append("eliminateTime", getEliminateTime())
            .append("eliminateReason", getEliminateReason())
            .append("createTime", getCreateTime())
            .append("createBy", getCreateBy())
            .append("remark", getRemark())
            .append("birthId", getBirthId())
            .toString();
    }
}
