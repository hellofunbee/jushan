package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 农事记录表 nc_normal_works
 *
 * @author ruoyi
 * @date 2019-07-31
 */
public class NcNormalWorks extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 工作记录id
     */
    private Long nWorkId;
    /**
     * 工作名称
     */
    @Excel(name="工作名称")
    private String nWorckName;
    /**
     * 参与人员
     */
    @Excel(name="参与人员")
    private String workers;
    /**
     * 核验人
     */
    @Excel(name="核验人")
    private String checkUser;
    /**
     * 工作时长
     */
    @Excel(name="工作时长")
    private BigDecimal timeLength;
    /**
     * 工作内容
     */
    @Excel(name="工作内容")
    private String content;
    /**
     * 类型： 1：标准 2：计划 3：订单 4：菜房1 5：菜房2 6：检测 7：温室 8：牛舍 9：鸡舍 10：植保 11：兽医 12：物资仓储 13：锅炉房 14：育苗管理
     */
    private Integer workType;

    @Override
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    @Excel(name="日期",width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    @Excel(name="备注")
    private String remark;

    @Override
    public String getRemark() {
        return remark;
    }

    @Override
    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Long getnWorkId() {
        return nWorkId;
    }

    public void setnWorkId(Long nWorkId) {
        this.nWorkId = nWorkId;
    }

    public String getnWorckName() {
        return nWorckName;
    }

    public void setnWorckName(String nWorckName) {
        this.nWorckName = nWorckName;
    }

    public String getWorkers() {
        return workers;
    }

    public void setWorkers(String workers) {
        this.workers = workers;
    }

    public String getCheckUser() {
        return checkUser;
    }

    public void setCheckUser(String checkUser) {
        this.checkUser = checkUser;
    }

    public BigDecimal getTimeLength() {
        return timeLength;
    }

    public void setTimeLength(BigDecimal timeLength) {
        this.timeLength = timeLength;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getWorkType() {
        return workType;
    }

    public void setWorkType(Integer workType) {
        this.workType = workType;
    }
}
