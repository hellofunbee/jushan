package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 发芽率表 nc_seed_rate
 * 
 * @author ruoyi
 * @date 2019-09-16
 */
public class NcSeedRate extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	
	/** 出库id */
	private Long seedRateId;
	/** 蔬菜批次 */
	@Excel(name = "蔬菜批次")
	private String caiCode;
	/** 作物品种ID */
	private Long cropVarieties;
	/** 作物类别 */
	private String cropTypeCn;
	/** 作物名称 */
	@Excel(name = "作物名称")
	private String cropNameCn;
	/** 作物品种 */
	@Excel(name = "作物品种")
	private String cropVarietiesCn;
	/** 育苗盘编号 */
	@Excel(name = "育苗盘编号")
	private String seedCode;
	/** 育苗盘规格 */
	@Excel(name = "育苗盘规格")
	private String seedUnit;
	/** 育苗数量(株) */
	@Excel(name = "育苗数量(株)")
	private Integer amount;
	/** 播种种子数 */
	@Excel(name = "播种种子数")
	private String seedCount;
	/** 发芽/出苗数 */
	@Excel(name = "发芽/出苗数")
	private BigDecimal seedOut;
	/** 发芽率/出苗数 */
	@Excel(name = "发芽率/出苗数")
	private BigDecimal seedRate;

	@Override
	public String getCreateBy() {
		return createBy;
	}

	@Override
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	@Excel(name = "统计人")
	private String createBy;

	@Override
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	@Excel(name = "日期", width = 30, dateFormat = "yyyy-MM-dd")
	private Date createTime;

	@Override
	public String getRemark() {
		return remark;
	}

	@Override
	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Excel(name = "备注")
	private String remark;

	public void setSeedRateId(Long seedRateId) 
	{
		this.seedRateId = seedRateId;
	}

	public Long getSeedRateId() 
	{
		return seedRateId;
	}
	public void setCaiCode(String caiCode) 
	{
		this.caiCode = caiCode;
	}

	public String getCaiCode() 
	{
		return caiCode;
	}
	public void setCropVarieties(Long cropVarieties) 
	{
		this.cropVarieties = cropVarieties;
	}

	public Long getCropVarieties() 
	{
		return cropVarieties;
	}
	public void setCropTypeCn(String cropTypeCn) 
	{
		this.cropTypeCn = cropTypeCn;
	}

	public String getCropTypeCn() 
	{
		return cropTypeCn;
	}
	public void setCropNameCn(String cropNameCn) 
	{
		this.cropNameCn = cropNameCn;
	}

	public String getCropNameCn() 
	{
		return cropNameCn;
	}
	public void setCropVarietiesCn(String cropVarietiesCn) 
	{
		this.cropVarietiesCn = cropVarietiesCn;
	}

	public String getCropVarietiesCn() 
	{
		return cropVarietiesCn;
	}
	public void setSeedCode(String seedCode) 
	{
		this.seedCode = seedCode;
	}

	public String getSeedCode() 
	{
		return seedCode;
	}
	public void setSeedUnit(String seedUnit) 
	{
		this.seedUnit = seedUnit;
	}

	public String getSeedUnit() 
	{
		return seedUnit;
	}
	public void setAmount(Integer amount) 
	{
		this.amount = amount;
	}

	public Integer getAmount() 
	{
		return amount;
	}
	public void setSeedCount(String seedCount) 
	{
		this.seedCount = seedCount;
	}

	public String getSeedCount() 
	{
		return seedCount;
	}
	public void setSeedOut(BigDecimal seedOut) 
	{
		this.seedOut = seedOut;
	}

	public BigDecimal getSeedOut() 
	{
		return seedOut;
	}
	public void setSeedRate(BigDecimal seedRate) 
	{
		this.seedRate = seedRate;
	}

	public BigDecimal getSeedRate() 
	{
		return seedRate;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("seedRateId", getSeedRateId())
            .append("caiCode", getCaiCode())
            .append("cropVarieties", getCropVarieties())
            .append("cropTypeCn", getCropTypeCn())
            .append("cropNameCn", getCropNameCn())
            .append("cropVarietiesCn", getCropVarietiesCn())
            .append("seedCode", getSeedCode())
            .append("seedUnit", getSeedUnit())
            .append("amount", getAmount())
            .append("seedCount", getSeedCount())
            .append("seedOut", getSeedOut())
            .append("seedRate", getSeedRate())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("remark", getRemark())
            .toString();
    }
}
