package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.Date;
import java.util.List;

/**
 * 生产（育苗）阶段 镜像表 nc_stage_mirror
 *
 * @author ruoyi
 * @date 2019-09-02
 */
public class NcStageMirrorExport1 extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 镜像主键
     */
    private Long stageMirrorId;
    /**
     * 阶段id
     */
    @Excel(name = "阶段序号")
    private Long stageId;
    /**
     * 计划id
     */
    @Excel(name = "计划序号")
    private Long planId;
    /**
     * 标准id
     */
    @Excel(name = "标准序号")
    private Long standardId;
    /**
     * 阶段名称
     */
    @Excel(name = "阶段名称")
    private String stageName;
    /**
     * 阶段天数
     */
    @Excel(name = "阶段天数")
    private Integer stageDays;
    /**
     * 排序号
     */
    @Excel(name = "排序号")
    private Integer orderNum;
    /**
     * 阶段描述
     */
    @Excel(name = "阶段描述")
    private String stageDetail;


    private List<NcWorkMirror> works;

    /*1:完成 2：执行中 3：未进入*/
    @Excel(name = "阶段状态",readConverterExp = "1=完成,2=执行中,3=未进入")
    private String stageStatus;


    @Excel(name = "完成时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date finishTime;//完成时间


    @Excel(name = "计划执行时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date planTime;//阶段对应计划的计划执行时间

    private NcPlan plan;

    private Integer delayDays;

    private List<NcStageMirrorExport1> stages;

    private NcStandard standard;

    private String cssClass;

    private Date workDate;//该执行的时间（）
    private Date workEndDate;//该结束时间

    /*按照执行时间分组的工作*/
    private List<NcGroupWorkMirror> groupWorks;
    public NcStageMirrorExport1() {

    }

    public NcStageMirrorExport1(NcStage s, Long planId) {
        this.planId = planId;
        if (s != null) {
            this.stageId = s.getStageId();
            this.standardId = s.getStandardId();
            this.stageName = s.getStageName();
            this.stageDays = s.getStageDays();
            this.orderNum = s.getOrderNum();
            this.stageDetail = s.getStageDetail();
        }
    }

    public void setStageMirrorId(Long stageMirrorId) {
        this.stageMirrorId = stageMirrorId;
    }

    public Long getStageMirrorId() {
        return stageMirrorId;
    }

    public void setStageId(Long stageId) {
        this.stageId = stageId;
    }

    public Long getStageId() {
        return stageId;
    }

    public void setPlanId(Long planId) {
        this.planId = planId;
    }

    public Long getPlanId() {
        return planId;
    }

    public void setStandardId(Long standardId) {
        this.standardId = standardId;
    }

    public Long getStandardId() {
        return standardId;
    }

    public void setStageName(String stageName) {
        this.stageName = stageName;
    }

    public String getStageName() {
        return stageName;
    }

    public void setStageDays(Integer stageDays) {
        this.stageDays = stageDays;
    }

    public Integer getStageDays() {
        return stageDays;
    }

    public void setOrderNum(Integer orderNum) {
        this.orderNum = orderNum;
    }

    public Integer getOrderNum() {
        return orderNum;
    }

    public void setStageDetail(String stageDetail) {
        this.stageDetail = stageDetail;
    }

    public String getStageDetail() {
        return stageDetail;
    }

    public List<NcWorkMirror> getWorks() {
        return works;
    }

    public void setWorks(List<NcWorkMirror> works) {
        this.works = works;
    }

    public String getStageStatus() {
        return stageStatus;
    }

    public void setStageStatus(String stageStatus) {
        this.stageStatus = stageStatus;
    }

    public Date getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Date finishTime) {
        this.finishTime = finishTime;
    }

    public NcPlan getPlan() {
        return plan;
    }

    public void setPlan(NcPlan plan) {
        this.plan = plan;
    }

    public List<NcStageMirrorExport1> getStages() {
        return stages;
    }

    public void setStages(List<NcStageMirrorExport1> stages) {
        this.stages = stages;
    }

    public Integer getDelayDays() {
        return delayDays;
    }

    public void setDelayDays(Integer delayDays) {
        this.delayDays = delayDays;
    }

    public NcStandard getStandard() {
        return standard;
    }

    public void setStandard(NcStandard standard) {
        this.standard = standard;
    }

    public String getCssClass() {
        return cssClass;
    }

    public void setCssClass(String cssClass) {
        this.cssClass = cssClass;
    }

    public List<NcGroupWorkMirror> getGroupWorks() {
        return groupWorks;
    }

    public void setGroupWorks(List<NcGroupWorkMirror> groupWorks) {
        this.groupWorks = groupWorks;
    }

    public Date getWorkDate() {
        return workDate;
    }

    public void setWorkDate(Date workDate) {
        this.workDate = workDate;
    }

    public Date getWorkEndDate() {
        return workEndDate;
    }

    public void setWorkEndDate(Date workEndDate) {
        this.workEndDate = workEndDate;
    }

    public Date getPlanTime() {
        return planTime;
    }

    public void setPlanTime(Date planTime) {
        this.planTime = planTime;
    }
}
