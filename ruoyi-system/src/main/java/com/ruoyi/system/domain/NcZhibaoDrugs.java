package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 植保用药表 nc_zhibao_drugs
 *
 * @author ruoyi
 * @date 2019-09-06
 */
public class NcZhibaoDrugs extends BaseEntity
{
	private static final long serialVersionUID = 1L;

	/** 植保id */
	private Long zhibaoId;
	/** 药品批次号 */
	private String drugCode;
	/** 药品名称 */
	private String drugName;
	/** 施药方式：字典 1：根灌 2：页面喷洒  */
	private String executeType;
	/** 施药量 */
	private Integer drugAmount;
	/** 单位:1盒 2袋 3ml */
	private String unit;
	/**  */
	private Long zhibaoDrugsId;

	public String getYaoId() {
		return yaoId;
	}

	public void setYaoId(String yaoId) {
		this.yaoId = yaoId;
	}

	private String yaoId;

	public void setZhibaoId(Long zhibaoId)
	{
		this.zhibaoId = zhibaoId;
	}

	public Long getZhibaoId()
	{
		return zhibaoId;
	}
	public void setDrugCode(String drugCode)
	{
		this.drugCode = drugCode;
	}

	public String getDrugCode()
	{
		return drugCode;
	}
	public void setDrugName(String drugName)
	{
		this.drugName = drugName;
	}

	public String getDrugName()
	{
		return drugName;
	}
	public void setExecuteType(String executeType)
	{
		this.executeType = executeType;
	}

	public String getExecuteType()
	{
		return executeType;
	}
	public void setDrugAmount(Integer drugAmount)
	{
		this.drugAmount = drugAmount;
	}

	public Integer getDrugAmount()
	{
		return drugAmount;
	}
	public void setUnit(String unit)
	{
		this.unit = unit;
	}

	public String getUnit()
	{
		return unit;
	}
	public void setZhibaoDrugsId(Long zhibaoDrugsId)
	{
		this.zhibaoDrugsId = zhibaoDrugsId;
	}

	public Long getZhibaoDrugsId()
	{
		return zhibaoDrugsId;
	}

	public String toString() {
		return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
				.append("zhibaoId", getZhibaoId())
				.append("drugCode", getDrugCode())
				.append("drugName", getDrugName())
				.append("executeType", getExecuteType())
				.append("drugAmount", getDrugAmount())
				.append("unit", getUnit())
				.append("createBy", getCreateBy())
				.append("createTime", getCreateTime())
				.append("remark", getRemark())
				.append("zhibaoDrugsId", getZhibaoDrugsId())
				.toString();
	}
}
