package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 物联网节点表 t_point
 * 
 * @author ruoyi
 * @date 2019-09-19
 */
public class TPoint extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	/*唯一id*/
	private String uuid;
	/** 树id */
	private Long tpId;
	/** 分类树枝名称 */
	private String tpName;
	/** 类型 1 站点 2 分类 3 设备 */
	private Long tpType;
	/** 父级id */
	private Long tpPid;
	/** 状态 */
	private Long tpState;
	/** 顺序 */
	private Long tpOrder;
	/** 时间 */
	private String tpTime;
	/** 该设备的ip */
	private String ip;
	/** 该设备端口 */
	private Integer port;
	/** 添加设备者id */
	private Long uid;
	/** 设备id */
	private String deviceId;
	/** 这个设备的监管者等等的id */
	private String tRole;
	/** 纬度 */
	private Double x;
	/** 经度 */
	private Double y;
	/** 上报ip的时间 */
	private String infoiptime;
	/** 缩放级别 */
	private Double zoom;
	/** 排序 */
	private Integer tpIndex;
	/** 生产者id */
	private Long produce;
	/** 监管者 */
	private Long superviser;

	public void setTpId(Long tpId)
	{
		this.tpId = tpId;
	}

	public Long getTpId()
	{
		return tpId;
	}
	public void setTpName(String tpName) 
	{
		this.tpName = tpName;
	}

	public String getTpName() 
	{
		return tpName;
	}
	public void setTpType(Long tpType) 
	{
		this.tpType = tpType;
	}

	public Long getTpType() 
	{
		return tpType;
	}
	public void setTpPid(Long tpPid) 
	{
		this.tpPid = tpPid;
	}

	public Long getTpPid() 
	{
		return tpPid;
	}
	public void setTpState(Long tpState) 
	{
		this.tpState = tpState;
	}

	public Long getTpState() 
	{
		return tpState;
	}
	public void setTpOrder(Long tpOrder) 
	{
		this.tpOrder = tpOrder;
	}

	public Long getTpOrder() 
	{
		return tpOrder;
	}
	public void setTpTime(String tpTime) 
	{
		this.tpTime = tpTime;
	}

	public String getTpTime() 
	{
		return tpTime;
	}
	public void setIp(String ip) 
	{
		this.ip = ip;
	}

	public String getIp() 
	{
		return ip;
	}
	public void setPort(Integer port) 
	{
		this.port = port;
	}

	public Integer getPort() 
	{
		return port;
	}
	public void setUid(Long uid) 
	{
		this.uid = uid;
	}

	public Long getUid() 
	{
		return uid;
	}
	public void setDeviceId(String deviceId) 
	{
		this.deviceId = deviceId;
	}

	public String getDeviceId() 
	{
		return deviceId;
	}
	public void setTRole(String tRole) 
	{
		this.tRole = tRole;
	}

	public String getTRole() 
	{
		return tRole;
	}
	public void setX(Double x) 
	{
		this.x = x;
	}

	public Double getX() 
	{
		return x;
	}
	public void setY(Double y) 
	{
		this.y = y;
	}

	public Double getY() 
	{
		return y;
	}
	public void setInfoiptime(String infoiptime) 
	{
		this.infoiptime = infoiptime;
	}

	public String getInfoiptime() 
	{
		return infoiptime;
	}
	public void setZoom(Double zoom) 
	{
		this.zoom = zoom;
	}

	public Double getZoom() 
	{
		return zoom;
	}
	public void setTpIndex(Integer tpIndex) 
	{
		this.tpIndex = tpIndex;
	}

	public Integer getTpIndex() 
	{
		return tpIndex;
	}
	public void setProduce(Long produce) 
	{
		this.produce = produce;
	}

	public Long getProduce() 
	{
		return produce;
	}
	public void setSuperviser(Long superviser) 
	{
		this.superviser = superviser;
	}

	public Long getSuperviser() 
	{
		return superviser;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("tpId", getTpId())
            .append("tpName", getTpName())
            .append("tpType", getTpType())
            .append("tpPid", getTpPid())
            .append("tpState", getTpState())
            .append("tpOrder", getTpOrder())
            .append("tpTime", getTpTime())
            .append("ip", getIp())
            .append("port", getPort())
            .append("uid", getUid())
            .append("deviceId", getDeviceId())
            .append("tRole", getTRole())
            .append("x", getX())
            .append("y", getY())
            .append("infoiptime", getInfoiptime())
            .append("zoom", getZoom())
            .append("tpIndex", getTpIndex())
            .append("produce", getProduce())
            .append("superviser", getSuperviser())
            .toString();
    }
}
