package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 兽医诊疗用药表 nc_zhenliao_drugs
 *
 * @author ruoyi
 * @date 2019-09-05
 */
public class NcZhenliaoDrugs extends BaseEntity
{
	private static final long serialVersionUID = 1L;

	/** 诊疗id */
	@Excel(name="诊疗序号")
	private Long zhenliaoId;
	/** 诊疗id */
	private Long zlId;

	/** 药品Id */
	private Long drugId;
	/** 药品批次号 */
	@Excel(name="药品批次号")
	private String drugCode;
	/** 药品名称 */
	@Excel(name="药品名称")
	private String drugName;
	/** 用药方式：字典 1：注射2：喂食 */
	@Excel(name="用药方式",dictType = "animalExecuteType")
	private Integer executeType;
	/** 喂药量 */
	@Excel(name="喂药量")
	private Integer drugAmount;
	/** 单位 */
	@Excel(name="用药方式",dictType = "unitType")
	private String unit;




	public Long getZhenliaoId() {
		return zhenliaoId;
	}

	public void setZhenliaoId(Long zhenliaoId) {
		this.zhenliaoId = zhenliaoId;
	}

	public NcZhenliaoDrugs() {
	}

	public NcZhenliaoDrugs(Long drugId,  Integer executeType, Integer drugAmount, String unit) {
		this.drugId = drugId;
		this.drugName = drugName;
		this.executeType = executeType;
		this.drugAmount = drugAmount;
		this.unit = unit;
	}

	public NcZhenliaoDrugs(Long drugId, String drugCode, String drugName, Integer executeType, Integer drugAmount, String unit,Long zhenliaoId) {
		this.drugId = drugId;
		this.drugCode = drugCode;
		this.drugName = drugName;
		this.executeType = executeType;
		this.drugAmount = drugAmount;
		this.unit = unit;
		this.zhenliaoId = zhenliaoId;
	}

	public Long getDrugId() {
		return drugId;
	}

	public void setDrugId(Long drugId) {
		this.drugId = drugId;
	}

	public void setZlId(Long zlId)
	{
		this.zlId = zlId;
	}

	public Long getZlId()
	{
		return zlId;
	}
	public void setDrugCode(String drugCode)
	{
		this.drugCode = drugCode;
	}

	public String getDrugCode()
	{
		return drugCode;
	}
	public void setDrugName(String drugName)
	{
		this.drugName = drugName;
	}

	public String getDrugName()
	{
		return drugName;
	}
	public void setExecuteType(Integer executeType)
	{
		this.executeType = executeType;
	}

	public Integer getExecuteType()
	{
		return executeType;
	}
	public void setDrugAmount(Integer drugAmount)
	{
		this.drugAmount = drugAmount;
	}

	public Integer getDrugAmount()
	{
		return drugAmount;
	}
	public void setUnit(String unit)
	{
		this.unit = unit;
	}

	public String getUnit()
	{
		return unit;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("zlId", getZlId())
            .append("drugCode", getDrugCode())
            .append("drugName", getDrugName())
            .append("executeType", getExecuteType())
            .append("drugAmount", getDrugAmount())
            .append("unit", getUnit())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("remark", getRemark())
            .toString();
    }
}
