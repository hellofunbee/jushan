package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.annotation.Excel.Type;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 物资仓储出入记录表 nc_goods_log
 *
 * @author ruoyi
 * @date 2019-08-06
 */
public class NcGoodsLog extends BaseEntity
{
	private static final long serialVersionUID = 1L;

	/** 仓储出库记录id */
	@Excel(name = "编号", prompt = "编号")
	private Long goodsLogId;
	/** 物资id */
	private Long goodsId;
	/** 出库数量 */
	@Excel(name = "出库数量", prompt = "出库数量")
	private Integer outputAmount;
	/** 库存 */
	@Excel(name = "库存", prompt = "库存")
	private Integer stockAmout;
	/** 领用人 */
	private String receiver;
	@Excel(name = "物资名称", prompt = "物资名称")
	private String meterialName;
	@Excel(name = "物资批次", prompt = "物资批次")
	private String goodsCode;
	@Excel(name = "单位", prompt = "单位")
	private String unit;

	private Integer inputAmount;

	private String createBy;

	private String purchaser;

	private Long meterialId;

	@Excel(name = "时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss", type = Type.EXPORT)
	private Date createTime;


	@Excel(name = "采购人", prompt = "采购人")
	private String purchaserName;
	@Excel(name = "入库人", prompt = "入库人")
	private String createByName;
	private String receiverName;

	@Excel(name="备注")
	private String remark;

	@Override
	public String getRemark() {
		return remark;
	}

	@Override
	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public Date getCreateTime() {
		return createTime;
	}

	@Override
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getReceiverName() {
		return receiverName;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public String getPurchaserName() {
		return purchaserName;
	}

	public void setPurchaserName(String purchaserName) {
		this.purchaserName = purchaserName;
	}

	public String getCreateByName() {
		return createByName;
	}

	public void setCreateByName(String createByName) {
		this.createByName = createByName;
	}

	public Long getMeterialId() {
		return meterialId;
	}

	public void setMeterialId(Long meterialId) {
		this.meterialId = meterialId;
	}

	public String getPurchaser() {
		return purchaser;
	}

	public void setPurchaser(String purchaser) {
		this.purchaser = purchaser;
	}

	public String getCreateBy() {
		return createBy;
	}


	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Integer getInputAmount() {
		return inputAmount;
	}

	public void setInputAmount(Integer inputAmount) {
		this.inputAmount = inputAmount;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getGoodsCode() {
		return goodsCode;
	}

	public void setGoodsCode(String goodsCode) {
		this.goodsCode = goodsCode;
	}

	public String getMeterialName() {
		return meterialName;
	}

	public void setMeterialName(String meterialName) {
		this.meterialName = meterialName;
	}

	public void setGoodsLogId(Long goodsLogId)
	{
		this.goodsLogId = goodsLogId;
	}

	public Long getGoodsLogId()
	{
		return goodsLogId;
	}
	public void setGoodsId(Long goodsId)
	{
		this.goodsId = goodsId;
	}

	public Long getGoodsId()
	{
		return goodsId;
	}
	public void setOutputAmount(Integer outputAmount)
	{
		this.outputAmount = outputAmount;
	}

	public Integer getOutputAmount()
	{
		return outputAmount;
	}
	public void setStockAmout(Integer stockAmout)
	{
		this.stockAmout = stockAmout;
	}

	public Integer getStockAmout()
	{
		return stockAmout;
	}
	public void setReceiver(String receiver)
	{
		this.receiver = receiver;
	}

	public String getReceiver()
	{
		return receiver;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("goodsLogId", getGoodsLogId())
            .append("goodsId", getGoodsId())
            .append("outputAmount", getOutputAmount())
            .append("stockAmout", getStockAmout())
            .append("receiver", getReceiver())
            .append("createTime", getCreateTime())
            .append("createBy", getCreateBy())
            .append("remark", getRemark())
            .toString();
    }
}
