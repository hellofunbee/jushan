package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.annotation.Excel.Type;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 牛奶入库表 nc_input_milk
 *
 * @author ruoyi
 * @date 2019-09-11
 */
public class NcInputMilk extends BaseEntity
{
	private static final long serialVersionUID = 1L;

	/** 入库id */
	@Excel(name = "编号", prompt = "编号")
	private Integer inputId;
	/** 牛奶批次 */
	@Excel(name = "牛奶批次", prompt = "牛奶批次")
	private String inputCode;
	/** 入库数量（kg） */
	private BigDecimal inputAmout;
	/** 剩余量 */
	@Excel(name = "库存", prompt = "库存")
	private BigDecimal leftAmout;
	/** 瓶数 */
	private Integer bottleAmount;
	/** 挤奶人员 */
	private String milkman;
	/** 消毒人员 */
	private String disinfector;

	//挤奶 消毒 入库经手人
	@Excel(name = "挤奶人员", prompt = "挤奶人员")
	private String milkmanName;
	@Excel(name = "消毒人员", prompt = "消毒人员")
	private String disinfectorName;

	/** 入库时间 */
	@Excel(name = "入库时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss", type = Type.EXPORT)
	private Date inputTime;
	/** 入库类型 ：字典 */
	private Integer inputType;
	/** 入库经手人 */
	@Excel(name = "入库经手人", prompt = "入库经手人")
	private String inputUserName;

	private String inputUser;

	private String orderId;



	public String getMilkmanName() {
		return milkmanName;
	}

	public void setMilkmanName(String milkmanName) {
		this.milkmanName = milkmanName;
	}

	public String getDisinfectorName() {
		return disinfectorName;
	}

	public void setDisinfectorName(String disinfectorName) {
		this.disinfectorName = disinfectorName;
	}

	public String getInputUserName() {
		return inputUserName;
	}

	public void setInputUserName(String inputUserName) {
		this.inputUserName = inputUserName;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public void setInputId(Integer inputId)
	{
		this.inputId = inputId;
	}

	public Integer getInputId()
	{
		return inputId;
	}
	public void setInputCode(String inputCode)
	{
		this.inputCode = inputCode;
	}

	public String getInputCode()
	{
		return inputCode;
	}
	public void setInputAmout(BigDecimal inputAmout)
	{
		this.inputAmout = inputAmout;
	}

	public BigDecimal getInputAmout()
	{
		return inputAmout;
	}
	public void setLeftAmout(BigDecimal leftAmout)
	{
		this.leftAmout = leftAmout;
	}

	public BigDecimal getLeftAmout()
	{
		return leftAmout;
	}
	public void setBottleAmount(Integer bottleAmount)
	{
		this.bottleAmount = bottleAmount;
	}

	public Integer getBottleAmount()
	{
		return bottleAmount;
	}
	public void setMilkman(String milkman)
	{
		this.milkman = milkman;
	}

	public String getMilkman()
	{
		return milkman;
	}
	public void setDisinfector(String disinfector)
	{
		this.disinfector = disinfector;
	}

	public String getDisinfector()
	{
		return disinfector;
	}
	public void setInputTime(Date inputTime)
	{
		this.inputTime = inputTime;
	}

	public Date getInputTime()
	{
		return inputTime;
	}
	public void setInputType(Integer inputType)
	{
		this.inputType = inputType;
	}

	public Integer getInputType()
	{
		return inputType;
	}
	public void setInputUser(String inputUser)
	{
		this.inputUser = inputUser;
	}

	public String getInputUser()
	{
		return inputUser;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("inputId", getInputId())
            .append("inputCode", getInputCode())
            .append("inputAmout", getInputAmout())
            .append("leftAmout", getLeftAmout())
            .append("bottleAmount", getBottleAmount())
            .append("milkman", getMilkman())
            .append("disinfector", getDisinfector())
            .append("inputTime", getInputTime())
            .append("inputType", getInputType())
            .append("inputUser", getInputUser())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("remark", getRemark())
            .toString();
    }
}
