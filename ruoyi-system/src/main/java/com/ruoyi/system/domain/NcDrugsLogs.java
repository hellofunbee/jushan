package com.ruoyi.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.Date;

/**
 * 兽药出库记录表 nc_drugs_logs
 * 
 * @author ruoyi
 * @date 2019-09-02
 */
public class NcDrugsLogs extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	
	/** 兽药出库记录id */
	private Long drugLogId;
	/** 兽药id */
	private Long drugId;

	/** 药品批次 */
	@Excel(name="药品批次")
	private String drugCode;
	/**
	 * 兽药名称
	 */
	@Excel(name="兽药名称")
	private String drugName;

	/** 功能主治 */
	@Excel(name="功能主治")
	private String mainFunction;
	/**
	 * 兽药生产厂商
	 */
	@Excel(name="兽药生产厂商")
	private String factory;



	/** 出库数量 */
	@Excel(name="出库数量")
	private Integer outputAmount;
	/** 单位 */
	@Excel(name="单位",dictType = "unitType")
	private String unit;
	/** 领用人 */
	@Excel(name="领取人")
	private String receiver;

	/** 创建者 */
	@Excel(name="创建者")
	private String createBy;


	/** 创建时间 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@Excel(name="巡视时间")
	private Date createTime;

	/** 备注 */
	@Excel(name="备注")
	private String remark;

	public void setDrugLogId(Long drugLogId) 
	{
		this.drugLogId = drugLogId;
	}

	public String getDrugCode() {
		return drugCode;
	}

	public void setDrugCode(String drugCode) {
		this.drugCode = drugCode;
	}

	public String getMainFunction() {
		return mainFunction;
	}

	public void setMainFunction(String mainFunction) {
		this.mainFunction = mainFunction;
	}

	public Long getDrugLogId()
	{
		return drugLogId;
	}
	public void setDrugId(Long drugId) 
	{
		this.drugId = drugId;
	}

	public String getDrugName() {
		return drugName;
	}

	public void setDrugName(String drugName) {
		this.drugName = drugName;
	}

	public String getFactory() {
		return factory;
	}

	public void setFactory(String factory) {
		this.factory = factory;
	}

	public Long getDrugId()
	{
		return drugId;
	}
	public void setOutputAmount(Integer outputAmount) 
	{
		this.outputAmount = outputAmount;
	}

	public Integer getOutputAmount() 
	{
		return outputAmount;
	}
	public void setUnit(String unit) 
	{
		this.unit = unit;
	}

	public String getUnit() 
	{
		return unit;
	}
	public void setReceiver(String receiver) 
	{
		this.receiver = receiver;
	}

	public String getReceiver() 
	{
		return receiver;
	}

	@Override
	public String getCreateBy() {
		return createBy;
	}

	@Override
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	@Override
	public Date getCreateTime() {
		return createTime;
	}

	@Override
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Override
	public String getRemark() {
		return remark;
	}

	@Override
	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("drugLogId", getDrugLogId())
            .append("drugId", getDrugId())
            .append("outputAmount", getOutputAmount())
            .append("unit", getUnit())
            .append("createTime", getCreateTime())
            .append("createBy", getCreateBy())
            .append("receiver", getReceiver())
            .append("remark", getRemark())
            .toString();
    }
}
