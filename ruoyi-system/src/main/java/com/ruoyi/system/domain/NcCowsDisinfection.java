package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.domain.BaseEntity;
import java.util.Date;

/**
 * 消毒记录（牛舍）表 nc_cows_disinfection
 *
 * @author ruoyi
 * @date 2019-09-04
 */
public class NcCowsDisinfection extends BaseEntity
{
	private static final long serialVersionUID = 1L;

	/** 消毒主键id */
	private Long dfId;
	/** 消毒时间 */
	@Excel(name="消毒时间",width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
	private Date dfDate;
	/** 药品批次 */
	@Excel(name="药品批次")
	private String dfCode;
	/** 消毒场所 */
	@Excel(name="消毒场所")
	private String dfPlace;
	/** 消毒药名称 */
	@Excel(name="消毒药名称")
	private String dfName;
	/** 使用量 */
	@Excel(name="使用量")
	private String dfUsed;
	/** 使用浓度 */
	@Excel(name="使用浓度")
	private String dfDensity;
	/** 消毒方法 */
	@Excel(name="消毒方法",readConverterExp = "1=预防消毒,2=临时消毒,3=终止消毒")
	private Long dfType;
	/** 消毒操作员 */
	@Excel(name="消毒操作员")
	private String dfUser;
	/** 管理类型 1:鸡舍 2：牛舍 */
	private Integer managerType;

	@Excel(name="备注")
	private String remark;
	/** 与药品关联的id */
	private Long yaoId;

	private Long workNoticeId;

	@Override
	public String getRemark() {
		return remark;
	}

	@Override
	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Long getWorkNoticeId() {
		return workNoticeId;
	}

	public void setWorkNoticeId(Long workNoticeId) {
		this.workNoticeId = workNoticeId;
	}

	public void setDfId(Long dfId)
	{
		this.dfId = dfId;
	}

	public Long getDfId()
	{
		return dfId;
	}
	public void setDfDate(Date dfDate)
	{
		this.dfDate = dfDate;
	}

	public Date getDfDate()
	{
		return dfDate;
	}
	public void setDfCode(String dfCode)
	{
		this.dfCode = dfCode;
	}

	public String getDfCode()
	{
		return dfCode;
	}
	public void setDfPlace(String dfPlace)
	{
		this.dfPlace = dfPlace;
	}

	public String getDfPlace()
	{
		return dfPlace;
	}
	public void setDfName(String dfName)
	{
		this.dfName = dfName;
	}

	public String getDfName()
	{
		return dfName;
	}
	public void setDfUsed(String dfUsed)
	{
		this.dfUsed = dfUsed;
	}

	public String getDfUsed()
	{
		return dfUsed;
	}
	public void setDfDensity(String dfDensity)
	{
		this.dfDensity = dfDensity;
	}

	public String getDfDensity()
	{
		return dfDensity;
	}
	public void setDfType(Long dfType)
	{
		this.dfType = dfType;
	}

	public Long getDfType()
	{
		return dfType;
	}
	public void setDfUser(String dfUser)
	{
		this.dfUser = dfUser;
	}

	public String getDfUser()
	{
		return dfUser;
	}
	public void setManagerType(Integer managerType)
	{
		this.managerType = managerType;
	}

	public Integer getManagerType()
	{
		return managerType;
	}
	public void setYaoId(Long yaoId)
	{
		this.yaoId = yaoId;
	}

	public Long getYaoId()
	{
		return yaoId;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("dfId", getDfId())
            .append("dfDate", getDfDate())
            .append("dfCode", getDfCode())
            .append("dfPlace", getDfPlace())
            .append("dfName", getDfName())
            .append("dfUsed", getDfUsed())
            .append("dfDensity", getDfDensity())
            .append("dfType", getDfType())
            .append("dfUser", getDfUser())
            .append("managerType", getManagerType())
            .append("remark", getRemark())
            .append("yaoId", getYaoId())
            .toString();
    }
}
