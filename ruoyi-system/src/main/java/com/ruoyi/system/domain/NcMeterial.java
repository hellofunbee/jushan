package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * 农资表 sys_dept
 *
 * @author ruoyi
 */
public class NcMeterial extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 农资ID
     */
    private Long meterialId;

    /**
     * 父农资ID
     */
    private Long parentId;

    /**
     * 祖级列表
     */
    private String ancestors;

    /**
     * 农资名称
     */
    private String meterialName;

    /**
     * 农资编码
     */
    private String code;

    /**
     * 显示顺序
     */
    private String orderNum;

    /**
     * 负责人
     */
    private Integer level;


    /**
     * 删除标志（0代表存在 2代表删除）
     */
    private String delFlag;

    /**
     * 父农资名称
     */
    private String parentName;
    /**
     * 和nc_goods_log 关联的时间
     */

    private String logCreateTime;
    /**
     *和nc_goods_log 关联的数量
     */
    private Integer amount;

    public NcMeterial() {
    }

    public Long getMeterialId() {
        return meterialId;
    }

    public void setMeterialId(Long meterialId) {
        this.meterialId = meterialId;
    }

    public String getMeterialName() {
        return meterialName;
    }

    public void setMeterialName(String meterialName) {
        this.meterialName = meterialName;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getAncestors() {
        return ancestors;
    }

    public void setAncestors(String ancestors) {
        this.ancestors = ancestors;
    }

    @NotBlank(message = "农资名称不能为空")
    @Size(min = 0, max = 30, message = "农资名称长度不能超过30个字符")


    @NotBlank(message = "显示顺序不能为空")
    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


    @Override
    public String toString() {
        return "NcMeterial{" +
                "meterialId=" + meterialId +
                ", parentId=" + parentId +
                ", ancestors='" + ancestors + '\'' +
                ", meterialName='" + meterialName + '\'' +
                ", orderNum='" + orderNum + '\'' +
                ", level=" + level +
                ", code='" + code + '\'' +
                ", delFlag='" + delFlag + '\'' +
                ", parentName='" + parentName + '\'' +
                '}';
    }

    public String getLogCreateTime() {
        return logCreateTime;
    }

    public void setLogCreateTime(String logCreateTime) {
        this.logCreateTime = logCreateTime;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }
}
