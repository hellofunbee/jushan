package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.domain.BaseEntity;
import java.util.Date;

/**
 * 设备数值表 t_vartriver_10002023
 * 
 * @author ruoyi
 * @date 2019-10-28
 */
public class TVartriver extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	
	/** 记录id */
	private Integer id;
	/** 设备id */
	private String deviceId;
	/** 上报时间 */
	private Date infoDataTime;
	/** 上报的channel */
	private String channel1;
	/**  */
	private String channel2;
	/**  */
	private String channel3;
	/**  */
	private String channel4;
	/**  */
	private String channel5;
	/**  */
	private String channel6;
	/**  */
	private String channel7;
	/**  */
	private String channel8;
	/**  */
	private String channel17;

	/**手动控制*/
	private String manualControl;
	/**组合*/
	private String zuhe;


	private String parentName;

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public Integer getId()
	{
		return id;
	}
	public void setDeviceId(String deviceId) 
	{
		this.deviceId = deviceId;
	}

	public String getDeviceId() 
	{
		return deviceId;
	}
	public void setInfoDataTime(Date infoDataTime) 
	{
		this.infoDataTime = infoDataTime;
	}

	public Date getInfoDataTime() 
	{
		return infoDataTime;
	}
	public void setChannel1(String channel1) 
	{
		this.channel1 = channel1;
	}

	public String getChannel1() 
	{
		return channel1;
	}
	public void setChannel2(String channel2) 
	{
		this.channel2 = channel2;
	}

	public String getChannel2() 
	{
		return channel2;
	}
	public void setChannel3(String channel3) 
	{
		this.channel3 = channel3;
	}

	public String getChannel3() 
	{
		return channel3;
	}
	public void setChannel4(String channel4) 
	{
		this.channel4 = channel4;
	}

	public String getChannel4() 
	{
		return channel4;
	}
	public void setChannel5(String channel5) 
	{
		this.channel5 = channel5;
	}

	public String getChannel5() 
	{
		return channel5;
	}
	public void setChannel6(String channel6) 
	{
		this.channel6 = channel6;
	}

	public String getChannel6() 
	{
		return channel6;
	}
	public void setChannel7(String channel7) 
	{
		this.channel7 = channel7;
	}

	public String getChannel7() 
	{
		return channel7;
	}
	public void setChannel8(String channel8) 
	{
		this.channel8 = channel8;
	}

	public String getChannel8() {
		return channel8;
	}

    public String getManualControl() {
        return manualControl;
    }

    public void setManualControl(String manualControl) {
        this.manualControl = manualControl;
    }

    public String getZuhe() {
        return zuhe;
    }

    public void setZuhe(String zuhe) {
        this.zuhe = zuhe;
    }

	public String getChannel17() {
		return channel17;
	}

	public void setChannel17(String channel17) {
		this.channel17 = channel17;
	}

	public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("deviceId", getDeviceId())
            .append("infoDataTime", getInfoDataTime())
            .append("channel1", getChannel1())
            .append("channel2", getChannel2())
            .append("channel3", getChannel3())
            .append("channel4", getChannel4())
            .append("channel5", getChannel5())
            .append("channel6", getChannel6())
            .append("channel7", getChannel7())
            .append("channel8", getChannel8())
            .append("createTime", getCreateTime())
            .toString();
    }
}
