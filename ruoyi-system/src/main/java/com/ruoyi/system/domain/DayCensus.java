package com.ruoyi.system.domain;

//日统计
public class DayCensus {
    private int cunAmount;//存栏数

    public int getCunAmount() {
        return cunAmount;
    }

    public void setCunAmount(int cunAmount) {
        this.cunAmount = cunAmount;
    }

    public int getInputAmount() {
        return inputAmount;
    }

    public void setInputAmount(int inputAmount) {
        this.inputAmount = inputAmount;
    }

    public int getOutputAmountBig() {
        return outputAmountBig;
    }

    public void setOutputAmountBig(int outputAmountBig) {
        this.outputAmountBig = outputAmountBig;
    }

    public int getOutputAmountLittle() {
        return outputAmountLittle;
    }

    public void setOutputAmountLittle(int outputAmountLittle) {
        this.outputAmountLittle = outputAmountLittle;
    }

    public int getNeiXiao() {
        return neiXiao;
    }

    public void setNeiXiao(int neiXiao) {
        this.neiXiao = neiXiao;
    }

    public int getzFeed() {
        return zFeed;
    }

    public void setzFeed(int zFeed) {
        this.zFeed = zFeed;
    }

    public int getfFeed() {
        return fFeed;
    }

    public void setfFeed(int fFeed) {
        this.fFeed = fFeed;
    }

    public int getDie() {
        return die;
    }

    public void setDie(int die) {
        this.die = die;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    private int inputAmount;//外购数
    private int outputAmountBig;//直供大只
    private int outputAmountLittle;//直供小只
    private int neiXiao;//内销
    private int zFeed;//主饲料
    private int fFeed;//辅饲料
    private int die;//死亡数
    private int dieBig;//死亡大只鸡数量

    public int getDieBig() {
        return dieBig;
    }

    public void setDieBig(int dieBig) {
        this.dieBig = dieBig;
    }

    public int getDieSmall() {
        return dieSmall;
    }

    public void setDieSmall(int dieSmall) {
        this.dieSmall = dieSmall;
    }

    private int dieSmall;//死亡小只鸡数量
    private String remark;//备注
    private String dateTime;



}

