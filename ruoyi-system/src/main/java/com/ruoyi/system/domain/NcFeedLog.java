package com.ruoyi.system.domain;

import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * 饲料出入记录表 nc_feed_log
 *
 * @author ruoyi
 * @date 2019-09-10
 */
public class NcFeedLog extends BaseEntity
{
	private static final long serialVersionUID = 1L;

	/** 饲料日志表 */
	private Long feedLogId;
	/** 饲料id */
	private Long feedId;
	/** 饲喂数量 （kg） */
	private BigDecimal outputAmount;
	/** 领用人 */
	private String receiver;
	/** 喂养人 */
	private String exeUser;

	/** 饲料名称*/
	private String feedName;
	/** 饲料批次*/
	private String feedCode;

	/**饲料类型 1主 2辅*/
	private Integer feedType;

	/**饲料集合**/
	private List<Map> feeds;

	private Map feedes;

	private String meterialName;
	private double amount;
	private int type;

	private Long workNoticeId;

	//喂养人exeUser  领取人receiver  经手人createBy
	private String exeUserName;
	private String receiverName;
	private String createByName;

	public String getExeUserName() {
		return exeUserName;
	}

	public void setExeUserName(String exeUserName) {
		this.exeUserName = exeUserName;
	}

	public String getReceiverName() {
		return receiverName;
	}

	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public String getCreateByName() {
		return createByName;
	}

	public void setCreateByName(String createByName) {
		this.createByName = createByName;
	}

	public Long getWorkNoticeId() {
		return workNoticeId;
	}

	public void setWorkNoticeId(Long workNoticeId) {
		this.workNoticeId = workNoticeId;
	}

	public int getManagerType() {
		return managerType;
	}

	public void setManagerType(int managerType) {
		this.managerType = managerType;
	}

	private int managerType;

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public Map getFeedes() {
		return feedes;
	}

	public void setFeedes(Map feedes) {
		this.feedes = feedes;
	}

	public String getMeterialName() {
		return meterialName;
	}

	public void setMeterialName(String meterialName) {
		this.meterialName = meterialName;
	}

	public List<Map> getFeeds() {
		return feeds;
	}

	public void setFeeds(List<Map> feeds) {
		this.feeds = feeds;
	}

	public Integer getFeedType() {
		return feedType;
	}

	public void setFeedType(Integer feedType) {
		this.feedType = feedType;
	}

	public String getFeedName() {
		return feedName;
	}

	public void setFeedName(String feedName) {
		this.feedName = feedName;
	}

	public String getFeedCode() {
		return feedCode;
	}

	public void setFeedCode(String feedCode) {
		this.feedCode = feedCode;
	}

	public void setFeedLogId(Long feedLogId)
	{
		this.feedLogId = feedLogId;
	}

	public Long getFeedLogId()
	{
		return feedLogId;
	}
	public void setFeedId(Long feedId)
	{
		this.feedId = feedId;
	}

	public Long getFeedId()
	{
		return feedId;
	}
	public void setOutputAmount(BigDecimal outputAmount)
	{
		this.outputAmount = outputAmount;
	}

	public BigDecimal getOutputAmount()
	{
		return outputAmount;
	}
	public void setReceiver(String receiver)
	{
		this.receiver = receiver;
	}

	public String getReceiver()
	{
		return receiver;
	}
	public void setExeUser(String exeUser)
	{
		this.exeUser = exeUser;
	}

	public String getExeUser()
	{
		return exeUser;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("feedLogId", getFeedLogId())
            .append("feedId", getFeedId())
            .append("outputAmount", getOutputAmount())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("receiver", getReceiver())
            .append("exeUser", getExeUser())
            .append("remark", getRemark())
            .toString();
    }
}
