package com.ruoyi.system.domain;

import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 知识课堂表 nc_knowledge_class
 *
 * @author ruoyi
 * @date 2019-09-10
 */
public class NcKnowledgeClass extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 自增主键
     */
    private Long kcId;
    /**
     * 文章标题
     */
    private String title;
    /**
     * 图片
     */
    private String pic;
    /**
     * 文章内容
     */
    private String kcContent;
    /**
     * 文章状态 0: 正常 1: 关闭
     */
    private Integer kcStatus;
    /**
     * 分类id
     */
    private Long kctypeId;

    /**
     * 分类名称
     */
    private String typeName;




    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public Long getKcId() {
        return kcId;
    }

    public void setKcId(Long kcId) {
        this.kcId = kcId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getKcContent() {
        return kcContent;
    }

    public void setKcContent(String kcContent) {
        this.kcContent = kcContent;
    }

	public Integer getKcStatus() {
		return kcStatus;
	}

	public void setKcStatus(Integer kcStatus) {
		this.kcStatus = kcStatus;
	}

	public Long getKctypeId() {
        return kctypeId;
    }

    public void setKctypeId(Long kctypeId) {
        this.kctypeId = kctypeId;
    }

    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("kcId", getKcId())
                .append("title", getTitle())
                .append("pic", getPic())
                .append("kcContent", getKcContent())
                .append("kcStatus", getKcStatus())
                .append("kctypeId", getKctypeId())
                .toString();
    }
}
