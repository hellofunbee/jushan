package com.ruoyi.system.domain.querydata;

public class ShowNcStandard {

    /** 标准类型  1:育苗标准 2：生产标准 */
    /** 作物类别 */
    private String cropTypeCn;
    private Integer amount;
    private String standardType;
    private  String percent;

    public String getPercent() {
        return percent;
    }

    public void setPercent(String percent) {
        this.percent = percent;
    }

    public String getStandardType() {
        return standardType;
    }

    public void setStandardType(String standardType) {
        this.standardType = standardType;
    }

    public String getCropTypeCn() {
        return cropTypeCn;
    }

    public void setCropTypeCn(String cropTypeCn) {
        this.cropTypeCn = cropTypeCn;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }
}
