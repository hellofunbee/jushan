package com.ruoyi.system.domain;

public class MonthCensus {
    //上月库存
    private Integer lMonthStack;//鸡
    private double lZhi;
    //本月增加
    private Integer monthAdd;
    private  double monthAddZhi;
    //本月减少
    private Integer monthReduceZhi;
    private Integer monthReduceNei;
    private Integer monthreduceSi;
    //本月盘存
    private double sumAmount;

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    private  String dateTime;

    public Integer getlMonthStack() {
        return lMonthStack;
    }

    public void setlMonthStack(Integer lMonthStack) {
        this.lMonthStack = lMonthStack;
    }

    public double getlZhi() {
        return lZhi;
    }

    public void setlZhi(double lZhi) {
        this.lZhi = lZhi;
    }

    public Integer getMonthAdd() {
        return monthAdd;
    }

    public void setMonthAdd(Integer monthAdd) {
        this.monthAdd = monthAdd;
    }

    public double getMonthAddZhi() {
        return monthAddZhi;
    }

    public void setMonthAddZhi(double monthAddZhi) {
        this.monthAddZhi = monthAddZhi;
    }

    public Integer getMonthReduceZhi() {
        return monthReduceZhi;
    }

    public void setMonthReduceZhi(Integer monthReduceZhi) {
        this.monthReduceZhi = monthReduceZhi;
    }

    public Integer getMonthReduceNei() {
        return monthReduceNei;
    }

    public void setMonthReduceNei(Integer monthReduceNei) {
        this.monthReduceNei = monthReduceNei;
    }

    public double getSumAmount() {
        return sumAmount;
    }

    public void setSumAmount(double sumAmount) {
        this.sumAmount = sumAmount;
    }

    public Integer getMonthreduceSi() {
        return monthreduceSi;
    }

    public void setMonthreduceSi(Integer monthreduceSi) {
        this.monthreduceSi = monthreduceSi;
    }
}
