package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 植保表 nc_zhibao
 *
 * @author ruoyi
 * @date 2019-08-30
 */
public class NcZhibao extends BaseEntity
{
	private static final long serialVersionUID = 1L;

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	private Long parentId;

	/** 植保id */
	private Long zhibaoId;
	/** 植保单Id */
	private Long orderId;
	/** 计划id */
	private Long planId;
	@Excel(name="计划编号")
	private String planCode;
	/**
	 * 作物名称
	 */
	@Excel(name="作物名称")
	private String cropNameCn;

	/**
	 * 作物品种
	 */

	@Excel(name="作物品种")
	private String cropVarietiesCn;
	/**
	 * 生产班组
	 */
	@Excel(name="生产班组")
	private String teamCn;
	/**
	 * 设施类型  1：日光温室、2：春秋棚、3：连栋温室、4：露地
	 */
	@Excel(name="生产大棚",readConverterExp = "1=日光温室,2=春秋棚,3=连栋温室,4=露地")
	private Integer landType;
	/**
	 * 育苗/定植时间
	 */
	@Excel(name="定植/播种时间",width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
	private Date planTime;
	/**
	 * 总面积(亩)
	 */
	@Excel(name="种植面积")
	private BigDecimal sumArea;
	/** 阶段id */
	private Long stageId;
	/** 病症 */
	@Excel(name="植物病症")
	private String symptom;
	/** 发病原因 */
	@Excel(name="发病原因")
	private String causes;
	/** 植保意见 */
	@Excel(name="植保意见")
	private String proposal;

	//植保用药表 nc_zhibao_drugs
	/** 药品批次号 */
	@Excel(name="药品批次号")
	private String drugCode;
	/** 药品名称 */
	@Excel(name="药品名称")
	private String drugName;
	/** 施药日期 */
	@Excel(name="施药日期",width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
	private Date applicationTime;

	/** 施药方式：字典 1：根灌 2：页面喷洒  */
	@Excel(name="施药方式",readConverterExp = "1=根灌,2=页面喷洒")
	private String executeType;
	/** 施药量 */
	@Excel(name="施药量")
	private Integer drugAmount;
	/** 单位 */
	@Excel(name="单位",readConverterExp = "1=盒,2=袋,3=ml")
	private String unit;
	/** 有效期（天） */
	@Excel(name="有效期（天）")
	private Integer validDays;
	@Excel(name="备注")
	private String remark;
		public String getRe() {
			return remark;
		}

	public void setRe(String remark) {
		this.remark = remark;
	}


	/** 植保人 */
	@Excel(name="植保人")
	private String executeBy;
	/** 来源：1:推送 2：新增 */
	@Excel(name="来源",readConverterExp = "1=推送,2=新增")
	private Integer sourceType;

	public Integer getConformStatus() {
		return conformStatus;
	}

	public void setConformStatus(Integer conformStatus) {
		this.conformStatus = conformStatus;
	}

	/** 状态: 1 ：已确认 2：待确认3:获取了植保记录单id */
	private Integer conformStatus;


	public List<Map> getYaos() {
		return yaos;
	}

	public void setYaos(List<Map> yaos) {
		this.yaos = yaos;
	}

	private List<Map> yaos;


	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public Long getZhibaoId() {
		return zhibaoId;
	}

	public void setZhibaoId(Long zhibaoId) {
		this.zhibaoId = zhibaoId;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public Long getPlanId() {
		return planId;
	}

	public void setPlanId(Long planId) {
		this.planId = planId;
	}

	public Long getStageId() {
		return stageId;
	}

	public void setStageId(Long stageId) {
		this.stageId = stageId;
	}

	public String getSymptom() {
		return symptom;
	}

	public void setSymptom(String symptom) {
		this.symptom = symptom;
	}

	public String getCauses() {
		return causes;
	}

	public void setCauses(String causes) {
		this.causes = causes;
	}

	public String getProposal() {
		return proposal;
	}

	public void setProposal(String proposal) {
		this.proposal = proposal;
	}

	public Date getApplicationTime() {
		return applicationTime;
	}

	public void setApplicationTime(Date applicationTime) {
		this.applicationTime = applicationTime;
	}

	public Integer getValidDays() {
		return validDays;
	}

	public void setValidDays(Integer validDays) {
		this.validDays = validDays;
	}

	public String getExecuteBy() {
		return executeBy;
	}

	public void setExecuteBy(String executeBy) {
		this.executeBy = executeBy;
	}

	public Integer getSourceType() {
		return sourceType;
	}

	public void setSourceType(Integer sourceType) {
		this.sourceType = sourceType;
	}

	public String getDrugCode() {
		return drugCode;
	}

	public void setDrugCode(String drugCode) {
		this.drugCode = drugCode;
	}

	public String getDrugName() {
		return drugName;
	}

	public void setDrugName(String drugName) {
		this.drugName = drugName;
	}

	public String getExecuteType() {
		return executeType;
	}

	public void setExecuteType(String executeType) {
		this.executeType = executeType;
	}

	public Integer getDrugAmount() {
		return drugAmount;
	}

	public void setDrugAmount(Integer drugAmount) {
		this.drugAmount = drugAmount;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Integer getPlanType() {
		return planType;
	}

	public void setPlanType(Integer planType) {
		this.planType = planType;
	}

	public Long getStandardId() {
		return standardId;
	}

	public void setStandardId(Long standardId) {
		this.standardId = standardId;
	}

	public String getPlanCode() {
		return planCode;
	}

	public void setPlanCode(String planCode) {
		this.planCode = planCode;
	}

	public Long getCropVarieties() {
		return cropVarieties;
	}

	public void setCropVarieties(Long cropVarieties) {
		this.cropVarieties = cropVarieties;
	}

	public String getCropTypeCn() {
		return cropTypeCn;
	}

	public void setCropTypeCn(String cropTypeCn) {
		this.cropTypeCn = cropTypeCn;
	}

	public String getCropNameCn() {
		return cropNameCn;
	}

	public void setCropNameCn(String cropNameCn) {
		this.cropNameCn = cropNameCn;
	}

	public String getCropVarietiesCn() {
		return cropVarietiesCn;
	}

	public void setCropVarietiesCn(String cropVarietiesCn) {
		this.cropVarietiesCn = cropVarietiesCn;
	}

	public Date getPlanTime() {
		return planTime;
	}

	public void setPlanTime(Date planTime) {
		this.planTime = planTime;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

	public String getTeamCn() {
		return teamCn;
	}

	public void setTeamCn(String teamCn) {
		this.teamCn = teamCn;
	}

	public Integer getLandType() {
		return landType;
	}

	public void setLandType(Integer landType) {
		this.landType = landType;
	}

	public Integer getPlateCount() {
		return plateCount;
	}

	public void setPlateCount(Integer plateCount) {
		this.plateCount = plateCount;
	}

	public Integer getChaci() {
		return chaci;
	}

	public void setChaci(Integer chaci) {
		this.chaci = chaci;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public BigDecimal getArea() {
		return area;
	}

	public void setArea(BigDecimal area) {
		this.area = area;
	}

	public Integer getJian() {
		return jian;
	}

	public void setJian(Integer jian) {
		this.jian = jian;
	}

	public BigDecimal getSumArea() {
		return sumArea;
	}

	public void setSumArea(BigDecimal sumArea) {
		this.sumArea = sumArea;
	}

	public BigDecimal getCycleArea() {
		return cycleArea;
	}

	public void setCycleArea(BigDecimal cycleArea) {
		this.cycleArea = cycleArea;
	}

	public Integer getCycleDays() {
		return cycleDays;
	}

	public void setCycleDays(Integer cycleDays) {
		this.cycleDays = cycleDays;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**计划表 nc_plan
	 * 计划类型 1：育苗 计划2：生产计划
	 */
	private Integer planType;

	/**
	 * 标准id
	 */
	private Long standardId;
	/**
	 * 作物品种id
	 */
	private Long cropVarieties;
	/**
	 * 作物类别
	 */

	private String cropTypeCn;


	/**
	 * 生产班组
	 */

	private String team;


	/**
	 * 盘数
	 */
	private Integer plateCount;
	/**
	 * 茬次
	 */
	private Integer chaci;
	/**
	 * 数量(株)
	 */
	private Integer count;
	/**
	 * 单茬面积(亩)
	 */
	private BigDecimal area;
	/**
	 * 间数
	 */
	private Integer jian;

	/**
	 * 循环面积(亩)
	 */
	private BigDecimal cycleArea;
	/**
	 * 循环天数
	 */
	private Integer cycleDays;

	/*状态 1:待生成订单 2：已生成订单*/
	private String status = "1";
	//提交人
	@Excel(name="提交人")
	private String createBy;

	@Override
	public String getCreateBy() {
		return createBy;
	}

	@Override
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	@Override
	public Date getCreateTime() {
		return createTime;
	}

	@Override
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	//提交时间
	@Excel(name="提交时间",width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;


}
