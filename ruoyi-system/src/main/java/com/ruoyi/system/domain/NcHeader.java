package com.ruoyi.system.domain;

import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 轮播图表 nc_header
 * 
 * @author ruoyi
 * @date 2019-09-15
 */
public class NcHeader extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	
	/** 轮播图id */
	private Integer hId;
	/** 标题 */
	private String hName;
	/** 简介 */
	private String hDesc;
	/** 链接 */
	private String hUrl;
	/** 图片 */
	private String hCover;
	/** 轮播图类型 */
	private String hType;
	/** 轮播图状态 1正常  2 不正常 */
	private String hState;

	public Integer gethId() {
		return hId;
	}

	public void sethId(Integer hId) {
		this.hId = hId;
	}

	public String gethName() {
		return hName;
	}

	public void sethName(String hName) {
		this.hName = hName;
	}

	public String gethDesc() {
		return hDesc;
	}

	public void sethDesc(String hDesc) {
		this.hDesc = hDesc;
	}

	public String gethUrl() {
		return hUrl;
	}

	public void sethUrl(String hUrl) {
		this.hUrl = hUrl;
	}

	public String gethCover() {
		return hCover;
	}

	public void sethCover(String hCover) {
		this.hCover = hCover;
	}

	public String gethType() {
		return hType;
	}

	public void sethType(String hType) {
		this.hType = hType;
	}

	public String gethState() {
		return hState;
	}

	public void sethState(String hState) {
		this.hState = hState;
	}
}
