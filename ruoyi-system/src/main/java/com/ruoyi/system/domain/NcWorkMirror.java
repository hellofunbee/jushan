package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.Date;
import java.util.List;

/**
 * 标准农事镜像表 nc_work_mirror
 *
 * @author ruoyi
 * @date 2019-09-02
 */
public class NcWorkMirror extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 农事id
     */
    private Long workId;
    /**
     * 镜像主键
     */
    @Excel(name = "农事序号")
    private Long workMirrorId;
    /**
     * 标准id
     */
    @Excel(name = "标准序号")
    private Long standardId;
    /**
     * 计划id
     */
    @Excel(name = "计划序号")
    private Long planId;
    /**
     * 阶段id
     */
    @Excel(name = "阶段序号")
    private Long stageId;
    /**
     * 农事名称
     */
    @Excel(name = "农事名称")
    private String workName;
    /**
     * 农事类型 1:栽培 2：农资
     */
    @Excel(name = "农事类型 栽培/农资",readConverterExp = "0=栽培,1=农资")
    private String workType;
    /**
     * 循环周期(天)
     */
    @Excel(name = "循环周期(天)")
    private Integer cycleDays;
    /**
     * 第几天开始
     */
    @Excel(name = "第几天开始")
    private Integer workStart;
    /**
     * 第几天结束
     */
    @Excel(name = "第几天结束")
    private Integer workEnd;
    /**
     * 动态调整 0: 不是 1：是
     */
    @Excel(name = "动态调整 不是/是 ",readConverterExp = "0=不是,1=是")
    private String isDynamic;
    /**
     * 农资名称
     */
    @Excel(name = "农资名称")
    private String inputCn;
    /**
     * 农资
     */
    private String input;
    /**
     * 数量/亩
     */
    @Excel(name = "数量/亩")
    private Integer inputCount;
    /**
     * 单位
     */
    @Excel(name = "单位",dictType = "unitType")
    private String inputUnit;
    /**
     * 工作内容
     */
    @Excel(name = "工作内容 ")
    private String workDetail;

    /*1:农事 2：阶段*/
    @Excel(name = "分类 农事/阶段 ", readConverterExp = "1=农事,2=阶段")
    private String type;

    /*完成情况 1:完成  2：未完成*/
    @Excel(name = "完成情况 完成/未完成 ", readConverterExp = "1=完成,2=未完成")
    private String workStatus;

    private Date workDate;
    private Date workEndDate;

    private NcPlan plan;

    private List<NcStageMirror> stages;

    private Long stageMirrorId;
    private NcStandard standard;

    public NcWorkMirror() {
    }

    public NcWorkMirror(NcWork work, Long planId) {
        this.planId = planId;
        if (work != null) {
            this.workId = work.getWorkId();
            this.standardId = work.getWorkId();
            this.stageId = work.getStageId();
            this.workName = work.getWorkName();
            this.workType = work.getWorkType();
            this.cycleDays = work.getCycleDays();
            this.workStart = work.getWorkStart();
            this.workEnd = work.getWorkEnd();
            this.isDynamic = work.getIsDynamic();
            this.inputCn = work.getInputCn();
            this.input = work.getInput();
            this.inputCount = work.getInputCount();
            this.inputUnit = work.getInputUnit();
            this.workDetail = work.getWorkDetail();


        }
    }

    public NcWorkMirror(NcWorkMirror work) {

        if (work != null) {
            this.planId = work.getPlanId();
            this.workId = work.getWorkId();
            this.standardId = work.getWorkId();
            this.stageId = work.getStageId();
            this.workName = work.getWorkName();
            this.workType = work.getWorkType();
            this.cycleDays = work.getCycleDays();
            this.workStart = work.getWorkStart();
            this.workEnd = work.getWorkEnd();
            this.isDynamic = work.getIsDynamic();
            this.inputCn = work.getInputCn();
            this.input = work.getInput();
            this.inputCount = work.getInputCount();
            this.inputUnit = work.getInputUnit();
            this.workDetail = work.getWorkDetail();
        }
    }

    public void setWorkId(Long workId) {
        this.workId = workId;
    }

    public Long getWorkId() {
        return workId;
    }

    public void setWorkMirrorId(Long workMirrorId) {
        this.workMirrorId = workMirrorId;
    }

    public Long getWorkMirrorId() {
        return workMirrorId;
    }

    public void setStandardId(Long standardId) {
        this.standardId = standardId;
    }

    public Long getStandardId() {
        return standardId;
    }

    public void setPlanId(Long planId) {
        this.planId = planId;
    }

    public Long getPlanId() {
        return planId;
    }

    public void setStageId(Long stageId) {
        this.stageId = stageId;
    }

    public Long getStageId() {
        return stageId;
    }

    public void setWorkName(String workName) {
        this.workName = workName;
    }

    public String getWorkName() {
        return workName;
    }

    public void setWorkType(String workType) {
        this.workType = workType;
    }

    public String getWorkType() {
        return workType;
    }

    public void setCycleDays(Integer cycleDays) {
        this.cycleDays = cycleDays;
    }

    public Integer getCycleDays() {
        return cycleDays;
    }

    public void setWorkStart(Integer workStart) {
        this.workStart = workStart;
    }

    public Integer getWorkStart() {
        return workStart;
    }

    public void setWorkEnd(Integer workEnd) {
        this.workEnd = workEnd;
    }

    public Integer getWorkEnd() {
        return workEnd;
    }

    public void setIsDynamic(String isDynamic) {
        this.isDynamic = isDynamic;
    }

    public String getIsDynamic() {
        return isDynamic;
    }

    public void setInputCn(String inputCn) {
        this.inputCn = inputCn;
    }

    public String getInputCn() {
        return inputCn;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public String getInput() {
        return input;
    }

    public void setInputCount(Integer inputCount) {
        this.inputCount = inputCount;
    }

    public Integer getInputCount() {
        return inputCount;
    }

    public void setInputUnit(String inputUnit) {
        this.inputUnit = inputUnit;
    }

    public String getInputUnit() {
        return inputUnit;
    }

    public void setWorkDetail(String workDetail) {
        this.workDetail = workDetail;
    }

    public String getWorkDetail() {
        return workDetail;
    }

    public String getWorkStatus() {
        return workStatus;
    }

    public void setWorkStatus(String workStatus) {
        this.workStatus = workStatus;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getWorkDate() {
        return workDate;
    }

    public void setWorkDate(Date workDate) {
        this.workDate = workDate;
    }

    public NcPlan getPlan() {
        return plan;
    }

    public void setPlan(NcPlan plan) {
        this.plan = plan;
    }

    public List<NcStageMirror> getStages() {
        return stages;
    }

    public void setStages(List<NcStageMirror> stages) {
        this.stages = stages;
    }

    public Long getStageMirrorId() {
        return stageMirrorId;
    }

    public void setStageMirrorId(Long stageMirrorId) {
        this.stageMirrorId = stageMirrorId;
    }

    public NcStandard getStandard() {
        return standard;
    }

    public void setStandard(NcStandard standard) {
        this.standard = standard;
    }

    public Date getWorkEndDate() {
        return workEndDate;
    }

    public void setWorkEndDate(Date workEndDate) {
        this.workEndDate = workEndDate;
    }
}
