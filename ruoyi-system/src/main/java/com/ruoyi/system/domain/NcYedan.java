package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.Date;

/**
 * 液氮表 nc_yedan
 *
 * @author ruoyi
 * @date 2019-09-02
 */
public class NcYedan extends BaseEntity
{
	private static final long serialVersionUID = 1L;

	/**  */
	private Long yedanId;
	/** 来源 */
	@Excel(name = "来源")
	private String comeFrom;
	/** 库存(升) */
	@Excel(name = "库存(升)")
	private Integer stockAmout;
	/** 数量(升) */
	@Excel(name = "入库数量(升)")
	private Integer inputAmount;
	/** 采购人 */
    @Excel(name = "采购人")
	private String purchaser;
	/** 采购人名字 */
	private String purchaserUserName;
	@Excel(name = "经手人")
	private String createBy;
	@Excel(name = "入库时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	@Excel(name = "备注")
	private String remark;



	@Override
	public Date getCreateTime() {
		return createTime;
	}

	@Override
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Override
	public String getCreateBy() {
		return createBy;
	}

    @Override
    public String getRemark() {
        return remark;
    }

    @Override
    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public void setYedanId(Long yedanId)
	{
		this.yedanId = yedanId;
	}

	public Long getYedanId()
	{
		return yedanId;
	}
	public void setComeFrom(String comeFrom)
	{
		this.comeFrom = comeFrom;
	}

	public String getComeFrom()
	{
		return comeFrom;
	}
	public void setStockAmout(Integer stockAmout)
	{
		this.stockAmout = stockAmout;
	}

	public Integer getStockAmout()
	{
		return stockAmout;
	}
	public void setInputAmount(Integer inputAmount)
	{
		this.inputAmount = inputAmount;
	}

	public Integer getInputAmount()
	{
		return inputAmount;
	}
	public void setPurchaser(String purchaser)
	{
		this.purchaser = purchaser;
	}

	public String getPurchaser()
	{
		return purchaser;
	}

	public String getPurchaserUserName() {
		return purchaserUserName;
	}

	public void setPurchaserUserName(String purchaserUserName) {
		this.purchaserUserName = purchaserUserName;
	}

	public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("yedanId", getYedanId())
            .append("createTime", getCreateTime())
            .append("comeFrom", getComeFrom())
            .append("stockAmout", getStockAmout())
            .append("inputAmount", getInputAmount())
            .append("purchaser", getPurchaser())
            .append("createBy", getCreateBy())
            .append("remark", getRemark())
            .toString();
    }
}
