package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.Date;

/**
 * 出库登记表 nc_out_put
 *
 * @author ruoyi
 * @date 2019-09-16
 */
public class NcOutPut extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 出库id
     */
    private Long outPutId;
    /**
     * 育苗日期
     */
    @Excel(name = "育苗日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss", type = Excel.Type.EXPORT)
    private Date planTime;
    /**
     * 作物品种ID
     */
    private Long cropVarieties;
    /**
     * 作物类别
     */
    @Excel(name = "作物类别")
    private String cropTypeCn;
    /**
     * 作物名称
     */
    @Excel(name = "作物名称")
    private String cropNameCn;
    /**
     * 作物品种
     */
    @Excel(name = "作物品种")
    private String cropVarietiesCn;
    /**
     * 育苗数量(株)
     */
    @Excel(name = "育苗数量(株)")
    private Integer amount;
    /**
     * 接收人
     */
    @Excel(name = "接收人")
    private String receiver;
    @Excel(name = "备注")
    private String remark;

    @Override
    public String getRemark() {
        return remark;
    }

    @Override
    public void setRemark(String remark) {
        this.remark = remark;
    }

    public void setOutPutId(Long outPutId) {
        this.outPutId = outPutId;
    }

    public Long getOutPutId() {
        return outPutId;
    }

    public void setPlanTime(Date planTime) {
        this.planTime = planTime;
    }

    public Date getPlanTime() {
        return planTime;
    }

    public void setCropVarieties(Long cropVarieties) {
        this.cropVarieties = cropVarieties;
    }

    public Long getCropVarieties() {
        return cropVarieties;
    }

    public void setCropTypeCn(String cropTypeCn) {
        this.cropTypeCn = cropTypeCn;
    }

    public String getCropTypeCn() {
        return cropTypeCn;
    }

    public void setCropNameCn(String cropNameCn) {
        this.cropNameCn = cropNameCn;
    }

    public String getCropNameCn() {
        return cropNameCn;
    }

    public void setCropVarietiesCn(String cropVarietiesCn) {
        this.cropVarietiesCn = cropVarietiesCn;
    }

    public String getCropVarietiesCn() {
        return cropVarietiesCn;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getReceiver() {
        return receiver;
    }

    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("outPutId", getOutPutId())
                .append("planTime", getPlanTime())
                .append("cropVarieties", getCropVarieties())
                .append("cropTypeCn", getCropTypeCn())
                .append("cropNameCn", getCropNameCn())
                .append("cropVarietiesCn", getCropVarietiesCn())
                .append("amount", getAmount())
                .append("createTime", getCreateTime())
                .append("createBy", getCreateBy())
                .append("receiver", getReceiver())
                .append("remark", getRemark())
                .toString();
    }
}
