package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.domain.BaseEntity;
import java.util.Date;

/**
 * 鸡入库表 nc_input_chicken
 * 
 * @author ruoyi
 * @date 2019-09-09
 */
public class NcInputChicken extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	
	/** 入库id */
	private Integer inputId;
	/** 入栏批次 */
	private String inputCode;
	/** 入栏鸡品种： 字典 */
	@Excel(name="入栏鸡品种",readConverterExp = "1=珍珠鸡,2=胡须鸡,3=清远麻鸡")
	private Integer inputVarieties;
	/** 供应商 */
	@Excel(name="供应商")
	private String supplier;
	/** 合格证（有/无） */
	@Excel(name="检疫合格证",readConverterExp = "1=有,2=无")
	private Integer isHege;
	/** 数量（只) */
	@Excel(name="数量（只）")
	private Integer inputAmount;
	/** 入栏时间 */
	@Excel(name="入栏时间",width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
	private Date inputTime;
	/** 入栏经手人 */
	@Excel(name="入栏经手人")
	private String inputUser;
	/** 鸡入库状态:  1待确认 2已生成订单id */
	private Long inputStatus;
	private String orderId;
	@Excel(name="备注")
	private String remark;

	@Override
	public String getRemark() {
		return remark;
	}

	@Override
	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getStackAmount() {
		return stackAmount;
	}

	public void setStackAmount(Integer stackAmount) {
		this.stackAmount = stackAmount;
	}

	//库存
	private Integer stackAmount;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Long getInputStatus() {
		return inputStatus;
	}

	public void setInputStatus(Long inputStatus) {
		this.inputStatus = inputStatus;
	}

	public void setInputId(Integer inputId)
	{
		this.inputId = inputId;
	}

	public Integer getInputId() 
	{
		return inputId;
	}
	public void setInputCode(String inputCode) 
	{
		this.inputCode = inputCode;
	}

	public String getInputCode() 
	{
		return inputCode;
	}
	public void setInputVarieties(Integer inputVarieties) 
	{
		this.inputVarieties = inputVarieties;
	}

	public Integer getInputVarieties() 
	{
		return inputVarieties;
	}
	public void setSupplier(String supplier) 
	{
		this.supplier = supplier;
	}

	public String getSupplier() 
	{
		return supplier;
	}
	public void setIsHege(Integer isHege) 
	{
		this.isHege = isHege;
	}

	public Integer getIsHege() 
	{
		return isHege;
	}
	public void setInputAmount(Integer inputAmount) 
	{
		this.inputAmount = inputAmount;
	}

	public Integer getInputAmount() 
	{
		return inputAmount;
	}
	public void setInputTime(Date inputTime) 
	{
		this.inputTime = inputTime;
	}

	public Date getInputTime() 
	{
		return inputTime;
	}
	public void setInputUser(String inputUser) 
	{
		this.inputUser = inputUser;
	}

	public String getInputUser() 
	{
		return inputUser;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("inputId", getInputId())
            .append("inputCode", getInputCode())
            .append("inputVarieties", getInputVarieties())
            .append("supplier", getSupplier())
            .append("isHege", getIsHege())
            .append("inputAmount", getInputAmount())
            .append("inputTime", getInputTime())
            .append("inputUser", getInputUser())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("remark", getRemark())
            .toString();
    }
}
