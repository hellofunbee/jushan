package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 牛档案表 nc_cows_files
 *
 * @author ruoyi
 * @date 2019-09-02
 */
public class NcCowsFiles extends BaseEntity
{
	private static final long serialVersionUID = 1L;

	/** 牛id */
	private Integer cowId;
	/** 耳号 */
	@Excel(name="耳号")
	private String cowCode;
	/** 出生年月 */
	@Excel(name="出生年月",width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
	private Date birthDay;
	/** 来源：1 ：自产 2：外购 */
	@Excel(name="来源",readConverterExp = "1=自产,2=外购")
	private Integer comeFrom;
	/** 入舍时间 */
	@Excel(name="入舍时间",width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
	private Date inputTime;
	/** 入舍时体重(kg) */
	@Excel(name="入舍时体重(kg)")
	private BigDecimal firstWeight;
	/** 产牛胎次 */
	@Excel(name="产牛胎次")
	private String childs;
	/** 淘汰时间 */
	@Excel(name="淘汰时间",width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
	private Date eliminateTime;
	/** 淘汰原因 */
	@Excel(name="淘汰原因")
	private String eliminateReason;

	/** 牛龄*/
	@Excel(name="牛龄")
	private String cowAge;

	/**是否选中 默认false*/
	private boolean flag = false;

	@Excel(name="备注")
	private String remark;

	@Override
	public String getRemark() {
		return remark;
	}

	@Override
	public void setRemark(String remark) {
		this.remark = remark;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public String getCowAge() {
		return cowAge;
	}

	public void setCowAge(String cowAge) {
		this.cowAge = cowAge;
	}

	public void setCowId(Integer cowId)
	{
		this.cowId = cowId;
	}

	public Integer getCowId()
	{
		return cowId;
	}
	public void setCowCode(String cowCode)
	{
		this.cowCode = cowCode;
	}

	public String getCowCode()
	{
		return cowCode;
	}
	public void setBirthDay(Date birthDay)
	{
		try {
			Date date = new Date();
			setCowAge(date.getYear() - birthDay.getYear()+"");
		}catch (Exception e){

		}
		this.birthDay = birthDay;
	}

	public Date getBirthDay()
	{
		return birthDay;
	}
	public void setComeFrom(Integer comeFrom)
	{
		this.comeFrom = comeFrom;
	}

	public Integer getComeFrom()
	{
		return comeFrom;
	}
	public void setInputTime(Date inputTime)
	{
		this.inputTime = inputTime;
	}

	public Date getInputTime()
	{
		return inputTime;
	}
	public void setFirstWeight(BigDecimal firstWeight)
	{
		this.firstWeight = firstWeight;
	}

	public BigDecimal getFirstWeight()
	{
		return firstWeight;
	}
	public void setChilds(String childs)
	{
		this.childs = childs;
	}

	public String getChilds()
	{
		return childs;
	}
	public void setEliminateTime(Date eliminateTime)
	{
		this.eliminateTime = eliminateTime;
	}

	public Date getEliminateTime()
	{
		return eliminateTime;
	}
	public void setEliminateReason(String eliminateReason)
	{
		this.eliminateReason = eliminateReason;
	}

	public String getEliminateReason()
	{
		return eliminateReason;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("cowId", getCowId())
            .append("cowCode", getCowCode())
            .append("birthDay", getBirthDay())
            .append("comeFrom", getComeFrom())
            .append("inputTime", getInputTime())
            .append("firstWeight", getFirstWeight())
            .append("childs", getChilds())
            .append("eliminateTime", getEliminateTime())
            .append("eliminateReason", getEliminateReason())
            .append("updateTime", getUpdateTime())
            .append("createTime", getCreateTime())
            .append("createBy", getCreateBy())
            .append("remark", getRemark())
            .toString();
    }
}
