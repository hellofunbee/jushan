package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.domain.BaseEntity;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 鸡出库表 nc_output_chicken
 * 
 * @author ruoyi
 * @date 2019-09-10
 */
public class NcOutputChicken extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	
	/** 入库id */
	private Integer outputId;
	/** 菜房订单id */
	private Long cforderId;
	/** 出库日期 */
	@Excel(name="出库日期",width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
	private Date outputTime;
	/** 需求数量（只 */
	@Excel(name="需求数量（只）")
	private Integer requestAmount;
	/** 供应数量（只） */
	@Excel(name="供应数量（只）")
	private Integer outputAmount;
	/** 供应大鸡数量（只） */
	@Excel(name="供应大鸡数量（只）")
	private Integer outputAmountBig;
	/** 供应小鸡数量（只） */
	@Excel(name="供应小鸡数量（只）")
	private Integer outputAmountLittle;
	/** 出库类型 字典 */
	@Excel(name="出库类型",readConverterExp = "1=直供,2=内销一,3=内销二,4=内销三,5=内销四")
	private Integer outputType;
	/** 抓鸡时间 */
	@Excel(name="抓鸡时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
	private Date catchTime;

	/** 母鸡状态 1:正常 2：不正常 */
	@Excel(name="母鸡状态",readConverterExp = "1=正常,2=不正常")
	private Integer chickenStatus;
	/** 宰杀时间 */
	@Excel(name="宰杀时间",width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
	private Date killTime;
	/** 宰杀后成品母鸡标准 1：健康 2：不健康 */
	@Excel(name="宰杀后成品母鸡标准",readConverterExp = "1=健康,2=不健康")
	private Integer isHealthy;
	/** 宰杀人员 */
	@Excel(name="宰杀人员")
	private String butcher;
	/** 菜房接收时间 */
	@Excel(name="菜房接收时间",width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
	private Date receiveTime;
	/** 菜房接收人员 */
	@Excel(name="菜房接收人员")
	private String receiver;
	/** 出库经手人 */
	@Excel(name="出库经手人")
	private String outputUser;
	/** 出库状态：1待确认 2确认  3生成订单id */
	private Long outputStatus;

	@Override
	public String getRemark() {
		return remark;
	}

	@Override
	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Excel(name="备注")
	private String remark;

	/*出库重量*/
	private BigDecimal outputWeight;

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	//记录出库
	private BigDecimal amount;

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	private Long orderId;
	public void setOutputId(Integer outputId) 
	{
		this.outputId = outputId;
	}

	public Integer getOutputId() 
	{
		return outputId;
	}
	public void setCforderId(Long cforderId) 
	{
		this.cforderId = cforderId;
	}

	public Long getCforderId() 
	{
		return cforderId;
	}
	public void setOutputTime(Date outputTime) 
	{
		this.outputTime = outputTime;
	}

	public Date getOutputTime() 
	{
		return outputTime;
	}
	public void setRequestAmount(Integer requestAmount) 
	{
		this.requestAmount = requestAmount;
	}

	public Integer getRequestAmount() 
	{
		return requestAmount;
	}
	public void setOutputAmount(Integer outputAmount) 
	{
		this.outputAmount = outputAmount;
	}

	public Integer getOutputAmount() 
	{
		return outputAmount;
	}
	public void setOutputAmountBig(Integer outputAmountBig) 
	{
		this.outputAmountBig = outputAmountBig;
	}

	public Integer getOutputAmountBig() 
	{
		return outputAmountBig;
	}
	public void setOutputAmountLittle(Integer outputAmountLittle) 
	{
		this.outputAmountLittle = outputAmountLittle;
	}

	public Integer getOutputAmountLittle() 
	{
		return outputAmountLittle;
	}
	public void setOutputType(Integer outputType) 
	{
		this.outputType = outputType;
	}

	public Integer getOutputType() 
	{
		return outputType;
	}
	public void setCatchTime(Date catchTime) 
	{
		this.catchTime = catchTime;
	}

	public Date getCatchTime() 
	{
		return catchTime;
	}
	public void setChickenStatus(Integer chickenStatus) 
	{
		this.chickenStatus = chickenStatus;
	}

	public Integer getChickenStatus() 
	{
		return chickenStatus;
	}
	public void setKillTime(Date killTime) 
	{
		this.killTime = killTime;
	}

	public Date getKillTime() 
	{
		return killTime;
	}
	public void setIsHealthy(Integer isHealthy) 
	{
		this.isHealthy = isHealthy;
	}

	public Integer getIsHealthy() 
	{
		return isHealthy;
	}
	public void setButcher(String butcher) 
	{
		this.butcher = butcher;
	}

	public String getButcher() 
	{
		return butcher;
	}
	public void setReceiveTime(Date receiveTime) 
	{
		this.receiveTime = receiveTime;
	}

	public Date getReceiveTime() 
	{
		return receiveTime;
	}
	public void setReceiver(String receiver) 
	{
		this.receiver = receiver;
	}

	public String getReceiver() 
	{
		return receiver;
	}
	public void setOutputUser(String outputUser) 
	{
		this.outputUser = outputUser;
	}

	public String getOutputUser() 
	{
		return outputUser;
	}
	public void setOutputStatus(Long outputStatus) 
	{
		this.outputStatus = outputStatus;
	}

	public Long getOutputStatus() 
	{
		return outputStatus;
	}

	public BigDecimal getOutputWeight() {
		return outputWeight;
	}

	public void setOutputWeight(BigDecimal outputWeight) {
		this.outputWeight = outputWeight;
	}

	public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("outputId", getOutputId())
            .append("cforderId", getCforderId())
            .append("outputTime", getOutputTime())
            .append("requestAmount", getRequestAmount())
            .append("outputAmount", getOutputAmount())
            .append("outputAmountBig", getOutputAmountBig())
            .append("outputAmountLittle", getOutputAmountLittle())
            .append("outputType", getOutputType())
            .append("catchTime", getCatchTime())
            .append("chickenStatus", getChickenStatus())
            .append("killTime", getKillTime())
            .append("isHealthy", getIsHealthy())
            .append("butcher", getButcher())
            .append("receiveTime", getReceiveTime())
            .append("receiver", getReceiver())
            .append("outputUser", getOutputUser())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("remark", getRemark())
            .append("outputStatus", getOutputStatus())
            .toString();
    }
}
