package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 检测表 nc_check
 *
 * @author ruoyi
 * @date 2019-08-29
 */
public class NcCheck extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 检测id
     */
    private Long checkId;
    /**
     * 蔬菜批次
     */
    @Excel(name = "蔬菜批次")
    private String piciCode;

    /**
     * 订单对象
     */

    @Excel(name = "订单编号", type = Excel.Type.EXPORT)
    private String excel1;
    @Excel(name = "作物名称", type = Excel.Type.EXPORT)
    private String excel2;
    @Excel(name = "作物品种", type = Excel.Type.EXPORT)
    private String excel3;

    /**
     * 检测关联的订单-所关联的计划的班组
     */
    @Excel(name = "生产班组")
    private String teamCn;

    /**
     * 菜房订单id
     */
    private Long cforderId;
    /**
     * 计划id
     */
    private Long planId;

    @Excel(name = "抑制率(%)")
    private BigDecimal yzl;
    /**
     * 检测结果 1:合格 2：不合格
     */
    @Excel(name = "检测结果", readConverterExp = "1=合格,2=不合格")
    private Integer checkResult;
    /**
     * 检测时间
     */
    @Excel(name = "检测时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss", type = Excel.Type.EXPORT)
    private Date checkTime;
    /**
     * 检测人员
     */
    @Excel(name = "检测人员")
    private String checkUser;

    private Long orderId;

    @Override
    public String getRemark() {
        return remark;
    }

    @Override
    public void setRemark(String remark) {
        this.remark = remark;
    }
    @Excel(name = "备注")
    private String remark;

    /*状态:1：待确认 2：已挂起 3：已确认：4：已生成订单*/
    private String status;

    /*菜房订单的状态*/
    private String cforderStatus;

    /**
     * 检测关联的订单
     */
    private NcOrderCf ncorderCf;


    /**
     * 检测关联的订单-所关联的计划的计划编号
     */
    private String planCode;

    public void setCheckId(Long checkId) {
        this.checkId = checkId;
    }

    public Long getCheckId() {
        return checkId;
    }

    public void setPiciCode(String piciCode) {
        this.piciCode = piciCode;
    }

    public String getPiciCode() {
        return piciCode;
    }

    public void setCforderId(Long cforderId) {
        this.cforderId = cforderId;
    }

    public Long getCforderId() {
        return cforderId;
    }

    public void setPlanId(Long planId) {
        this.planId = planId;
    }

    public Long getPlanId() {
        return planId;
    }

    public void setYzl(BigDecimal yzl) {
        this.yzl = yzl;
    }

    public BigDecimal getYzl() {
        return yzl;
    }

    public void setCheckResult(Integer checkResult) {
        this.checkResult = checkResult;
    }

    public Integer getCheckResult() {
        return checkResult;
    }

    public void setCheckTime(Date checkTime) {
        this.checkTime = checkTime;
    }

    public Date getCheckTime() {
        return checkTime;
    }

    public void setCheckUser(String checkUser) {
        this.checkUser = checkUser;
    }

    public String getCheckUser() {
        return checkUser;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getCforderStatus() {
        return cforderStatus;
    }

    public void setCforderStatus(String cforderStatus) {
        this.cforderStatus = cforderStatus;
    }

    public NcOrderCf getNcorderCf() {
        return ncorderCf;
    }

    public void setNcorderCf(NcOrderCf ncorderCf) {
        if (ncorderCf != null) {
            excel1 = ncorderCf.getCforderCode();
            excel2 = ncorderCf.getCropNameCn();
            excel3 = ncorderCf.getCropVarietiesCn();
        }
        this.ncorderCf = ncorderCf;
    }

    public String getTeamCn() {
        return teamCn;
    }

    public void setTeamCn(String teamCn) {
        this.teamCn = teamCn;
    }

    public String getPlanCode() {
        return planCode;
    }

    public void setPlanCode(String planCode) {
        this.planCode = planCode;
    }
}
