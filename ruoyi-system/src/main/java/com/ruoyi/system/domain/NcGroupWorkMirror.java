package com.ruoyi.system.domain;

import com.ruoyi.common.core.domain.BaseEntity;

import java.util.List;

/**
 * 标准农事镜像 按照工作时间分组
 *
 * @author ruoyi
 * @date 2019-09-02
 */
public class NcGroupWorkMirror extends BaseEntity {
    private static final long serialVersionUID = 1L;


    private String workDate;
    private List<NcWorkMirror> works;

    public String getWorkDate() {
        return workDate;
    }

    public void setWorkDate(String workDate) {
        this.workDate = workDate;
    }

    public List<NcWorkMirror> getWorks() {
        return works;
    }

    public void setWorks(List<NcWorkMirror> works) {
        this.works = works;
    }
}
