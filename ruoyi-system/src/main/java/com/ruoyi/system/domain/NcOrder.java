package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.Date;

/**
 * 订单主（系统中的所有订单共用此）表 nc_order
 *
 * @author ruoyi
 * @date 2019-07-31
 */
public class NcOrder extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 单子id
     */
    private Long orderId;
    /**
     * 单号
     */
    private String orderCode;
    /**
     * 时间
     */
    private Date orderTime;
    /**
     * 单子类型： 1：生产计划单 2：育苗计划单 3：订单记录单 4：检测记录单 5：菜房1：入库单 6：菜房1：出库单 7：菜房2：入库单 8：菜房2：出库单 9：植保记录单 10：牛舍入库单 11：牛舍：出库单 12：鸡舍入库单 13：鸡舍出库单 14：牛舍诊疗单 15：牛舍免疫单 ：16：牛舍巡视单  17：鸡舍诊疗单 18：鸡舍免疫单 ：19：鸡舍巡视单:  20: 菜房2入库  21:菜房2出库
     */
    private Integer orderType;
    /**
     * 出库类型 或者订单类型
     */
    private String outputType;
    /**
     * 0：默认 1：执行中 2：已完成
     */
    private Integer status = 0;

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderTime(Date orderTime) {
        this.orderTime = orderTime;
    }

    public Date getOrderTime() {
        return orderTime;
    }

    public void setOrderType(Integer orderType) {
        this.orderType = orderType;
    }

    public Integer getOrderType() {
        return orderType;
    }

    public void setOutputType(String outputType) {
        this.outputType = outputType;
    }

    public String getOutputType() {
        return outputType;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getStatus() {
        return status;
    }

    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("orderId", getOrderId())
                .append("orderCode", getOrderCode())
                .append("orderTime", getOrderTime())
                .append("createBy", getCreateBy())
                .append("orderType", getOrderType())
                .append("outputType", getOutputType())
                .append("status", getStatus())
                .append("createTime", getCreateTime())
                .append("remark", getRemark())
                .toString();
    }
}
