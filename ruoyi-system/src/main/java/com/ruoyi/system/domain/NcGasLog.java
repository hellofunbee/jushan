package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.annotation.Excel.Type;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 气值表 nc_gas_log
 *
 * @author ruoyi
 * @date 2019-07-31
 */
public class NcGasLog extends BaseEntity
{
	private static final long serialVersionUID = 1L;

	/** 企标记录id */
	@Excel(name = "编号", prompt = "编号")
	private Long gasLogId;
	/** 气表值 */
	@Excel(name = "气表值", prompt = "编号")
	private BigDecimal gasValue;
	@Excel(name = "记录时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss", type = Type.EXPORT)
	private Date createTime;
	@Excel(name = "查表人员", prompt = "编号")
	private String createBy;

	private Double gasTop;

	private Double gasLow;

	@Excel(name="备注")
	private String remark;

	@Override
	public String getRemark() {
		return remark;
	}

	@Override
	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Double getGasTop() {
		return gasTop;
	}

	public void setGasTop(Double gasTop) {
		this.gasTop = gasTop;
	}

	public Double getGasLow() {
		return gasLow;
	}

	public void setGasLow(Double gasLow) {
		this.gasLow = gasLow;
	}

	@Override
	public Date getCreateTime() {
		return createTime;
	}

	@Override
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Override
	public String getCreateBy() {
		return createBy;
	}

	@Override
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public void setGasLogId(Long gasLogId)
	{
		this.gasLogId = gasLogId;
	}

	public Long getGasLogId()
	{
		return gasLogId;
	}
	public void setGasValue(BigDecimal gasValue)
	{
		this.gasValue = gasValue;
	}

	public BigDecimal getGasValue()
	{
		return gasValue;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("gasLogId", getGasLogId())
            .append("gasValue", getGasValue())
            .append("createTime", getCreateTime())
            .append("createBy", getCreateBy())
            .append("remark", getRemark())
            .toString();
    }
}
