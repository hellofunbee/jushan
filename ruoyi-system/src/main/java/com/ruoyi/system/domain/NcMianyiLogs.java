package com.ruoyi.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.domain.BaseEntity;
import java.util.Date;

/**
 * 免疫记录表 nc_mianyi_logs
 * 
 * @author ruoyi
 * @date 2019-09-02
 */
public class NcMianyiLogs extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	
	/** 诊疗id */
	private Long myId;
	/** 免疫类型 1:牛舍 2：鸡舍 */
	private Integer myType;
	/** 状态:1：待确认 2：已挂起 3：已确认：4：已生成订单 */
	private Integer myStatus;
	/** 圈舍号/鸡舍号 */
	@Excel(name="圈舍号/鸡舍号")
	private String roomNum;
	/** 订单id */
	private Long orderId;
	/** 存栏数量 */
	@Excel(name="存栏数量")
	private Integer stockAmount;
	/** 免疫数量 */
	@Excel(name="免疫数量")
	private Integer myAmount;
	/** 疫苗id */
	@Excel(name="疫苗id")
	private Long ymId;
	/** 疫苗名称 */
	@Excel(name="疫苗名称")
	private String ymName;
	/** 疫苗生产厂家 */
	@Excel(name="ymFactory")
	private String ymFactory;
	/** 批号（有效期） */
	@Excel(name=" 批号（有效期")
	private String ymCode;
	/** 免疫方法 */
	@Excel(name="免疫方法",dictType = "animalExecuteType")
	private String myWays;
	/** 免疫剂量 */
	@Excel(name=" 免疫剂量")
	private Integer myDose;

	/** 创建者 */
	@Excel(name="创建者")
	private String createBy;


	/** 创建时间 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@Excel(name="巡视时间")
	private Date createTime;

	/** 备注 */
	@Excel(name="备注")
	private String remark;


	@Override
	public String getCreateBy() {
		return createBy;
	}

	@Override
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	@Override
	public Date getCreateTime() {
		return createTime;
	}

	@Override
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Override
	public String getRemark() {
		return remark;
	}

	@Override
	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Long getYmId() {
		return ymId;
	}

	public void setYmId(Long ymId) {
		this.ymId = ymId;
	}

	public void setMyId(Long myId)
	{
		this.myId = myId;
	}

	public Long getMyId() 
	{
		return myId;
	}
	public void setMyType(Integer myType) 
	{
		this.myType = myType;
	}

	public Integer getMyType() 
	{
		return myType;
	}
	public void setMyStatus(Integer myStatus) 
	{
		this.myStatus = myStatus;
	}

	public Integer getMyStatus() 
	{
		return myStatus;
	}
	public void setRoomNum(String roomNum) 
	{
		this.roomNum = roomNum;
	}

	public String getRoomNum() 
	{
		return roomNum;
	}
	public void setOrderId(Long orderId)
	{
		this.orderId = orderId;
	}

	public Long getOrderId()
	{
		return orderId;
	}
	public void setStockAmount(Integer stockAmount) 
	{
		this.stockAmount = stockAmount;
	}

	public Integer getStockAmount() 
	{
		return stockAmount;
	}
	public void setMyAmount(Integer myAmount) 
	{
		this.myAmount = myAmount;
	}

	public Integer getMyAmount() 
	{
		return myAmount;
	}
	public void setYmName(String ymName) 
	{
		this.ymName = ymName;
	}

	public String getYmName() 
	{
		return ymName;
	}
	public void setYmFactory(String ymFactory) 
	{
		this.ymFactory = ymFactory;
	}

	public String getYmFactory() 
	{
		return ymFactory;
	}
	public void setYmCode(String ymCode) 
	{
		this.ymCode = ymCode;
	}

	public String getYmCode() 
	{
		return ymCode;
	}
	public void setMyWays(String myWays) 
	{
		this.myWays = myWays;
	}

	public String getMyWays() 
	{
		return myWays;
	}
	public void setMyDose(Integer myDose)
	{
		this.myDose = myDose;
	}

	public Integer getMyDose()
	{
		return myDose;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("myId", getMyId())
            .append("myType", getMyType())
            .append("myStatus", getMyStatus())
            .append("roomNum", getRoomNum())
            .append("orderId", getOrderId())
            .append("stockAmount", getStockAmount())
            .append("myAmount", getMyAmount())
            .append("ymName", getYmName())
            .append("ymFactory", getYmFactory())
            .append("ymCode", getYmCode())
            .append("myWays", getMyWays())
            .append("myDose", getMyDose())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("remark", getRemark())
            .toString();
    }
}
