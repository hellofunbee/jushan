package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Map;

/**
 * 蔬菜表 sys_dept
 *
 * @author ruoyi
 */
public class NcCai extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 蔬菜ID
     */
    private Long caiId;

    /**
     * 父蔬菜ID
     */
    private Long parentId;

    /**
     * 祖级列表
     */
    private String ancestors;

    /**
     * 蔬菜类别
     */
    @Excel(name = "作物类别")
    private String parentType;
    /**
     * 蔬菜名称
     */
    @Excel(name = "作物名称")
    private String parentName;
    /**
     * 蔬菜名称
     */
    @Excel(name = "作物品种")
    private String caiName;

    /**
     * 蔬菜编码
     */
    private String code;

    /**
     * 显示顺序
     */
    private String orderNum;

    /**
     * 负责人
     */
    private Integer level;


    /**
     * 删除标志（0代表存在 2代表删除）
     */
    private String delFlag;



    private Map planCounts;

    public String getParentType() {
        return parentType;
    }

    public void setParentType(String parentType) {
        this.parentType = parentType;
    }

    public Long getCaiId() {
        return caiId;
    }

    public void setCaiId(Long caiId) {
        this.caiId = caiId;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getAncestors() {
        return ancestors;
    }

    public void setAncestors(String ancestors) {
        this.ancestors = ancestors;
    }

    @NotBlank(message = "蔬菜名称不能为空")
    @Size(min = 0, max = 30, message = "蔬菜名称长度不能超过30个字符")
    public String getCaiName() {
        return caiName;
    }

    public void setCaiName(String caiName) {
        this.caiName = caiName;
    }

    @NotBlank(message = "显示顺序不能为空")
    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Map getPlanCounts() {
        return planCounts;
    }

    public void setPlanCounts(Map planCounts) {
        this.planCounts = planCounts;
    }

    @Override
    public String toString() {
        return "NcCai{" +
                "caiId=" + caiId +
                ", parentId=" + parentId +
                ", ancestors='" + ancestors + '\'' +
                ", caiName='" + caiName + '\'' +
                ", orderNum='" + orderNum + '\'' +
                ", level=" + level +
                ", code='" + code + '\'' +
                ", delFlag='" + delFlag + '\'' +
                ", parentName='" + parentName + '\'' +
                '}';
    }
}
