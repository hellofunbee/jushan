package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.annotation.Excels;

import java.util.List;

public class NcStandardTemplate {




    /**
     * 导入标准序号
     */

    @Excel(name = "标准序号")
    private Double serialStandardNumber;

    private String standardType;
    /** 标准名称 */
    private String standardName;
    /** 图片 */
    private String pic;
    /** 作物品种ID */
    private Long cropVarieties;
    @Excel(name = "作物类别")
    private String cropTypeCn;
    @Excel(name = "作物名称")
    private String cropNameCn;
    /** 作物品种 */
    @Excel(name = "作物品种")
    private String cropVarietiesCn;
    /** 适宜季节 */
    @Excel(name = "适宜季节 春/夏/秋/冬/四季", readConverterExp = "1=春,2=夏,3=秋,4=冬")
    private String season;
    /** 总周期(天) */
    @Excel(name = "总周期(天)")
    private String totalCycle;
    /** 删除标志（1代表存在 2代表删除） */
    private String delFlag;
    /** 状态码：1:代表默认 0:其他  */
    private String status;
    /** 编码 */
    private String code;
    /** 父编码 */
    private String parentCode;
    /*用于选中状态显示*/
    private boolean flag;
    /**
     * 备注
     */
    @Excel(name = "备注")
    private String remark;

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * 阶段id
     */
    private Long stageId;
    /**
     * 标准id
     */
    private Long standardId;
    /**
     * 标准名称
     */
    private String stageName;
    /**
     * 阶段天数
     */
    private Integer stageDays;
    /**
     * 排序号
     */
    private Integer orderNum;
    /**
     * 阶段描述
     */
    private String stageDetail;

    /** 农事名称 */
    private String workName;
    /** 农事类型 1:栽培 2：农资 */
    private String workType;
    /** 循环周期(天) */
    private Integer cycleDays;
    /** 第几天开始 */
    private Integer workStart;
    /** 第几天结束 */
    private Integer workEnd;
    /** 动态调整 0: 不是 1：是 */
    private String isDynamic;
    /** 农资名称 */
    private String inputCn;
    /** 农资 */
    private String input;
    /** 数量/亩 */
    private Integer inputCount;
    /** 单位 */
    private String inputUnit;
    /** 工作内容 */
    private String workDetail;
    /** 分类 1：农事 2：阶段 */
    private String type;

    public String getStandardType() {
        return standardType;
    }

    public void setStandardType(String standardType) {
        this.standardType = standardType;
    }

    public String getStandardName() {
        return standardName;
    }

    public void setStandardName(String standardName) {
        this.standardName = standardName;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public Long getCropVarieties() {
        return cropVarieties;
    }

    public void setCropVarieties(Long cropVarieties) {
        this.cropVarieties = cropVarieties;
    }

    public String getCropTypeCn() {
        return cropTypeCn;
    }

    public void setCropTypeCn(String cropTypeCn) {
        this.cropTypeCn = cropTypeCn;
    }

    public String getCropNameCn() {
        return cropNameCn;
    }

    public void setCropNameCn(String cropNameCn) {
        this.cropNameCn = cropNameCn;
    }

    public String getCropVarietiesCn() {
        return cropVarietiesCn;
    }

    public void setCropVarietiesCn(String cropVarietiesCn) {
        this.cropVarietiesCn = cropVarietiesCn;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public String getTotalCycle() {
        return totalCycle;
    }

    public void setTotalCycle(String totalCycle) {
        this.totalCycle = totalCycle;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getParentCode() {
        return parentCode;
    }

    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public Long getStageId() {
        return stageId;
    }

    public void setStageId(Long stageId) {
        this.stageId = stageId;
    }

    public Long getStandardId() {
        return standardId;
    }

    public void setStandardId(Long standardId) {
        this.standardId = standardId;
    }

    public String getStageName() {
        return stageName;
    }

    public void setStageName(String stageName) {
        this.stageName = stageName;
    }

    public Integer getStageDays() {
        return stageDays;
    }

    public void setStageDays(Integer stageDays) {
        this.stageDays = stageDays;
    }

    public Integer getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(Integer orderNum) {
        this.orderNum = orderNum;
    }

    public String getStageDetail() {
        return stageDetail;
    }

    public void setStageDetail(String stageDetail) {
        this.stageDetail = stageDetail;
    }

    public String getWorkName() {
        return workName;
    }

    public void setWorkName(String workName) {
        this.workName = workName;
    }

    public String getWorkType() {
        return workType;
    }

    public void setWorkType(String workType) {
        this.workType = workType;
    }

    public Integer getCycleDays() {
        return cycleDays;
    }

    public void setCycleDays(Integer cycleDays) {
        this.cycleDays = cycleDays;
    }

    public Integer getWorkStart() {
        return workStart;
    }

    public void setWorkStart(Integer workStart) {
        this.workStart = workStart;
    }

    public Integer getWorkEnd() {
        return workEnd;
    }

    public void setWorkEnd(Integer workEnd) {
        this.workEnd = workEnd;
    }

    public String getIsDynamic() {
        return isDynamic;
    }

    public void setIsDynamic(String isDynamic) {
        this.isDynamic = isDynamic;
    }

    public String getInputCn() {
        return inputCn;
    }

    public void setInputCn(String inputCn) {
        this.inputCn = inputCn;
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public Integer getInputCount() {
        return inputCount;
    }

    public void setInputCount(Integer inputCount) {
        this.inputCount = inputCount;
    }

    public String getInputUnit() {
        return inputUnit;
    }

    public void setInputUnit(String inputUnit) {
        this.inputUnit = inputUnit;
    }

    public String getWorkDetail() {
        return workDetail;
    }

    public void setWorkDetail(String workDetail) {
        this.workDetail = workDetail;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
