package com.ruoyi.system.domain;

import com.ruoyi.common.core.domain.BaseEntity;

import java.util.Date;

/**
 * 工作提醒表 nc_work_notice
 *
 * @author ruoyi
 * @date 2019-09-04
 */
public class NcWorkNotice extends BaseEntity
{
	private static final long serialVersionUID = 1L;

	/** 工作提醒id */
	private Long workNoticeId;
	/** 工作名称 */
	private String worckNoticeName;
	/** 工作内容 */
	private String content;
	/** 时间点 */
	private String time;
	/** 上次执行时间 */
	private Date lastTime;
	/** 提醒类型： 1:牛舍 2：鸡舍 */
	private Integer noticeType;
	/** 工作类型： 1：消毒 2：饲喂 3：挤奶 */
	private Integer workType;
	/** 循环周期(天) */
	private Integer cycleDays;

	@Override
	public String toString() {
		return "NcWorkNotice{" +
				"workNoticeId=" + workNoticeId +
				", worckNoticeName='" + worckNoticeName + '\'' +
				", content='" + content + '\'' +
				", time='" + time + '\'' +
				", lastTime=" + lastTime +
				", noticeType=" + noticeType +
				", workType=" + workType +
				", cycleDays=" + cycleDays +
				'}';
	}

	public Long getWorkNoticeId() {
		return workNoticeId;
	}

	public void setWorkNoticeId(Long workNoticeId) {
		this.workNoticeId = workNoticeId;
	}

	public String getWorckNoticeName() {
		return worckNoticeName;
	}

	public void setWorckNoticeName(String worckNoticeName) {
		this.worckNoticeName = worckNoticeName;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public Date getLastTime() {
		return lastTime;
	}

	public void setLastTime(Date lastTime) {
		this.lastTime = lastTime;
	}

	public Integer getNoticeType() {
		return noticeType;
	}

	public void setNoticeType(Integer noticeType) {
		this.noticeType = noticeType;
	}

	public Integer getWorkType() {
		return workType;
	}

	public void setWorkType(Integer workType) {
		this.workType = workType;
	}

	public Integer getCycleDays() {
		return cycleDays;
	}

	public void setCycleDays(Integer cycleDays) {
		this.cycleDays = cycleDays;
	}
}
