package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 鸡日报表 nc_chicken_day_census
 * 
 * @author ruoyi
 * @date 2019-09-19
 */
public class NcChickenDayCensus extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	
	/** 主键 */
	private Long censusId;
	/** 存栏数量 */
	@Excel(name="存栏数量")
	private Long cunAmount;
	/** 外购数量 */
	@Excel(name="外购数量")
	private Long inputAmount;
	/** 直供大只数量 */
	@Excel(name="直供大只数量")
	private Long outputAmountBig;
	/** 直供小只数量 */
	@Excel(name="直供小只数量")
	private Long outputAmountLittle;
	/** 内销数量 */
	@Excel(name="内销数量")
	private Long neiXiao;
	/** 日喂主料*/
	@Excel(name="日喂主料")
	private Long zFeed;
	/** 日喂辅料 */
	@Excel(name="日喂辅料")
	private Long fFeed;
	/** 死亡数量 */
	private Long die;

	public Long getDieSmall() {
		return dieSmall;
	}

	public void setDieSmall(Long dieSmall) {
		this.dieSmall = dieSmall;
	}

	public Long getDieBig() {
		return dieBig;
	}

	public void setDieBig(Long dieBig) {
		this.dieBig = dieBig;
	}

	@Excel(name="死亡小只鸡数量")
	private Long dieSmall;
	@Excel(name="死亡大只鸡数量")
	private Long dieBig;
	/** 时间 */
	@Excel(name="时间")
	private String dateTime;


	public void setCensusId(Long censusId) 
	{
		this.censusId = censusId;
	}

	public Long getCensusId() 
	{
		return censusId;
	}
	public void setCunAmount(Long cunAmount) 
	{
		this.cunAmount = cunAmount;
	}

	public Long getCunAmount() 
	{
		return cunAmount;
	}
	public void setInputAmount(Long inputAmount) 
	{
		this.inputAmount = inputAmount;
	}

	public Long getInputAmount() 
	{
		return inputAmount;
	}
	public void setOutputAmountBig(Long outputAmountBig) 
	{
		this.outputAmountBig = outputAmountBig;
	}

	public Long getOutputAmountBig() 
	{
		return outputAmountBig;
	}
	public void setOutputAmountLittle(Long outputAmountLittle) 
	{
		this.outputAmountLittle = outputAmountLittle;
	}

	public Long getOutputAmountLittle() 
	{
		return outputAmountLittle;
	}
	public void setNeiXiao(Long neiXiao) 
	{
		this.neiXiao = neiXiao;
	}

	public Long getNeiXiao() 
	{
		return neiXiao;
	}
	public void setZFeed(Long zFeed) 
	{
		this.zFeed = zFeed;
	}

	public Long getZFeed() 
	{
		return zFeed;
	}
	public void setFFeed(Long fFeed) 
	{
		this.fFeed = fFeed;
	}

	public Long getFFeed() 
	{
		return fFeed;
	}
	public void setDie(Long die) 
	{
		this.die = die;
	}

	public Long getDie() 
	{
		return die;
	}
	public void setDateTime(String dateTime) 
	{
		this.dateTime = dateTime;
	}

	public String getDateTime() 
	{
		return dateTime;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("censusId", getCensusId())
            .append("cunAmount", getCunAmount())
            .append("inputAmount", getInputAmount())
            .append("outputAmountBig", getOutputAmountBig())
            .append("outputAmountLittle", getOutputAmountLittle())
            .append("neiXiao", getNeiXiao())
            .append("zFeed", getZFeed())
            .append("fFeed", getFFeed())
            .append("die", getDie())
            .append("remark", getRemark())
            .append("dateTime", getDateTime())
            .toString();
    }
}
