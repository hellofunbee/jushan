package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 药品类别表 nc_yao_class
 *
 * @author ruoyi
 * @date 2019-08-29
 */
public class NcYaoClass extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 药品id */
    private Long yaoId;
    /** 上一级id */
    private Long parentId;
    /** 药品编码 */
    private String code;
    /** 祖级列表 */
    private String ancestors;
    /** 药品名称 */
    private String yaoName;
    /** 显示顺序 */
    private Integer orderNum;
    /** 层级 */
    private Integer level;
    /** 删除标志（0代表存在 2代表删除） */
    private Integer delFlag;

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    //父药品名称
    private String parentName;

    public void setYaoId(Long yaoId)
    {
        this.yaoId = yaoId;
    }

    public Long getYaoId()
    {
        return yaoId;
    }
    public void setParentId(Long parentId)
    {
        this.parentId = parentId;
    }

    public Long getParentId()
    {
        return parentId;
    }
    public void setCode(String code)
    {
        this.code = code;
    }

    public String getCode()
    {
        return code;
    }
    public void setAncestors(String ancestors)
    {
        this.ancestors = ancestors;
    }

    public String getAncestors()
    {
        return ancestors;
    }
    public void setYaoName(String yaoName)
    {
        this.yaoName = yaoName;
    }

    public String getYaoName()
    {
        return yaoName;
    }
    public void setOrderNum(Integer orderNum)
    {
        this.orderNum = orderNum;
    }

    public Integer getOrderNum()
    {
        return orderNum;
    }
    public void setLevel(Integer level)
    {
        this.level = level;
    }

    public Integer getLevel()
    {
        return level;
    }
    public void setDelFlag(Integer delFlag)
    {
        this.delFlag = delFlag;
    }

    public Integer getDelFlag()
    {
        return delFlag;
    }

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("yaoId", getYaoId())
                .append("parentId", getParentId())
                .append("code", getCode())
                .append("ancestors", getAncestors())
                .append("yaoName", getYaoName())
                .append("orderNum", getOrderNum())
                .append("level", getLevel())
                .append("delFlag", getDelFlag())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .toString();
    }
}
