package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.domain.BaseEntity;
import java.util.Date;

/**
 * 菜房出库表 nc_transport_info
 * 
 * @author ruoyi
 * @date 2019-09-05
 */
public class NcTransportInfo extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	
	/** 运输id */
	private Integer transId;
	/** 日期 */
	@Excel(name = "日期", width = 30, dateFormat = "yyyy-MM-dd")
	private Date transDate;
	/** 车牌号 */
	@Excel(name = "车牌号")
	private String carNum;
	/** 框数 */
	@Excel(name = "框数")
	private Integer boxAmount;
	/** 出场时间 */
	@Excel(name = "出场时间" , width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
	private Date outTime;
	/** 到供应科时间 */
	@Excel(name = "到供应科时间" , width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
	private Date gykTime;
	/** 回场时间 */
	@Excel(name = "回场时间" , width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
	private Date backTime;
	/** 到场时间 */
	@Excel(name = "到场时间" , width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
	private Date reachTime;
	/** 经手人 */
	@Excel(name = "经手人")
	private String transUser;
	/** 质检员确认签字 */
	@Excel(name = "质检员确认签字")
	private String inspectionSign;
	/** 司机出场确认签字 */
	@Excel(name = "司机出场确认签字")
	private String driverOutSign;
	/** 协送员出场确认签字 */
	@Excel(name = "协送员出场确认签字")
	private String helpingOutSign;
	/** 协送员回厂确认签字 */
	@Excel(name = "协送员回厂确认签字")
	private String helpingBackSign;
	/** 司机回厂确认签字 */
	@Excel(name = "司机回厂确认签字")
	private String driverBackSign;

	@Override
	public String getRemark() {
		return remark;
	}

	@Override
	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Excel(name = "备注")
	private String remark;

	public void setTransId(Integer transId) 
	{
		this.transId = transId;
	}

	public Integer getTransId() 
	{
		return transId;
	}
	public void setTransDate(Date transDate) 
	{
		this.transDate = transDate;
	}

	public Date getTransDate() 
	{
		return transDate;
	}
	public void setCarNum(String carNum) 
	{
		this.carNum = carNum;
	}

	public String getCarNum() 
	{
		return carNum;
	}
	public void setBoxAmount(Integer boxAmount) 
	{
		this.boxAmount = boxAmount;
	}

	public Integer getBoxAmount() 
	{
		return boxAmount;
	}
	public void setOutTime(Date outTime) 
	{
		this.outTime = outTime;
	}

	public Date getOutTime() 
	{
		return outTime;
	}
	public void setGykTime(Date gykTime) 
	{
		this.gykTime = gykTime;
	}

	public Date getGykTime() 
	{
		return gykTime;
	}
	public void setBackTime(Date backTime) 
	{
		this.backTime = backTime;
	}

	public Date getBackTime() 
	{
		return backTime;
	}
	public void setReachTime(Date reachTime) 
	{
		this.reachTime = reachTime;
	}

	public Date getReachTime() 
	{
		return reachTime;
	}
	public void setTransUser(String transUser) 
	{
		this.transUser = transUser;
	}

	public String getTransUser() 
	{
		return transUser;
	}
	public void setInspectionSign(String inspectionSign) 
	{
		this.inspectionSign = inspectionSign;
	}

	public String getInspectionSign() 
	{
		return inspectionSign;
	}
	public void setDriverOutSign(String driverOutSign) 
	{
		this.driverOutSign = driverOutSign;
	}

	public String getDriverOutSign() 
	{
		return driverOutSign;
	}
	public void setHelpingOutSign(String helpingOutSign) 
	{
		this.helpingOutSign = helpingOutSign;
	}

	public String getHelpingOutSign() 
	{
		return helpingOutSign;
	}
	public void setHelpingBackSign(String helpingBackSign) 
	{
		this.helpingBackSign = helpingBackSign;
	}

	public String getHelpingBackSign() 
	{
		return helpingBackSign;
	}
	public void setDriverBackSign(String driverBackSign) 
	{
		this.driverBackSign = driverBackSign;
	}

	public String getDriverBackSign() 
	{
		return driverBackSign;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("transId", getTransId())
            .append("transDate", getTransDate())
            .append("carNum", getCarNum())
            .append("boxAmount", getBoxAmount())
            .append("outTime", getOutTime())
            .append("gykTime", getGykTime())
            .append("backTime", getBackTime())
            .append("reachTime", getReachTime())
            .append("transUser", getTransUser())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("remark", getRemark())
            .append("inspectionSign", getInspectionSign())
            .append("driverOutSign", getDriverOutSign())
            .append("helpingOutSign", getHelpingOutSign())
            .append("helpingBackSign", getHelpingBackSign())
            .append("driverBackSign", getDriverBackSign())
            .toString();
    }
}
