package com.ruoyi.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.annotation.Excel.Type;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 锅炉房值班记录表 nc_gas_duty
 *
 * @author ruoyi
 * @date 2019-07-31
 */
public class NcGasDuty extends BaseEntity
{
	private static final long serialVersionUID = 1L;

	/** 值班记录id */

	private Long dutyId;
	/** 当班人 */

	private String nextDutyUser;

	@Excel(name = "值班人", prompt = "值班人")
	private String  dutyUserName;

	private String nextDutyUserName;
	/** 值班人 */

	/** 创建时间 */
	@Excel(name = "记录时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss", type = Type.EXPORT)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;
	private String onDutyUser;
	/** 班次 ： 1:全天 2：白班 3：夜班 字典取值 */
	@Excel(name = "夜间最低温度(℃)")
	private BigDecimal dutyNum;
	/** 锅炉号 */
	@Excel(name = "锅炉号", prompt = "锅炉号")
	private Integer boilerNum;

	@Excel(name = "气压阈值", prompt = "气压阈值")
	private BigDecimal qyfz;

	@Excel(name = "今日用气量", prompt = "今日用气量")
	private BigDecimal yql;

	/** 锅炉运行压力（Mpa） */
	@Excel(name = "锅炉运行压力（Mpa）", prompt = "锅炉运行压力（Mpa）")
	private BigDecimal boilerPressure;
	/** 锅炉运行温度（℃） */
	@Excel(name = "锅炉运行温度（℃）", prompt = "锅炉运行温度（℃）")
	private BigDecimal boilerTemperature;
	/** 循环泵1运行压力（Mpa） */
	@Excel(name = "循环泵1运行压力（Mpa）", prompt = "循环泵1运行压力（Mpa）")
	private BigDecimal pressure1;
	/** 循环泵2运行压力（Mpa） */
	@Excel(name = "循环泵2运行压力（Mpa）", prompt = "循环泵2运行压力（Mpa）")
	private BigDecimal pressure2;
	/** 循环泵3运行压力（Mpa） */
	@Excel(name = "循环泵3运行压力（Mpa）", prompt = "循环泵3运行压力（Mpa）")
	private BigDecimal pressure3;
	/** 燃烧器运行情况 1：正常 2：异常 */
	@Excel(name = "燃烧器运行情况", readConverterExp = "1=正常,2=异常")
	private Integer burner;
	/** 系统补水情况 1:正常 2:异常 */
	@Excel(name = "系统补水情况", readConverterExp = "1=正常,2=异常")
	private Integer water;
	/** 环境卫生情况 1:优 2:良 3：差 */
	@Excel(name = "环境卫生情况", readConverterExp = "1=优,2=良,3=差")
	private Integer health;
	/** 排污情况 1：已排 2：未排 */
	@Excel(name = "排污情况", readConverterExp = "1=已排,2=未排")
	private Integer blowOff;




	/**是否选中 默认false*/
	private boolean flag = false;
	@Excel(name = "排污情况")
	private String remark;

	@Override
	public String getRemark() {
		return remark;
	}

	@Override
	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public Date getCreateTime() {
		return createTime;
	}

	@Override
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public String getDutyUserName() {
		return dutyUserName;
	}

	public void setDutyUserName(String dutyUserName) {
		this.dutyUserName = dutyUserName;
	}

	public String getNextDutyUserName() {
		return nextDutyUserName;
	}

	public void setNextDutyUserName(String nextDutyUserName) {
		this.nextDutyUserName = nextDutyUserName;
	}

	public void setDutyId(Long dutyId)
	{
		this.dutyId = dutyId;
	}

	public Long getDutyId()
	{
		return dutyId;
	}
	public void setNextDutyUser(String nextDutyUser)
	{
		this.nextDutyUser = nextDutyUser;
	}

	public String getNextDutyUser()
	{
		return nextDutyUser;
	}
	public void setOnDutyUser(String onDutyUser)
	{
		this.onDutyUser = onDutyUser;
	}

	public String getOnDutyUser()
	{
		return onDutyUser;
	}
	public void setDutyNum(BigDecimal dutyNum)
	{
		this.dutyNum = dutyNum;
	}

	public BigDecimal getDutyNum()
	{
		return dutyNum;
	}
	public void setBoilerNum(Integer boilerNum)
	{
		this.boilerNum = boilerNum;
	}

	public Integer getBoilerNum()
	{
		return boilerNum;
	}
	public void setBoilerPressure(BigDecimal boilerPressure)
	{
		this.boilerPressure = boilerPressure;
	}

	public BigDecimal getBoilerPressure()
	{
		return boilerPressure;
	}
	public void setBoilerTemperature(BigDecimal boilerTemperature)
	{
		this.boilerTemperature = boilerTemperature;
	}

	public BigDecimal getBoilerTemperature()
	{
		return boilerTemperature;
	}
	public void setPressure1(BigDecimal pressure1)
	{
		this.pressure1 = pressure1;
	}

	public BigDecimal getPressure1()
	{
		return pressure1;
	}
	public void setPressure2(BigDecimal pressure2)
	{
		this.pressure2 = pressure2;
	}

	public BigDecimal getPressure2()
	{
		return pressure2;
	}
	public void setPressure3(BigDecimal pressure3)
	{
		this.pressure3 = pressure3;
	}

	public BigDecimal getPressure3()
	{
		return pressure3;
	}
	public void setBurner(Integer burner)
	{
		this.burner = burner;
	}

	public Integer getBurner()
	{
		return burner;
	}
	public void setWater(Integer water)
	{
		this.water = water;
	}

	public Integer getWater()
	{
		return water;
	}
	public void setHealth(Integer health)
	{
		this.health = health;
	}

	public Integer getHealth()
	{
		return health;
	}
	public void setBlowOff(Integer blowOff)
	{
		this.blowOff = blowOff;
	}

	public Integer getBlowOff()
	{
		return blowOff;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("dutyId", getDutyId())
            .append("nextDutyUser", getNextDutyUser())
            .append("onDutyUser", getOnDutyUser())
            .append("createTime", getCreateTime())
            .append("dutyNum", getDutyNum())
            .append("boilerNum", getBoilerNum())
            .append("boilerPressure", getBoilerPressure())
            .append("boilerTemperature", getBoilerTemperature())
            .append("pressure1", getPressure1())
            .append("pressure2", getPressure2())
            .append("pressure3", getPressure3())
            .append("burner", getBurner())
            .append("water", getWater())
            .append("health", getHealth())
            .append("blowOff", getBlowOff())
            .append("createBy", getCreateBy())
            .append("remark", getRemark())
            .toString();
    }

	public BigDecimal getQyfz() {
		return qyfz;
	}

	public void setQyfz(BigDecimal qyfz) {
		this.qyfz = qyfz;
	}

	public BigDecimal getYql() {
		return yql;
	}

	public void setYql(BigDecimal yql) {
		this.yql = yql;
	}
}
