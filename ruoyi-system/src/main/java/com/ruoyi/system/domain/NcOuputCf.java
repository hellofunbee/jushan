package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.annotation.Excels;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.domain.BaseEntity;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 菜房出库表 nc_ouput_cf
 * 
 * @author ruoyi
 * @date 2019-08-30
 */
public class NcOuputCf extends BaseEntity
{
	private static final long serialVersionUID = 1L;

	/**检测的批次code*/
	@Excel(name = "蔬菜批次")
	private String piciCode;

	@Excels({
			@Excel(name = "订单编号", targetAttr = "cforderCode", type = Excel.Type.EXPORT),

			@Excel(name = "作物名称", targetAttr = "cropNameCn", type = Excel.Type.EXPORT),
			@Excel(name = "作物品种", targetAttr = "cropVarietiesCn", type = Excel.Type.EXPORT),
	})

	/**入库对应菜房的订单id*/
	private NcOrderCf ncorderCf;
	/**入库对应计划的班组*/
	@Excel(name = "生产班组")
	private String teamCn;

	/**入库对应计划的计划编码*/
	private String planCode;

	/** 出库id */
	private Integer outputId;
	/** 菜房订单id */
	private Long cforderId;
	/** 计划id */
	private Long planId;
	/** 检测id */
	private Integer checkId;
	/** 入库id */
	private Integer inputId;
	/** 出库类型 ：字典 */

	@Excel(name = "出库类型" ,dictType = "cforderType")
	private Integer outputType;
	/** 实际提供数量（kg） */
	@Excel(name = "实际提供数量（kg）")
	private BigDecimal realAmout;
	/** 框数 */
	private Integer boxAmount;
	/** 出库时间 */
	@Excel(name = "出库时间" , width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss", type = Excel.Type.EXPORT)
	private Date outputTime;
	/** 出库经手人 */
	@Excel(name = "出库经手人")
	private String outputUser;
	/** 状态 1:待生成订单 2：已生成订单*/
	private String status;

	private Long orderId;

	@Excel(name = "备注")
	private String remark;


	public void setOutputId(Integer outputId) 
	{
		this.outputId = outputId;
	}

	public Integer getOutputId() 
	{
		return outputId;
	}
	public void setCforderId(Long cforderId) 
	{
		this.cforderId = cforderId;
	}

	public Long getCforderId() 
	{
		return cforderId;
	}
	public void setPlanId(Long planId) 
	{
		this.planId = planId;
	}

	public Long getPlanId() 
	{
		return planId;
	}
	public void setCheckId(Integer checkId) 
	{
		this.checkId = checkId;
	}

	public Integer getCheckId() 
	{
		return checkId;
	}
	public void setInputId(Integer inputId) 
	{
		this.inputId = inputId;
	}

	public Integer getInputId() 
	{
		return inputId;
	}
	public void setOutputType(Integer outputType) 
	{
		this.outputType = outputType;
	}

	public Integer getOutputType() 
	{
		return outputType;
	}
	public void setRealAmout(BigDecimal realAmout)
	{
		this.realAmout = realAmout;
	}

	public BigDecimal getRealAmout()
	{
		return realAmout;
	}
	public void setBoxAmount(Integer boxAmount) 
	{
		this.boxAmount = boxAmount;
	}

	public Integer getBoxAmount() 
	{
		return boxAmount;
	}
	public void setOutputTime(Date outputTime) 
	{
		this.outputTime = outputTime;
	}

	public Date getOutputTime() 
	{
		return outputTime;
	}
	public void setOutputUser(String outputUser) 
	{
		this.outputUser = outputUser;
	}

	public String getOutputUser() 
	{
		return outputUser;
	}
	public void setStatus(String status) 
	{
		this.status = status;
	}

	public String getStatus() 
	{
		return status;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public NcOrderCf getNcorderCf() {
		return ncorderCf;
	}

	public void setNcorderCf(NcOrderCf ncorderCf) {
		this.ncorderCf = ncorderCf;
	}

	public String getTeamCn() {
		return teamCn;
	}

	public void setTeamCn(String teamCn) {
		this.teamCn = teamCn;
	}

	public String getPlanCode() {
		return planCode;
	}

	public void setPlanCode(String planCode) {
		this.planCode = planCode;
	}

	public String getPiciCode() {
		return piciCode;
	}

	public void setPiciCode(String piciCode) {
		this.piciCode = piciCode;
	}

	@Override
	public String getRemark() {
		return remark;
	}

	@Override
	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("outputId", getOutputId())
            .append("cforderId", getCforderId())
            .append("planId", getPlanId())
            .append("checkId", getCheckId())
            .append("inputId", getInputId())
            .append("outputType", getOutputType())
            .append("realAmout", getRealAmout())
            .append("boxAmount", getBoxAmount())
            .append("outputTime", getOutputTime())
            .append("outputUser", getOutputUser())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("remark", getRemark())
            .append("status", getStatus())
            .toString();
    }
}
