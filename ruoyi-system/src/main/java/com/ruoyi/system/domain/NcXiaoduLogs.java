package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 消毒记录（植保）表 nc_xiaodu_logs
 * 
 * @author ruoyi
 * @date 2019-08-30
 */
public class NcXiaoduLogs extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	
	/** 消毒主键 */
	private Long xdId;
	/** 消毒位置 */
	@Excel(name = "消毒位置")
	private String xdPlace;
	/** 消毒方式 */
	@Excel(name = "消毒方式", readConverterExp = "1=预防消毒,2=临时消毒,3=终止消毒")
	private Long xdType;
	/** 消毒内容 */
	@Excel(name = "消毒内容")
	private String xdInfo;
	/** 消毒人id */

	private Long xdUser;
	/** 消毒人 */
	@Excel(name = "消毒人")
	private String xdUsername;
	/** 部门 */
	@Excel(name = "消毒班组")
	private String xdDeptname;
	/** 班组 */
	private Integer xdClassname;
	/** 消毒时间 */
	@Excel(name = "消毒时间",width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
	private Date xdTime;

	@Override
	public String getRemark() {
		return remark;
	}

	@Override
	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Excel(name = "备注")
	private String remark;

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}
	//消毒次数
	private int amount;

	public void setXdId(Long xdId) 
	{
		this.xdId = xdId;
	}

	public Long getXdId() 
	{
		return xdId;
	}
	public void setXdPlace(String xdPlace) 
	{
		this.xdPlace = xdPlace;
	}

	public String getXdPlace() 
	{
		return xdPlace;
	}
	public void setXdType(Long xdType) 
	{
		this.xdType = xdType;
	}

	public Long getXdType()
	{
		return xdType;
	}
	public void setXdInfo(String  xdInfo)
	{
		this.xdInfo = xdInfo;
	}

	public String  getXdInfo()
	{
		return xdInfo;
	}
	public void setXdUser(Long xdUser) 
	{
		this.xdUser = xdUser;
	}

	public Long getXdUser() 
	{
		return xdUser;
	}
	public void setXdUsername(String xdUsername) 
	{
		this.xdUsername = xdUsername;
	}

	public String getXdUsername() 
	{
		return xdUsername;
	}
	public void setXdDeptname(String xdDeptname) 
	{
		this.xdDeptname = xdDeptname;
	}

	public String getXdDeptname() 
	{
		return xdDeptname;
	}
	public void setXdClassname(Integer xdClassname) 
	{
		this.xdClassname = xdClassname;
	}

	public Integer getXdClassname() 
	{
		return xdClassname;
	}
	public void setXdTime(Date xdTime) 
	{
		this.xdTime = xdTime;
	}

	public Date getXdTime() 
	{
		return xdTime;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("xdId", getXdId())
            .append("xdPlace", getXdPlace())
            .append("xdType", getXdType())
            .append("xdInfo", getXdInfo())
            .append("xdUser", getXdUser())
            .append("xdUsername", getXdUsername())
            .append("xdDeptname", getXdDeptname())
            .append("xdClassname", getXdClassname())
            .append("xdTime", getXdTime())
            .append("createTime", getCreateTime())
            .append("createBy", getCreateBy())
            .append("remark", getRemark())
            .toString();
    }
}
