package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.annotation.Excel.Type;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 牛奶出库表 nc_output_milk
 *
 * @author ruoyi
 * @date 2019-09-11
 */
public class NcOutputMilk extends BaseEntity
{
	private static final long serialVersionUID = 1L;

	/** 出库id */
	@Excel(name = "编号", prompt = "编号")
	private Long outputId;
	/** 入库id */
	private Long inputId;
	/** 菜房订单id */
	private Integer cforderId;
	/** 实际提供数量（kg） */
	@Excel(name = "实际提供量", prompt = "实际提供量")
	private BigDecimal outputAmout;
	/** 瓶数 */
	private Integer bottleAmount;
	/** 出库时间 */
	@Excel(name = "出库时间",  width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss", type = Type.EXPORT)
	private Date outputTime;
	/** 出库类型 ：字典 */
	@Excel(name = "出库类型",dictType = "outputType")
	private Integer outputType;
	/** 出库经手人 */
	@Excel(name = "出库经手人", prompt = "出库经手人")
	private String outputUser;

	private Long orderId;

	private Long ncorderCfId;
	@Excel(name = "牛奶批次", prompt = "牛奶批次")
	private String cforderCode;

	private String cropNameCn;
	private Long cropVarieties;

	private String cropVarietiesCn;

	@Excel(name="备注")
	private String remark;

	@Override
	public String getRemark() {
		return remark;
	}

	@Override
	public void setRemark(String remark) {
		this.remark = remark;
	}

	//出库类型 cforderType
	private Integer cforderType;

	public Integer getCforderType() {
		return cforderType;
	}

	public void setCforderType(Integer cforderType) {
		this.cforderType = cforderType;
	}

	//订单提供的需求数量
	@Excel(name = "需求量", prompt = "需求量")
	private BigDecimal requestAmount;

	//挤奶 消毒 出库经手人
	private String milkmanName;
	private String disinfectorName;
	private String outputUserName;

	public String getMilkmanName() {
		return milkmanName;
	}

	public void setMilkmanName(String milkmanName) {
		this.milkmanName = milkmanName;
	}

	public String getDisinfectorName() {
		return disinfectorName;
	}

	public void setDisinfectorName(String disinfectorName) {
		this.disinfectorName = disinfectorName;
	}

	public String getOutputUserName() {
		return outputUserName;
	}

	public void setOutputUserName(String outputUserName) {
		this.outputUserName = outputUserName;
	}

	public BigDecimal getRequestAmount() {
		return requestAmount;
	}

	public void setRequestAmount(BigDecimal requestAmount) {
		this.requestAmount = requestAmount;
	}

	public String getCropVarietiesCn() {
        return cropVarietiesCn;
    }

    public void setCropVarietiesCn(String cropVarietiesCn) {
        this.cropVarietiesCn = cropVarietiesCn;
    }

    public Long getCropVarieties() {
		return cropVarieties;
	}

	public void setCropVarieties(Long cropVarieties) {
		this.cropVarieties = cropVarieties;
	}

	public String getCropNameCn() {
		return cropNameCn;
	}

	public void setCropNameCn(String cropNameCn) {
		this.cropNameCn = cropNameCn;
	}

	public String getCforderCode() {
		return cforderCode;
	}

	public void setCforderCode(String cforderCode) {
		this.cforderCode = cforderCode;
	}

	private String cropName;
	private Double cfAmounts;
	private String milkCode;
	private String milkman;
	private String disinfector;

	public String getMilkman() {
		return milkman;
	}

	public void setMilkman(String milkman) {
		this.milkman = milkman;
	}

	public String getDisinfector() {
		return disinfector;
	}

	public void setDisinfector(String disinfector) {
		this.disinfector = disinfector;
	}

	public Long getNcorderCfId() {
		return ncorderCfId;
	}

	public void setNcorderCfId(Long ncorderCfId) {
		this.ncorderCfId = ncorderCfId;
	}

	public Long getOutputId() {
		return outputId;
	}

	public void setOutputId(Long outputId) {
		this.outputId = outputId;
	}

	public Long getInputId() {
		return inputId;
	}

	public void setInputId(Long inputId) {
		this.inputId = inputId;
	}

	public Integer getCforderId() {
		return cforderId;
	}

	public void setCforderId(Integer cforderId) {
		this.cforderId = cforderId;
	}

	public BigDecimal getOutputAmout() {
		return outputAmout;
	}

	public void setOutputAmout(BigDecimal outputAmout) {
		this.outputAmout = outputAmout;
	}

	public Integer getBottleAmount() {
		return bottleAmount;
	}

	public void setBottleAmount(Integer bottleAmount) {
		this.bottleAmount = bottleAmount;
	}

	public Date getOutputTime() {
		return outputTime;
	}

	public void setOutputTime(Date outputTime) {
		this.outputTime = outputTime;
	}

	public Integer getOutputType() {
		return outputType;
	}

	public void setOutputType(Integer outputType) {
		this.outputType = outputType;
	}

	public String getOutputUser() {
		return outputUser;
	}

	public void setOutputUser(String outputUser) {
		this.outputUser = outputUser;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("outputId", getOutputId())
            .append("inputId", getInputId())
            .append("cforderId", getCforderId())
            .append("outputAmout", getOutputAmout())
            .append("bottleAmount", getBottleAmount())
            .append("outputTime", getOutputTime())
            .append("outputType", getOutputType())
            .append("outputUser", getOutputUser())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("remark", getRemark())
            .toString();
    }

	public String getMilkCode() {
		return milkCode;
	}

	public void setMilkCode(String milkCode) {
		this.milkCode = milkCode;
	}

	public Double getCfAmounts() {
		return cfAmounts;
	}

	public void setCfAmounts(Double cfAmounts) {
		this.cfAmounts = cfAmounts;
	}

	public String getCropName() {
		return cropName;
	}

	public void setCropName(String cropName) {
		this.cropName = cropName;
	}
}
