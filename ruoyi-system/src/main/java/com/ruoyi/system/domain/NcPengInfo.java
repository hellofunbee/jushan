package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.domain.BaseEntity;
import java.math.BigDecimal;

/**
 * 计划大棚表 nc_peng_info
 * 
 * @author ruoyi
 * @date 2019-09-08
 */
public class NcPengInfo extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	
	/** 主键 */
	private Long pengInfoId;
	/** 计划id */
	private Long planId;
	/** 面积 */
	private BigDecimal area;
	/** 大棚id */
	private Long deptId;
	/** 大棚名字 */
	private String deptName;

	public void setPengInfoId(Long pengInfoId) 
	{
		this.pengInfoId = pengInfoId;
	}

	public Long getPengInfoId() 
	{
		return pengInfoId;
	}
	public void setPlanId(Long planId) 
	{
		this.planId = planId;
	}

	public Long getPlanId() 
	{
		return planId;
	}
	public void setArea(BigDecimal area) 
	{
		this.area = area;
	}

	public BigDecimal getArea() 
	{
		return area;
	}
	public void setDeptId(Long deptId) 
	{
		this.deptId = deptId;
	}

	public Long getDeptId() 
	{
		return deptId;
	}
	public void setDeptName(String deptName) 
	{
		this.deptName = deptName;
	}

	public String getDeptName() 
	{
		return deptName;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("pengInfoId", getPengInfoId())
            .append("planId", getPlanId())
            .append("area", getArea())
            .append("deptId", getDeptId())
            .append("deptName", getDeptName())
            .append("createTime", getCreateTime())
            .append("createBy", getCreateBy())
            .append("remark", getRemark())
            .toString();
    }
}
