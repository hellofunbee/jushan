package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.annotation.Excel.Type;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 物资仓储存表 nc_goods
 *
 * @author ruoyi
 * @date 2019-08-05
 */
public class NcGoods extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	/*@Excel(name = "帐号状态", readConverterExp = "0=正常,1=停用")
	@Excel(name = "最后登陆时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss", type = Type.EXPORT)*/
	/** 物资id */
	@Excel(name = "编号", prompt = "编号")
	private Long goodsId;
	/** 物资批次 */
	@Excel(name = "物资批次", prompt = "物资批次")
	private String goodsCode;
	/** 物资名称 */

	private String goodsName;
	/** 入库数量 */
	@Excel(name = "入库数量", prompt = "入库数量")
	private Integer inputAmount;
	/** 库存 */
	@Excel(name = "库存", prompt = "库存")
	private Integer stockAmout;
	/** 单位 */
	@Excel(name = "单位",dictType = "material_unit")
	private String unit;
	/** 有效期 */
	@Excel(name = "有效期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss", type = Type.EXPORT)
	private Date validDate;
	/** 采购人 */

	private String purchaser;

	/** 物资id*/
	private Long meterialId;

	private NcMeterial ncMeterial;
	@Excel(name = "物资名称", prompt = "物资名称")
	private String meterialName;

	@Excel(name = "采购人", prompt = "采购人")
	private String  purchaserName;
	@Excel(name = "入库人", prompt = "入库人")
	private String createByName;

	private String receiver;

	private String reAmounts;

	private String reTime;

	@Excel(name="备注")
	private String remark;

	@Override
	public String getRemark() {
		return remark;
	}

	@Override
	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public String getPurchaserName() {
		return purchaserName;
	}

	public void setPurchaserName(String purchaserName) {
		this.purchaserName = purchaserName;
	}

	public String getCreateByName() {
		return createByName;
	}

	public void setCreateByName(String createByName) {
		this.createByName = createByName;
	}

	public String getMeterialName() {
		return meterialName;
	}

	public void setMeterialName(String meterialName) {
		this.meterialName = meterialName;
	}

	public NcMeterial getNcMeterial() {
		return ncMeterial;
	}

	public void setNcMeterial(NcMeterial ncMeterial) {
		this.ncMeterial = ncMeterial;
	}

	public Long getMeterialId() {
		return meterialId;
	}

	public void setMeterialId(Long meterialId) {
		this.meterialId = meterialId;
	}

	public void setGoodsId(Long goodsId)
	{
		this.goodsId = goodsId;
	}

	public Long getGoodsId()
	{
		return goodsId;
	}
	public void setGoodsCode(String goodsCode)
	{
		this.goodsCode = goodsCode;
	}

	public String getGoodsCode()
	{
		return goodsCode;
	}
	public void setGoodsName(String goodsName)
	{
		this.goodsName = goodsName;
	}

	public String getGoodsName()
	{
		return goodsName;
	}
	public void setInputAmount(Integer inputAmount)
	{
		this.inputAmount = inputAmount;
	}

	public Integer getInputAmount()
	{
		return inputAmount;
	}
	public void setStockAmout(Integer stockAmout)
	{
		this.stockAmout = stockAmout;
	}

	public Integer getStockAmout()
	{
		return stockAmout;
	}
	public void setUnit(String unit)
	{
		this.unit = unit;
	}

	public String getUnit()
	{
		return unit;
	}
	public void setValidDate(Date validDate)
	{
		this.validDate = validDate;
	}

	public Date getValidDate()
	{
		return validDate;
	}
	public void setPurchaser(String purchaser)
	{
		this.purchaser = purchaser;
	}

	public String getPurchaser()
	{
		return purchaser;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("goodsId", getGoodsId())
            .append("goodsCode", getGoodsCode())
            .append("goodsName", getGoodsName())
            .append("inputAmount", getInputAmount())
            .append("stockAmout", getStockAmout())
            .append("unit", getUnit())
            .append("validDate", getValidDate())
            .append("purchaser", getPurchaser())
            .append("createTime", getCreateTime())
            .append("createBy", getCreateBy())
            .append("remark", getRemark())
			.append("meterialId",getMeterialId())
			.append("ncMeterial",getNcMeterial())
            .toString();
    }

	public String getReAmounts() {
		return reAmounts;
	}

	public void setReAmounts(String reAmounts) {
		this.reAmounts = reAmounts;
	}

	public String getReTime() {
		return reTime;
	}

	public void setReTime(String reTime) {
		this.reTime = reTime;
	}
}
