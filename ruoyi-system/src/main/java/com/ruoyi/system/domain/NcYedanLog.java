package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.Date;

/**
 * 液氮出库表 nc_yedan_log
 *
 * @author ruoyi
 * @date 2019-09-02
 */
public class NcYedanLog extends BaseEntity
{
	private static final Long serialVersionUID = 1L;

	/**  */
	private Long yedanLogId;
	/** 液氮id */
	private Long yedanId;
	/** 库存(升) */
	@Excel(name = "库存(升)")
	private Integer stockAmout;
	/** 出库数量(升) */
	@Excel(name = "出库数量(升)")
	private Integer outputAmount;

	/** 领取购人 */
	@Excel(name = "领取购人")
	private String receiver;

	/** 来源 */
	private String comeFrom;

    @Excel(name = "经手人")
    private String createBy;

	@Excel(name = "入库时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	@Excel(name = "备注")
	private String remark;

    @Override
    public String getCreateBy() {
        return createBy;
    }

    @Override
    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    @Override
	public Date getCreateTime() {
		return createTime;
	}

	@Override
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Override
	public String getRemark() {
		return remark;
	}

	@Override
	public void setRemark(String remark) {
		this.remark = remark;
	}

	public void setYedanLogId(Long yedanLogId)
	{
		this.yedanLogId = yedanLogId;
	}

	public Long getYedanLogId()
	{
		return yedanLogId;
	}
	public void setYedanId(Long yedanId)
	{
		this.yedanId = yedanId;
	}

	public Long getYedanId()
	{
		return yedanId;
	}
	public void setStockAmout(Integer stockAmout)
	{
		this.stockAmout = stockAmout;
	}

	public Integer getStockAmout()
	{
		return stockAmout;
	}
	public void setOutputAmount(Integer outputAmount)
	{
		this.outputAmount = outputAmount;
	}

	public Integer getOutputAmount()
	{
		return outputAmount;
	}
	public void setReceiver(String receiver)
	{
		this.receiver = receiver;
	}

	public String getReceiver()
	{
		return receiver;
	}

	public String getComeFrom() {
		return comeFrom;
	}

	public void setComeFrom(String comeFrom) {
		this.comeFrom = comeFrom;
	}

	public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("yedanLogId", getYedanLogId())
            .append("yedanId", getYedanId())
            .append("createTime", getCreateTime())
            .append("stockAmout", getStockAmout())
            .append("outputAmount", getOutputAmount())
            .append("receiver", getReceiver())
            .append("createBy", getCreateBy())
            .append("remark", getRemark())
            .toString();
    }
}
