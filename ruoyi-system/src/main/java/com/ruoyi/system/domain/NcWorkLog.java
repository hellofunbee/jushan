package com.ruoyi.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 农事记录表 nc_work_log
 *
 * @author ruoyi
 * @date 2019-09-01
 */
public class NcWorkLog extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 栽培记录主键
     */
    private Long zaipeiId;
    /**
     * 计划id
     */
    private Long planId;

    /**
     * 农事镜像id
     */
    private Long workMirrorId;
    /**
     * 农事id
     */
    private Long workId;
    /**
     * 标准id
     */
    private Long standardId;
    /**
     * 阶段id
     */
    private Long stageId;
    /**
     * 农事名称
     */
    @Excel(name = "农事名称")
    private String workName;
    /**
     * 农事类型 1:栽培 2：农资
     */
    @Excel(name = "农事类型", readConverterExp = "1=栽培,2=农资")
    private String workType;
    /**
     * 来源 1:标准 2:新增
     */
    @Excel(name = "来源", readConverterExp = "1=标准,2=新增")
    private Integer source;
    /**
     * 循环周期(天)
     */
    private Integer cycleDays;
    /**
     * 第几天开始
     */
    private Integer workStart;
    /**
     * 第几天结束
     */
    private Integer workEnd;
    /**
     * 动态调整 0: 不是 1：是
     */
    private String isDynamic;
    /**
     * 农资名称
     */
    @Excel(name = "农资名称")
    private String inputCn;
    /**
     * 农资
     */
    private String input;
    /**
     * 数量/亩
     */
    @Excel(name = "数量/亩")
    private Integer inputCount;
    /**
     * 单位
     */
    @Excel(name = "单位", dictType = "normal_unit")
    private String inputUnit;
    /**
     * 工作内容
     */
    @Excel(name = "工作内容", dictType = "normal_unit")
    private String workDetail;

    @Excel(name = "完成时间" , width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss", type = Excel.Type.EXPORT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @Excel(name = "备注")
    private String remark;


    private NcPlan plan;

    public NcWorkLog() {

    }

    public NcWorkLog(NcWorkMirror m) {
        if (m != null) {
            this.planId = m.getPlanId();
            this.workMirrorId = m.getWorkMirrorId();
            this.workId = m.getWorkId();
            this.standardId = m.getStandardId();
            this.stageId = m.getStageMirrorId();
            this.workName = m.getWorkName();
            this.workType = m.getWorkType();
            this.cycleDays = m.getCycleDays();
            this.workStart = m.getWorkStart();
            this.workEnd = m.getWorkEnd();
            this.isDynamic = m.getIsDynamic();
            this.inputCn = m.getInputCn();
            this.input = m.getInput();
            this.inputCount = m.getInputCount();
            this.inputUnit = m.getInputUnit();
            this.workDetail = m.getWorkDetail();
            this.setCreateTime(m.getCreateTime());
            this.setRemark(m.getRemark());

        }

    }

    public void setZaipeiId(Long zaipeiId) {
        this.zaipeiId = zaipeiId;
    }

    public Long getZaipeiId() {
        return zaipeiId;
    }

    public void setWorkId(Long workId) {
        this.workId = workId;
    }

    public Long getWorkId() {
        return workId;
    }

    public void setStandardId(Long standardId) {
        this.standardId = standardId;
    }

    public Long getStandardId() {
        return standardId;
    }

    public void setStageId(Long stageId) {
        this.stageId = stageId;
    }

    public Long getStageId() {
        return stageId;
    }

    public void setWorkName(String workName) {
        this.workName = workName;
    }

    public String getWorkName() {
        return workName;
    }

    public void setWorkType(String workType) {
        this.workType = workType;
    }

    public String getWorkType() {
        return workType;
    }

    public void setCycleDays(Integer cycleDays) {
        this.cycleDays = cycleDays;
    }

    public Integer getCycleDays() {
        return cycleDays;
    }

    public void setWorkStart(Integer workStart) {
        this.workStart = workStart;
    }

    public Integer getWorkStart() {
        return workStart;
    }

    public void setWorkEnd(Integer workEnd) {
        this.workEnd = workEnd;
    }

    public Integer getWorkEnd() {
        return workEnd;
    }

    public void setIsDynamic(String isDynamic) {
        this.isDynamic = isDynamic;
    }

    public String getIsDynamic() {
        return isDynamic;
    }

    public void setInputCn(String inputCn) {
        this.inputCn = inputCn;
    }

    public String getInputCn() {
        return inputCn;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public String getInput() {
        return input;
    }

    public void setInputCount(Integer inputCount) {
        this.inputCount = inputCount;
    }

    public Integer getInputCount() {
        return inputCount;
    }

    public void setInputUnit(String inputUnit) {
        this.inputUnit = inputUnit;
    }

    public String getInputUnit() {
        return inputUnit;
    }

    public void setWorkDetail(String workDetail) {
        this.workDetail = workDetail;
    }

    public String getWorkDetail() {
        return workDetail;
    }

    public Long getWorkMirrorId() {
        return workMirrorId;
    }

    public void setWorkMirrorId(Long workMirrorId) {
        this.workMirrorId = workMirrorId;
    }

    public Long getPlanId() {
        return planId;
    }

    public void setPlanId(Long planId) {
        this.planId = planId;
    }

    public Integer getSource() {
        return source;
    }

    public void setSource(Integer source) {
        this.source = source;
    }

    public NcPlan getPlan() {
        return plan;
    }

    public void setPlan(NcPlan plan) {
        this.plan = plan;
    }

    @Override
    public Date getCreateTime() {
        return createTime;
    }

    @Override
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String getRemark() {
        return remark;
    }

    @Override
    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("zaipeiId", getZaipeiId())
                .append("workId", getWorkId())
                .append("standardId", getStandardId())
                .append("stageId", getStageId())
                .append("workName", getWorkName())
                .append("workType", getWorkType())
                .append("cycleDays", getCycleDays())
                .append("workStart", getWorkStart())
                .append("workEnd", getWorkEnd())
                .append("isDynamic", getIsDynamic())
                .append("inputCn", getInputCn())
                .append("input", getInput())
                .append("inputCount", getInputCount())
                .append("inputUnit", getInputUnit())
                .append("workDetail", getWorkDetail())
                .append("createTime", getCreateTime())
                .append("createBy", getCreateBy())
                .append("remark", getRemark())
                .toString();
    }
}
