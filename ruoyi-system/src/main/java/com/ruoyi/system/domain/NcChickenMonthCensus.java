package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;

/**
 * 鸡舍月报表 nc_chicken_month_census
 * 
 * @author ruoyi
 * @date 2019-09-19
 */
public class NcChickenMonthCensus extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	
	/**  */
	private Long cenusId;
	/** 上月鸡库存 */
	@Excel(name="上月鸡库存")
	private Long lastChickenStack;
	/** 上月饲料库存 */
	@Excel(name="上月饲料库存")
	private BigDecimal lastFeedStack;
	/** 本月增加鸡数量 */
	@Excel(name="本月增加鸡数量")
	private Long chickenStack;
	/** 本月增加饲料数量 */
	@Excel(name="本月增加饲料数量")
	private BigDecimal feedStack;
	/** 直供 */
	@Excel(name="直供")
	private Long zhiChicken;
	/** 内销 */
	@Excel(name="内销")
	private Long neiChicken;
	@Excel(name="死亡")
	private Long siChicken;
	/** 内销 */
	@Excel(name="盘存")
	private BigDecimal stack;
	/**  */
	@Excel(name="时间")
	private String dataTime;

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}
	@Excel(name="项目",readConverterExp = "1=鸡舍,2=饲料")
	private Integer type;

	public void setCenusId(Long cenusId) 
	{
		this.cenusId = cenusId;
	}

	public Long getCenusId() 
	{
		return cenusId;
	}
	public void setLastChickenStack(Long lastChickenStack) 
	{
		this.lastChickenStack = lastChickenStack;
	}

	public Long getLastChickenStack() 
	{
		return lastChickenStack;
	}
	public void setLastFeedStack(BigDecimal lastFeedStack) 
	{
		this.lastFeedStack = lastFeedStack;
	}

	public BigDecimal getLastFeedStack() 
	{
		return lastFeedStack;
	}
	public void setChickenStack(Long chickenStack) 
	{
		this.chickenStack = chickenStack;
	}

	public Long getChickenStack() 
	{
		return chickenStack;
	}
	public void setFeedStack(BigDecimal feedStack) 
	{
		this.feedStack = feedStack;
	}

	public BigDecimal getFeedStack() 
	{
		return feedStack;
	}
	public void setZhiChicken(Long zhiChicken) 
	{
		this.zhiChicken = zhiChicken;
	}

	public Long getZhiChicken() 
	{
		return zhiChicken;
	}
	public void setNeiChicken(Long neiChicken) 
	{
		this.neiChicken = neiChicken;
	}

	public Long getNeiChicken() 
	{
		return neiChicken;
	}
	public void setStack(BigDecimal stack)
	{
		this.stack = stack;
	}

	public BigDecimal getStack()
	{
		return stack;
	}
	public void setDataTime(String dataTime)
	{
		this.dataTime = dataTime;
	}

	public String getDataTime()
	{
		return dataTime;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("cenusId", getCenusId())
            .append("lastChickenStack", getLastChickenStack())
            .append("lastFeedStack", getLastFeedStack())
            .append("chickenStack", getChickenStack())
            .append("feedStack", getFeedStack())
            .append("zhiChicken", getZhiChicken())
            .append("neiChicken", getNeiChicken())
            .append("stack", getStack())
            .append("dataTime", getDataTime())
            .toString();
    }

	public Long getSiChicken() {
		return siChicken;
	}

	public void setSiChicken(Long siChicken) {
		this.siChicken = siChicken;
	}
}
