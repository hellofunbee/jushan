package com.ruoyi.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.domain.BaseEntity;
import java.util.Date;

/**
 * 兽药库存表 nc_drugs
 *
 * @author ruoyi
 * @date 2019-09-03
 */
public class NcDrugs extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 兽药id */
    private Long drugId;
    private Long yaoId;
    /** 药品批次 */
    @Excel(name="药品批次")
    private String drugCode;
    /** 兽药名称 */
    @Excel(name="兽药名称")
    private String drugName;
    /** 生产厂家 */
    @Excel(name="生产厂家")
    private String factory;
    /** 合格证（有/无） 1:有 2：无 */
    @Excel(name="生产厂家",readConverterExp = "1=有,2=无" )
    private Integer isHege;
    /** 入库数量 */
    @Excel(name="入库数量")
    private Integer inputAmount;
    /** 库存 */
    @Excel(name="库存")
    private Integer stockAmout;
    /** 单位 */
    @Excel(name="单位",dictType = "unitType")
    private String unit;
    /** 有效期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name="有效期")
    private Date validDate;
    /** 采购人 */
    @Excel(name="采购人")
    private String purchaser;
    /** 功能主治 */
    @Excel(name="功能主治")
    private String mainFunction;
    /** 创建者 */
    @Excel(name="创建者")
    private String createBy;


    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name="巡视时间")
    private Date createTime;

    /** 备注 */
    @Excel(name="备注")
    private String remark;
    public Long getYaoId() {
        return yaoId;
    }

    public void setYaoId(Long yaoId) {
        this.yaoId = yaoId;
    }

    public void setDrugId(Long drugId)
    {
        this.drugId = drugId;
    }

    public Long getDrugId()
    {
        return drugId;
    }
    public void setDrugCode(String drugCode)
    {
        this.drugCode = drugCode;
    }

    public String getDrugCode()
    {
        return drugCode;
    }
    public void setDrugName(String drugName)
    {
        this.drugName = drugName;
    }

    public String getDrugName()
    {
        return drugName;
    }
    public void setFactory(String factory)
    {
        this.factory = factory;
    }

    public String getFactory()
    {
        return factory;
    }
    public void setIsHege(Integer isHege)
    {
        this.isHege = isHege;
    }

    public Integer getIsHege()
    {
        return isHege;
    }
    public void setInputAmount(Integer inputAmount)
    {
        this.inputAmount = inputAmount;
    }

    public Integer getInputAmount()
    {
        return inputAmount;
    }
    public void setStockAmout(Integer stockAmout)
    {
        this.stockAmout = stockAmout;
    }

    public Integer getStockAmout()
    {
        return stockAmout;
    }
    public void setUnit(String unit)
    {
        this.unit = unit;
    }

    public String getUnit()
    {
        return unit;
    }
    public void setValidDate(Date validDate)
    {
        this.validDate = validDate;
    }

    public Date getValidDate()
    {
        return validDate;
    }
    public void setPurchaser(String purchaser)
    {
        this.purchaser = purchaser;
    }

    public String getPurchaser()
    {
        return purchaser;
    }
    public void setMainFunction(String mainFunction)
    {
        this.mainFunction = mainFunction;
    }

    public String getMainFunction()
    {
        return mainFunction;
    }

    @Override
    public String getCreateBy() {
        return createBy;
    }

    @Override
    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    @Override
    public Date getCreateTime() {
        return createTime;
    }

    @Override
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String getRemark() {
        return remark;
    }

    @Override
    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("drugId", getDrugId())
                .append("yaoId", getYaoId())
                .append("drugCode", getDrugCode())
                .append("drugName", getDrugName())
                .append("factory", getFactory())
                .append("isHege", getIsHege())
                .append("inputAmount", getInputAmount())
                .append("stockAmout", getStockAmout())
                .append("unit", getUnit())
                .append("createTime", getCreateTime())
                .append("createBy", getCreateBy())
                .append("validDate", getValidDate())
                .append("purchaser", getPurchaser())
                .append("remark", getRemark())
                .append("mainFunction", getMainFunction())
                .toString();
    }
}
