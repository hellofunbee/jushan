package com.ruoyi.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.Date;

/**
 * 免疫记录表 nc_xunshi_logs
 * 
 * @author ruoyi
 * @date 2019-09-02
 */
public class NcXunshiLogs extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	
	/** 巡视id */
	private Long xsId;
	/** 巡视类型 1:牛舍 2：鸡舍 */
	@Excel(name="巡视类型",readConverterExp = "1=牛舍,2=鸡舍")
	private Integer xsType;
	/**
	 * 	状态:1：待确认 2：已挂起 3：已确认：4：已生成订单
	 */
	private Integer xsStatus;
	/** 订单id */
	private Long orderId;
	/** 圈舍号/鸡舍号 */
	@Excel(name="圈舍号/鸡舍号")
	private String roomNum;
	/** 存栏数量 */
	@Excel(name="存栏数量")
	private Integer stockAmount;
	/** 巡视数量 */
	@Excel(name="巡视数量")
	private Integer xsAmount;

	/** 创建者 */
	@Excel(name="创建者")
	private String createBy;


	/** 创建时间 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@Excel(name="巡视时间")
	private Date createTime;

	/** 备注 */
	@Excel(name="备注")
	private String remark;

	public void setXsId(Long xsId) 
	{
		this.xsId = xsId;
	}

	public Long getXsId() 
	{
		return xsId;
	}
	public void setXsType(Integer xsType) 
	{
		this.xsType = xsType;
	}

	public Integer getXsType() 
	{
		return xsType;
	}
	public void setXsStatus(Integer xsStatus)
	{
		this.xsStatus = xsStatus;
	}

	public Integer getXsStatus()
	{
		return xsStatus;
	}
	public void setOrderId(Long orderId)
	{
		this.orderId = orderId;
	}

	public Long getOrderId()
	{
		return orderId;
	}
	public void setRoomNum(String roomNum) 
	{
		this.roomNum = roomNum;
	}

	public String getRoomNum() 
	{
		return roomNum;
	}
	public void setStockAmount(Integer stockAmount) 
	{
		this.stockAmount = stockAmount;
	}

	public Integer getStockAmount() 
	{
		return stockAmount;
	}
	public void setXsAmount(Integer xsAmount) 
	{
		this.xsAmount = xsAmount;
	}

	public Integer getXsAmount() 
	{
		return xsAmount;
	}

	@Override
	public String getCreateBy() {
		return createBy;
	}

	@Override
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	@Override
	public Date getCreateTime() {
		return createTime;
	}

	@Override
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Override
	public String getRemark() {
		return remark;
	}

	@Override
	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("xsId", getXsId())
            .append("xsType", getXsType())
            .append("xsStatus", getXsStatus())
            .append("orderId", getOrderId())
            .append("roomNum", getRoomNum())
            .append("stockAmount", getStockAmount())
            .append("xsAmount", getXsAmount())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("remark", getRemark())
            .toString();
    }
}
