package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 宰杀表 nc_zaisha
 * 
 * @author ruoyi
 * @date 2019-09-04
 */
public class NcZaisha extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	
	/** 宰杀Id */
	private Long zaishaId;
	/** 宰杀时间
 */
	@Excel(name="宰杀时间",width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
	private Date zaishaTime;
	/** 宰杀数量 */
	@Excel(name="宰杀数量")
	private Long zaishaCount;
	/** 宰杀前体况 */
	@Excel(name="宰杀前体况",readConverterExp = "1=正常,2=不正常")
	private Long zaishaQianTraining;
	/** 宰杀后体况
 */
	@Excel(name="宰杀前体况",readConverterExp = "1=健康,2=不健康")
	private Long zaishaHouTraining;
	/** 宰杀操作人 */
	@Excel(name="宰杀操作人")
	private String zaishaOperation;

	@Excel(name="备注")
	private String remark;

	@Override
	public String getRemark() {
		return remark;
	}

	@Override
	public void setRemark(String remark) {
		this.remark = remark;
	}

	public void setZaishaId(Long zaishaId)
	{
		this.zaishaId = zaishaId;
	}

	public Long getZaishaId() 
	{
		return zaishaId;
	}
	public void setZaishaTime(Date zaishaTime) 
	{
		this.zaishaTime = zaishaTime;
	}

	public Date getZaishaTime() 
	{
		return zaishaTime;
	}
	public void setZaishaCount(Long zaishaCount) 
	{
		this.zaishaCount = zaishaCount;
	}

	public Long getZaishaCount() 
	{
		return zaishaCount;
	}
	public void setZaishaQianTraining(Long zaishaQianTraining) 
	{
		this.zaishaQianTraining = zaishaQianTraining;
	}

	public Long getZaishaQianTraining() 
	{
		return zaishaQianTraining;
	}
	public void setZaishaHouTraining(Long zaishaHouTraining) 
	{
		this.zaishaHouTraining = zaishaHouTraining;
	}

	public Long getZaishaHouTraining() 
	{
		return zaishaHouTraining;
	}
	public void setZaishaOperation(String zaishaOperation) 
	{
		this.zaishaOperation = zaishaOperation;
	}

	public String getZaishaOperation() 
	{
		return zaishaOperation;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("zaishaId", getZaishaId())
            .append("zaishaTime", getZaishaTime())
            .append("zaishaCount", getZaishaCount())
            .append("zaishaQianTraining", getZaishaQianTraining())
            .append("zaishaHouTraining", getZaishaHouTraining())
            .append("zaishaOperation", getZaishaOperation())
            .append("remark", getRemark())
            .toString();
    }
}
