package com.ruoyi.system.domain;

import java.util.List;

public class ExcelNeeded {
    public String sheetName;
    public String titleName;
    public String fileName;
    public int columnNumber;
    public int[] columnWidth;
    public String[] columnName;
    public List<List<String>> dataList;
}
