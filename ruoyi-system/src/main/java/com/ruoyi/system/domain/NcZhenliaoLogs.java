
package com.ruoyi.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 诊疗单表 nc_zhenliao_logs
 *
 * @author ruoyi
 * @date 2019-09-04
 */
public class NcZhenliaoLogs extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 诊疗id
     */
    @Excel(name="诊疗序号")
    private Long zlId;
    /**
     * 诊疗类型 1:牛舍 2：鸡舍
     */
    private Integer zlType;
    /**
     * 状态:1：待确认 2：已挂起 3：已确认：4：已生成订单 5: 待发送
     */
    @Excel(name="状态",readConverterExp = "1=待确认,2=已挂起,3=已确认,4=已生成订单,5=待发送")
    private String zlStatus;
    /**
     * 订单id
     */
    private Long orderId;
    /**
     * 牛id
     */
    private Long cowId;
    /**
     * 牛耳號
     */
    @Excel(name="牛耳号")
    private String cowCode;
    /**
     * 圈舍号/鸡舍号
     */
    @Excel(name="鸡舍号/圈舍号")
    private String roomNum;
    /**
     * 日龄
     */
    @Excel(name="日龄")
    private Integer ageDays;
    /**
     * 发病次数
     */
    @Excel(name="发病次数")
    private Integer illMuns;
    /**
     * 症状
     */
    @Excel(name="症状")
    private String symptom;


    /**
     * 推送人
     */
    @Excel(name="推送人")
    private Long reportUser;

    /**
     * 推送人名字
     */
    @Excel(name="推送人名字")
    private String reportUserName;

    /**
     * 诊疗人员
     */
    @Excel(name="诊疗人员")
    private Long zlUser;

    /**
     * 诊疗人员名字
     */
    @Excel(name="诊疗人员名字")
    private String zlUserName;


    /**
     * 诊疗用药id
     */
    private Long drugId;
    /**
     * 药批次
     */
    private String drugCode;
    /**
     * 药名称
     */
    private String drugName;
    /**
     * 用药方法
     */
    private Integer drugMethod;
    private String drugMethodStr;
    /**
     * 用药剂量
     */
    private Integer drugAmount;
    private String drugAmountStr;

    /**
     * 用药单位
     */
    private String unit;
    /**
     * 诊疗结果
     */
    @Excel(name="诊疗结果")
    private String zlResult;
    /**
     * 诊疗时间
     */
    @Excel(name="诊疗时间",width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date zlTime;

    /** 创建者 */
    @Excel(name="创建者")
    private String createBy;


    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name="巡视时间")
    private Date createTime;

    /** 备注 */
    @Excel(name="备注")
    private String remark;

    @Override
    public String getCreateBy() {
        return createBy;
    }

    @Override
    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    @Override
    public Date getCreateTime() {
        return createTime;
    }

    @Override
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String getRemark() {
        return remark;
    }

    @Override
    public void setRemark(String remark) {
        this.remark = remark;
    }


    public String getDrugMethodStr() {
        return drugMethodStr;
    }

    public void setDrugMethodStr(String drugMethodStr) {
        this.drugMethodStr = drugMethodStr;
    }

    /**
     * 药品id
     */
    private Long yaoId;

	/**
	 * 药品名称
	 */
	private String yaoName;
    /**
     * 药品编码
     */
    private String code;

    private List<Map<String,Object>> yaos;


    public List<Map<String, Object>> getYaos() {
        return yaos;
    }

    public void setYaos(List<Map<String, Object>> yaos) {
        this.yaos = yaos;
    }

    public Long getZlId() {
        return zlId;
    }

    public void setZlId(Long zlId) {
        this.zlId = zlId;
    }

    public Integer getZlType() {
        return zlType;
    }

    public void setZlType(Integer zlType) {
        this.zlType = zlType;
    }

    public String getZlStatus() {
        return zlStatus;
    }

    public void setZlStatus(String zlStatus) {
        this.zlStatus = zlStatus;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getCowId() {
        return cowId;
    }

    public void setCowId(Long cowId) {
        this.cowId = cowId;
    }

    public String getCowCode() {
        return cowCode;
    }

    public void setCowCode(String cowCode) {
        this.cowCode = cowCode;
    }

    public String getRoomNum() {
        return roomNum;
    }

    public void setRoomNum(String roomNum) {
        this.roomNum = roomNum;
    }

    public Integer getAgeDays() {
        return ageDays;
    }

    public void setAgeDays(Integer ageDays) {
        this.ageDays = ageDays;
    }

    public Integer getIllMuns() {
        return illMuns;
    }

    public void setIllMuns(Integer illMuns) {
        this.illMuns = illMuns;
    }

    public String getSymptom() {
        return symptom;
    }

    public void setSymptom(String symptom) {
        this.symptom = symptom;
    }

    public Long getReportUser() {
        return reportUser;
    }

    public void setReportUser(Long reportUser) {
        this.reportUser = reportUser;
    }

    public String getReportUserName() {
        return reportUserName;
    }

    public void setReportUserName(String reportUserName) {
        this.reportUserName = reportUserName;
    }

    public Long getZlUser() {
        return zlUser;
    }

    public void setZlUser(Long zlUser) {
        this.zlUser = zlUser;
    }

    public String getZlUserName() {
        return zlUserName;
    }

    public void setZlUserName(String zlUserName) {
        this.zlUserName = zlUserName;
    }

    public Long getDrugId() {
        return drugId;
    }

    public void setDrugId(Long drugId) {
        this.drugId = drugId;
    }

    public String getDrugCode() {
        return drugCode;
    }

    public void setDrugCode(String drugCode) {
        this.drugCode = drugCode;
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public Integer getDrugMethod() {
        return drugMethod;
    }

    public void setDrugMethod(Integer drugMethod) {
        this.drugMethod = drugMethod;
    }

    public Integer getDrugAmount() {
        return drugAmount;
    }

    public void setDrugAmount(Integer drugAmount) {
        this.drugAmount = drugAmount;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getZlResult() {
        return zlResult;
    }

    public void setZlResult(String zlResult) {
        this.zlResult = zlResult;
    }

    public Date getZlTime() {
        return zlTime;
    }

    public void setZlTime(Date zlTime) {
        this.zlTime = zlTime;
    }

    public Long getYaoId() {
        return yaoId;
    }

    public void setYaoId(Long yaoId) {
        this.yaoId = yaoId;
    }

    public String getYaoName() {
        return yaoName;
    }

    public void setYaoName(String yaoName) {
        this.yaoName = yaoName;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "NcZhenliaoLogs{" +
                "zlId=" + zlId +
                ", zlType=" + zlType +
                ", zlStatus=" + zlStatus +
                ", orderId=" + orderId +
                ", cowId=" + cowId +
                ", cowCode='" + cowCode + '\'' +
                ", roomNum='" + roomNum + '\'' +
                ", ageDays=" + ageDays +
                ", illMuns=" + illMuns +
                ", symptom='" + symptom + '\'' +
                ", reportUser='" + reportUser + '\'' +
                ", zlUser='" + zlUser + '\'' +
                ", drugId=" + drugId +
                ", drugCode='" + drugCode + '\'' +
                ", drugName='" + drugName + '\'' +
                ", drugMethod=" + drugMethod +
                ", drugAmount=" + drugAmount +
                ", unit='" + unit + '\'' +
                ", zlResult='" + zlResult + '\'' +
                ", zlTime=" + zlTime +
                '}';
    }
}
