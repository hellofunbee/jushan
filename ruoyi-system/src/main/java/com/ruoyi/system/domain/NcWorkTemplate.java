package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 标准农事表 nc_work
 *
 * @author ruoyi
 * @date 2019-07-28
 */
public class NcWorkTemplate extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	/**
	 * 导入标准序号
	 */
	@Excel(name = "标准序号")
	private Double serialStandardNumber;
	/**
	 * 导入阶段序号
	 */
	@Excel(name = "阶段序号")
	private Double serialStageNumber;

	/** 农事id */
	private Long workId;
	/** 标准id */
	private Long standardId;
	/** 阶段id */
	private Long stageId;
	/** 农事名称 */
	@Excel(name = "农事名称")
	private String workName;
	/** 农事类型 1:栽培 2：农资 */
	@Excel(name = "农事类型 栽培/农资",readConverterExp = "0=栽培,1=农资")
	private String workType;
	/** 循环周期(天) */
	@Excel(name = "循环周期(天)")
	private Integer cycleDays;
	/** 第几天开始 */
	@Excel(name = "第几天开始")
	private Integer workStart;
	/** 第几天结束 */
	@Excel(name = "第几天结束")
	private Integer workEnd;
	/** 动态调整 0: 不是 1：是 */
	@Excel(name = "动态调整 不是/是 ",readConverterExp = "1=不是,2=是 \"")
	private String isDynamic;
	/** 农资名称 */
	@Excel(name = "农资名称")
	private String inputCn;
	/** 农资 */
//	@Excel(name = "农资")
	private String input;
	/** 数量/亩 */
	@Excel(name = "数量/亩")
	private Integer inputCount;
	/** 单位 */
	@Excel(name = "单位",dictType = "unitType")
	private String inputUnit;
	/** 工作内容 */
	@Excel(name = "工作内容 ")
	private String workDetail;
	/** 分类 1：农事 2：阶段 */
	private String type;

	@Excel(name = "备注")
	private String remark;

	@Override
	public String getRemark() {
		return remark;
	}

	@Override
	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Double getSerialStandardNumber() {
		return serialStandardNumber;
	}

	public void setSerialStandardNumber(Double serialStandardNumber) {
		this.serialStandardNumber = serialStandardNumber;
	}


	public Double getSerialStageNumber() {
		return serialStageNumber;
	}

	public void setSerialStageNumber(Double serialStageNumber) {
		this.serialStageNumber = serialStageNumber;
	}

	public void setWorkId(Long workId)
	{
		this.workId = workId;
	}

	public Long getWorkId()
	{
		return workId;
	}
	public void setStandardId(Long standardId)
	{
		this.standardId = standardId;
	}

	public Long getStandardId()
	{
		return standardId;
	}
	public void setStageId(Long stageId)
	{
		this.stageId = stageId;
	}

	public Long getStageId()
	{
		return stageId;
	}
	public void setWorkName(String workName)
	{
		this.workName = workName;
	}

	public String getWorkName()
	{
		return workName;
	}
	public void setWorkType(String workType)
	{
		this.workType = workType;
	}

	public String getWorkType()
	{
		return workType;
	}
	public void setCycleDays(Integer cycleDays)
	{
		this.cycleDays = cycleDays;
	}

	public Integer getCycleDays()
	{
		return cycleDays;
	}
	public void setWorkStart(Integer workStart)
	{
		this.workStart = workStart;
	}

	public Integer getWorkStart()
	{
		return workStart;
	}
	public void setWorkEnd(Integer workEnd)
	{
		this.workEnd = workEnd;
	}

	public Integer getWorkEnd()
	{
		return workEnd;
	}
	public void setIsDynamic(String isDynamic)
	{
		this.isDynamic = isDynamic;
	}

	public String getIsDynamic()
	{
		return isDynamic;
	}
	public void setInputCn(String inputCn)
	{
		this.inputCn = inputCn;
	}

	public String getInputCn()
	{
		return inputCn;
	}
	public void setInput(String input)
	{
		this.input = input;
	}

	public String getInput()
	{
		return input;
	}
	public void setInputCount(Integer inputCount)
	{
		this.inputCount = inputCount;
	}

	public Integer getInputCount()
	{
		return inputCount;
	}
	public void setInputUnit(String inputUnit)
	{
		this.inputUnit = inputUnit;
	}

	public String getInputUnit()
	{
		return inputUnit;
	}
	public void setWorkDetail(String workDetail)
	{
		this.workDetail = workDetail;
	}

	public String getWorkDetail()
	{
		return workDetail;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("workId", getWorkId())
            .append("standardId", getStandardId())
            .append("stageId", getStageId())
            .append("workName", getWorkName())
            .append("workType", getWorkType())
            .append("cycleDays", getCycleDays())
            .append("workStart", getWorkStart())
            .append("workEnd", getWorkEnd())
            .append("isDynamic", getIsDynamic())
            .append("inputCn", getInputCn())
            .append("input", getInput())
            .append("inputCount", getInputCount())
            .append("inputUnit", getInputUnit())
            .append("workDetail", getWorkDetail())
            .append("createTime", getCreateTime())
            .append("createBy", getCreateBy())
            .append("remark", getRemark())
            .toString();
    }
}
