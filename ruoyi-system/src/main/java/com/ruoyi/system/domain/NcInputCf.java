package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 菜房入库表 nc_input_cf
 *
 * @author ruoyi
 * @date 2019-08-30
 */
public class NcInputCf extends BaseEntity {
    private static final long serialVersionUID = 1L;




    @Excel(name = "订单编号",  type = Excel.Type.EXPORT)
    private String cforderCode2;

    @Excel(name = "蔬菜批次")
    private String piciCode;

    @Excel(name = "作物名称")
    private String cropNameCn;
    @Excel(name = "作物品种")
    private String cropVarietiesCn;

    /**
     * 入库对应计划的班组
     */
    @Excel(name = "生产班组")
    private String teamCn;

    /**
     * 入库id
     */
    private Long inputId;
    /**
     * 菜房订单id
     */
    private Long cforderId;
    /**
     * 计划id
     */
    private Long planId;
    /**
     * 检测id
     */
    private Long checkId;
    /**
     * 入库类型 ：字典
     */

    @Excel(name = "入库类型",dictType = "cforderType")

    private Integer inputType2;
    private Integer inputType;
    /**
     * 入库数量（kg）
     */
    @Excel(name = "入库数量（kg）")
    private BigDecimal inputAmout;
    /**
     * 剩余量
     */
    @Excel(name = "剩余量（kg）")
    private BigDecimal leftAmount;
    /**
     * 框数
     */
    @Excel(name = "框数")
    private Integer boxAmount;
    /**
     * 入库时间
     */
    @Excel(name = "入库时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss", type = Excel.Type.EXPORT)
    private Date inputTime;
    /**
     * 入库经手人
     */
    @Excel(name = "入库经手人")
    private String inputUser;
    /**
     * 状态 1：待生成入库单 2：已生成入库单
     */
    private String status;

    private long orderId;

    //额外参数


    /**
     * 入库对应菜房的订单id
     */
    private NcOrderCf ncorderCf;

    /**
     * 入库对应计划的计划编码
     */
    private String planCode;
    @Excel(name = "备注")
    private String remark;

    public void setInputId(Long inputId) {
        this.inputId = inputId;
    }

    public Long getInputId() {
        return inputId;
    }

    public void setCforderId(Long cforderId) {
        this.cforderId = cforderId;
    }

    public Long getCforderId() {
        return cforderId;
    }

    public void setPlanId(Long planId) {
        this.planId = planId;
    }

    public Long getPlanId() {
        return planId;
    }

    public void setCheckId(Long checkId) {
        this.checkId = checkId;
    }

    public Long getCheckId() {
        return checkId;
    }

    public void setInputType(Integer inputType) {
        this.inputType = inputType;
    }

    public Integer getInputType() {
        return inputType;
    }

    public void setInputAmout(BigDecimal inputAmout) {
        this.inputAmout = inputAmout;
    }

    public BigDecimal getInputAmout() {
        return inputAmout;
    }

    public void setLeftAmount(BigDecimal leftAmount) {
        this.leftAmount = leftAmount;
    }

    public BigDecimal getLeftAmount() {
        return leftAmount;
    }

    public void setBoxAmount(Integer boxAmount) {
        this.boxAmount = boxAmount;
    }

    public Integer getBoxAmount() {
        return boxAmount;
    }

    public void setInputTime(Date inputTime) {
        this.inputTime = inputTime;
    }

    public Date getInputTime() {
        return inputTime;
    }

    public void setInputUser(String inputUser) {
        this.inputUser = inputUser;
    }

    public String getInputUser() {
        return inputUser;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public String getCropNameCn() {
        return cropNameCn;
    }

    public void setCropNameCn(String cropNameCn) {
        this.cropNameCn = cropNameCn;
    }

    public String getCropVarietiesCn() {
        return cropVarietiesCn;
    }

    public void setCropVarietiesCn(String cropVarietiesCn) {
        this.cropVarietiesCn = cropVarietiesCn;
    }

    public String getPiciCode() {
        return piciCode;
    }

    public void setPiciCode(String piciCode) {
        this.piciCode = piciCode;
    }

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public NcOrderCf getNcorderCf() {
        return ncorderCf;
    }

    public void setNcorderCf(NcOrderCf ncorderCf) {
        this.ncorderCf = ncorderCf;
    }

    public String getTeamCn() {
        return teamCn;
    }

    public void setTeamCn(String teamCn) {
        this.teamCn = teamCn;
    }

    public String getPlanCode() {
        return planCode;
    }

    public void setPlanCode(String planCode) {
        this.planCode = planCode;
    }

    @Override
    public String getRemark() {
        return remark;
    }

    @Override
    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getInputType2() {
        return inputType2;
    }

    public void setInputType2(Integer inputType2) {
        this.inputType2 = inputType2;
    }

    public String getCforderCode2() {
        return cforderCode2;
    }

    public void setCforderCode2(String cforderCode2) {
        this.cforderCode2 = cforderCode2;
    }

    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("inputId", getInputId())
                .append("cforderId", getCforderId())
                .append("planId", getPlanId())
                .append("checkId", getCheckId())
                .append("inputType", getInputType())
                .append("inputAmout", getInputAmout())
                .append("leftAmount", getLeftAmount())
                .append("boxAmount", getBoxAmount())
                .append("inputTime", getInputTime())
                .append("inputUser", getInputUser())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("remark", getRemark())
                .append("status", getStatus())
                .toString();
    }
}
