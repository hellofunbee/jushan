package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 菜房订单表 nc_order_cf
 *
 * @author ruoyi
 * @date 2019-08-29
 */
public class NcOrderCf extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 菜房订单主键
     */
    private Long cforderId;
    /**
     * 订单id
     */
    private Long orderId;
    /**
     * 计划id
     */
    private Integer planId;
    /**
     * 订单类型： 字典维护 1:直供 2：内销
     */
    @Excel(name = "订单类型",dictType = "cforderType")
    private Integer cforderType;
    /**
     * 订单编号
     */
    @Excel(name = "订单编号",type = Excel.Type.EXPORT)
    private String cforderCode;
    /**
     * 作物名称
     */
    @Excel(name = "作物名称")
    private String cropNameCn;
    /**
     * 作物品种id
     */
    private Long cropVarieties;
    /**
     * 作物品种
     */
    @Excel(name = "作物品种")
    private String cropVarietiesCn;
    /**
     * 需求数量
     */
    @Excel(name = "需求数量")
    private BigDecimal requestAmount;
    /**
     * 单位
     */
    @Excel(name = "单位/kg")
    private String unit;
    /**
     * 需求时间
     */
    @Excel(name = "需求时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date requestTime;
    /**
     * 完成时间
     */
    private Date finishTime;

    private BigDecimal outputWeight;

    private String status;

    /*检测的批次*/
    private String piciCode;
    /**
     * 状态
     * 蔬菜类状态：1、待分配，2、待检测，3、待入库，4、待出库，5、已完成
     * 牛奶鸡蛋：6、牛舍待确认，7、鸡舍待确认，8、牛舍已确认，9、鸡舍已确认 10.挂起
     */
    @Excel(name="状态",readConverterExp = "1=待分配,2=待检测,3=待入库,4=待出库,5=已完成,6=牛舍待确认,7=鸡舍待确认,8=牛舍已确认,9=鸡舍已确认,10=挂起")
    private String cforderStatus;
    /**
     * 分配情况 1：检测 2：仓储
     */
    @Excel(name="分配情况",readConverterExp = "1=检测,2=仓储")
    private String distribution;
    /**
     * 菜房 1：菜房1 2：菜房2
     */
    private Integer cforderFor;

    /**是否匹配计划 1:匹配 2：未匹配 */
    private String hasPlan;

    /**是否检测订单 1:是 2：不是 */
    private Integer isCheck;


    /**入库对应计划的班组*/
    private String teamCn;
    /**入库对应计划的计划编码*/
    @Excel(name="匹配计划")
    private String planCode;
    @Excel(name = "创建时间",width =30,dateFormat = "yyyy-MM-dd")
    private Date createTime;

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public void setCforderId(Long cforderId) {
        this.cforderId = cforderId;
    }

    public Long getCforderId() {
        return cforderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setPlanId(Integer planId) {
        this.planId = planId;
    }

    public Integer getPlanId() {
        return planId;
    }

    public void setCforderType(Integer cforderType) {
        this.cforderType = cforderType;
    }

    public Integer getCforderType() {
        return cforderType;
    }

    public void setCforderCode(String cforderCode) {
        this.cforderCode = cforderCode;
    }

    public String getCforderCode() {
        return cforderCode;
    }

    public void setCropNameCn(String cropNameCn) {
        this.cropNameCn = cropNameCn;
    }

    public String getCropNameCn() {
        return cropNameCn;
    }

    public void setCropVarieties(Long cropVarieties) {
        this.cropVarieties = cropVarieties;
    }

    public Long getCropVarieties() {
        return cropVarieties;
    }

    public void setCropVarietiesCn(String cropVarietiesCn) {
        this.cropVarietiesCn = cropVarietiesCn;
    }

    public String getCropVarietiesCn() {
        return cropVarietiesCn;
    }

    public void setRequestAmount(BigDecimal requestAmount) {
        this.requestAmount = requestAmount;
    }

    public BigDecimal getRequestAmount() {
        return requestAmount;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getUnit() {
        return unit;
    }

    public void setRequestTime(Date requestTime) {
        this.requestTime = requestTime;
    }

    public Date getRequestTime() {
        return requestTime;
    }

    public void setCforderStatus(String cforderStatus) {
        this.cforderStatus = cforderStatus;
    }

    public String getCforderStatus() {
        return cforderStatus;
    }

    public void setDistribution(String distribution) {
        this.distribution = distribution;
    }

    public String getDistribution() {
        return distribution;
    }

    public void setCforderFor(Integer cforderFor) {
        this.cforderFor = cforderFor;
    }

    public Integer getCforderFor() {
        return cforderFor;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigDecimal getOutputWeight() {
        return outputWeight;
    }

    public void setOutputWeight(BigDecimal outputWeight) {
        this.outputWeight = outputWeight;
    }

    public Date getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Date finishTime) {
        this.finishTime = finishTime;
    }

    public String getHasPlan() {
        return hasPlan;
    }

    public void setHasPlan(String hasPlan) {
        this.hasPlan = hasPlan;
    }

    public String getTeamCn() {
        return teamCn;
    }

    public void setTeamCn(String teamCn) {
        this.teamCn = teamCn;
    }

    public String getPlanCode() {
        return planCode;
    }

    public void setPlanCode(String planCode) {
        this.planCode = planCode;
    }

    public Integer getIsCheck() {
        return isCheck;
    }

    public void setIsCheck(Integer isCheck) {
        this.isCheck = isCheck;
    }

    public String getPiciCode() {
        return piciCode;
    }

    public void setPiciCode(String piciCode) {
        this.piciCode = piciCode;
    }
}
