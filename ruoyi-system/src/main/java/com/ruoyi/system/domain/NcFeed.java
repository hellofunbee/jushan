package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 饲料库存表 nc_feed
 *
 * @author ruoyi
 * @date 2019-09-10
 */
public class NcFeed extends BaseEntity
{
	private static final long serialVersionUID = 1L;

	/** 饲料id */
	private Long feedId;
	/** 饲料批次 */
	@Excel(name="饲料批次")
	private String feedCode;
	/** 饲料名称 */
	@Excel(name="饲料名称")
	private String feedName;
	/** 生产厂家 */
	@Excel(name="生产厂家")
	private String factory;
	/** 入库数量(kg) */
	@Excel(name="入库数量(kg)")
	private BigDecimal inputAmount;
	/** 库存(kg) */
	@Excel(name="库存(kg)")
	private BigDecimal stockAmout;
	@Excel(name="入库时间",width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;
	/** 单位 */
	private String unit;
	/** 有效期 */
	@Excel(name="有效期",width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
	private Date validDate;


	/** 采购人 */
	//采购人，经手人
	@Excel(name="采购人")
	private String purchaserName;
	private String purchaser;
	@Excel(name="经手人")
	private String createByName;
	/** 饲料类型 1:主饲料 2：辅饲料 */
	private Integer feedType;

	@Override
	public String getRemark() {
		return remark;
	}

	@Override
	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Excel(name="备注")
	private String remark;


	public Integer getFeedType() {
		return feedType;
	}

	public void setFeedType(Integer feedType) {
		this.feedType = feedType;
	}

	/** 管理类型 1:鸡舍 2：牛舍 */
	private Integer managerType;

	/**物资id*/
	private Long meterialId;
	/**饲料集合**/
	private List<Map> feeds;

	private Integer outputAmount;

	private String meterialName;


	@Override
	public Date getCreateTime() {
		return createTime;
	}

	@Override
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getPurchaserName() {
		return purchaserName;
	}

	public void setPurchaserName(String purchaserName) {
		this.purchaserName = purchaserName;
	}

	public String getCreateByName() {
		return createByName;
	}

	public void setCreateByName(String createByName) {
		this.createByName = createByName;
	}

	public String getMeterialName() {
		return meterialName;
	}

	public void setMeterialName(String meterialName) {
		this.meterialName = meterialName;
	}

	public Long getMeterialId() {
		return meterialId;
	}

	public void setMeterialId(Long meterialId) {
		this.meterialId = meterialId;
	}

	public Long getFeedId() {
		return feedId;
	}

	public void setFeedId(Long feedId) {
		this.feedId = feedId;
	}

	public String getFeedCode() {
		return feedCode;
	}

	public void setFeedCode(String feedCode) {
		this.feedCode = feedCode;
	}

	public String getFeedName() {
		return feedName;
	}

	public void setFeedName(String feedName) {
		this.feedName = feedName;
	}

	public String getFactory() {
		return factory;
	}

	public void setFactory(String factory) {
		this.factory = factory;
	}

	public BigDecimal getInputAmount() {
		return inputAmount;
	}

	public void setInputAmount(BigDecimal inputAmount) {
		this.inputAmount = inputAmount;
	}

	public BigDecimal getStockAmout() {
		return stockAmout;
	}

	public void setStockAmout(BigDecimal stockAmout) {
		this.stockAmout = stockAmout;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Date getValidDate() {
		return validDate;
	}

	public void setValidDate(Date validDate) {
		this.validDate = validDate;
	}

	public String getPurchaser() {
		return purchaser;
	}

	public void setPurchaser(String purchaser) {
		this.purchaser = purchaser;
	}

	public Integer getManagerType() {
		return managerType;
	}

	public void setManagerType(Integer managerType) {
		this.managerType = managerType;
	}

	public List<Map> getFeeds() {
		return feeds;
	}

	public void setFeeds(List<Map> feeds) {
		this.feeds = feeds;
	}

	public Integer getOutputAmount() {
		return outputAmount;
	}

	public void setOutputAmount(Integer outputAmount) {
		this.outputAmount = outputAmount;
	}



    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("feedId", getFeedId())
            .append("feedCode", getFeedCode())
            .append("feedName", getFeedName())
            .append("factory", getFactory())
            .append("inputAmount", getInputAmount())
            .append("stockAmout", getStockAmout())
            .append("unit", getUnit())
            .append("createTime", getCreateTime())
            .append("createBy", getCreateBy())
            .append("validDate", getValidDate())
            .append("purchaser", getPurchaser())
            .append("remark", getRemark())
            .append("feedType", getFeedType())
            .append("managerType", getManagerType())
            .toString();
    }
}
