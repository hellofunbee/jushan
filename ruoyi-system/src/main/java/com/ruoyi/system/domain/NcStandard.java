package com.ruoyi.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;
import java.util.List;

/**
 * 标准主表 nc_standard
 * 
 * @author ruoyi
 * @date 2019-07-28
 */
public class NcStandard extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	
	/** 标准id */
	@Excel(name = "标准序号")
	private Long standardId;
	/** 父标准id */
	private Long parentId;
	/** 计划id */
	private Long planId;
	/** 标准类型  1:育苗标准 2：生产标准 */
//	@Excel(name = "标准类型", readConverterExp = "1=育苗标准,2=生产标准")
	private String standardType;
	/** 标准名称 */
	@Excel(name = "标准名称")
	private String standardName;
	/** 图片 */
	private String pic;
	/** 作物品种ID */
	private Long cropVarieties;
	/** 作物类别 */
	@Excel(name = "作物类别")
	private String cropTypeCn;
	/** 作物名称 */
	@Excel(name = "作物名称")
	private String cropNameCn;
	/** 作物品种 */
	@Excel(name = "作物品种")
	private String cropVarietiesCn;
	/** 适宜季节 */
	@Excel(name = "适宜季节 春/夏/秋/冬/四季", readConverterExp = "1=春,2=夏,3=秋,4=冬,5=四季")
	private String season;
	/** 总周期(天) */

	@Excel(name = "总周期(天)")
	private String totalCycle;
	/** 删除标志（1代表存在 2代表删除） */
	private String delFlag;
	/** 状态码：1:代表默认 0:其他  */
	@Excel(name = "启用状态", readConverterExp = "1=默认,2=其他")
	private String status;
	/** 编码 */
	private String code;
	/** 父编码 */
	private String parentCode;
	/*用于选中状态显示*/
	private boolean flag;

	private List<NcStage> stages;
	/** 创建者 */
	@Excel(name = "创建人员")
	private String createBy;

	/** 创建时间 */
	@Excel(name = "创建时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	/**
	 * 备注
	 */
	@Excel(name = "备注")
	private String remark;
	/**
	 * 导入标准序号
	 */
	private Double serialStandardNumber;

	public Double getSerialStandardNumber() {
		return serialStandardNumber;
	}

	public void setSerialStandardNumber(Double serialStandardNumber) {
		this.serialStandardNumber = serialStandardNumber;
	}

	@Override
	public String getRemark() {
		return remark;
	}

	@Override
	public void setRemark(String remark) {
		this.remark = remark;
	}

	public void setStandardId(Long standardId)
	{
		this.standardId = standardId;
	}

	public Long getStandardId() 
	{
		return standardId;
	}
	public void setParentId(Long parentId) 
	{
		this.parentId = parentId;
	}

	public Long getParentId() 
	{
		return parentId;
	}
	public void setPlanId(Long planId) 
	{
		this.planId = planId;
	}

	public Long getPlanId() 
	{
		return planId;
	}
	public void setStandardType(String standardType) 
	{
		this.standardType = standardType;
	}

	public String getStandardType() 
	{
		return standardType;
	}
	public void setStandardName(String standardName) 
	{
		this.standardName = standardName;
	}

	public String getStandardName() 
	{
		return standardName;
	}
	public void setPic(String pic) 
	{
		this.pic = pic;
	}

	public String getPic() 
	{
		return pic;
	}
	public void setCropVarieties(Long cropVarieties) 
	{
		this.cropVarieties = cropVarieties;
	}

	public Long getCropVarieties() 
	{
		return cropVarieties;
	}
	public void setCropTypeCn(String cropTypeCn) 
	{
		this.cropTypeCn = cropTypeCn;
	}

	public String getCropTypeCn() 
	{
		return cropTypeCn;
	}
	public void setCropNameCn(String cropNameCn) 
	{
		this.cropNameCn = cropNameCn;
	}

	public String getCropNameCn() 
	{
		return cropNameCn;
	}
	public void setCropVarietiesCn(String cropVarietiesCn) 
	{
		this.cropVarietiesCn = cropVarietiesCn;
	}

	public String getCropVarietiesCn() 
	{
		return cropVarietiesCn;
	}
	public void setSeason(String season) 
	{
		this.season = season;
	}

	public String getSeason() 
	{
		return season;
	}
	public void setTotalCycle(String totalCycle) 
	{
		this.totalCycle = totalCycle;
	}

	public String getTotalCycle() 
	{
		return totalCycle;
	}
	public void setDelFlag(String delFlag) 
	{
		this.delFlag = delFlag;
	}

	public String getDelFlag() 
	{
		return delFlag;
	}
	public void setStatus(String status) 
	{
		this.status = status;
	}

	public String getStatus() 
	{
		return status;
	}
	public void setCode(String code) 
	{
		this.code = code;
	}

	public String getCode() 
	{
		return code;
	}
	public void setParentCode(String parentCode) 
	{
		this.parentCode = parentCode;
	}

	public String getParentCode() 
	{
		return parentCode;
	}


	public List<NcStage> getStages() {
		return stages;
	}

	public void setStages(List<NcStage> stages) {
		this.stages = stages;
	}

	@Override
	public String getCreateBy() {
		return createBy;
	}

	@Override
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	@Override
	public Date getCreateTime() {
		return createTime;
	}

	@Override
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("standardId", getStandardId())
            .append("parentId", getParentId())
            .append("planId", getPlanId())
            .append("standardType", getStandardType())
            .append("standardName", getStandardName())
            .append("pic", getPic())
            .append("cropVarieties", getCropVarieties())
            .append("cropTypeCn", getCropTypeCn())
            .append("cropNameCn", getCropNameCn())
            .append("cropVarietiesCn", getCropVarietiesCn())
            .append("season", getSeason())
            .append("totalCycle", getTotalCycle())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createBy", getCreateBy())
            .append("updateBy", getUpdateBy())
            .append("remark", getRemark())
            .append("delFlag", getDelFlag())
            .append("status", getStatus())
            .append("code", getCode())
            .append("parentCode", getParentCode())
            .toString();
    }

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}
}
