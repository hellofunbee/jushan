package com.ruoyi.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 牛舍精液表 nc_bull_jingye
 * 
 * @author ruoyi
 * @date 2019-08-30
 */
public class NcBullJingye extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	
	/**  */
	private Integer jingyeId;
	/** 公牛耳号 */
	@Excel(name="公牛耳号")
	private String bullCode;
	/** 来源 */
	@Excel(name="来源 ")
	private String comeFrom;
	/** 库存(支) */
	@Excel(name="库存(支)")
	private Integer stockAmout;
	/** 数量(支) */
	@Excel(name="数量(支)")
	private Integer inputAmount;
	/** 采购人 */
	@Excel(name="采购人")
	private String purchaser;


	/** 创建者 */
	@Excel(name="创建者")
	private String createBy;


	/** 创建时间 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@Excel(name="巡视时间")
	private Date createTime;

	/** 备注 */
	@Excel(name="备注")
	private String remark;

	public void setJingyeId(Integer jingyeId) 
	{
		this.jingyeId = jingyeId;
	}

	public Integer getJingyeId() 
	{
		return jingyeId;
	}
	public void setBullCode(String bullCode) 
	{
		this.bullCode = bullCode;
	}

	public String getBullCode() 
	{
		return bullCode;
	}
	public void setComeFrom(String comeFrom) 
	{
		this.comeFrom = comeFrom;
	}

	public String getComeFrom() 
	{
		return comeFrom;
	}
	public void setStockAmout(Integer stockAmout) 
	{
		this.stockAmout = stockAmout;
	}

	public Integer getStockAmout() 
	{
		return stockAmout;
	}
	public void setInputAmount(Integer inputAmount) 
	{
		this.inputAmount = inputAmount;
	}

	public Integer getInputAmount() 
	{
		return inputAmount;
	}
	public void setPurchaser(String purchaser) 
	{
		this.purchaser = purchaser;
	}

	public String getPurchaser() 
	{
		return purchaser;
	}

	@Override
	public String getCreateBy() {
		return createBy;
	}

	@Override
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	@Override
	public Date getCreateTime() {
		return createTime;
	}

	@Override
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Override
	public String getRemark() {
		return remark;
	}

	@Override
	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("jingyeId", getJingyeId())
            .append("bullCode", getBullCode())
            .append("createTime", getCreateTime())
            .append("comeFrom", getComeFrom())
            .append("stockAmout", getStockAmout())
            .append("inputAmount", getInputAmount())
            .append("purchaser", getPurchaser())
            .append("createBy", getCreateBy())
            .append("remark", getRemark())
            .toString();
    }
}
