package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.annotation.Excel.Type;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 产奶记录表 nc_cows_milk
 *
 * @author ruoyi
 * @date 2019-09-06
 */
public class NcCowsMilk extends BaseEntity
{
	private static final long serialVersionUID = 1L;

	/** 牛奶id（主键） */
	@Excel(name = "编号", prompt = "编号")
	private Integer cowsMilkId;
	/** 牛id */
	@Excel(name = "耳号", prompt = "耳号")
	private Integer cowId;
	/** 产奶数量（kg） */
	@Excel(name = "产奶数量（kg）", prompt = "产奶数量")
	private BigDecimal milkAmount;
	/** 日期 */
	private Date milkDate;
	/** 来源：1 ：早 2：晚 */
	@Excel(name = "来源",dictType = "TimeSlot")
	private Integer timeStage;

	@Excel(name = "日期", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss", type = Type.EXPORT)
	private Date createTime;

	//创建人
	@Excel(name = "创建人", prompt = "创建人")
	private String createByName;

	private List<Map> milks;

	private Long workNoticeId;


	@Override
	public Date getCreateTime() {
		return createTime;
	}

	@Override
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Long getWorkNoticeId() {
		return workNoticeId;
	}

	public void setWorkNoticeId(Long workNoticeId) {
		this.workNoticeId = workNoticeId;
	}

	public List<Map> getMilks() {
		return milks;
	}

	public void setMilks(List<Map> milks) {
		this.milks = milks;
	}

	public String getCreateByName() {
		return createByName;
	}

	public void setCreateByName(String createByName) {
		this.createByName = createByName;
	}

	public void setCowsMilkId(Integer cowsMilkId)
	{
		this.cowsMilkId = cowsMilkId;
	}

	public Integer getCowsMilkId()
	{
		return cowsMilkId;
	}
	public void setCowId(Integer cowId)
	{
		this.cowId = cowId;
	}

	public Integer getCowId()
	{
		return cowId;
	}
	public void setMilkAmount(BigDecimal milkAmount)
	{
		this.milkAmount = milkAmount;
	}

	public BigDecimal getMilkAmount()
	{
		return milkAmount;
	}
	public void setMilkDate(Date milkDate)
	{
		this.milkDate = milkDate;
	}

	public Date getMilkDate()
	{
		return milkDate;
	}
	public void setTimeStage(Integer timeStage)
	{
		this.timeStage = timeStage;
	}

	public Integer getTimeStage()
	{
		return timeStage;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("cowsMilkId", getCowsMilkId())
            .append("cowId", getCowId())
            .append("milkAmount", getMilkAmount())
            .append("milkDate", getMilkDate())
            .append("timeStage", getTimeStage())
            .append("createTime", getCreateTime())
            .append("createBy", getCreateBy())
            .append("remark", getRemark())
            .toString();
    }
}
