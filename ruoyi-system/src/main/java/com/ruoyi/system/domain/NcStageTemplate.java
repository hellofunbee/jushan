package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.List;

/**
 * 生产（育苗）阶段表 nc_stage
 *
 * @author ruoyi
 * @date 2019-07-28
 */
public class NcStageTemplate extends BaseEntity {
    private static final long serialVersionUID = 1L;
    /**
     * 导入标准序号
     */
    @Excel(name = "标准序号")
    private Double serialStandardNumber;
    /**
     * 导入阶段序号
     */
    @Excel(name = "阶段序号")
    private Double serialStageNumber;
    /**
     * 阶段名称
     */
    @Excel(name = "阶段名称")
    private String stageName;
    /**
     * 阶段天数
     */
    @Excel(name = "阶段天数")
    private Integer stageDays;

    /**
     * 阶段id
     */
    private Long stageId;
    /**
     * 标准id
     */
    private Long standardId;

    /**
     * 排序号
     */
    @Excel(name = "排序号")
    private Integer orderNum;
    /**
     * 阶段描述
     */
    @Excel(name = "阶段描述")
    private String stageDetail;


    private String cssClass;

    /**
     * 备注
     */
    @Excel(name = "备注")
    private String remark;

    @Override
    public String getRemark() {
        return remark;
    }

    @Override
    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Double getSerialStandardNumber() {
        return serialStandardNumber;
    }

    public void setSerialStandardNumber(Double serialStandardNumber) {
        this.serialStandardNumber = serialStandardNumber;
    }



    public Double getSerialStageNumber() {
        return serialStageNumber;
    }

    public void setSerialStageNumber(Double serialStageNumber) {
        this.serialStageNumber = serialStageNumber;
    }



    private List<NcWork> works;

    public String getCssClass() {
        return cssClass;
    }

    public void setCssClass(String cssClass) {
        this.cssClass = cssClass;
    }

    public Long getStageId() {
        return stageId;
    }

    public void setStageId(Long stageId) {
        this.stageId = stageId;
    }

    public Long getStandardId() {
        return standardId;
    }

    public void setStandardId(Long standardId) {
        this.standardId = standardId;
    }

    public String getStageName() {
        return stageName;
    }

    public void setStageName(String stageName) {
        this.stageName = stageName;
    }

    public Integer getStageDays() {
        return stageDays;
    }

    public void setStageDays(Integer stageDays) {
        this.stageDays = stageDays;
    }

    public Integer getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(Integer orderNum) {
        this.orderNum = orderNum;
    }

    public String getStageDetail() {
        return stageDetail;
    }

    public void setStageDetail(String stageDetail) {
        this.stageDetail = stageDetail;
    }

    public List<NcWork> getWorks() {
        return works;
    }

    public void setWorks(List<NcWork> works) {
        this.works = works;
    }

    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("stageId", getStageId())
                .append("standardId", getStandardId())
                .append("stageName", getStageName())
                .append("stageDays", getStageDays())
                .append("orderNum", getOrderNum())
                .append("stageDetail", getStageDetail())
                .toString();
    }
}
