package com.ruoyi.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.Date;

/**
 * 牛舍精液表 nc_bull_jingye_log
 * 
 * @author ruoyi
 * @date 2019-08-30
 */
public class NcBullJingyeLog extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	
	/**  */
	private Long jingyeLogId;
	/** 精液id */
	private Integer jingyeId;
	/** 库存(支) */
	@Excel(name="库存(支)")
	private Integer stockAmout;
	/** 出库数量(支) */
	@Excel(name="出库数量(支)")
	private Integer inputAmount;
	/** 领取购人 */
	@Excel(name="领取人")
	private String receiver;
	/** 公牛耳号 */
	@Excel(name="公牛耳号")
	private String bullCode;
	/** 来源 */
	@Excel(name="来源 ")
	private String comeFrom;


	/** 创建者 */
	@Excel(name="创建者")
	private String createBy;


	/** 创建时间 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@Excel(name="巡视时间")
	private Date createTime;

	/** 备注 */
	@Excel(name="备注")
	private String remark;

	@Override
	public String getCreateBy() {
		return createBy;
	}

	@Override
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	@Override
	public Date getCreateTime() {
		return createTime;
	}

	@Override
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Override
	public String getRemark() {
		return remark;
	}

	@Override
	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getBullCode() {
		return bullCode;
	}

	public void setBullCode(String bullCode) {
		this.bullCode = bullCode;
	}

	public String getComeFrom() {
		return comeFrom;
	}

	public void setComeFrom(String comeFrom) {
		this.comeFrom = comeFrom;
	}

	public void setJingyeLogId(Long jingyeLogId)
	{
		this.jingyeLogId = jingyeLogId;
	}

	public Long getJingyeLogId() 
	{
		return jingyeLogId;
	}

	public Integer getJingyeId() {
		return jingyeId;
	}

	public void setJingyeId(Integer jingyeId) {
		this.jingyeId = jingyeId;
	}

	public void setStockAmout(Integer stockAmout)
	{
		this.stockAmout = stockAmout;
	}

	public Integer getStockAmout() 
	{
		return stockAmout;
	}
	public void setInputAmount(Integer inputAmount) 
	{
		this.inputAmount = inputAmount;
	}

	public Integer getInputAmount() 
	{
		return inputAmount;
	}
	public void setReceiver(String receiver) 
	{
		this.receiver = receiver;
	}

	public String getReceiver() 
	{
		return receiver;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("jingyeLogId", getJingyeLogId())
            .append("jingyeId", getJingyeId())
            .append("createTime", getCreateTime())
            .append("stockAmout", getStockAmout())
            .append("inputAmount", getInputAmount())
            .append("receiver", getReceiver())
            .append("createBy", getCreateBy())
            .append("remark", getRemark())
            .toString();
    }
}
