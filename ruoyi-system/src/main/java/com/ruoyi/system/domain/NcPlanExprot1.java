package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 计划表 nc_plan
 *
 * @author ruoyi
 * @date 2019-07-31
 */
public class NcPlanExprot1 extends BaseEntity {
    private static final long serialVersionUID = 1L;


    /**
     * 计划id
     */
    @Excel(name = "计划序号")
    private Long planId;

    /**
     * 标准id
     */
    @Excel(name = "标准序号")
    private Long standardId;

    /**
     * 导入计划序号
     */
    private String serialPlanNumber;


    /**
     * 计划编号
     */
    @Excel(name = "计划编号")
    private String planCode;


    /**
     * 作物名称
     */
    @Excel(name = "作物名称")
    private String cropNameCn;


    /**
     * 作物品种
     */
    @Excel(name = "作物品种")
    private String cropVarietiesCn;


    /**
     * 育苗/定植时间
     */
    @Excel(name = "育苗时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date planTime;


    /**
     * 生产班组名称
     */
    @Excel(name = "生产班组")
    private String deptName;


    /**
     * 茬次
     */
    @Excel(name = "茬次")
    private Integer chaci;

    /**
     * 数量(株)
     */
    @Excel(name = "数量(株)")
    private Integer count;

    /**
     * 盘数
     */
    @Excel(name = "盘数")
    private Integer plateCount;

    /**
     * 循环天数
     */
    @Excel(name = "循环天数")
    private Integer cycleDays;


    /**
     * 单茬面积(亩)
     */
    @Excel(name = "单茬面积(亩)")
    private BigDecimal area;
    /**
     * 间数
     */
    @Excel(name = "间数")
    private Double jian;
    /**
     * 生产班组
     */
    private String teamCn;
    /**
     * 设施类型  1：日光温室、2：春秋棚、3：连栋温室、4：露地
     */
    private Integer landType;


    /** 创建者 */
    @Excel(name = "创建者")
    private String createBy;

    /**
     * 备注
     */
    private String remark;
    @Excel(name = "标准名称")
    private String standardName;
    /**
     * 计划类型 1：育苗 计划2：生产计划
     */
    private Integer planType;
    /**
     * 订单id
     */
    private Long orderId;


    /**
     * 作物品种ID
     */
    private Long cropVarieties;
    /**
     * 作物类别
     */
//    @Excel(name = "作物类别")
    private String cropTypeCn;

    /**
     * 生产班组
     */
    private String team;


    /**
     * 实际提供数量
     */
    private BigDecimal realAmout;

    /**
     * 当前选中
     */
    private boolean flag = false;


    /**
     * 总面积(亩)
     */
//    @Excel(name = "总面积(亩)")
    private BigDecimal sumArea;
    /**
     * 循环面积(亩)
     */
//    @Excel(name = "循环面积(亩)")
    private BigDecimal cycleArea;

    /*状态 1:待生成订单 2：已生成订单 3.在执行 4.已完成*/
    private String status;

    /*是否匹配标准 1:匹配 2：未匹配*/
    private String hasStandard;

    /*匹配的标准*/
    private NcStandard standard;


    private List<NcStageMirror> stages;


    /*大棚 信息*/
    private List<Map> daPengs;

    private Integer preDays;

    private String peng;
    public String getStandardName() {
        return standardName;
    }

    public void setStandardName(String standardName) {
        this.standardName = standardName;
    }

    @Override
    public String getRemark() {
        return remark;
    }

    @Override
    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getSerialPlanNumber() {
        return serialPlanNumber;
    }

    public void setSerialPlanNumber(String serialPlanNumber) {
        this.serialPlanNumber = serialPlanNumber;
    }

    public BigDecimal getRealAmout() {
        return realAmout;
    }

    public void setRealAmout(BigDecimal realAmout) {
        this.realAmout = realAmout;
    }

    public Long getPlanId() {
        return planId;
    }

    public void setPlanId(Long planId) {
        this.planId = planId;
    }

    public Integer getPlanType() {
        return planType;
    }

    public void setPlanType(Integer planType) {
        this.planType = planType;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getStandardId() {
        return standardId;
    }

    public void setStandardId(Long standardId) {
        this.standardId = standardId;
    }

    public String getPlanCode() {
        return planCode;
    }

    public void setPlanCode(String planCode) {
        this.planCode = planCode;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public Long getCropVarieties() {
        return cropVarieties;
    }

    public void setCropVarieties(Long cropVarieties) {
        this.cropVarieties = cropVarieties;
    }

    public String getCropTypeCn() {
        return cropTypeCn;
    }

    public void setCropTypeCn(String cropTypeCn) {
        this.cropTypeCn = cropTypeCn;
    }

    public String getCropNameCn() {
        return cropNameCn;
    }

    public void setCropNameCn(String cropNameCn) {
        this.cropNameCn = cropNameCn;
    }

    public String getCropVarietiesCn() {
        return cropVarietiesCn;
    }

    public void setCropVarietiesCn(String cropVarietiesCn) {
        this.cropVarietiesCn = cropVarietiesCn;
    }

    public Date getPlanTime() {
        return planTime;
    }

    public void setPlanTime(Date planTime) {
        this.planTime = planTime;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public Integer getLandType() {
        return landType;
    }

    public void setLandType(Integer landType) {
        this.landType = landType;
    }

    public Integer getPlateCount() {
        return plateCount;
    }

    public void setPlateCount(Integer plateCount) {
        this.plateCount = plateCount;
    }

    public Integer getChaci() {
        return chaci;
    }

    public void setChaci(Integer chaci) {
        this.chaci = chaci;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public BigDecimal getArea() {
        return area;
    }

    public void setArea(BigDecimal area) {
        this.area = area;
    }

    public Double getJian() {
        return jian;
    }

    public void setJian(Double jian) {
        this.jian = jian;
    }

    public BigDecimal getSumArea() {
        return sumArea;
    }

    public void setSumArea(BigDecimal sumArea) {
        this.sumArea = sumArea;
    }

    public BigDecimal getCycleArea() {
        return cycleArea;
    }

    public void setCycleArea(BigDecimal cycleArea) {
        this.cycleArea = cycleArea;
    }

    public Integer getCycleDays() {
        return cycleDays;
    }

    public void setCycleDays(Integer cycleDays) {
        this.cycleDays = cycleDays;
    }

    public String getTeamCn() {
        return teamCn;
    }

    public void setTeamCn(String teamCn) {
        this.teamCn = teamCn;
    }

    public List<Map> getDaPengs() {
        return daPengs;
    }

    public void setDaPengs(List<Map> daPengs) {
        this.daPengs = daPengs;
    }

    public Integer getPreDays() {
        return preDays;
    }

    public void setPreDays(Integer preDays) {
        this.preDays = preDays;
    }

    public String getPeng() {
        return peng;
    }

    public void setPeng(String peng) {
        this.peng = peng;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("planId", getPlanId())
                .append("planType", getPlanType())
                .append("orderId", getOrderId())
                .append("standardId", getStandardId())
                .append("planCode", getPlanCode())
                .append("cropVarieties", getCropVarieties())
                .append("cropTypeCn", getCropTypeCn())
                .append("cropNameCn", getCropNameCn())
                .append("cropVarietiesCn", getCropVarietiesCn())
                .append("planTime", getPlanTime())
                .append("team", getTeam())
                .append("landType", getLandType())
                .append("plateCount", getPlateCount())
                .append("chaci", getChaci())
                .append("count", getCount())
                .append("area", getArea())
                .append("jian", getJian())
                .append("sumArea", getSumArea())
                .append("realAmout", getRealAmout())
                .append("cycleArea", getCycleArea())
                .append("cycleDays", getCycleDays())
                .append("createTime", getCreateTime())
                .append("createBy", getCreateBy())
                .append("remark", getRemark())
                .toString();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getHasStandard() {
        return hasStandard;
    }

    public void setHasStandard(String hasStandard) {
        this.hasStandard = hasStandard;
    }

    public NcStandard getStandard() {
        return standard;
    }

    public void setStandard(NcStandard standard) {
        this.standard = standard;
    }

    public List<NcStageMirror> getStages() {
        return stages;
    }

    public void setStages(List<NcStageMirror> stages) {
        this.stages = stages;
    }
}
