package com.ruoyi.system.domain;

import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 知识课堂类别表 nc_knowledge_class_type
 * 
 * @author ruoyi
 * @date 2019-09-10
 */
public class NcKnowledgeClassType extends BaseEntity
{
	private static final long serialVersionUID = 1L;

	/** 知识课堂类别id */
	private Long kctypeId;
	/** 上一级id */
	private Long parentId;
	//上一级名称
	private String parentName;
	/** 知识课堂编码 */
	private String code;
	/** 祖级列表 */
	private String ancestors;
	/** 知识课堂名称 */
	private String kcTypeName;
	/** 显示顺序 */
	private Integer orderNum;
	/** 层级 */
	private Integer level;
	/** 删除标志（0代表存在 2代表删除） */
	private Integer delFlag;

	public Long getKctypeId() {
		return kctypeId;
	}

	public void setKctypeId(Long kctypeId) {
		this.kctypeId = kctypeId;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getAncestors() {
		return ancestors;
	}

	public void setAncestors(String ancestors) {
		this.ancestors = ancestors;
	}

	public String getKcTypeName() {
		return kcTypeName;
	}

	public void setKcTypeName(String kcTypeName) {
		this.kcTypeName = kcTypeName;
	}

	public Integer getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(Integer orderNum) {
		this.orderNum = orderNum;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Integer getDelFlag() {
		return delFlag;
	}

	public void setDelFlag(Integer delFlag) {
		this.delFlag = delFlag;
	}

	@Override
	public String toString() {
		return "NcKnowledgeClassType{" +
				"kctypeId=" + kctypeId +
				", parentId=" + parentId +
				", parentName='" + parentName + '\'' +
				", code='" + code + '\'' +
				", ancestors='" + ancestors + '\'' +
				", kcTypeName='" + kcTypeName + '\'' +
				", orderNum=" + orderNum +
				", level=" + level +
				", delFlag=" + delFlag +
				'}';
	}
}
