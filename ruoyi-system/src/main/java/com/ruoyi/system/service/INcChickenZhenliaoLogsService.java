package com.ruoyi.system.service;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.system.domain.NcZhenliaoLogs;
import com.ruoyi.system.domain.SysUser;

import java.util.List;

/**
 * 诊疗单 服务层
 * 
 * @author ruoyi
 * @date 2019-08-28
 */
public interface INcChickenZhenliaoLogsService
{
	/**
     * 查询诊疗单信息
     *
     * @param zlId 诊疗单ID
     * @return 诊疗单信息
     */
	public NcZhenliaoLogs selectNcZhenliaoLogsById(Long zlId);

	/**
     * 查询诊疗单列表
     *
     * @param ncZhenliaoLogs 诊疗单信息
     * @return 诊疗单集合
     */
	public List<NcZhenliaoLogs> selectNcZhenliaoLogsList(NcZhenliaoLogs ncZhenliaoLogs);

	/**
	 * 查询诊疗单列表
	 *
	 * @param ncZhenliaoLogs 诊疗单信息
	 * @return 诊疗单集合
	 */
	public List<NcZhenliaoLogs> selectNcZhenliaoLogsListConfirm(NcZhenliaoLogs ncZhenliaoLogs);


	/**
     * 新增诊疗单
     *
     * @param ncZhenliaoLogs 诊疗单信息
     * @return 结果
     */
	public AjaxResult insertNcZhenliaoLogs(NcZhenliaoLogs ncZhenliaoLogs, SysUser sysUser);
	
	/**
     * 修改诊疗单
     * 
     * @param ncZhenliaoLogs 诊疗单信息
     * @return 结果
     */
	public AjaxResult updateNcZhenliaoLogs(NcZhenliaoLogs ncZhenliaoLogs);
		
	/**
     * 删除诊疗单信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcZhenliaoLogsByIds(String ids);


	//用户创建诊疗单
	int createZlOrderByIds(String ids, SysUser user);

	int askNcZhenliaoLogs(NcZhenliaoLogs ncZhenliaoLogs, SysUser sysUser);


	/**
	 * 确认单条诊疗单
	 * @param ncZhenliaoLogs 诊疗单对象
	 * @return AjaxResult
	 */
	AjaxResult confirmNcZhenliaoLogs(NcZhenliaoLogs ncZhenliaoLogs, SysUser sysUser);
}
