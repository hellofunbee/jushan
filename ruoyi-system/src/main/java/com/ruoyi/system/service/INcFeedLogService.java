package com.ruoyi.system.service;

import com.ruoyi.system.domain.NcFeedLog;
import java.util.List;
import java.util.Map;

/**
 * 饲料出入记录 服务层
 * 
 * @author ruoyi
 * @date 2019-09-10
 */
public interface INcFeedLogService 
{
	/**
     * 查询饲料出入记录信息
     * 
     * @param feedLogId 饲料出入记录ID
     * @return 饲料出入记录信息
     */
	public NcFeedLog selectNcFeedLogById(Long feedLogId);
	
	/**
     * 查询饲料出入记录列表
     * 
     * @param ncFeedLog 饲料出入记录信息
     * @return 饲料出入记录集合
     */
	public List<NcFeedLog> selectNcFeedLogList(NcFeedLog ncFeedLog);
	
	/**
     * 新增饲料出入记录
     * 
     * @param ncFeedLog 饲料出入记录信息
     * @return 结果
     */
	public int insertNcFeedLog(NcFeedLog ncFeedLog);
	
	/**
     * 修改饲料出入记录
     * 
     * @param ncFeedLog 饲料出入记录信息
     * @return 结果
     */
	public int updateNcFeedLog(NcFeedLog ncFeedLog);
		
	/**
     * 删除饲料出入记录信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcFeedLogByIds(String ids);
	public NcFeedLog selectNcFeedLogAmount(NcFeedLog ncFeedLog);
	public Double selectNcFeedLogMonthAmount(NcFeedLog ncFeedLog);
	public NcFeedLog selectNcFeedLogAmountCows(NcFeedLog ncFeedLog);
	public Double selectNcFeedLogMonthAmountCows(NcFeedLog ncFeedLog);
	public Double selectNcFeedLogScreen(NcFeedLog ncFeedLog);
	List<Map> selectCountByTime(Integer dataSpan, String beginTime, String endTime);
	public List<Map> selectCowCountByTime(Integer spanType, String beginTime, String endTime);

}
