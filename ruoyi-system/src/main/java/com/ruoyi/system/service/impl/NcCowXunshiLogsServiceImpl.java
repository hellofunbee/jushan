package com.ruoyi.system.service.impl;

import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.utils.NcUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.NcOrder;
import com.ruoyi.system.domain.NcXunshiLogs;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.mapper.NcXunshiLogsMapper;
import com.ruoyi.system.service.INcChickenXunshiLogsService;
import com.ruoyi.system.service.INcCowXunshiLogsService;
import com.ruoyi.system.service.INcOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * 免疫记录 服务层实现
 * 
 * @author ruoyi
 * @date 2019-09-02
 */
@Service
public class NcCowXunshiLogsServiceImpl implements INcCowXunshiLogsService
{
	@Autowired
	private NcXunshiLogsMapper ncXunshiLogsMapper;

	@Autowired
	private INcOrderService orderService;
	/**
     * 查询免疫记录信息
     * 
     * @param xsId 免疫记录ID
     * @return 免疫记录信息
     */
    @Override
	public NcXunshiLogs selectNcXunshiLogsById(Long xsId)
	{
	    return ncXunshiLogsMapper.selectNcXunshiLogsById(xsId);
	}
	
	/**
     * 查询免疫记录列表
     * 
     * @param ncXunshiLogs 免疫记录信息
     * @return 免疫记录集合
     */
	@Override
	public List<NcXunshiLogs> selectNcXunshiLogsList(NcXunshiLogs ncXunshiLogs)
	{
	    return ncXunshiLogsMapper.selectNcXunshiLogsList(ncXunshiLogs);
	}
	
    /**
     * 新增免疫记录
     * 
     * @param ncXunshiLogs 免疫记录信息
     * @return 结果
     */
	@Override
	public AjaxResult insertNcXunshiLogs(NcXunshiLogs ncXunshiLogs)
	{


        if (ncXunshiLogs.getStockAmount() <0 || ncXunshiLogs.getXsAmount() < 0){

            return AjaxResult.success("请输入正确数量") ;
        }


        ncXunshiLogsMapper.insertNcXunshiLogs(ncXunshiLogs);

	    return AjaxResult.success("新增巡视记录成功") ;
	}
	
	/**
     * 修改免疫记录
     * 
     * @param ncXunshiLogs 免疫记录信息
     * @return 结果
     */
	@Override
	public int updateNcXunshiLogs(NcXunshiLogs ncXunshiLogs)
	{
	    return ncXunshiLogsMapper.updateNcXunshiLogs(ncXunshiLogs);
	}

	/**
     * 删除免疫记录对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteNcXunshiLogsByIds(String ids)
	{
		return ncXunshiLogsMapper.deleteNcXunshiLogsByIds(Convert.toStrArray(ids));
	}

	@Override
	@Transactional
	public int createZlOrderByIds(String ids, SysUser user) {


		NcOrder o = new NcOrder();

		if (StringUtils.isNotEmpty(ids)) {
			try {
				/*创建巡视订单*/
				o.setOrderCode(NcUtils.getOrderCode());
				//牛舍巡视单
				o.setOrderType(Constants.ORDER_COW_XUNSHI);
				o.setOrderTime(new Date());
				o.setCreateTime(new Date());
				o.setCreateBy(user.getLoginName());
				orderService.insertNcOrder(o);

				String[] idArr = Convert.toStrArray(ids);
				for (String id : idArr) {
					NcXunshiLogs ncXunshiLogs = this.ncXunshiLogsMapper.selectNcXunshiLogsById(Long.parseLong(id));
					ncXunshiLogs.setOrderId(o.getOrderId());
					ncXunshiLogs.setXsStatus(4);
					this.ncXunshiLogsMapper.updateNcXunshiLogs(ncXunshiLogs);
				}

				return 1;
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		} else {
			return 0;
		}
		return 0;
	}

}
