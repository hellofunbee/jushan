package com.ruoyi.system.service.impl;

import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.NcUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.NcOrder;
import com.ruoyi.system.domain.NcOutputChicken;
import com.ruoyi.system.domain.SysDictData;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.mapper.NcOutputChickenMapper;
import com.ruoyi.system.service.INcOrderCfService;
import com.ruoyi.system.service.INcOrderService;
import com.ruoyi.system.service.INcOutputChickenService;
import com.ruoyi.system.service.ISysDictDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import java.text.DecimalFormat;
import java.util.*;

/**
 * 鸡出库 服务层实现
 * 
 * @author ruoyi
 * @date 2019-09-09
 */
@Service
public class NcOutputChickenServiceImpl implements INcOutputChickenService 
{
	@Autowired
	private NcOutputChickenMapper ncOutputChickenMapper;
	@Autowired
	private INcOrderService orderService;
	@Autowired
	private INcOrderCfService orderCfService;
	@Autowired
	private ISysDictDataService dictDataService;


	/**
     * 查询鸡出库信息
     * 
     * @param outputId 鸡出库ID
     * @return 鸡出库信息
     */
    @Override
	public NcOutputChicken selectNcOutputChickenById(Integer outputId)
	{
	    return ncOutputChickenMapper.selectNcOutputChickenById(outputId);
	}
	
	/**
     * 查询鸡出库列表
     * 
     * @param ncOutputChicken 鸡出库信息
     * @return 鸡出库集合
     */
	@Override
	public List<NcOutputChicken> selectNcOutputChickenList(NcOutputChicken ncOutputChicken)
	{
	    return ncOutputChickenMapper.selectNcOutputChickenList(ncOutputChicken);
	}
	
    /**
     * 新增鸡出库
     * 
     * @param ncOutputChicken 鸡出库信息
     * @return 结果
     */
	@Override
	public int insertNcOutputChicken(NcOutputChicken ncOutputChicken)
	{
	    return ncOutputChickenMapper.insertNcOutputChicken(ncOutputChicken);
	}
	
	/**
     * 修改鸡出库
     * 
     * @param ncOutputChicken 鸡出库信息
     * @return 结果
     */
	@Override
	public int updateNcOutputChicken(NcOutputChicken ncOutputChicken)
	{
	    return ncOutputChickenMapper.updateNcOutputChicken(ncOutputChicken);
	}

	/**
     * 删除鸡出库对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteNcOutputChickenByIds(String ids)
	{
		return ncOutputChickenMapper.deleteNcOutputChickenByIds(Convert.toStrArray(ids));
	}

	@Override
	public int createOrderByIds(String ids, SysUser user) {
		NcOrder o = new NcOrder();

		if (StringUtils.isNotEmpty(ids)) {
			/*创建鸡出库订单*/
			o.setOrderCode(NcUtils.getOrderCode());
			o.setOrderType(Constants.ORDER_CHICKEN_OUTPUT);
			o.setOrderTime(new Date());
			o.setCreateTime(new Date());
			o.setCreateBy(user.getUserName());
			orderService.insertNcOrder(o);

			System.out.println("----------orderId-------:" + o.getOrderId());
			String[] idArr = Convert.toStrArray(ids);
			for (String id : idArr) {
				NcOutputChicken outputChicken = ncOutputChickenMapper.selectNcOutputChickenById(Integer.valueOf(id));
				outputChicken.setOrderId(o.getOrderId());
				outputChicken.setOutputStatus(Long.valueOf(3));
				updateNcOutputChicken(outputChicken);
			}

			return 1;
		} else {
			return 0;
		}
	}

	@Override
	public double selectNcOutputAmount(NcOutputChicken ncOutputChicken) {
		return ncOutputChickenMapper.selectNcOutputAmount(ncOutputChicken);
	}

	@Override
	public Double selectNcOutputMonthAmount(NcOutputChicken ncOutputChicken) {
		return ncOutputChickenMapper.selectNcOutputMonthAmount(ncOutputChicken);
	}

	@Override
	public void innoInfo(String beginTime, String endTime, ModelMap mmap, String dataType, String output) {
		Map chkMap = new HashMap();
		chkMap.put("name","鸡肉总产量");
		Double totalWeight = ncOutputChickenMapper.selectChickenByMonthRange(beginTime,endTime,output);
		// 趋势和日均
		int totalDays = findDays(dataType);
		if("year".equals(dataType)){  // 年趋势
			List<String> monthList = DateUtils.getEachMonthList();
			List<Double> totalList = new ArrayList<>();
			for(String s : monthList){
				Double total =ncOutputChickenMapper.selectChickenTotalByMonth(s,output);
				totalList.add(total == null ? 0 : total);
			}
			mmap.put("yDateChk",totalList);
		}else if("month".equals(dataType)){  // 月趋势
			List<String> daysList = DateUtils.getEachDaysList();
			List<Double> totalList = new ArrayList<>();
			for(String s : daysList){
				Double total = ncOutputChickenMapper.selectChkicenTotalByDays(s,output);
				totalList.add(total == null ? 0 : total);
			}
			mmap.put("yDateChk",totalList);
		}else if("day".equals(dataType)){   // 日趋势
			List<String> daysList = DateUtils.getBeforeSevenDay();
			List<Double> totalList = new ArrayList<>();
			for(String s : daysList){
				Double total = ncOutputChickenMapper.selectChkicenTotalByDays(s,output);
				totalList.add(total == null ? 0 : total);
			}
			mmap.put("yDateChk",totalList);
		}else if("search".equals(dataType)){   // 时间段查询
			List<String> daysList = DateUtils.getBetweenTime(beginTime,endTime);
			totalDays = daysList.size();
			List<Double> totalList = new ArrayList<>();
			for(String s : daysList){
				Double total = ncOutputChickenMapper.selectChkicenTotalByDays(s,output);
				totalList.add(total == null ? 0 : total);
			}
			mmap.put("yDateChk",totalList);
		}else{
		}
		chkMap.put("amount",totalWeight == null ? 0 : totalWeight);
		chkMap.put("dayAmount",totalWeight == null ? 0 : division(totalWeight,totalDays));
		mmap.put("chk",chkMap);


	}
	private Object division(Double a, int b) {
		String result = "";
		double num =(double)a/b;
		DecimalFormat df = new DecimalFormat("0.0");
		result = df.format(num);
		return result;
	}
	private int findDays(String dataType) {
		Date yearsDate = DateUtils.getBeginDayOfYear();
		Date monthDate = DateUtils.getBeginDayOfMonth();
		Date nowDate = DateUtils.getNowDate();
		if("year".equals(dataType)){
			return DateUtils.getDiffDays(yearsDate,nowDate);
		}else if("month".equals(dataType)){
			return DateUtils.getDiffDays(monthDate,nowDate);
		}else{
			return 1;
		}

	}

	@Override
	public void innoInfoType(String beginTime, String endTime, ModelMap mmap, String type) {

	}
	public List<Map> selectCountByTime(Integer spanType, String beginTime, String endTime,Integer outputType) {
		 Date s = null;
		Date e = null;
		Map se = orderCfService.calTimeSpan(s, e, beginTime, endTime, spanType);
		s = (Date) se.get("s");
		e = (Date) se.get("e");

		return getSumByDate(spanType, s, e,outputType);
	}


	public void setChickenTypes(ModelMap mmap, NcOutputChicken outputChicken, 	Integer outputType, int type) {
		List<SysDictData> chickenTypes = dictDataService.selectDictDataByType("cforderType");

		//key
		List<String> keys = new ArrayList<>();
		List<Double> cf = new ArrayList<>();
		for (SysDictData d : chickenTypes) {
			keys.add(d.getDictLabel());
		}
		for (SysDictData d : chickenTypes) {
			if(outputType != null){
				outputChicken.setOutputType(outputType);
			}
			else{
				outputChicken.setOutputType(Integer.parseInt(d.getDictValue()));
			}
			cf.add(ncOutputChickenMapper.selectNcOutputAmount(outputChicken));
		}
			keys.add(0, "共计总量");

		double total = 0;
		for (double i : cf) {
			total += i;
		}
		cf.add(0, total);


		List<Map> list = new ArrayList<>();

		for (int i = 0; i < keys.size(); i++) {
			Map m = new HashMap();
			if (i != 0) {
				m.put("type", chickenTypes.get(i - 1).getDictValue());
			} else {
				m.put("type", null);
			}

			m.put("key", keys.get(i));
			m.put("value", cf.get(i));
			m.put("class", (i + 1) + " " + (i == 0 ? "active" : ""));
			list.add(m);
		}
		mmap.put("cf", cf);
		mmap.put("keys", keys);
		mmap.put("list", list);
	}
	//前端内部消息
	@Override
	public List<Map> selectInfoByTime(Integer spanType, String beginTime, String endTime,String outputType) {
		Date s = null;
		Date e = null;
		Map se = orderCfService.calTimeSpan(s, e, beginTime, endTime, spanType);
		s = (Date) se.get("s");
		e = (Date) se.get("e");

		return getSumInfoByDate(spanType, s, e,outputType);
	}
	//前端内部消息
	private List<Map> getSumInfoByDate(Integer spanType, Date s, Date e,String outputType) {
		List<Map> result = new ArrayList<>();
		if (spanType == 1) {
			while (e.getTime() >= s.getTime()) {
				String dateSpan = DateUtils.parseDateToStr("yyyy", s);

				Map m = new HashMap();
				m.put("x", dateSpan);
				m.put("chickenCount", getByDate(dateSpan, spanType,outputType));
				result.add(m);

				s = DateUtils.addYears(s, 1);
			}
		} else if (spanType == 2) {
			while (e.getTime() >= s.getTime()) {
				String dateSpan = DateUtils.parseDateToStr("yyyy-MM", s);
				Map m = new HashMap();
				m.put("x", dateSpan);
				m.put("chickenCount", getByDate(dateSpan, spanType,outputType));
				result.add(m);
				s = DateUtils.addMonths(s, 1);
			}
		} else if (spanType == 3) {
			while (e.getTime() >= s.getTime()) {
				String dateSpan = DateUtils.parseDateToStr("yyyy-MM-dd", s);
				Map m = new HashMap();
				m.put("x", dateSpan);
				m.put("chickenCount", getByDate(dateSpan, spanType,outputType));
				result.add(m);
				s = DateUtils.addDays(s, 1);
			}
		}
		return result;
	}
	//前端内部消息
	private Double getByDate(String dateSpan, Integer dataSpan,String outputType) {
		NcOutputChicken chicken = new NcOutputChicken();
		if (dataSpan == null || dataSpan == 2) {
			chicken.getParams().put("dateSpanMonth", dateSpan);
		} else if (dataSpan == 1) {
			chicken.getParams().put("dateSpanYear", dateSpan);
		} else if (dataSpan == 3) {
			chicken.getParams().put("dateSpanDay", dateSpan);
		}
		if(outputType != null &&  outputType.equals("all") == false){
			chicken.setOutputType(Integer.valueOf(outputType));
		}
		return ncOutputChickenMapper.selectChickenInfoByMonthRange(chicken);
	}


	private List<Map> getSumByDate(Integer spanType, Date s, Date e,Integer outputType) {
		List<Map> result = new ArrayList<>();
		if (spanType == 1) {
			while (e.getTime() >= s.getTime()) {
				String dateSpan = DateUtils.parseDateToStr("yyyy", s);

				Map m = new HashMap();
				m.put("x", dateSpan);
				m.put("count", getSumByDate(dateSpan, spanType,outputType));
				result.add(m);

				s = DateUtils.addYears(s, 1);
			}
		} else if (spanType == 2) {
			while (e.getTime() >= s.getTime()) {
				String dateSpan = DateUtils.parseDateToStr("yyyy-MM", s);
				Map m = new HashMap();
				m.put("x", dateSpan);
				m.put("count", getSumByDate(dateSpan, spanType,outputType));
				result.add(m);
				s = DateUtils.addMonths(s, 1);
			}
		} else if (spanType == 3) {
			while (e.getTime() >= s.getTime()) {
				String dateSpan = DateUtils.parseDateToStr("yyyy-MM-dd", s);
				Map m = new HashMap();
				m.put("x", dateSpan);
				m.put("count", getSumByDate(dateSpan, spanType,outputType));
				result.add(m);
				s = DateUtils.addDays(s, 1);
			}
		}
		return result;
	}
	private Double getSumByDate(String dateSpan, Integer dataSpan,Integer outputType) {
		NcOutputChicken chicken = new NcOutputChicken();
		if (dataSpan == null || dataSpan == 2) {
			chicken.getParams().put("dateSpanMonth", dateSpan);
		} else if (dataSpan == 1) {
			chicken.getParams().put("dateSpanYear", dateSpan);
		} else if (dataSpan == 3) {
			chicken.getParams().put("dateSpanDay", dateSpan);
		}
		chicken.setOutputType(outputType);
		return ncOutputChickenMapper.selectNcOutputMonthAmount(chicken);
	}


}
