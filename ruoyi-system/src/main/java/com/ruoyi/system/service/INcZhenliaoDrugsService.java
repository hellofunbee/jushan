package com.ruoyi.system.service;

import com.ruoyi.system.domain.NcZhenliaoDrugs;
import java.util.List;

/**
 * 兽医诊疗用药 服务层
 * 
 * @author ruoyi
 * @date 2019-09-05
 */
public interface INcZhenliaoDrugsService 
{
	/**
     * 查询兽医诊疗用药信息
     * 
     * @param zlId 兽医诊疗用药ID
     * @return 兽医诊疗用药信息
     */
	public NcZhenliaoDrugs selectNcZhenliaoDrugsById(Long zlId);
	
	/**
     * 查询兽医诊疗用药列表
     * 
     * @param ncZhenliaoDrugs 兽医诊疗用药信息
     * @return 兽医诊疗用药集合
     */
	public List<NcZhenliaoDrugs> selectNcZhenliaoDrugsList(NcZhenliaoDrugs ncZhenliaoDrugs);
	
	/**
     * 新增兽医诊疗用药
     * 
     * @param ncZhenliaoDrugs 兽医诊疗用药信息
     * @return 结果
     */
	public int insertNcZhenliaoDrugs(NcZhenliaoDrugs ncZhenliaoDrugs);
	
	/**
     * 修改兽医诊疗用药
     * 
     * @param ncZhenliaoDrugs 兽医诊疗用药信息
     * @return 结果
     */
	public int updateNcZhenliaoDrugs(NcZhenliaoDrugs ncZhenliaoDrugs);
		
	/**
     * 删除兽医诊疗用药信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcZhenliaoDrugsByIds(String ids);

}
