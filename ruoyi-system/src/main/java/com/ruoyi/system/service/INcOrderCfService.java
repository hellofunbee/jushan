package com.ruoyi.system.service;

import com.ruoyi.system.domain.NcOrderCf;
import com.ruoyi.system.domain.SysUser;
import org.springframework.ui.ModelMap;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 菜房订单 服务层
 *
 * @author ruoyi
 * @date 2019-08-29
 */
public interface INcOrderCfService {
    /**
     * 查询菜房订单信息
     *
     * @param cforderId 菜房订单ID
     * @return 菜房订单信息
     */
    public NcOrderCf selectNcOrderCfById(Long cforderId);

    /**
     * 查询菜房订单列表
     *
     * @param ncOrderCf 菜房订单信息
     * @return 菜房订单集合
     */
    public List<NcOrderCf> selectNcOrderCfList(NcOrderCf ncOrderCf);

    /**
     * 新增菜房订单
     *
     * @param ncOrderCf 菜房订单信息
     * @return 结果
     */
    public int insertNcOrderCf(NcOrderCf ncOrderCf);

    /**
     * 修改菜房订单
     *
     * @param ncOrderCf 菜房订单信息
     * @return 结果
     */
    public int updateNcOrderCf(NcOrderCf ncOrderCf);

    /**
     * 删除菜房订单信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteNcOrderCfByIds(String ids);

    NcOrderCf fillValues(NcOrderCf ncOrderCf);

    int createNcOrderCfByIds(String ids, SysUser user);


    void setOrderTypes(ModelMap mmap, NcOrderCf ncOrderCf, String orderStatus, int type);

    List<Map> selectCountByTime(Integer orderType, Integer dataSpan, String beginTime, String endTime, int type);

    Map calTimeSpan(Date s, Date e, String beginTime, String endTime, Integer spanType);

    /**
     * 导入订单数据
     *
     * @param orders 用户数据列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importOrder(List<NcOrderCf> orders, Boolean isUpdateSupport, String operName);
    public Integer caiScreen(NcOrderCf order);
}
