package com.ruoyi.system.service;

import com.ruoyi.system.domain.NcCowsDisinfection;
import java.util.List;

/**
 * 消毒记录（牛舍） 服务层
 * 
 * @author ruoyi
 * @date 2019-09-04
 */
public interface INcCowsDisinfectionService 
{
	/**
     * 查询消毒记录（牛舍）信息
     * 
     * @param dfId 消毒记录（牛舍）ID
     * @return 消毒记录（牛舍）信息
     */
	public NcCowsDisinfection selectNcCowsDisinfectionById(Long dfId);
	
	/**
     * 查询消毒记录（牛舍）列表
     * 
     * @param ncCowsDisinfection 消毒记录（牛舍）信息
     * @return 消毒记录（牛舍）集合
     */
	public List<NcCowsDisinfection> selectNcCowsDisinfectionList(NcCowsDisinfection ncCowsDisinfection);
	
	/**
     * 新增消毒记录（牛舍）
     * 
     * @param ncCowsDisinfection 消毒记录（牛舍）信息
     * @return 结果
     */
	public int insertNcCowsDisinfection(NcCowsDisinfection ncCowsDisinfection);
	
	/**
     * 修改消毒记录（牛舍）
     * 
     * @param ncCowsDisinfection 消毒记录（牛舍）信息
     * @return 结果
     */
	public int updateNcCowsDisinfection(NcCowsDisinfection ncCowsDisinfection);
		
	/**
     * 删除消毒记录（牛舍）信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcCowsDisinfectionByIds(String ids);
	
}
