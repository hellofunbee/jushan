package com.ruoyi.system.service;

import com.ruoyi.system.domain.NcGasLog;
import org.springframework.ui.ModelMap;

import java.util.List;

/**
 * 气值 服务层
 *
 * @author ruoyi
 * @date 2019-07-31
 */
public interface INcGasLogService
{
	/**
     * 查询气值信息
     *
     * @param gasLogId 气值ID
     * @return 气值信息
     */
	public NcGasLog selectNcGasLogById(Long gasLogId);

	/**
     * 查询气值列表
     *
     * @param ncGasLog 气值信息
     * @return 气值集合
     */
	public List<NcGasLog> selectNcGasLogList(NcGasLog ncGasLog);

	/**
     * 新增气值
     *
     * @param ncGasLog 气值信息
     * @return 结果
     */
	public int insertNcGasLog(NcGasLog ncGasLog);

	/**
     * 修改气值
     *
     * @param ncGasLog 气值信息
     * @return 结果
     */
	public int updateNcGasLog(NcGasLog ncGasLog);

	/**
     * 删除气值信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcGasLogByIds(String ids);

    void showMag(String beginTime, String endTime, ModelMap mmap);

	/**
	 * 总气值量
	 */
	public Double selectGasAmount(NcGasLog ncGasLog);

	public Double selectGasMonthAmount(NcGasLog ncGasLog);

	Double selectGasMonth(NcGasLog ncGasLog);

	Double selectGasMonthMin(NcGasLog ncGasLog);

	List<NcGasLog> selectGasMonthList (NcGasLog ncGasLog);

	/**
	 * 找到时间区间的头一个值
	 */
	Double seleGasTop(NcGasLog ncGasLog);

	/**
	 * 找到时间区间的末一个值
	 */
	Double seleGasLow(NcGasLog ncGasLog);

	/**
	 * 时间的最后一个值
	 * @param ncGasLog
	 * @return
	 */
	String seleTimeLow(NcGasLog ncGasLog);
	//大屏值
	double seleGasLowScreen(NcGasLog ncGasLog);
}
