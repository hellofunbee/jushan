package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.NcTransportInfoMapper;
import com.ruoyi.system.domain.NcTransportInfo;
import com.ruoyi.system.service.INcTransportInfoService;
import com.ruoyi.common.core.text.Convert;

/**
 * 菜房出库 服务层实现
 * 
 * @author ruoyi
 * @date 2019-09-05
 */
@Service
public class NcTransportInfoServiceImpl implements INcTransportInfoService 
{
	@Autowired
	private NcTransportInfoMapper ncTransportInfoMapper;

	/**
     * 查询菜房出库信息
     * 
     * @param transId 菜房出库ID
     * @return 菜房出库信息
     */
    @Override
	public NcTransportInfo selectNcTransportInfoById(Integer transId)
	{
	    return ncTransportInfoMapper.selectNcTransportInfoById(transId);
	}
	
	/**
     * 查询菜房出库列表
     * 
     * @param ncTransportInfo 菜房出库信息
     * @return 菜房出库集合
     */
	@Override
	public List<NcTransportInfo> selectNcTransportInfoList(NcTransportInfo ncTransportInfo)
	{
	    return ncTransportInfoMapper.selectNcTransportInfoList(ncTransportInfo);
	}
	
    /**
     * 新增菜房出库
     * 
     * @param ncTransportInfo 菜房出库信息
     * @return 结果
     */
	@Override
	public int insertNcTransportInfo(NcTransportInfo ncTransportInfo)
	{
	    return ncTransportInfoMapper.insertNcTransportInfo(ncTransportInfo);
	}
	
	/**
     * 修改菜房出库
     * 
     * @param ncTransportInfo 菜房出库信息
     * @return 结果
     */
	@Override
	public int updateNcTransportInfo(NcTransportInfo ncTransportInfo)
	{
	    return ncTransportInfoMapper.updateNcTransportInfo(ncTransportInfo);
	}

	/**
     * 删除菜房出库对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteNcTransportInfoByIds(String ids)
	{
		return ncTransportInfoMapper.deleteNcTransportInfoByIds(Convert.toStrArray(ids));
	}
	
}
