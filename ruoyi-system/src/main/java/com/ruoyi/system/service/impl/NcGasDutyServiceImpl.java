package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.text.Convert;
import com.ruoyi.system.domain.NcGasDuty;
import com.ruoyi.system.mapper.NcGasDutyMapper;
import com.ruoyi.system.mapper.SysUserMapper;
import com.ruoyi.system.service.INcGasDutyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 锅炉房值班记录 服务层实现
 *
 * @author ruoyi
 * @date 2019-07-31
 */
@Service
public class NcGasDutyServiceImpl implements INcGasDutyService
{
	@Autowired
	private NcGasDutyMapper ncGasDutyMapper;

	@Autowired
	private SysUserMapper sysUserMapper;
	/**
     * 查询锅炉房值班记录信息
     *
     * @param dutyId 锅炉房值班记录ID
     * @return 锅炉房值班记录信息
     */
    @Override
	public NcGasDuty selectNcGasDutyById(Long dutyId)
	{

	    return ncGasDutyMapper.selectNcGasDutyById(dutyId);
	}

	/**
     * 查询锅炉房值班记录列表
     *
     * @param ncGasDuty 锅炉房值班记录信息
     * @return 锅炉房值班记录集合
     */
	@Override
	public List<NcGasDuty> selectNcGasDutyList(NcGasDuty ncGasDuty)
	{
	    return ncGasDutyMapper.selectNcGasDutyList(ncGasDuty);
	}

    /**
     * 新增锅炉房值班记录
     *
     * @param ncGasDuty 锅炉房值班记录信息
     * @return 结果
     */
	@Override
	public int insertNcGasDuty(NcGasDuty ncGasDuty)
	{
	    return ncGasDutyMapper.insertNcGasDuty(ncGasDuty);
	}

	/**
     * 修改锅炉房值班记录
     *
     * @param ncGasDuty 锅炉房值班记录信息
     * @return 结果
     */
	@Override
	public int updateNcGasDuty(NcGasDuty ncGasDuty)
	{
	    return ncGasDutyMapper.updateNcGasDuty(ncGasDuty);
	}

	/**
     * 删除锅炉房值班记录对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteNcGasDutyByIds(String ids)
	{
		return ncGasDutyMapper.deleteNcGasDutyByIds(Convert.toStrArray(ids));
	}

}
