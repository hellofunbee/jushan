package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.NcHeaderMapper;
import com.ruoyi.system.domain.NcHeader;
import com.ruoyi.system.service.INcHeaderService;
import com.ruoyi.common.core.text.Convert;

/**
 * 轮播图 服务层实现
 * 
 * @author ruoyi
 * @date 2019-09-15
 */
@Service
public class NcHeaderServiceImpl implements INcHeaderService 
{
	@Autowired
	private NcHeaderMapper ncHeaderMapper;

	/**
     * 查询轮播图信息
     * 
     * @param hId 轮播图ID
     * @return 轮播图信息
     */
    @Override
	public NcHeader selectNcHeaderById(Integer hId)
	{
	    return ncHeaderMapper.selectNcHeaderById(hId);
	}
	
	/**
     * 查询轮播图列表
     * 
     * @param ncHeader 轮播图信息
     * @return 轮播图集合
     */
	@Override
	public List<NcHeader> selectNcHeaderList(NcHeader ncHeader)
	{
	    return ncHeaderMapper.selectNcHeaderList(ncHeader);
	}
	
    /**
     * 新增轮播图
     * 
     * @param ncHeader 轮播图信息
     * @return 结果
     */
	@Override
	public int insertNcHeader(NcHeader ncHeader)
	{
	    return ncHeaderMapper.insertNcHeader(ncHeader);
	}
	
	/**
     * 修改轮播图
     * 
     * @param ncHeader 轮播图信息
     * @return 结果
     */
	@Override
	public int updateNcHeader(NcHeader ncHeader)
	{
	    return ncHeaderMapper.updateNcHeader(ncHeader);
	}

	/**
     * 删除轮播图对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteNcHeaderByIds(String ids)
	{
		return ncHeaderMapper.deleteNcHeaderByIds(Convert.toStrArray(ids));
	}

	@Override
	public int changeStatus(NcHeader h) {

		return ncHeaderMapper.updateNcHeader(h);
	}
}
