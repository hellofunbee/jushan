package com.ruoyi.system.service;

import com.ruoyi.system.domain.NcGoods;
import java.util.List;
import java.util.Map;

/**
 * 物资仓储存 服务层
 *
 * @author ruoyi
 * @date 2019-08-05
 */
public interface INcGoodsService
{
	/**
     * 查询物资仓储存信息
     *
     * @param goodsId 物资仓储存ID
     * @return 物资仓储存信息
     */
	public NcGoods selectNcGoodsById(Long goodsId);

	/**
     * 查询物资仓储存列表
     *
     * @param ncGoods 物资仓储存信息
     * @return 物资仓储存集合
     */
	public List<NcGoods> selectNcGoodsList(NcGoods ncGoods);

	/**
     * 新增物资仓储存
     *
     * @param ncGoods 物资仓储存信息
     * @return 结果
     */
	public int insertNcGoods(NcGoods ncGoods);

	/**
     * 修改物资仓储存
     *
     * @param ncGoods 物资仓储存信息
     * @return 结果
     */
	public int updateNcGoods(NcGoods ncGoods);

	/**
     * 删除物资仓储存信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcGoodsByIds(String ids);

	public Long selectmeterialIdByName(String meterialName);

    List<NcGoods> selectNcGoodsByMeterialId(String type, String beginTime, String endTime);

    List<Map> selectCountByTime(Integer dataSpan, String beginTime, String endTime, String metialType);
}
