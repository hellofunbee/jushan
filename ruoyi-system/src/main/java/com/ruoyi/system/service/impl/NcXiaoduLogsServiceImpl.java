package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.annotation.DataScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.NcXiaoduLogsMapper;
import com.ruoyi.system.domain.NcXiaoduLogs;
import com.ruoyi.system.service.INcXiaoduLogsService;
import com.ruoyi.common.core.text.Convert;

/**
 * 消毒记录（植保） 服务层实现
 * 
 * @author ruoyi
 * @date 2019-08-30
 */
@Service
public class NcXiaoduLogsServiceImpl implements INcXiaoduLogsService 
{
	@Autowired
	private NcXiaoduLogsMapper ncXiaoduLogsMapper;

	/**
     * 查询消毒记录（植保）信息
     * 
     * @param xdId 消毒记录（植保）ID
     * @return 消毒记录（植保）信息
     */
    @Override
	public NcXiaoduLogs selectNcXiaoduLogsById(Long xdId)
	{
	    return ncXiaoduLogsMapper.selectNcXiaoduLogsById(xdId);
	}
	
	/**
     * 查询消毒记录（植保）列表
     * 
     * @param ncXiaoduLogs 消毒记录（植保）信息
     * @return 消毒记录（植保）集合
     */
	@Override
	@DataScope(deptAlias = "d", userAlias = "u")
	public List<NcXiaoduLogs> selectNcXiaoduLogsList(NcXiaoduLogs ncXiaoduLogs)
	{
	    return ncXiaoduLogsMapper.selectNcXiaoduLogsList(ncXiaoduLogs);
	}
	
    /**
     * 新增消毒记录（植保）
     * 
     * @param ncXiaoduLogs 消毒记录（植保）信息
     * @return 结果
     */
	@Override
	public int insertNcXiaoduLogs(NcXiaoduLogs ncXiaoduLogs)
	{
	    return ncXiaoduLogsMapper.insertNcXiaoduLogs(ncXiaoduLogs);
	}
	
	/**
     * 修改消毒记录（植保）
     * 
     * @param ncXiaoduLogs 消毒记录（植保）信息
     * @return 结果
     */
	@Override
	public int updateNcXiaoduLogs(NcXiaoduLogs ncXiaoduLogs)
	{
	    return ncXiaoduLogsMapper.updateNcXiaoduLogs(ncXiaoduLogs);
	}

	/**
     * 删除消毒记录（植保）对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteNcXiaoduLogsByIds(String ids)
	{
		return ncXiaoduLogsMapper.deleteNcXiaoduLogsByIds(Convert.toStrArray(ids));
	}

	@Override
	public NcXiaoduLogs selectNcXiaoduLogsAmount(NcXiaoduLogs ncXiaoduLogs) {
		return ncXiaoduLogsMapper.selectNcXiaoduLogsAmount(ncXiaoduLogs);
	}

}
