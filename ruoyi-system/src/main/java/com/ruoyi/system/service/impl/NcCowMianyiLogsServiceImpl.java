package com.ruoyi.system.service.impl;

import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.utils.NcUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.mapper.NcMianyiLogsMapper;
import com.ruoyi.system.service.INcCowMianyiLogsService;
import com.ruoyi.system.service.INcDrugsService;
import com.ruoyi.system.service.INcOrderService;
import com.ruoyi.system.service.INcZhenliaoDrugsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * 免疫记录 服务层实现
 *
 * @author ruoyi
 * @date 2019-09-02
 */
@Service
public class NcCowMianyiLogsServiceImpl implements INcCowMianyiLogsService {
    @Autowired
    private NcMianyiLogsMapper ncMianyiLogsMapper;

    @Autowired
    private INcOrderService orderService;

    @Autowired
    private INcDrugsService iNcDrugsService;


    @Autowired
    private INcZhenliaoDrugsService zhenliaoDrugsService;


    /**
     * 查询免疫记录信息
     *
     * @param myId 免疫记录ID
     * @return 免疫记录信息
     */
    @Override
    public NcMianyiLogs selectNcMianyiLogsById(Long myId) {
        return ncMianyiLogsMapper.selectNcMianyiLogsById(myId);
    }

    /**
     * 查询免疫记录列表
     *
     * @param ncMianyiLogs 免疫记录信息
     * @return 免疫记录集合
     */
    @Override
    public List<NcMianyiLogs> selectNcMianyiLogsList(NcMianyiLogs ncMianyiLogs) {
        return ncMianyiLogsMapper.selectNcMianyiLogsList(ncMianyiLogs);
    }

    /**
     * 新增免疫记录
     *
     * @param ncMianyiLogs 免疫记录信息
     * @return 结果
     */
    @Override
    public AjaxResult insertNcMianyiLogs(NcMianyiLogs ncMianyiLogs) {


        //疫苗id  -- > 药品id
        Long ymId = ncMianyiLogs.getYmId();

        NcDrugs ncDrugs = iNcDrugsService.selectNcDrugsById(ymId);

        //计算药库存数量是否足够
        if (ncDrugs.getStockAmout() < ncMianyiLogs.getMyDose()) {
            return AjaxResult.error("药品库存不足");
        }

        //根据选择药品id  查询药品库存 设生 疫苗名称 生产厂家 批号
        ncMianyiLogs.setYmName(ncDrugs.getDrugName());
        ncMianyiLogs.setYmFactory(ncDrugs.getFactory());
        ncMianyiLogs.setYmCode(ncDrugs.getDrugCode());


        ncMianyiLogsMapper.insertNcMianyiLogs(ncMianyiLogs);

        return AjaxResult.success("免疫记录添加成功");

    }

    /**
     * 修改免疫记录
     *
     * @param ncMianyiLogs 免疫记录信息
     * @return 结果
     */
    @Override
    public int updateNcMianyiLogs(NcMianyiLogs ncMianyiLogs) {
        return ncMianyiLogsMapper.updateNcMianyiLogs(ncMianyiLogs);
    }

    /**
     * 删除免疫记录对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteNcMianyiLogsByIds(String ids) {
        return ncMianyiLogsMapper.deleteNcMianyiLogsByIds(Convert.toStrArray(ids));
    }

    @Override
    @Transactional
    public int createZlOrderByIds(String ids, SysUser user) {

        NcOrder o = new NcOrder();

        if (StringUtils.isNotEmpty(ids)) {
            //*创建牛舍免疫订单*//*
            o.setOrderCode(NcUtils.getOrderCode());
            //牛舍免疫单
            o.setOrderType(Constants.ORDER_COW_MIANYI);
            o.setOrderTime(new Date());
            o.setCreateTime(new Date());
            o.setCreateBy(user.getLoginName());
            this.orderService.insertNcOrder(o);

            String[] idArr = Convert.toStrArray(ids);
            for (String id : idArr) {
                NcMianyiLogs ncMianyiLogs = this.ncMianyiLogsMapper.selectNcMianyiLogsById(Long.parseLong(id));
                ncMianyiLogs.setOrderId(o.getOrderId());
                ncMianyiLogs.setMyStatus(4);


                //减少药品库存
                NcDrugs ncDrugs = iNcDrugsService.selectNcDrugsById(ncMianyiLogs.getYmId());


                // 判断库存是否足够

                //TODO  计量单位待定
                ncDrugs.setStockAmout(ncDrugs.getStockAmout() - ncMianyiLogs.getMyDose());
                //生成诊疗药用药单
//                NcZhenliaoDrugs ncZhenliaoDrugs = new NcZhenliaoDrugs(null, ncDrugs.getDrugCode(), ncDrugs.getDrugName(), Integer.parseInt(ncMianyiLogs.getMyWays()), ncMianyiLogs.getMyDose(), null);
//                ncZhenliaoDrugs.setCreateBy(user.getLoginName());
//                ncZhenliaoDrugs.setCreateTime(ncMianyiLogs.getCreateTime());


                this.ncMianyiLogsMapper.updateNcMianyiLogs(ncMianyiLogs);
//                zhenliaoDrugsService.insertNcZhenliaoDrugs(ncZhenliaoDrugs);
                iNcDrugsService.updateNcDrugs(ncDrugs);

            }

            return 1;
        } else {
            return 0;
        }

    }


}
