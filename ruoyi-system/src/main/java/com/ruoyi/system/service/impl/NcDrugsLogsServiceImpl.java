package com.ruoyi.system.service.impl;

import java.util.Date;
import java.util.List;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.system.domain.NcBullJingye;
import com.ruoyi.system.domain.NcDrugs;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.mapper.NcDrugsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.NcDrugsLogsMapper;
import com.ruoyi.system.domain.NcDrugsLogs;
import com.ruoyi.system.service.INcDrugsLogsService;
import com.ruoyi.common.core.text.Convert;

/**
 * 兽药出库记录 服务层实现
 *
 * @author ruoyi
 * @date 2019-09-02
 */
@Service
public class NcDrugsLogsServiceImpl implements INcDrugsLogsService {
    @Autowired
    private NcDrugsLogsMapper ncDrugsLogsMapper;

    @Autowired
    private NcDrugsMapper ncDrugsMapper;

    /**
     * 查询兽药出库记录信息
     *
     * @param drugLogId 兽药出库记录ID
     * @return 兽药出库记录信息
     */
    @Override
    public NcDrugsLogs selectNcDrugsLogsById(Long drugLogId) {
        return ncDrugsLogsMapper.selectNcDrugsLogsById(drugLogId);
    }

    /**
     * 查询兽药出库记录列表
     *
     * @param ncDrugsLogs 兽药出库记录信息
     * @return 兽药出库记录集合
     */
    @Override
    public List<NcDrugsLogs> selectNcDrugsLogsList(NcDrugsLogs ncDrugsLogs) {
        return ncDrugsLogsMapper.selectNcDrugsLogsList(ncDrugsLogs);
    }

    /**
     * 新增兽药出库记录
     *
     * @param ncDrugsLogs 兽药出库记录信息
     * @return 结果
     */
    @Override
    public int insertNcDrugsLogs(NcDrugsLogs ncDrugsLogs) {
        return ncDrugsLogsMapper.insertNcDrugsLogs(ncDrugsLogs);
    }

    /**
     * 修改兽药出库记录
     *
     * @param ncDrugsLogs 兽药出库记录信息
     * @return 结果
     */
    @Override
    public int updateNcDrugsLogs(NcDrugsLogs ncDrugsLogs) {
        return ncDrugsLogsMapper.updateNcDrugsLogs(ncDrugsLogs);
    }

    /**
     * 删除兽药出库记录对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteNcDrugsLogsByIds(String ids) {
        return ncDrugsLogsMapper.deleteNcDrugsLogsByIds(Convert.toStrArray(ids));
    }

    @Override
    public AjaxResult checkOutDrugs(NcDrugsLogs ncDrugsLogs, SysUser sysUser) {

        NcDrugs ncDrugs = ncDrugsMapper.selectNcDrugsById(ncDrugsLogs.getDrugId());

        //经手人
        ncDrugsLogs.setCreateBy(sysUser.getLoginName());
        //现有库存数量
        Integer stockAmoutNow = ncDrugs.getStockAmout();
        //出库数量
        Integer outputAmount = ncDrugsLogs.getOutputAmount();


        if (outputAmount > 0 && stockAmoutNow >= outputAmount) {
            //出库
            //减少库存
            ncDrugs.setStockAmout(ncDrugs.getStockAmout() - outputAmount);
            ncDrugsMapper.updateNcDrugs(ncDrugs);
            //添加出库记录
            ncDrugsLogsMapper.insertNcDrugsLogs(ncDrugsLogs);
            return AjaxResult.success("出库成功");
        }else if(outputAmount <= 0){
            return AjaxResult.error("请输入正确的出库数量");
        }else if (stockAmoutNow < outputAmount){
            //数量不足 出库失败
            return AjaxResult.error("库存不足");
        }

        return AjaxResult.error();
    }


}

