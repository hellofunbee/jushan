package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.NcFeedLog;
import com.ruoyi.system.mapper.NcFeedLogMapper;
import com.ruoyi.system.service.INcFeedLogService;
import com.ruoyi.system.service.INcOrderCfService;
import com.ruoyi.system.service.INcOrderService;
import com.ruoyi.system.service.ISysDictDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * 饲料出入记录 服务层实现
 *
 * @author ruoyi
 * @date 2019-09-10
 */
@Service
public class NcFeedLogServiceImpl implements INcFeedLogService
{
	@Autowired
	private NcFeedLogMapper ncFeedLogMapper;
	@Autowired
	private INcOrderService orderService;
	@Autowired
	private INcOrderCfService orderCfService;
	@Autowired
	private ISysDictDataService dictDataService;
	/**
     * 查询饲料出入记录信息
     *
     * @param feedLogId 饲料出入记录ID
     * @return 饲料出入记录信息
     */
    @Override
	public NcFeedLog selectNcFeedLogById(Long feedLogId)
	{
	    return ncFeedLogMapper.selectNcFeedLogById(feedLogId);
	}

	/**
     * 查询饲料出入记录列表
     *
     * @param ncFeedLog 饲料出入记录信息
     * @return 饲料出入记录集合
     */
	@Override
	public List<NcFeedLog> selectNcFeedLogList(NcFeedLog ncFeedLog)
	{
	    return ncFeedLogMapper.selectNcFeedLogList(ncFeedLog);
	}

    /**
     * 新增饲料出入记录
     *
     * @param ncFeedLog 饲料出入记录信息
     * @return 结果
     */
	@Override
	public int insertNcFeedLog(NcFeedLog ncFeedLog)
	{
	    return ncFeedLogMapper.insertNcFeedLog(ncFeedLog);
	}

	/**
     * 修改饲料出入记录
     *
     * @param ncFeedLog 饲料出入记录信息
     * @return 结果
     */
	@Override
	public int updateNcFeedLog(NcFeedLog ncFeedLog)
	{
	    return ncFeedLogMapper.updateNcFeedLog(ncFeedLog);
	}

	/**
     * 删除饲料出入记录对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteNcFeedLogByIds(String ids)
	{
		return ncFeedLogMapper.deleteNcFeedLogByIds(Convert.toStrArray(ids));
	}

	@Override
	public NcFeedLog selectNcFeedLogAmount(NcFeedLog ncFeedLog) {
		return ncFeedLogMapper.selectNcFeedLogAmount(ncFeedLog);
	}

	@Override
	public Double selectNcFeedLogMonthAmount(NcFeedLog ncFeedLog) {
		return ncFeedLogMapper.selectNcFeedLogMonthAmount(ncFeedLog);
	}

	@Override
	public NcFeedLog selectNcFeedLogAmountCows(NcFeedLog ncFeedLog) {
		return ncFeedLogMapper.selectNcFeedLogAmountCows(ncFeedLog);
	}

	@Override
	public Double selectNcFeedLogMonthAmountCows(NcFeedLog ncFeedLog) {
		return ncFeedLogMapper.selectNcFeedLogMonthAmountCows(ncFeedLog);
	}

	@Override
	public Double selectNcFeedLogScreen(NcFeedLog ncFeedLog) {
		return ncFeedLogMapper.selectNcFeedLogScreen(ncFeedLog);
	}

	//鸡舍
	public List<Map> selectCountByTime(Integer spanType, String beginTime, String endTime) {
		Date s = null;
		Date e = null;
		Map se = orderCfService.calTimeSpan(s, e, beginTime, endTime, spanType);
		s = (Date) se.get("s");
		e = (Date) se.get("e");

		return getSumByDate(spanType, s, e);
	}
	//鸡舍
	private List<Map> getSumByDate(Integer spanType, Date s, Date e) {
		List<Map> result = new ArrayList<>();
		if (spanType == 1) {
			while (e.getTime() >= s.getTime()) {
				String dateSpan = DateUtils.parseDateToStr("yyyy", s);

				Map m = new HashMap();
				m.put("x", dateSpan);
				m.put("zcount", getSumByDate(dateSpan, spanType,1));
				m.put("fcount", getSumByDate(dateSpan, spanType,2));
				result.add(m);

				s = DateUtils.addYears(s, 1);
			}
		} else if (spanType == 2) {
			while (e.getTime() >= s.getTime()) {
				String dateSpan = DateUtils.parseDateToStr("yyyy-MM", s);
				Map m = new HashMap();
				m.put("x", dateSpan);
				m.put("zcount", getSumByDate(dateSpan, spanType,1));
				m.put("fcount", getSumByDate(dateSpan, spanType,2));
				result.add(m);
				s = DateUtils.addMonths(s, 1);
			}
		} else if (spanType == 3) {
			while (e.getTime() >= s.getTime()) {
				String dateSpan = DateUtils.parseDateToStr("yyyy-MM-dd", s);
				Map m = new HashMap();
				m.put("x", dateSpan);
				m.put("zcount", getSumByDate(dateSpan, spanType,1));
				m.put("fcount", getSumByDate(dateSpan, spanType,2));
				result.add(m);
				s = DateUtils.addDays(s, 1);
			}
		}
		return result;
	}
	//鸡舍
	private Double getSumByDate(String dateSpan, Integer dataSpan,Integer outputType) {
		NcFeedLog feedLog = new NcFeedLog();
		if (dataSpan == null || dataSpan == 2) {
			feedLog.getParams().put("dateSpanMonth", dateSpan);
		} else if (dataSpan == 1) {
			feedLog.getParams().put("dateSpanYear", dateSpan);
		} else if (dataSpan == 3) {
			feedLog.getParams().put("dateSpanDay", dateSpan);
		}
		feedLog.setType(outputType);
		return ncFeedLogMapper.selectNcFeedAmount(feedLog);
	}
	//牛舍
	public List<Map> selectCowCountByTime(Integer spanType, String beginTime, String endTime) {
		Date s = null;
		Date e = null;
		Map se = orderCfService.calTimeSpan(s, e, beginTime, endTime, spanType);
		s = (Date) se.get("s");
		e = (Date) se.get("e");

		return getCowSumByDate(spanType, s, e);
	}
	//牛舍
	private List<Map> getCowSumByDate(Integer spanType, Date s, Date e) {
		List<Map> result = new ArrayList<>();
		if (spanType == 1) {
			while (e.getTime() >= s.getTime()) {
				String dateSpan = DateUtils.parseDateToStr("yyyy", s);

				Map m = new HashMap();
				m.put("x", dateSpan);
				m.put("zcount", getcowSumByDate(dateSpan, spanType,1));
				m.put("fcount", getcowSumByDate(dateSpan, spanType,2));
				result.add(m);

				s = DateUtils.addYears(s, 1);
			}
		} else if (spanType == 2) {
			while (e.getTime() >= s.getTime()) {
				String dateSpan = DateUtils.parseDateToStr("yyyy-MM", s);
				Map m = new HashMap();
				m.put("x", dateSpan);
				m.put("zcount", getcowSumByDate(dateSpan, spanType,1));
				m.put("fcount", getcowSumByDate(dateSpan, spanType,2));
				result.add(m);
				s = DateUtils.addMonths(s, 1);
			}
		} else if (spanType == 3) {
			while (e.getTime() >= s.getTime()) {
				String dateSpan = DateUtils.parseDateToStr("yyyy-MM-dd", s);
				Map m = new HashMap();
				m.put("x", dateSpan);
				m.put("zcount", getcowSumByDate(dateSpan, spanType,1));
				m.put("fcount", getcowSumByDate(dateSpan, spanType,2));
				result.add(m);
				s = DateUtils.addDays(s, 1);
			}
		}
		return result;
	}
	//牛舍
	private Double getcowSumByDate(String dateSpan, Integer dataSpan,Integer outputType) {
		NcFeedLog feedLog = new NcFeedLog();
		if (dataSpan == null || dataSpan == 2) {
			feedLog.getParams().put("dateSpanMonth", dateSpan);
		} else if (dataSpan == 1) {
			feedLog.getParams().put("dateSpanYear", dateSpan);
		} else if (dataSpan == 3) {
			feedLog.getParams().put("dateSpanDay", dateSpan);
		}
		feedLog.setType(outputType);
		return ncFeedLogMapper.selectNcCowFeedAmount(feedLog);
	}


}
