package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.system.domain.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.NcBullJingyeMapper;
import com.ruoyi.system.domain.NcBullJingye;
import com.ruoyi.system.service.INcBullJingyeService;
import com.ruoyi.common.core.text.Convert;

/**
 * 牛舍精液 服务层实现
 * 
 * @author ruoyi
 * @date 2019-08-30
 */
@Service
public class NcBullJingyeServiceImpl implements INcBullJingyeService 
{
	@Autowired
	private NcBullJingyeMapper ncBullJingyeMapper;

	/**
     * 查询牛舍精液信息
     * 
     * @param jingyeId 牛舍精液ID
     * @return 牛舍精液信息
     */
    @Override
	public NcBullJingye selectNcBullJingyeById(Integer jingyeId)
	{
	    return ncBullJingyeMapper.selectNcBullJingyeById(jingyeId);
	}
	
	/**
     * 查询牛舍精液列表
     * 
     * @param ncBullJingye 牛舍精液信息
     * @return 牛舍精液集合
     */
	@Override
	public List<NcBullJingye> selectNcBullJingyeList(NcBullJingye ncBullJingye)
	{
	    return ncBullJingyeMapper.selectNcBullJingyeList(ncBullJingye);
	}
	
    /**
     * 新增牛舍精液
     * 
     * @param ncBullJingye 牛舍精液信息
     * @return 结果
     */
	@Override
	public int insertNcBullJingye(NcBullJingye ncBullJingye, SysUser sysUser)
	{


		//经手人（当前用户）
		ncBullJingye.setCreateBy(sysUser.getLoginName());


		//设置库存
		ncBullJingye.setStockAmout(ncBullJingye.getInputAmount());

	    return ncBullJingyeMapper.insertNcBullJingye(ncBullJingye);
	}
	
	/**
     * 修改牛舍精液
     * 
     * @param ncBullJingye 牛舍精液信息
     * @return 结果
     */
	@Override
	public int updateNcBullJingye(NcBullJingye ncBullJingye)
	{
	    return ncBullJingyeMapper.updateNcBullJingye(ncBullJingye);
	}

	/**
     * 删除牛舍精液对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteNcBullJingyeByIds(String ids)
	{
		return ncBullJingyeMapper.deleteNcBullJingyeByIds(Convert.toStrArray(ids));
	}
	
}
