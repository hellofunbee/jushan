package com.ruoyi.system.service;

import com.ruoyi.system.domain.NcOutputMilk;
import com.ruoyi.system.domain.SysUser;
import org.springframework.ui.ModelMap;

import java.util.List;
import java.util.Map;

/**
 * 牛奶出库 服务层
 *
 * @author ruoyi
 * @date 2019-09-11
 */
public interface INcOutputMilkService
{
	/**
     * 查询牛奶出库信息
     *
     * @param outputId 牛奶出库ID
     * @return 牛奶出库信息
     */
	public NcOutputMilk selectNcOutputMilkById(Long outputId);

	/**
     * 查询牛奶出库列表
     *
     * @param ncOutputMilk 牛奶出库信息
     * @return 牛奶出库集合
     */
	public List<NcOutputMilk> selectNcOutputMilkList(NcOutputMilk ncOutputMilk);

	/**
     * 新增牛奶出库
     *
     * @param ncOutputMilk 牛奶出库信息
     * @return 结果
     */
	public int insertNcOutputMilk(NcOutputMilk ncOutputMilk);

	/**
     * 修改牛奶出库
     *
     * @param ncOutputMilk 牛奶出库信息
     * @return 结果
     */
	public int updateNcOutputMilk(NcOutputMilk ncOutputMilk);

	/**
     * 删除牛奶出库信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcOutputMilkByIds(String ids);

	//确定生成牛奶出库订单
	public int createOrderByIds(String ids, SysUser user);

	// 前端牛奶出库的分类查询
    void SelectNcOutMilkTotal(ModelMap mmap, NcOutputMilk ncOutputMilk);

    void selectNcOutputMilkOrderInfoByOrderId(String orderId, ModelMap mmap);

	List<NcOutputMilk> selectNcoutputMilkOrderInfoByOrderId(Long orderId);
	// 内部信息的接口方法
	void innoInfo(String beginTime, String endTime, ModelMap mmap, String dataType, String output);

    void innoInfoType(String beginTime, String endTime, ModelMap mmap, String type);

	List<Map> selectCountByTime(Integer dataSpan, String beginTime, String endTime , Integer cforderType);

    List<NcOutputMilk> selectNcoutputMilkOrderInfoByOutputType(Integer outputType);
	public double selectNcCowsMilkScreen(NcOutputMilk ncOutputMilk);
	List<Map> selectInfoByTime(Integer dataSpan, String beginTime, String endTime,String outputType);
}
