package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.MonthCensus;
import com.ruoyi.system.mapper.MonthCensusMapper;
import com.ruoyi.system.service.INcMonthCensusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NcMonthCensusServiceImpl implements INcMonthCensusService {
    @Autowired
    private MonthCensusMapper mapper;
    @Override
    public MonthCensus lMonthStackAmount(MonthCensus monthCensus) {
        return mapper.lMonthStackAmount(monthCensus);
    }

    @Override
    public MonthCensus lZhiAmount(MonthCensus monthCensus) {
        return mapper.lZhiAmount(monthCensus);
    }

    @Override
    public MonthCensus monthAddAmount(MonthCensus monthCensus) {
        return mapper.monthAddAmount(monthCensus);
    }

    @Override
    public MonthCensus monthAddZhiAmount(MonthCensus monthCensus) {
        return mapper.monthAddZhiAmount(monthCensus);
    }

    @Override
    public MonthCensus monthReduceZhiAmount(MonthCensus monthCensus) {
        return mapper.monthAddZhiAmount(monthCensus);
    }

    @Override
    public MonthCensus monthReduceNeiAmount(MonthCensus monthCensus) {
        return mapper.monthReduceNeiAmount(monthCensus);
    }

    @Override
    public MonthCensus monthReduceSiAmount(MonthCensus monthCensus) {
        return mapper.monthReduceSiAmount(monthCensus);
    }

    @Override
    public MonthCensus chickenAmount(MonthCensus monthCensus) {
        return mapper.chickenAmount(monthCensus);
    }

    @Override
    public MonthCensus feedAmount(MonthCensus monthCensus) {
        return mapper.feedAmount(monthCensus);
    }
}
