package com.ruoyi.system.service.impl;

import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.utils.NcUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.NcInputCf;
import com.ruoyi.system.domain.NcOrder;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.mapper.NcInputCfMapper;
import com.ruoyi.system.service.INcInputCfService;
import com.ruoyi.system.service.INcOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 菜房入库 服务层实现
 * 
 * @author ruoyi
 * @date 2019-08-30
 */
@Service
public class NcInputCfServiceImpl implements INcInputCfService 
{
	@Autowired
	private NcInputCfMapper ncInputCfMapper;

	@Autowired
	private INcOrderService orderService;

	/**
     * 查询菜房入库信息
     * 
     * @param inputId 菜房入库ID
     * @return 菜房入库信息
     */
    @Override
	public NcInputCf selectNcInputCfById(Integer inputId)
	{
	    return ncInputCfMapper.selectNcInputCfById(inputId);
	}
	
	/**
     * 查询菜房入库列表
     * 
     * @param ncInputCf 菜房入库信息
     * @return 菜房入库集合
     */
	@Override
	public List<NcInputCf> selectNcInputCfList(NcInputCf ncInputCf)
	{
	    return ncInputCfMapper.selectNcInputCfList(ncInputCf);
	}
	
    /**
     * 新增菜房入库
     * 
     * @param ncInputCf 菜房入库信息
     * @return 结果
     */
	@Override
	public int insertNcInputCf(NcInputCf ncInputCf)
	{
	    return ncInputCfMapper.insertNcInputCf(ncInputCf);
	}
	
	/**
     * 修改菜房入库
     * 
     * @param ncInputCf 菜房入库信息
     * @return 结果
     */
	@Override
	public int updateNcInputCf(NcInputCf ncInputCf)
	{
	    return ncInputCfMapper.updateNcInputCf(ncInputCf);
	}

	/**
     * 删除菜房入库对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteNcInputCfByIds(String ids)
	{
		return ncInputCfMapper.deleteNcInputCfByIds(Convert.toStrArray(ids));
	}

	@Override
	public int createInputOrderByIds(String ids, SysUser user,Integer caifangType) {
		NcOrder o = new NcOrder();

		if (StringUtils.isNotEmpty(ids)) {
			/*创建菜房出库单*/
			o.setOrderCode(NcUtils.getOrderCode());
			if(caifangType != null && caifangType == 2){
				o.setOrderType(Constants.ORDER_INPUT2);
			}
			else{
				o.setOrderType(Constants.ORDER_INPUT1);
			}
			o.setCreateTime(new Date());
			o.setCreateBy(user.getUserName());
			orderService.insertNcOrder(o);

			System.out.println("----------orderId-------:" + o.getOrderId());
			String[] idArr = Convert.toStrArray(ids);
			for (String id : idArr) {
				NcInputCf input = selectNcInputCfById(Integer.parseInt(id));
				input.setOrderId(o.getOrderId());
				input.setStatus("2");//已生成订单
				updateNcInputCf(input);
			}

			return 1;
		} else {
			return 0;
		}
	}

	@Override
	public double selectCFinPutScreen(NcInputCf inputCf) {
		return ncInputCfMapper.selectCFinPutScreen(inputCf);
	}
}
