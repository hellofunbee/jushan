package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.text.Convert;
import com.ruoyi.system.domain.NcKnowledgeClass;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.mapper.NcKnowledgeClassMapper;
import com.ruoyi.system.service.INcKnowledgeClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 知识课堂 服务层实现
 * 
 * @author ruoyi
 * @date 2019-09-10
 */
@Service
public class NcKnowledgeClassServiceImpl implements INcKnowledgeClassService
{
	@Autowired
	private NcKnowledgeClassMapper ncKnowledgeClassMapper;

	/**
     * 查询知识课堂信息
     * 
     * @param kcId 知识课堂ID
     * @return 知识课堂信息
     */
    @Override
	public NcKnowledgeClass selectNcKnowledgeClassById(Long kcId)
	{
	    return ncKnowledgeClassMapper.selectNcKnowledgeClassById(kcId);
	}
	
	/**
     * 查询知识课堂列表
     * 
     * @param ncKnowledgeClass 知识课堂信息
     * @return 知识课堂集合
     */
	@Override
	public List<NcKnowledgeClass> selectNcKnowledgeClassList(NcKnowledgeClass ncKnowledgeClass)
	{
	    return ncKnowledgeClassMapper.selectNcKnowledgeClassList(ncKnowledgeClass);
	}

	@Override
	public List<NcKnowledgeClass> listNcKnowledgeClassList(NcKnowledgeClass ncKnowledgeClass) {


		return ncKnowledgeClassMapper.listNcKnowledgeClassList(ncKnowledgeClass);
	}

	@Override
	public List<NcKnowledgeClass> selectFourNcKnowledgeClassList(Long  kctypeId) {

		return ncKnowledgeClassMapper.selectFourNcKnowledgeClassList(kctypeId);

	}

	/**
     * 新增知识课堂
     * 
     * @param ncKnowledgeClass 知识课堂信息
     * @return 结果
     */
	@Override
	public int insertNcKnowledgeClass(NcKnowledgeClass ncKnowledgeClass, SysUser sysUser)
	{
		ncKnowledgeClass.setCreateBy(sysUser.getLoginName());
		ncKnowledgeClass.setCreateTime(new Date());


	    return ncKnowledgeClassMapper.insertNcKnowledgeClass(ncKnowledgeClass);
	}
	
	/**
     * 修改知识课堂
     *
     * @param ncKnowledgeClass 知识课堂信息
     * @return 结果
     */
	@Override
	public int updateNcKnowledgeClass(NcKnowledgeClass ncKnowledgeClass, SysUser sysUser)
	{

		ncKnowledgeClass.setUpdateBy(sysUser.getLoginName());
		ncKnowledgeClass.setUpdateTime(new Date());
	    return ncKnowledgeClassMapper.updateNcKnowledgeClass(ncKnowledgeClass);
	}

	/**
     * 删除知识课堂对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteNcKnowledgeClassByIds(String ids)
	{
		return ncKnowledgeClassMapper.deleteNcKnowledgeClassByIds(Convert.toStrArray(ids));
	}
	
}
