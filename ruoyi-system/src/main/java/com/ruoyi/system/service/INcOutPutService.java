package com.ruoyi.system.service;

import com.ruoyi.system.domain.NcOutPut;
import java.util.List;

/**
 * 出库登记 服务层
 * 
 * @author ruoyi
 * @date 2019-09-16
 */
public interface INcOutPutService 
{
	/**
     * 查询出库登记信息
     * 
     * @param outPutId 出库登记ID
     * @return 出库登记信息
     */
	public NcOutPut selectNcOutPutById(Long outPutId);
	
	/**
     * 查询出库登记列表
     * 
     * @param ncOutPut 出库登记信息
     * @return 出库登记集合
     */
	public List<NcOutPut> selectNcOutPutList(NcOutPut ncOutPut);
	
	/**
     * 新增出库登记
     * 
     * @param ncOutPut 出库登记信息
     * @return 结果
     */
	public int insertNcOutPut(NcOutPut ncOutPut);
	
	/**
     * 修改出库登记
     * 
     * @param ncOutPut 出库登记信息
     * @return 结果
     */
	public int updateNcOutPut(NcOutPut ncOutPut);
		
	/**
     * 删除出库登记信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcOutPutByIds(String ids);
	
}
