package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.NcZhibaoDrugsMapper;
import com.ruoyi.system.domain.NcZhibaoDrugs;
import com.ruoyi.system.service.INcZhibaoDrugsService;
import com.ruoyi.common.core.text.Convert;

/**
 * 植保用药 服务层实现
 *
 * @author ruoyi
 * @date 2019-09-06
 */
@Service
public class NcZhibaoDrugsServiceImpl implements INcZhibaoDrugsService
{
	@Autowired
	private NcZhibaoDrugsMapper ncZhibaoDrugsMapper;

	/**
	 * 查询植保用药信息
	 *
	 * @param zhibaoDrugsId 植保用药ID
	 * @return 植保用药信息
	 */
	@Override
	public NcZhibaoDrugs selectNcZhibaoDrugsById(Long zhibaoDrugsId)
	{
		return ncZhibaoDrugsMapper.selectNcZhibaoDrugsById(zhibaoDrugsId);
	}

	/**
	 * 查询植保用药列表
	 *
	 * @param ncZhibaoDrugs 植保用药信息
	 * @return 植保用药集合
	 */
	@Override
	public List<NcZhibaoDrugs> selectNcZhibaoDrugsList(NcZhibaoDrugs ncZhibaoDrugs)
	{
		return ncZhibaoDrugsMapper.selectNcZhibaoDrugsList(ncZhibaoDrugs);
	}

	/**
	 * 新增植保用药
	 *
	 * @param ncZhibaoDrugs 植保用药信息
	 * @return 结果
	 */
	@Override
	public int insertNcZhibaoDrugs(NcZhibaoDrugs ncZhibaoDrugs)
	{
		return ncZhibaoDrugsMapper.insertNcZhibaoDrugs(ncZhibaoDrugs);
	}

	/**
	 * 修改植保用药
	 *
	 * @param ncZhibaoDrugs 植保用药信息
	 * @return 结果
	 */
	@Override
	public int updateNcZhibaoDrugs(NcZhibaoDrugs ncZhibaoDrugs)
	{
		return ncZhibaoDrugsMapper.updateNcZhibaoDrugs(ncZhibaoDrugs);
	}

	/**
	 * 删除植保用药对象
	 *
	 * @param ids 需要删除的数据ID
	 * @return 结果
	 */
	@Override
	public int deleteNcZhibaoDrugsByIds(String ids)
	{
		return ncZhibaoDrugsMapper.deleteNcZhibaoDrugsByIds(Convert.toStrArray(ids));
	}

}
