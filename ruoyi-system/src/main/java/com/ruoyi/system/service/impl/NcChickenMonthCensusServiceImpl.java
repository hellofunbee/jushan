package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.text.Convert;
import com.ruoyi.system.domain.NcChickenMonthCensus;
import com.ruoyi.system.mapper.NcChickenMonthCensusMapper;
import com.ruoyi.system.service.INcChickenMonthCensusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 鸡舍月报 服务层实现
 * 
 * @author ruoyi
 * @date 2019-09-19
 */
@Service
public class NcChickenMonthCensusServiceImpl implements INcChickenMonthCensusService 
{
	@Autowired
	private NcChickenMonthCensusMapper ncChickenMonthCensusMapper;

	/**
     * 查询鸡舍月报信息
     * 
     * @param cenusId 鸡舍月报ID
     * @return 鸡舍月报信息
     */
    @Override
	public NcChickenMonthCensus selectNcChickenMonthCensusById(Long cenusId)
	{
	    return ncChickenMonthCensusMapper.selectNcChickenMonthCensusById(cenusId);
	}
	
	/**
     * 查询鸡舍月报列表
     * 
     * @param ncChickenMonthCensus 鸡舍月报信息
     * @return 鸡舍月报集合
     */
	@Override
	public List<NcChickenMonthCensus> selectNcChickenMonthCensusList(NcChickenMonthCensus ncChickenMonthCensus)
	{
	    return ncChickenMonthCensusMapper.selectNcChickenMonthCensusList(ncChickenMonthCensus);
	}
	
    /**
     * 新增鸡舍月报
     * 
     * @param ncChickenMonthCensus 鸡舍月报信息
     * @return 结果
     */
	@Override
	public int insertNcChickenMonthCensus(NcChickenMonthCensus ncChickenMonthCensus)
	{
	    return ncChickenMonthCensusMapper.insertNcChickenMonthCensus(ncChickenMonthCensus);
	}
	
	/**
     * 修改鸡舍月报
     * 
     * @param ncChickenMonthCensus 鸡舍月报信息
     * @return 结果
     */
	@Override
	public int updateNcChickenMonthCensus(NcChickenMonthCensus ncChickenMonthCensus)
	{
	    return ncChickenMonthCensusMapper.updateNcChickenMonthCensus(ncChickenMonthCensus);
	}

	/**
     * 删除鸡舍月报对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteNcChickenMonthCensusByIds(String ids)
	{
		return ncChickenMonthCensusMapper.deleteNcChickenMonthCensusByIds(Convert.toStrArray(ids));
	}
	
}
