package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.domain.Ztree;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.NcKnowledgeClassType;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.mapper.NcKnowledgeClassTypeMapper;
import com.ruoyi.system.service.INcKnowledgeClassTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 知识课堂类别 服务层实现
 *
 * @author ruoyi
 * @date 2019-09-10
 */
@Service
public class NcKnowledgeClassTypeServiceImpl implements INcKnowledgeClassTypeService {
    @Autowired
    private NcKnowledgeClassTypeMapper ncKnowledgeClassTypeMapper;

    /**
     * 查询知识课堂类别信息
     *
     * @param kctypeId 知识课堂类别ID
     * @return 知识课堂类别信息
     */
    @Override
    public NcKnowledgeClassType selectNcKnowledgeClassTypeById(Long kctypeId) {
        return ncKnowledgeClassTypeMapper.selectNcKnowledgeClassTypeById(kctypeId);
    }

    /**
     * 查询知识课堂类别列表
     *
     * @param ncKnowledgeClassType 知识课堂类别信息
     * @return 知识课堂类别集合
     */
    @Override
    public List<NcKnowledgeClassType> selectNcKnowledgeClassTypeList(NcKnowledgeClassType ncKnowledgeClassType) {
        return ncKnowledgeClassTypeMapper.selectNcKnowledgeClassTypeList(ncKnowledgeClassType);
    }

    @Override
    public List<NcKnowledgeClassType> listSecondLevelNcKnowledgeClassTypeList() {
        return  ncKnowledgeClassTypeMapper.listSecondLevelNcKnowledgeClassTypeList();
    }

    /**
     * 新增知识课堂类别
     *
     * @param ncKnowledgeClassType 知识课堂类别信息
     * @return 结果
     */
    @Override
    public int insertNcKnowledgeClassType(NcKnowledgeClassType ncKnowledgeClassType, SysUser sysUser) {


        //创建人
        ncKnowledgeClassType.setCreateBy(sysUser.getLoginName());
        //创建时间
        ncKnowledgeClassType.setCreateTime(new Date());

        return ncKnowledgeClassTypeMapper.insertNcKnowledgeClassType(ncKnowledgeClassType);
    }

    /**
     * 修改知识课堂类别
     *
     * @param ncKnowledgeClassType 知识课堂类别信息
     * @return 结果
     */
    @Override
    public int updateNcKnowledgeClassType(NcKnowledgeClassType ncKnowledgeClassType, SysUser sysUser) {

        ncKnowledgeClassType.setUpdateBy(sysUser.getLoginName());
        ncKnowledgeClassType.setUpdateTime(new Date());
        return ncKnowledgeClassTypeMapper.updateNcKnowledgeClassType(ncKnowledgeClassType);
    }

    /**
     * 删除知识课堂类别对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteNcKnowledgeClassTypeByIds(Long ids) {
        return ncKnowledgeClassTypeMapper.deleteNcKnowledgeClassTypeById(ids);
    }

    @Override
    public List<Ztree> selectKCTypeClassTree(NcKnowledgeClassType ncKnowledgeClassType) {
        List<NcKnowledgeClassType> ncKnowledgeClassTypeList = ncKnowledgeClassTypeMapper.selectNcKnowledgeClassTypeList(ncKnowledgeClassType);
        List<Ztree> ztrees = initZtree(ncKnowledgeClassTypeList);
        return ztrees;

    }

    @Override
    public int selectKCTypeClassCount(Long kctypeId) {
        NcKnowledgeClassType ncKnowledgeClassType = new NcKnowledgeClassType();
        ncKnowledgeClassType.setParentId(kctypeId);
        return ncKnowledgeClassTypeMapper.selectKCTypeClassCount(ncKnowledgeClassType);

    }

    @Override
    public boolean checkKCClassExistKnowClass(Long kctypeId) {

        int result = ncKnowledgeClassTypeMapper.checkKCClassExistKnowClass(kctypeId);
        return result > 0;
    }

    @Override
    public List<NcKnowledgeClassType> selectNCTypeByPid(Long parentId) {
        return ncKnowledgeClassTypeMapper.selectNCTypeByPid(parentId);
    }

    /**
     * 对象转药品树
     *
     * @param ncKnowledgeClassTypeList 药品列表
     * @return 树结构列表
     */
    public List<Ztree> initZtree(List<NcKnowledgeClassType> ncKnowledgeClassTypeList) {
        return initZtree(ncKnowledgeClassTypeList, null);
    }

    /**
     * 对象转知识课堂类别
     *
     * @param ncKnowledgeClassTypeList 知识课堂列表
     * @param roleKCTypeClassList      角色已存在菜单列表
     * @return 树结构列表
     */
    public List<Ztree> initZtree(List<NcKnowledgeClassType> ncKnowledgeClassTypeList, List<String> roleKCTypeClassList) {

        List<Ztree> ztrees = new ArrayList<Ztree>();
        boolean isCheck = StringUtils.isNotNull(roleKCTypeClassList);
        for (NcKnowledgeClassType knowledgeClassTypec : ncKnowledgeClassTypeList) {

            Ztree ztree = new Ztree();
            ztree.setId(knowledgeClassTypec.getKctypeId());
            ztree.setpId(knowledgeClassTypec.getParentId());
            ztree.setName(knowledgeClassTypec.getKcTypeName());
            ztree.setTitle(knowledgeClassTypec.getKcTypeName());
            if (isCheck) {
                ztree.setChecked(ncKnowledgeClassTypeList.contains(knowledgeClassTypec.getKctypeId() + knowledgeClassTypec.getKcTypeName()));
            }
            ztrees.add(ztree);

        }
        return ztrees;
    }


}
