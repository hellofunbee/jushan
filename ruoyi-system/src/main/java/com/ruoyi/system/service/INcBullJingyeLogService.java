package com.ruoyi.system.service;

import com.ruoyi.system.domain.NcBullJingyeLog;
import com.ruoyi.system.domain.SysUser;

import java.util.List;

/**
 * 牛舍精液 服务层
 * 
 * @author ruoyi
 * @date 2019-08-30
 */
public interface INcBullJingyeLogService 
{
	/**
     * 查询牛舍精液信息
     * 
     * @param jingyeLogId 牛舍精液ID
     * @return 牛舍精液信息
     */
	public NcBullJingyeLog selectNcBullJingyeLogById(Long jingyeLogId);
	
	/**
     * 查询牛舍精液列表
     * 
     * @param ncBullJingyeLog 牛舍精液信息
     * @return 牛舍精液集合
     */
	public List<NcBullJingyeLog> selectNcBullJingyeLogList(NcBullJingyeLog ncBullJingyeLog);
	
	/**
     * 新增牛舍精液
     * 
     * @param ncBullJingyeLog 牛舍精液信息
     * @return 结果
     */
	public int insertNcBullJingyeLog(NcBullJingyeLog ncBullJingyeLog);
	
	/**
     * 修改牛舍精液
     * 
     * @param ncBullJingyeLog 牛舍精液信息
     * @return 结果
     */
	public int updateNcBullJingyeLog(NcBullJingyeLog ncBullJingyeLog);
		
	/**
     * 删除牛舍精液信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcBullJingyeLogByIds(String ids);


	/**
	 *牛舍精液出库方法
	 *
	 * @param ncBullJingyeLog 出库对象
	 * @return
	 */
    boolean checkOutBullJingye(NcBullJingyeLog ncBullJingyeLog, SysUser sysUser);
}
