package com.ruoyi.system.service;

import com.ruoyi.system.domain.NcZhibao;
import com.ruoyi.system.domain.SysUser;

import java.util.List;
import java.util.Map;

/**
 * 植保 服务层
 * 
 * @author ruoyi
 * @date 2019-08-28
 */
public interface INcZhibaoService 
{
	/**
     * 查询植保信息
     * 
     * @param zhibaoId 植保ID
     * @return 植保信息
     */
	public NcZhibao selectNcZhibaoById(Long zhibaoId);
	
	/**
     * 查询植保列表
     * 
     * @param ncZhibao 植保信息
     * @return 植保集合
     */
	public List<NcZhibao> selectNcZhibaoList(NcZhibao ncZhibao);
	
	/**
     * 新增植保
     * 
     * @param ncZhibao 植保信息
     * @return 结果
     */
	public int insertNcZhibao(NcZhibao ncZhibao);
	
	/**
     * 修改植保
     * 
     * @param ncZhibao 植保信息
     * @return 结果
     */
	public int updateNcZhibao(NcZhibao ncZhibao);
		
	/**
     * 删除植保信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcZhibaoByIds(String ids);
	/**
	 * 查询植保记录列表
	 *
	 * @param ncZhibao 植保信息
	 * @return 结果
	 */
	public List<NcZhibao> selectNcZhibaoJiluList(NcZhibao ncZhibao);
	//确定生成植保记录单
	public int createOrderByIds(String ids, SysUser user);
	//修改植保记录
	public int updateZhibaoLog(NcZhibao ncZhibao);
	//增加植保记录
	int insertZhibaoJilu(NcZhibao ncZhibao);
	public Long selectChartAmount(NcZhibao ncZhibao);
	//植保详情接口
	public List<NcZhibao> selectzhibaoDetail(NcZhibao ncZhibao);
	List<Map> selectCountByTime(Integer dataSpan, String beginTime, String endTime,Integer zhibaoType);

	
}
