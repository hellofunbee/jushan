package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.text.Convert;
import com.ruoyi.system.domain.NcCowsBirth;
import com.ruoyi.system.mapper.NcCowsBirthMapper;
import com.ruoyi.system.service.INcCowsBirthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 产犊记录 服务层实现
 *
 * @author ruoyi
 * @date 2019-09-02
 */
@Service
public class NcCowsBirthServiceImpl implements INcCowsBirthService
{
	@Autowired
	private NcCowsBirthMapper ncCowsBirthMapper;

	/**
     * 查询产犊记录信息
     *
     * @param cowId 产犊记录ID
     * @return 产犊记录信息
     */
    @Override
	public NcCowsBirth selectNcCowsBirthById(Integer cowId)
	{
	    return ncCowsBirthMapper.selectNcCowsBirthById(cowId);
	}

	@Override
	public NcCowsBirth selectNcCowsBirthByBId(Integer birthId) {
		return ncCowsBirthMapper.selectNcCowsBirthByBId(birthId);
	}

	/**
     * 查询产犊记录列表
     *
     * @param ncCowsBirth 产犊记录信息
     * @return 产犊记录集合
     */
	@Override
	public List<NcCowsBirth> selectNcCowsBirthList(NcCowsBirth ncCowsBirth)
	{
	    return ncCowsBirthMapper.selectNcCowsBirthList(ncCowsBirth);
	}

    /**
     * 新增产犊记录
     *
     * @param ncCowsBirth 产犊记录信息
     * @return 结果
     */
	@Override
	public int insertNcCowsBirth(NcCowsBirth ncCowsBirth)
	{

		ncCowsBirth.setCreateTime(new Date());
	    return ncCowsBirthMapper.insertNcCowsBirth(ncCowsBirth);
	}

	/**
     * 修改产犊记录
     *
     * @param ncCowsBirth 产犊记录信息
     * @return 结果
     */
	@Override
	public int updateNcCowsBirth(NcCowsBirth ncCowsBirth)
	{
	    return ncCowsBirthMapper.updateNcCowsBirth(ncCowsBirth);
	}

	/**
     * 删除产犊记录对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteNcCowsBirthByIds(String ids)
	{
		return ncCowsBirthMapper.deleteNcCowsBirthByIds(Convert.toStrArray(ids));
	}

	@Override
	public List<String> selectAllChildCode() {


		return ncCowsBirthMapper.selectAllChildCode();
	}

	@Override
	public NcCowsBirth selectCowByChildCode(String childCode) {
		return ncCowsBirthMapper.selectCowByChildCode(childCode);	}

	@Override
	public Integer selectCowCodeOne(String childCode){
		return ncCowsBirthMapper.selectCowCodeOne(childCode);
	}
}
