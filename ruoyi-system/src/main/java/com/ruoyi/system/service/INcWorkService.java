package com.ruoyi.system.service;

import com.ruoyi.system.domain.*;

import java.util.List;

/**
 * 标准农事 服务层
 * 
 * @author ruoyi
 * @date 2019-07-28
 */
public interface INcWorkService 
{
	/**
     * 查询标准农事信息
     * 
     * @param workId 标准农事ID
     * @return 标准农事信息
     */
	public NcWork selectNcWorkById(Long workId);
	
	/**
     * 查询标准农事列表
     * 
     * @param ncWork 标准农事信息
     * @return 标准农事集合
     */
	public List<NcWork> selectNcWorkList(NcWork ncWork);
	
	/**
     * 新增标准农事
     * 
     * @param ncWork 标准农事信息
     * @return 结果
     */
	public int insertNcWork(NcWork ncWork);
	
	/**
     * 修改标准农事
     * 
     * @param ncWork 标准农事信息
     * @return 结果
     */
	public int updateNcWork(NcWork ncWork);
		
	/**
     * 删除标准农事信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcWorkByIds(String ids);

	/*获取当天的工作*/
    List<NcWorkMirror> listTodays(NcWork ncWork, SysUser sysUser);

	List<NcStageMirror> listOnExe(NcStageMirror ncWork, SysUser sysUser);

	List<NcPlan> listpreExe(NcPlan plan, SysUser sysUser);
}
