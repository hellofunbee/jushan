package com.ruoyi.system.service;

import com.ruoyi.system.domain.NcSeedRate;
import java.util.List;

/**
 * 发芽率 服务层
 * 
 * @author ruoyi
 * @date 2019-09-16
 */
public interface INcSeedRateService 
{
	/**
     * 查询发芽率信息
     * 
     * @param seedRateId 发芽率ID
     * @return 发芽率信息
     */
	public NcSeedRate selectNcSeedRateById(Long seedRateId);
	
	/**
     * 查询发芽率列表
     * 
     * @param ncSeedRate 发芽率信息
     * @return 发芽率集合
     */
	public List<NcSeedRate> selectNcSeedRateList(NcSeedRate ncSeedRate);
	
	/**
     * 新增发芽率
     * 
     * @param ncSeedRate 发芽率信息
     * @return 结果
     */
	public int insertNcSeedRate(NcSeedRate ncSeedRate);
	
	/**
     * 修改发芽率
     * 
     * @param ncSeedRate 发芽率信息
     * @return 结果
     */
	public int updateNcSeedRate(NcSeedRate ncSeedRate);
		
	/**
     * 删除发芽率信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcSeedRateByIds(String ids);

    NcSeedRate fillValues(NcSeedRate ncSeedRate);
}
