package com.ruoyi.system.service;

import com.ruoyi.system.domain.NcXiaoduLogs;
import java.util.List;

/**
 * 消毒记录（植保） 服务层
 * 
 * @author ruoyi
 * @date 2019-08-30
 */
public interface INcXiaoduLogsService 
{
	/**
     * 查询消毒记录（植保）信息
     * 
     * @param xdId 消毒记录（植保）ID
     * @return 消毒记录（植保）信息
     */
	public NcXiaoduLogs selectNcXiaoduLogsById(Long xdId);
	
	/**
     * 查询消毒记录（植保）列表
     * 
     * @param ncXiaoduLogs 消毒记录（植保）信息
     * @return 消毒记录（植保）集合
     */
	public List<NcXiaoduLogs> selectNcXiaoduLogsList(NcXiaoduLogs ncXiaoduLogs);
	
	/**
     * 新增消毒记录（植保）
     * 
     * @param ncXiaoduLogs 消毒记录（植保）信息
     * @return 结果
     */
	public int insertNcXiaoduLogs(NcXiaoduLogs ncXiaoduLogs);
	
	/**
     * 修改消毒记录（植保）
     * 
     * @param ncXiaoduLogs 消毒记录（植保）信息
     * @return 结果
     */
	public int updateNcXiaoduLogs(NcXiaoduLogs ncXiaoduLogs);
		
	/**
     * 删除消毒记录（植保）信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcXiaoduLogsByIds(String ids);
	public NcXiaoduLogs selectNcXiaoduLogsAmount(NcXiaoduLogs ncXiaoduLogs);
	
}
