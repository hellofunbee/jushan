package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.NcNormalWorksMapper;
import com.ruoyi.system.domain.NcNormalWorks;
import com.ruoyi.system.service.INcNormalWorksService;
import com.ruoyi.common.core.text.Convert;

/**
 * 农事记录 服务层实现
 * 
 * @author ruoyi
 * @date 2019-07-31
 */
@Service
public class NcNormalWorksServiceImpl implements INcNormalWorksService 
{
	@Autowired
	private NcNormalWorksMapper ncNormalWorksMapper;

	/**
     * 查询农事记录信息
     * 
     * @param nWorkId 农事记录ID
     * @return 农事记录信息
     */
    @Override
	public NcNormalWorks selectNcNormalWorksById(Long nWorkId)
	{
	    return ncNormalWorksMapper.selectNcNormalWorksById(nWorkId);
	}
	
	/**
     * 查询农事记录列表
     * 
     * @param ncNormalWorks 农事记录信息
     * @return 农事记录集合
     */
	@Override
	public List<NcNormalWorks> selectNcNormalWorksList(NcNormalWorks ncNormalWorks)
	{
	    return ncNormalWorksMapper.selectNcNormalWorksList(ncNormalWorks);
	}
	
    /**
     * 新增农事记录
     * 
     * @param ncNormalWorks 农事记录信息
     * @return 结果
     */
	@Override
	public int insertNcNormalWorks(NcNormalWorks ncNormalWorks)
	{
	    return ncNormalWorksMapper.insertNcNormalWorks(ncNormalWorks);
	}
	
	/**
     * 修改农事记录
     * 
     * @param ncNormalWorks 农事记录信息
     * @return 结果
     */
	@Override
	public int updateNcNormalWorks(NcNormalWorks ncNormalWorks)
	{
	    return ncNormalWorksMapper.updateNcNormalWorks(ncNormalWorks);
	}

	/**
     * 删除农事记录对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteNcNormalWorksByIds(String ids)
	{
		return ncNormalWorksMapper.deleteNcNormalWorksByIds(Convert.toStrArray(ids));
	}
	
}
