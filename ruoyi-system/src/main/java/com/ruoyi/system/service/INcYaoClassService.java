package com.ruoyi.system.service;

import com.ruoyi.common.core.domain.Ztree;
import com.ruoyi.system.domain.NcYaoClass;
import com.ruoyi.system.domain.NcYaoClass;
import com.ruoyi.system.domain.SysRole;

import java.util.List;

/**
 * 药品类别 服务层
 * 
 * @author ruoyi
 * @date 2019-08-29
 */
public interface INcYaoClassService 
{
	/**
	 * 查询药品管理数据
	 *
	 * @param yaoClass 药品信息
	 * @return 药品信息集合
	 */
	public List<NcYaoClass> selectYaoClassList(NcYaoClass yaoClass);

	/**
	 * 查询药品管理树
	 *
	 * @param yaoClass 药品信息
	 * @return 所有药品信息
	 */
	public List<Ztree> selectYaoClassTree(NcYaoClass yaoClass);

	/**
	 * 根据角色ID查询菜单
	 *
	 * @param role 角色对象
	 * @return 菜单列表
	 */
	public List<Ztree> roleYaoClassTreeData(SysRole role);

	/**
	 * 查询药品人数
	 *
	 * @param parentId 父药品ID
	 * @return 结果
	 */
	public int selectYaoClassCount(Long parentId);

	/**
	 * 查询药品是否存在用户
	 *
	 * @param yaoClassId 药品ID
	 * @return 结果 true 存在 false 不存在
	 */
	public boolean checkYaoClassExistUser(Long yaoClassId);

	/**
	 * 删除药品管理信息
	 *
	 * @param yaoClassId 药品ID
	 * @return 结果
	 */
	public int deleteYaoClassById(Long yaoClassId);

	/**
	 * 新增保存药品信息
	 *
	 * @param yaoClass 药品信息
	 * @return 结果
	 */
	public int insertYaoClass(NcYaoClass yaoClass);

	/**
	 * 修改保存药品信息
	 *
	 * @param yaoClass 药品信息
	 * @return 结果
	 */
	public int updateYaoClass(NcYaoClass yaoClass);

	/**
	 * 根据药品ID查询信息
	 *
	 * @param yaoClassId 药品ID
	 * @return 药品信息
	 */
	public NcYaoClass selectYaoClassById(Long yaoClassId);

	/**
	 * 校验药品名称是否唯一
	 *
	 * @param yaoClass 药品信息
	 * @return 结果
	 */
	public String checkYaoClassNameUnique(NcYaoClass yaoClass);

}
