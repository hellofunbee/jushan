package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.NcPengInfoMapper;
import com.ruoyi.system.domain.NcPengInfo;
import com.ruoyi.system.service.INcPengInfoService;
import com.ruoyi.common.core.text.Convert;

/**
 * 计划大棚 服务层实现
 * 
 * @author ruoyi
 * @date 2019-09-08
 */
@Service
public class NcPengInfoServiceImpl implements INcPengInfoService 
{
	@Autowired
	private NcPengInfoMapper ncPengInfoMapper;

	/**
     * 查询计划大棚信息
     * 
     * @param pengInfoId 计划大棚ID
     * @return 计划大棚信息
     */
    @Override
	public NcPengInfo selectNcPengInfoById(Long pengInfoId)
	{
	    return ncPengInfoMapper.selectNcPengInfoById(pengInfoId);
	}
	
	/**
     * 查询计划大棚列表
     * 
     * @param ncPengInfo 计划大棚信息
     * @return 计划大棚集合
     */
	@Override
	public List<NcPengInfo> selectNcPengInfoList(NcPengInfo ncPengInfo)
	{
	    return ncPengInfoMapper.selectNcPengInfoList(ncPengInfo);
	}
	
    /**
     * 新增计划大棚
     * 
     * @param ncPengInfo 计划大棚信息
     * @return 结果
     */
	@Override
	public int insertNcPengInfo(NcPengInfo ncPengInfo)
	{
	    return ncPengInfoMapper.insertNcPengInfo(ncPengInfo);
	}
	
	/**
     * 修改计划大棚
     * 
     * @param ncPengInfo 计划大棚信息
     * @return 结果
     */
	@Override
	public int updateNcPengInfo(NcPengInfo ncPengInfo)
	{
	    return ncPengInfoMapper.updateNcPengInfo(ncPengInfo);
	}

	/**
     * 删除计划大棚对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteNcPengInfoByIds(String ids)
	{
		return ncPengInfoMapper.deleteNcPengInfoByIds(Convert.toStrArray(ids));
	}

	/*根据大棚Id 获取正在执行的计划数量*/
	@Override
	public Integer getPlaningCount(Long deptId) {
		return ncPengInfoMapper.getPlaningCount(deptId);
	}
}
