package com.ruoyi.system.service;

import com.ruoyi.system.domain.NcWorkLog;
import java.util.List;

/**
 * 农事记录 服务层
 * 
 * @author ruoyi
 * @date 2019-09-01
 */
public interface INcWorkLogService 
{
	/**
     * 查询农事记录信息
     * 
     * @param zaipeiId 农事记录ID
     * @return 农事记录信息
     */
	public NcWorkLog selectNcWorkLogById(Long zaipeiId);
	
	/**
     * 查询农事记录
     * 
     * @param ncWorkLog 农事记录信息
     * @return 农事记录集合
     */
	public List<NcWorkLog> selectNcWorkLogList(NcWorkLog ncWorkLog);
	
	/**
     * 新增农事记录
     * 
     * @param ncWorkLog 农事记录信息
     * @return 结果
     */
	public int insertNcWorkLog(NcWorkLog ncWorkLog);
	
	/**
     * 修改农事记录
     * 
     * @param ncWorkLog 农事记录信息
     * @return 结果
     */
	public int updateNcWorkLog(NcWorkLog ncWorkLog);
		
	/**
     * 删除农事记录信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcWorkLogByIds(String ids);
	
}
