package com.ruoyi.system.service;

import com.ruoyi.system.domain.NcCai;
import com.ruoyi.system.domain.NcOrderCf;
import com.ruoyi.system.domain.NcOuputCf;
import com.ruoyi.system.domain.SysUser;
import org.springframework.ui.ModelMap;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * 菜房出库 服务层
 * 
 * @author ruoyi
 * @date 2019-08-30
 */
public interface INcOuputCfService 
{


	/**
     * 查询菜房出库信息
     * 
     * @param outputId 菜房出库ID
     * @return 菜房出库信息
     */
	public NcOuputCf selectNcOuputCfById(Integer outputId);
	
	/**
     * 查询菜房出库列表
     * 
     * @param ncOuputCf 菜房出库信息
     * @return 菜房出库集合
     */
	public List<NcOuputCf> selectNcOuputCfList(NcOuputCf ncOuputCf);
	
	/**
     * 新增菜房出库
     * 
     * @param ncOuputCf 菜房出库信息
     * @return 结果
     */
	public int insertNcOuputCf(NcOuputCf ncOuputCf);
	
	/**
     * 修改菜房出库
     * 
     * @param ncOuputCf 菜房出库信息
     * @return 结果
     */
	public int updateNcOuputCf(NcOuputCf ncOuputCf);
		
	/**
     * 删除菜房出库信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcOuputCfByIds(String ids);

    int createOutputOrderByIds(String ids, SysUser user,Integer caifangType);

    List<Map> selectSumByStatus(NcOrderCf cf);

	Double selectSum(NcOrderCf order);

	Map getOutputCount(NcOrderCf order);

	List<NcCai> getBigCaiClass(NcOrderCf order);

	void setOutputTypes(ModelMap mmap, NcOrderCf order);

	void innoInfo(String beginTime, String endTime, ModelMap mmap, String dataType, String output);

	/**
	 * 根据计划id查询菜房订单id查询菜房出库数量
	 * @param planId 计划id
	 * @return
	 */
	BigDecimal queryOutAmoutByPlanId(Long planId );

    void innoInfoType(String beginTime, String endTime, ModelMap mmap, String type);

	List<Map> selectCountByTime(Integer outputType, Integer dataSpan, String beginTime, String endTime, int cf);
	public double selectCFoutPutScreen(NcOuputCf ncOuputCf);
	public List<NcOuputCf> avgCFoutPutScreen(NcOuputCf ncOuputCf);
	List<Map> selectInfoByTime(Integer dataSpan, String beginTime, String endTime,String outputType);
}
