package com.ruoyi.system.service;

import com.ruoyi.system.domain.DayCensus;
import org.springframework.stereotype.Service;

import java.util.List;
public interface DayCensusService {
    //存栏数量
    public DayCensus cunAmountList(DayCensus dayCensus);
    //入购数量
    public DayCensus inputAmountList(DayCensus dayCensus);
    //内供大只鸡数量
    public DayCensus outputAmountBigList(DayCensus dayCensus);
    //内供小只鸡数量
    public DayCensus outputAmountLittleList(DayCensus dayCensus);
    //内销鸡数量
    public DayCensus neiXiaoList(DayCensus dayCensus);
    //主饲料数量
    public DayCensus zFeedList(DayCensus dayCensus);
    //辅饲料数量
    public DayCensus fFeedList(DayCensus dayCensus);
    //死亡数量
    public DayCensus dieList(DayCensus dayCensus);
    //死亡大只鸡数量
    public DayCensus dieBig(DayCensus ncChickenDayCensus);
    //死亡小只鸡数量
    public DayCensus dieSmall(DayCensus ncChickenDayCensus);
}
