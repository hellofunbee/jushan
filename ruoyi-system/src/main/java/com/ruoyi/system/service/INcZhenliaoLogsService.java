package com.ruoyi.system.service;

import com.ruoyi.system.domain.NcZhenliaoLogs;
import java.util.List;

/**
 * 诊疗单 服务层
 * 
 * @author ruoyi
 * @date 2019-09-11
 */
public interface INcZhenliaoLogsService 
{
	/**
     * 查询诊疗单信息
     * 
     * @param zlId 诊疗单ID
     * @return 诊疗单信息
     */
	public NcZhenliaoLogs selectNcZhenliaoLogsById(Long zlId);
	
	/**
     * 查询诊疗单列表
     * 
     * @param ncZhenliaoLogs 诊疗单信息
     * @return 诊疗单集合
     */
	public List<NcZhenliaoLogs> selectNcZhenliaoLogsList(NcZhenliaoLogs ncZhenliaoLogs);
	
	/**
     * 新增诊疗单
     * 
     * @param ncZhenliaoLogs 诊疗单信息
     * @return 结果
     */
	public int insertNcZhenliaoLogs(NcZhenliaoLogs ncZhenliaoLogs);
	
	/**
     * 修改诊疗单
     * 
     * @param ncZhenliaoLogs 诊疗单信息
     * @return 结果
     */
	public int updateNcZhenliaoLogs(NcZhenliaoLogs ncZhenliaoLogs);
		
	/**
     * 删除诊疗单信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcZhenliaoLogsByIds(String ids);


	//鸡舍中兽医请求往诊疗单发送消息
	public int updateZhenliao(String ids);

	
}
