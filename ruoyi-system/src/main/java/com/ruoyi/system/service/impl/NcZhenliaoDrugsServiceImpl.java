package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.NcZhenliaoDrugsMapper;
import com.ruoyi.system.domain.NcZhenliaoDrugs;
import com.ruoyi.system.service.INcZhenliaoDrugsService;
import com.ruoyi.common.core.text.Convert;

/**
 * 兽医诊疗用药 服务层实现
 * 
 * @author ruoyi
 * @date 2019-09-05
 */
@Service
public class NcZhenliaoDrugsServiceImpl implements INcZhenliaoDrugsService{
	@Autowired
	private NcZhenliaoDrugsMapper ncZhenliaoDrugsMapper;

	/**
     * 查询兽医诊疗用药信息
     * 
     * @param zlId 兽医诊疗用药ID
     * @return 兽医诊疗用药信息
     */
    @Override
	public NcZhenliaoDrugs selectNcZhenliaoDrugsById(Long zlId)
	{
	    return ncZhenliaoDrugsMapper.selectNcZhenliaoDrugsById(zlId);
	}
	
	/**
     * 查询兽医诊疗用药列表
     * 
     * @param ncZhenliaoDrugs 兽医诊疗用药信息
     * @return 兽医诊疗用药集合
     */
	@Override
	public List<NcZhenliaoDrugs> selectNcZhenliaoDrugsList(NcZhenliaoDrugs ncZhenliaoDrugs)
	{
	    return ncZhenliaoDrugsMapper.selectNcZhenliaoDrugsList(ncZhenliaoDrugs);
	}
	
    /**
     * 新增兽医诊疗用药
     * 
     * @param ncZhenliaoDrugs 兽医诊疗用药信息
     * @return 结果
     */
	@Override
	public int insertNcZhenliaoDrugs(NcZhenliaoDrugs ncZhenliaoDrugs)
	{
	    return ncZhenliaoDrugsMapper.insertNcZhenliaoDrugs(ncZhenliaoDrugs);
	}
	
	/**
     * 修改兽医诊疗用药
     * 
     * @param ncZhenliaoDrugs 兽医诊疗用药信息
     * @return 结果
     */
	@Override
	public int updateNcZhenliaoDrugs(NcZhenliaoDrugs ncZhenliaoDrugs)
	{
	    return ncZhenliaoDrugsMapper.updateNcZhenliaoDrugs(ncZhenliaoDrugs);
	}

	/**
     * 删除兽医诊疗用药对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteNcZhenliaoDrugsByIds(String ids)
	{
		return ncZhenliaoDrugsMapper.deleteNcZhenliaoDrugsByIds(Convert.toStrArray(ids));
	}

}
