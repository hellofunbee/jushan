package com.ruoyi.system.service;

import com.ruoyi.system.domain.NcXunshiLogs;
import com.ruoyi.system.domain.SysUser;

import java.util.List;

/**
 * 巡视记录 服务层
 * 
 * @author ruoyi
 * @date 2019-09-02
 */
public interface INcChickenXunshiLogsService 
{
	/**
     * 查询巡视记录信息
     * 
     * @param xsId 巡视记录ID
     * @return 巡视记录信息
     */
	public NcXunshiLogs selectNcXunshiLogsById(Long xsId);
	
	/**
     * 查询巡视记录列表
     * 
     * @param ncXunshiLogs 巡视记录信息
     * @return 巡视记录集合
     */
	public List<NcXunshiLogs> selectNcXunshiLogsList(NcXunshiLogs ncXunshiLogs);
	
	/**
     * 新增巡视记录
     * 
     * @param ncXunshiLogs 巡视记录信息
     * @return 结果
     */
	public int insertNcXunshiLogs(NcXunshiLogs ncXunshiLogs);
	
	/**
     * 修改巡视记录
     * 
     * @param ncXunshiLogs 巡视记录信息
     * @return 结果
     */
	public int updateNcXunshiLogs(NcXunshiLogs ncXunshiLogs);
		
	/**
     * 删除巡视记录信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcXunshiLogsByIds(String ids);

	/**
	 * 创建巡视单
	 * @param ids
	 * @param user
	 * @return
	 */
    int createZlOrderByIds(String ids, SysUser user);
}
