package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.system.domain.NcDrugs;
import com.ruoyi.system.domain.NcYedan;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.mapper.NcYedanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.NcYedanLogMapper;
import com.ruoyi.system.domain.NcYedanLog;
import com.ruoyi.system.service.INcYedanLogService;
import com.ruoyi.common.core.text.Convert;
import org.springframework.transaction.annotation.Transactional;

/**
 * 液氮出库 服务层实现
 *
 * @author ruoyi
 * @date 2019-09-02
 */
@Service
public class NcYedanLogServiceImpl implements INcYedanLogService
{
	@Autowired
	private NcYedanLogMapper ncYedanLogMapper;


	@Autowired

	private NcYedanMapper ncYedanMapper;

	/**
     * 查询液氮出库信息
     *
     * @param yedanLogId 液氮出库ID
     * @return 液氮出库信息
     */
    @Override
	public NcYedanLog selectNcYedanLogById(Long yedanLogId)
	{
	    return ncYedanLogMapper.selectNcYedanLogById(yedanLogId);
	}

	/**
     * 查询液氮出库列表
     *
     * @param ncYedanLog 液氮出库信息
     * @return 液氮出库集合
     */
	@Override
	public List<NcYedanLog> selectNcYedanLogList(NcYedanLog ncYedanLog)
	{

        List<NcYedanLog> ncYedanLogs = ncYedanLogMapper.selectNcYedanLogList(ncYedanLog);

        for (NcYedanLog yedanLog : ncYedanLogs) {
            NcYedan ncYedan = ncYedanMapper.selectNcYedanById(yedanLog.getYedanId());

            if (ncYedan != null ) {
            //液氮出库液氮来源
            yedanLog.setComeFrom(ncYedan.getComeFrom());
            }

        }
        return ncYedanLogs;
	}

    /**
     * 新增液氮出库
     *
     * @param ncYedanLog 液氮出库信息
     * @return 结果
     */
	@Override
	public int insertNcYedanLog(NcYedanLog ncYedanLog)
	{


	    return ncYedanLogMapper.insertNcYedanLog(ncYedanLog);
	}

	/**
     * 修改液氮出库
     *
     * @param ncYedanLog 液氮出库信息
     * @return 结果
     */
	@Override
	public int updateNcYedanLog(NcYedanLog ncYedanLog)
	{
	    return ncYedanLogMapper.updateNcYedanLog(ncYedanLog);
	}

	/**
     * 删除液氮出库对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteNcYedanLogByIds(String ids)
	{
		return ncYedanLogMapper.deleteNcYedanLogByIds(Convert.toStrArray(ids));
	}

	@Override
	public boolean checkOutYeDan(NcYedanLog ncYedanLog,SysUser sysUser) {

		//经手人
		ncYedanLog.setCreateBy(sysUser.getLoginName());
		NcYedan ncYedan = ncYedanMapper.selectNcYedanById(ncYedanLog.getYedanId());

		//现有库存数量
		Integer stockAmoutNow = ncYedan.getStockAmout();
		//出库数量
		Integer outputAmount = ncYedanLog.getOutputAmount();


		if (outputAmount > 0 && stockAmoutNow >= outputAmount) {
			//出库
			//减少库存
			ncYedan.setStockAmout(ncYedan.getStockAmout() - outputAmount);

			updateYeDanAndInsertLog(ncYedanLog, ncYedan);

			return true;
		}

		//数量不足 出库失败
		return false;

	}


	@Transactional
	void updateYeDanAndInsertLog(NcYedanLog ncYedanLog, NcYedan ncYedan) {
		ncYedanMapper.updateNcYedan(ncYedan);
		//添加出库记录
		ncYedanLogMapper.insertNcYedanLog(ncYedanLog);
	}

}
