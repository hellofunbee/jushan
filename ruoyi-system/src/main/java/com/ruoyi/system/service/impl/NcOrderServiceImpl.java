package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.NcOrderMapper;
import com.ruoyi.system.domain.NcOrder;
import com.ruoyi.system.service.INcOrderService;
import com.ruoyi.common.core.text.Convert;

/**
 * 订单主（系统中的所有订单共用此） 服务层实现
 * 
 * @author ruoyi
 * @date 2019-07-31
 */
@Service
public class NcOrderServiceImpl implements INcOrderService 
{
	@Autowired
	private NcOrderMapper ncOrderMapper;

	/**
     * 查询订单主（系统中的所有订单共用此）信息
     * 
     * @param orderId 订单主（系统中的所有订单共用此）ID
     * @return 订单主（系统中的所有订单共用此）信息
     */
    @Override
	public NcOrder selectNcOrderById(Long orderId)
	{
	    return ncOrderMapper.selectNcOrderById(orderId);
	}
	
	/**
     * 查询订单主（系统中的所有订单共用此）列表
     * 
     * @param ncOrder 订单主（系统中的所有订单共用此）信息
     * @return 订单主（系统中的所有订单共用此）集合
     */
	@Override
	public List<NcOrder> selectNcOrderList(NcOrder ncOrder)
	{
	    return ncOrderMapper.selectNcOrderList(ncOrder);
	}
	
    /**
     * 新增订单主（系统中的所有订单共用此）
     * 
     * @param ncOrder 订单主（系统中的所有订单共用此）信息
     * @return 结果
     */
	@Override
	public int insertNcOrder(NcOrder ncOrder)
	{
	    return ncOrderMapper.insertNcOrder(ncOrder);
	}
	
	/**
     * 修改订单主（系统中的所有订单共用此）
     * 
     * @param ncOrder 订单主（系统中的所有订单共用此）信息
     * @return 结果
     */
	@Override
	public int updateNcOrder(NcOrder ncOrder)
	{
	    return ncOrderMapper.updateNcOrder(ncOrder);
	}

	/**
     * 删除订单主（系统中的所有订单共用此）对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteNcOrderByIds(String ids)
	{
		return ncOrderMapper.deleteNcOrderByIds(Convert.toStrArray(ids));
	}
	
}
