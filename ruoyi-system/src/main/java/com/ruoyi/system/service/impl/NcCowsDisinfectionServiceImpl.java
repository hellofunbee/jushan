package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.text.Convert;
import com.ruoyi.system.domain.NcCowsDisinfection;
import com.ruoyi.system.mapper.NcCowsDisinfectionMapper;
import com.ruoyi.system.service.INcCowsDisinfectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 消毒记录（牛舍） 服务层实现
 *
 * @author ruoyi
 * @date 2019-09-04
 */
@Service
public class NcCowsDisinfectionServiceImpl implements INcCowsDisinfectionService
{
	@Autowired
	private NcCowsDisinfectionMapper ncCowsDisinfectionMapper;

	/**
     * 查询消毒记录（牛舍）信息
     *
     * @param dfId 消毒记录（牛舍）ID
     * @return 消毒记录（牛舍）信息
     */
    @Override
	public NcCowsDisinfection selectNcCowsDisinfectionById(Long dfId)
	{
	    return ncCowsDisinfectionMapper.selectNcCowsDisinfectionById(dfId);
	}

	/**
     * 查询消毒记录（牛舍）列表
     *
     * @param ncCowsDisinfection 消毒记录（牛舍）信息
     * @return 消毒记录（牛舍）集合
     */
	@Override
	public List<NcCowsDisinfection> selectNcCowsDisinfectionList(NcCowsDisinfection ncCowsDisinfection)
	{
	    return ncCowsDisinfectionMapper.selectNcCowsDisinfectionList(ncCowsDisinfection);
	}

    /**
     * 新增消毒记录（牛舍）
     *
     * @param ncCowsDisinfection 消毒记录（牛舍）信息
     * @return 结果
     */
	@Override
	public int insertNcCowsDisinfection(NcCowsDisinfection ncCowsDisinfection)
	{
	    return ncCowsDisinfectionMapper.insertNcCowsDisinfection(ncCowsDisinfection);
	}

	/**
     * 修改消毒记录（牛舍）
     *
     * @param ncCowsDisinfection 消毒记录（牛舍）信息
     * @return 结果
     */
	@Override
	public int updateNcCowsDisinfection(NcCowsDisinfection ncCowsDisinfection)
	{
	    return ncCowsDisinfectionMapper.updateNcCowsDisinfection(ncCowsDisinfection);
	}

	/**
     * 删除消毒记录（牛舍）对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteNcCowsDisinfectionByIds(String ids)
	{
		return ncCowsDisinfectionMapper.deleteNcCowsDisinfectionByIds(Convert.toStrArray(ids));
	}

}
