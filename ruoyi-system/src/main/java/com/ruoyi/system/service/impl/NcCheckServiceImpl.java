package com.ruoyi.system.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.utils.NcUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.service.INcCaiService;
import com.ruoyi.system.service.INcOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.NcCheckMapper;
import com.ruoyi.system.service.INcCheckService;
import com.ruoyi.common.core.text.Convert;

/**
 * 检测 服务层实现
 *
 * @author ruoyi
 * @date 2019-08-29
 */
@Service
public class NcCheckServiceImpl implements INcCheckService {
    @Autowired
    private NcCheckMapper ncCheckMapper;

    @Autowired
    private INcOrderService orderService;

    @Autowired
    private INcCaiService ncCaiService;

    /**
     * 查询检测信息
     *
     * @param checkId 检测ID
     * @return 检测信息
     */
    @Override
    public NcCheck selectNcCheckById(Long checkId) {
        return ncCheckMapper.selectNcCheckById(checkId);
    }

    /**
     * 查询检测列表
     *
     * @param ncCheck 检测信息
     * @return 检测集合
     */
    @Override
    public List<NcCheck> selectNcCheckList(NcCheck ncCheck) {
        return ncCheckMapper.selectNcCheckList(ncCheck);
    }

    /**
     * 新增检测
     *
     * @param ncCheck 检测信息
     * @return 结果
     */
    @Override
    public int insertNcCheck(NcCheck ncCheck) {
        return ncCheckMapper.insertNcCheck(ncCheck);
    }

    /**
     * 修改检测
     *
     * @param ncCheck 检测信息
     * @return 结果
     */
    @Override
    public int updateNcCheck(NcCheck ncCheck) {
        return ncCheckMapper.updateNcCheck(ncCheck);
    }

    /**
     * 删除检测对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteNcCheckByIds(String ids) {
        return ncCheckMapper.deleteNcCheckByIds(Convert.toStrArray(ids));
    }

    @Override
    public int createOrderByIds(String ids, SysUser user) {
        NcOrder o = new NcOrder();

        if (StringUtils.isNotEmpty(ids)) {
            /*创建育苗订单*/
            o.setOrderCode(NcUtils.getOrderCode());
            o.setOrderType(Constants.ORDER_CHECK);
            o.setOrderTime(new Date());
            o.setCreateTime(new Date());
            o.setCreateBy(user.getUserName());
            orderService.insertNcOrder(o);

            System.out.println("----------orderId-------:" + o.getOrderId());
            String[] idArr = Convert.toStrArray(ids);
            for (String id : idArr) {
                NcCheck plan = selectNcCheckById(Long.parseLong(id));
                plan.setOrderId(o.getOrderId());
                plan.setStatus("2");//已生成检测单
                updateNcCheck(plan);
            }

            return 1;
        } else {
            return 0;
        }

    }

    @Override
    public List<NcCai> getBigCaiClass(NcCheck check) {
        //1、计算各大蔬菜类型的检测数量
        NcCai cai = new NcCai();
        cai.setAncestors("0");
        cai.setParentId(new Long(0));
        List<NcCai> result = ncCaiService.selectBigClass(cai);
        //过滤
        for (NcCai c : result) {
            //遍历取检测数量数量
            check.getParams().put("ancestors", c.getCaiId());
            Map m = new HashMap();
            m.put("count", selectSumByStatus(check));
            c.setPlanCounts(m);
        }
        //计算占比
        Integer total = 0;
        for (NcCai c : result) {
            Map counts = c.getPlanCounts();
            total += NcUtils.getCount(counts, "count");
        }

        for (NcCai c : result) {
            Map counts = c.getPlanCounts();
            counts.put("rate", NcUtils.getRate(total, counts, "count"));
        }
        //统计数量
        return result;
    }


    public int selectSumByStatus(NcCheck check) {
        return ncCheckMapper.selectSum(check);
    }
}
