package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.NcGasLog;
import com.ruoyi.system.mapper.NcGasLogMapper;
import com.ruoyi.system.service.INcGasLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * 气值 服务层实现
 *
 * @author ruoyi
 * @date 2019-07-31
 */
@Service
public class NcGasLogServiceImpl implements INcGasLogService
{
	@Autowired
	private NcGasLogMapper ncGasLogMapper;

	/**
     * 查询气值信息
     *
     * @param gasLogId 气值ID
     * @return 气值信息
     */
    @Override
	public NcGasLog selectNcGasLogById(Long gasLogId)
	{
	    return ncGasLogMapper.selectNcGasLogById(gasLogId);
	}

	/**
     * 查询气值列表
     *
     * @param ncGasLog 气值信息
     * @return 气值集合
     */
	@Override
	public List<NcGasLog> selectNcGasLogList(NcGasLog ncGasLog)
	{
	    return ncGasLogMapper.selectNcGasLogList(ncGasLog);
	}

    /**
     * 新增气值
     *
     * @param ncGasLog 气值信息
     * @return 结果
     */
	@Override
	public int insertNcGasLog(NcGasLog ncGasLog)
	{
	    return ncGasLogMapper.insertNcGasLog(ncGasLog);
	}

	/**
     * 修改气值
     *
     * @param ncGasLog 气值信息
     * @return 结果
     */
	@Override
	public int updateNcGasLog(NcGasLog ncGasLog)
	{
	    return ncGasLogMapper.updateNcGasLog(ncGasLog);
	}

	/**
     * 删除气值对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteNcGasLogByIds(String ids)
	{
		return ncGasLogMapper.deleteNcGasLogByIds(Convert.toStrArray(ids));
	}

	@Override
	public void showMag(String beginTime, String endTime, ModelMap mmap) {
		List<Double> yDate = new ArrayList<>();
		//曲线图数据
		String[] last12Months = new String[12];
		Calendar cal = Calendar.getInstance();
		List zfeed=new ArrayList();
		List ffeed=new ArrayList();

		if (StringUtils.isNotEmpty(endTime)) {
			cal.setTime(DateUtils.dateTime("yyyy-MM-dd",endTime));
			//如果当前日期大于二月份的天数28天或者29天会导致计算月份错误，会多出一个三月份，故设置一个靠前日期解决此问题
			cal.set(Calendar.DAY_OF_MONTH, 1);
			for (int i = 0; i < 12; i++) {
				if(cal.get(Calendar.MONTH)+1<10){
					last12Months[11 - i] = cal.get(Calendar.YEAR) + "-0" + (cal.get(Calendar.MONTH) + 1);
				}else{
					last12Months[11 - i] = cal.get(Calendar.YEAR) + "-" + (cal.get(Calendar.MONTH) + 1);
				}
				cal.set(Calendar.MONTH, cal.get(Calendar.MONTH) - 1); //逐次往前推1个月

			}
		}
		else if(StringUtils.isNotEmpty(beginTime)){
			cal.setTime(DateUtils.dateTime("yyyy-MM-dd",beginTime));
			//如果当前日期大于二月份的天数28天或者29天会导致计算月份错误，会多出一个三月份，故设置一个靠前日期解决此问题
			cal.set(Calendar.DAY_OF_MONTH, 1);
			for (int i = 0; i < 12; i++) {
				if(cal.get(Calendar.MONTH)+1<10){
					last12Months[11 - i] = cal.get(Calendar.YEAR) + "-0" + (cal.get(Calendar.MONTH) + 1);
				}else{
					last12Months[11 - i] = cal.get(Calendar.YEAR) + "-" + (cal.get(Calendar.MONTH) + 1);
				}
				cal.set(Calendar.MONTH, cal.get(Calendar.MONTH) - 1); //逐次往前推1个月

			}
		}
		else{
			//如果当前日期大于二月份的天数28天或者29天会导致计算月份错误，会多出一个三月份，故设置一个靠前日期解决此问题
			cal.set(Calendar.DAY_OF_MONTH, 1);
			for (int i = 0; i < 12; i++) {
				if(cal.get(Calendar.MONTH)+1<10){
					last12Months[11 - i] = cal.get(Calendar.YEAR) + "-0" + (cal.get(Calendar.MONTH) + 1);
				}else{
					last12Months[11 - i] = cal.get(Calendar.YEAR) + "-" + (cal.get(Calendar.MONTH) + 1);
				}
				cal.set(Calendar.MONTH, cal.get(Calendar.MONTH) - 1); //逐次往前推1个月

			}
		}
		for(int i = 0 ; i < last12Months.length; i++){
			Double total = ncGasLogMapper.selectGasValByMonth(last12Months[i]);
			yDate.add(total == null ? 0 : total);
		}
		mmap.put("xData",last12Months);
		mmap.put("yData",yDate);
	}

	@Override
	public Double selectGasAmount(NcGasLog ncGasLog) {
		return ncGasLogMapper.selectGasAmount(ncGasLog);
	}

	@Override
	public Double selectGasMonthAmount(NcGasLog ncGasLog) {
		return ncGasLogMapper.selectGasMonthAmount(ncGasLog);
	}

	@Override
	public Double selectGasMonth(NcGasLog ncGasLog){
		return ncGasLogMapper.selectGasMonth(ncGasLog);
	}
	@Override
	public Double selectGasMonthMin(NcGasLog ncGasLog){
		return ncGasLogMapper.selectGasMonthMin(ncGasLog);
	}
	@Override
	public List<NcGasLog> selectGasMonthList(NcGasLog ncGasLog){
		return ncGasLogMapper.selectGasMonthList(ncGasLog);
	}

	@Override
	public Double seleGasTop(NcGasLog ncGasLog){
		return ncGasLogMapper.seleGasTop(ncGasLog);
	}

	@Override
	public Double seleGasLow(NcGasLog ncGasLog){
		return ncGasLogMapper.seleGasLow(ncGasLog);
	}

	@Override
	public String seleTimeLow(NcGasLog ncGasLog){
		return ncGasLogMapper.seleTimeLow(ncGasLog);
	}

	@Override
	public double seleGasLowScreen(NcGasLog ncGasLog) {
		return ncGasLogMapper.seleGasLowScreen(ncGasLog);
	}
}

