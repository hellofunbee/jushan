package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.text.Convert;
import com.ruoyi.system.domain.NcChickenDayCensus;
import com.ruoyi.system.mapper.NcChickenDayCensusMapper;
import com.ruoyi.system.service.INcChickenDayCensusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 鸡日报 服务层实现
 * 
 * @author ruoyi
 * @date 2019-09-19
 */
@Service
public class NcChickenDayCensusServiceImpl implements INcChickenDayCensusService 
{
	@Autowired
	private NcChickenDayCensusMapper ncChickenDayCensusMapper;

	/**
     * 查询鸡日报信息
     * 
     * @param censusId 鸡日报ID
     * @return 鸡日报信息
     */
    @Override
	public NcChickenDayCensus selectNcChickenDayCensusById(Long censusId)
	{
	    return ncChickenDayCensusMapper.selectNcChickenDayCensusById(censusId);
	}
	
	/**
     * 查询鸡日报列表
     * 
     * @param ncChickenDayCensus 鸡日报信息
     * @return 鸡日报集合
     */
	@Override
	public List<NcChickenDayCensus> selectNcChickenDayCensusList(NcChickenDayCensus ncChickenDayCensus)
	{
	    return ncChickenDayCensusMapper.selectNcChickenDayCensusList(ncChickenDayCensus);
	}
	
    /**
     * 新增鸡日报
     * 
     * @param ncChickenDayCensus 鸡日报信息
     * @return 结果
     */
	@Override
	public int insertNcChickenDayCensus(NcChickenDayCensus ncChickenDayCensus)
	{
	    return ncChickenDayCensusMapper.insertNcChickenDayCensus(ncChickenDayCensus);
	}
	
	/**
     * 修改鸡日报
     * 
     * @param ncChickenDayCensus 鸡日报信息
     * @return 结果
     */
	@Override
	public int updateNcChickenDayCensus(NcChickenDayCensus ncChickenDayCensus)
	{
	    return ncChickenDayCensusMapper.updateNcChickenDayCensus(ncChickenDayCensus);
	}

	/**
     * 删除鸡日报对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteNcChickenDayCensusByIds(String ids)
	{
		return ncChickenDayCensusMapper.deleteNcChickenDayCensusByIds(Convert.toStrArray(ids));
	}
	
}
