package com.ruoyi.system.service;

import com.ruoyi.system.domain.NcOutputChicken;
import com.ruoyi.system.domain.SysUser;
import org.springframework.ui.ModelMap;

import java.util.List;
import java.util.Map;

/**
 * 鸡出库 服务层
 * 
 * @author ruoyi
 * @date 2019-09-09
 */
public interface INcOutputChickenService 
{
	/**
     * 查询鸡出库信息
     * 
     * @param outputId 鸡出库ID
     * @return 鸡出库信息
     */
	public NcOutputChicken selectNcOutputChickenById(Integer outputId);
	
	/**
     * 查询鸡出库列表
     * 
     * @param ncOutputChicken 鸡出库信息
     * @return 鸡出库集合
     */
	public List<NcOutputChicken> selectNcOutputChickenList(NcOutputChicken ncOutputChicken);
	
	/**
     * 新增鸡出库
     * 
     * @param ncOutputChicken 鸡出库信息
     * @return 结果
     */
	public int insertNcOutputChicken(NcOutputChicken ncOutputChicken);
	
	/**
     * 修改鸡出库
     * 
     * @param ncOutputChicken 鸡出库信息
     * @return 结果
     */
	public int updateNcOutputChicken(NcOutputChicken ncOutputChicken);
		
	/**
     * 删除鸡出库信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcOutputChickenByIds(String ids);
	//确定生成鸡出库订单
	public int createOrderByIds(String ids, SysUser user);
	//查询出库数量
	public double selectNcOutputAmount(NcOutputChicken ncOutputChicken);

	public Double selectNcOutputMonthAmount(NcOutputChicken ncOutputChicken);

    void innoInfo(String beginTime, String endTime, ModelMap mmap, String dataType, String output);

    void innoInfoType(String beginTime, String endTime, ModelMap mmap, String type);
	List<Map> selectCountByTime(Integer dataSpan, String beginTime, String endTime,Integer outputType);
	void setChickenTypes(ModelMap mmap, NcOutputChicken outputChicken,Integer outputType, int type);
	List<Map> selectInfoByTime(Integer dataSpan, String beginTime, String endTime,String outputType);
}
