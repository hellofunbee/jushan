package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.text.Convert;
import com.ruoyi.system.domain.NcCai;
import com.ruoyi.system.domain.NcSeedRate;
import com.ruoyi.system.mapper.NcSeedRateMapper;
import com.ruoyi.system.service.INcCaiService;
import com.ruoyi.system.service.INcSeedRateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 发芽率 服务层实现
 *
 * @author ruoyi
 * @date 2019-09-16
 */
@Service
public class NcSeedRateServiceImpl implements INcSeedRateService {
    @Autowired
    private NcSeedRateMapper ncSeedRateMapper;
    @Autowired
    private INcCaiService ncCaiService;

    /**
     * 查询发芽率信息
     *
     * @param seedRateId 发芽率ID
     * @return 发芽率信息
     */
    @Override
    public NcSeedRate selectNcSeedRateById(Long seedRateId) {
        return ncSeedRateMapper.selectNcSeedRateById(seedRateId);
    }

    /**
     * 查询发芽率列表
     *
     * @param ncSeedRate 发芽率信息
     * @return 发芽率集合
     */
    @Override
    public List<NcSeedRate> selectNcSeedRateList(NcSeedRate ncSeedRate) {
        return ncSeedRateMapper.selectNcSeedRateList(ncSeedRate);
    }

    /**
     * 新增发芽率
     *
     * @param ncSeedRate 发芽率信息
     * @return 结果
     */
    @Override
    public int insertNcSeedRate(NcSeedRate ncSeedRate) {
        return ncSeedRateMapper.insertNcSeedRate(ncSeedRate);
    }

    /**
     * 修改发芽率
     *
     * @param ncSeedRate 发芽率信息
     * @return 结果
     */
    @Override
    public int updateNcSeedRate(NcSeedRate ncSeedRate) {
        return ncSeedRateMapper.updateNcSeedRate(ncSeedRate);
    }

    /**
     * 删除发芽率对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteNcSeedRateByIds(String ids) {
        return ncSeedRateMapper.deleteNcSeedRateByIds(Convert.toStrArray(ids));
    }

    @Override
    public NcSeedRate fillValues(NcSeedRate ncSeedRate) {
        //找到菜 并找到这个菜的父类，祖父类
        NcCai ncCai = ncCaiService.selectCaiById(ncSeedRate.getCropVarieties());
        if (ncCai == null || ncCai.getAncestors() == null || ncCai.getAncestors().split(",").length != 3) {
            return null;
        }
        //蔬菜名称
        NcCai cropNameBean = ncCaiService.selectCaiById(ncCai.getParentId());
        //蔬菜分类
        NcCai cropTypeBean = ncCaiService.selectCaiById(cropNameBean.getParentId());

        ncSeedRate.setCropVarietiesCn(ncCai.getCaiName());
        ncSeedRate.setCropNameCn(cropNameBean.getCaiName());
        ncSeedRate.setCropTypeCn(cropTypeBean.getCaiName());

        return ncSeedRate;
    }
}
