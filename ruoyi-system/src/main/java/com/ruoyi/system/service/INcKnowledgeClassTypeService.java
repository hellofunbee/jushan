package com.ruoyi.system.service;

import com.ruoyi.common.core.domain.Ztree;
import com.ruoyi.system.domain.NcKnowledgeClassType;
import com.ruoyi.system.domain.SysUser;

import java.util.List;

/**
 * 知识课堂类别 服务层
 *
 * @author ruoyi
 * @date 2019-09-10
 */
public interface INcKnowledgeClassTypeService {
    /**
     * 查询知识课堂类别信息
     *
     * @param kctypeId 知识课堂类别ID
     * @return 知识课堂类别信息
     */
	NcKnowledgeClassType selectNcKnowledgeClassTypeById(Long kctypeId);

    /**
     * 查询知识课堂类别列表
     *
     * @param ncKnowledgeClassType 知识课堂类别信息
     * @return 知识课堂类别集合
     */
	List<NcKnowledgeClassType> selectNcKnowledgeClassTypeList(NcKnowledgeClassType ncKnowledgeClassType);

    /**
     * 查询知识课堂二级类别列表
     *
     * @return 知识课堂类别集合
     */
    List<NcKnowledgeClassType> listSecondLevelNcKnowledgeClassTypeList();

    /**
     * 新增知识课堂类别
     *
     * @param ncKnowledgeClassType 知识课堂类别信息
     * @return 结果
     */
	int insertNcKnowledgeClassType(NcKnowledgeClassType ncKnowledgeClassType, SysUser sysUser);

    /**
     * 修改知识课堂类别
     *
     * @param ncKnowledgeClassType 知识课堂类别信息
     * @return 结果
     */
	int updateNcKnowledgeClassType(NcKnowledgeClassType ncKnowledgeClassType, SysUser sysUser);

    /**
     * 删除知识课堂类别信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	int deleteNcKnowledgeClassTypeByIds(Long ids);

    /**
     * 加载知识课堂列表树
     *
     * @param ncKnowledgeClassType
     * @return
     */
    List<Ztree> selectKCTypeClassTree(NcKnowledgeClassType ncKnowledgeClassType);

    /**
     * 根据id查询类别的子类别的个数
     * @param kctypeId
     * @return
     */
    int selectKCTypeClassCount(Long kctypeId);


    /**
     * 根据id查询是否有该分类下的知识课堂
     * @param kctypeId idd
     * @return
     */
    boolean checkKCClassExistKnowClass(Long kctypeId);


    /**
     * 根据父id 查询
     * @param parentId
     * @return
     */
    List<NcKnowledgeClassType> selectNCTypeByPid(Long parentId);




}
