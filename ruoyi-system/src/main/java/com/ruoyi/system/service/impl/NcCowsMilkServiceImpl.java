package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.text.Convert;
import com.ruoyi.system.domain.NcCowsMilk;
import com.ruoyi.system.mapper.NcCowsMilkMapper;
import com.ruoyi.system.service.INcCowsMilkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 产奶记录 服务层实现
 *
 * @author ruoyi
 * @date 2019-09-06
 */
@Service
public class NcCowsMilkServiceImpl implements INcCowsMilkService
{
	@Autowired
	private NcCowsMilkMapper ncCowsMilkMapper;

	/**
     * 查询产奶记录信息
     *
     * @param cowsMilkId 产奶记录ID
     * @return 产奶记录信息
     */
    @Override
	public NcCowsMilk selectNcCowsMilkById(Integer cowsMilkId)
	{
	    return ncCowsMilkMapper.selectNcCowsMilkById(cowsMilkId);
	}

	/**
     * 查询产奶记录列表
     *
     * @param ncCowsMilk 产奶记录信息
     * @return 产奶记录集合
     */
	@Override
	public List<NcCowsMilk> selectNcCowsMilkList(NcCowsMilk ncCowsMilk)
	{
	    return ncCowsMilkMapper.selectNcCowsMilkList(ncCowsMilk);
	}

    /**
     * 新增产奶记录
     *
     * @param ncCowsMilk 产奶记录信息
     * @return 结果
     */
	@Override
	public int insertNcCowsMilk(NcCowsMilk ncCowsMilk)
	{
	    return ncCowsMilkMapper.insertNcCowsMilk(ncCowsMilk);
	}

	/**
     * 修改产奶记录
     *
     * @param ncCowsMilk 产奶记录信息
     * @return 结果
     */
	@Override
	public int updateNcCowsMilk(NcCowsMilk ncCowsMilk)
	{
	    return ncCowsMilkMapper.updateNcCowsMilk(ncCowsMilk);
	}

	/**
     * 删除产奶记录对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteNcCowsMilkByIds(String ids)
	{
		return ncCowsMilkMapper.deleteNcCowsMilkByIds(Convert.toStrArray(ids));
	}

}
