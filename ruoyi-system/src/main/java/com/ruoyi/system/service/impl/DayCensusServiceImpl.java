package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.DayCensus;
import com.ruoyi.system.mapper.DayCensusMapper;
import com.ruoyi.system.service.DayCensusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class DayCensusServiceImpl implements DayCensusService {
    @Autowired
    private DayCensusMapper mapper;
    @Override
    public DayCensus cunAmountList(DayCensus dayCensus) {
        return mapper.cunAmountList(dayCensus);
    }

    @Override
    public DayCensus inputAmountList(DayCensus dayCensus) {
        return mapper.inputAmountList(dayCensus);
    }

    @Override
    public DayCensus outputAmountBigList(DayCensus dayCensus) {
        return mapper.outputAmountBigList(dayCensus);
    }

    @Override
    public DayCensus outputAmountLittleList(DayCensus dayCensus) {
        return mapper.outputAmountLittleList(dayCensus);
    }

    @Override
    public DayCensus neiXiaoList(DayCensus dayCensus) {
        return mapper.neiXiaoList(dayCensus);
    }

    @Override
    public DayCensus zFeedList(DayCensus dayCensus) {
        return mapper.zFeedList(dayCensus);
    }

    @Override
    public DayCensus fFeedList(DayCensus dayCensus) {
        return mapper.fFeedList(dayCensus);
    }

    @Override
    public DayCensus dieList(DayCensus dayCensus) {
        return mapper.dieList(dayCensus);
    }

    @Override
    public DayCensus dieBig(DayCensus ncChickenDayCensus) {
        return mapper.dieBig(ncChickenDayCensus);
    }

    @Override
    public DayCensus dieSmall(DayCensus ncChickenDayCensus) {
        return mapper.dieSmall(ncChickenDayCensus);
    }
}
