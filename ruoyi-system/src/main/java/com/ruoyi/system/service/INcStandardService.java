package com.ruoyi.system.service;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.system.domain.NcCai;
import com.ruoyi.system.domain.NcStandard;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.domain.querydata.ShowNcStandard;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * 标准主 服务层
 * 
 * @author ruoyi
 * @date 2019-07-28
 */
public interface INcStandardService 
{
	/**
     * 查询标准主信息
     * 
     * @param standardId 标准主ID
     * @return 标准主信息
     */
	public NcStandard selectNcStandardById(Long standardId);
	
	/**
     * 查询标准主列表
     * 
     * @param ncStandard 标准主信息
     * @return 标准主集合
     */
	public List<NcStandard> selectNcStandardList(NcStandard ncStandard);

	/**
	 * 查询标准主列表
	 *
	 * @param ncStandard 标准主信息
	 * @return 标准主集合
	 */
	public List<NcStandard> selectExportNcStandardList(NcStandard ncStandard);
	
	/**
     * 新增标准主
     * 
     * @param ncStandard 标准主信息
     * @return 结果
     */
	public int insertNcStandard(NcStandard ncStandard);
	
	/**
     * 修改标准主
     * 
     * @param ncStandard 标准主信息
     * @return 结果
     */
	public int updateNcStandard(NcStandard ncStandard);
		
	/**
     * 删除标准主信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcStandardByIds(String ids);

    NcStandard fillValues(NcStandard ncStandard,String parentCode);

	List<NcStandard> selectNcStandardListByIds(String toString);



	/**
	 * 标准管理 所需查询
	 * @return
	 */
	List<ShowNcStandard> showNcStandard(String  standardType);


    Map getStatusCount(NcStandard ncStandard);

    List<Map> selectSum(NcStandard ncStandard);

	List<NcCai> getBigCaiClass(NcStandard ncStandard);


	/**
	 * 导入标准数据
	 * @param ncStandardList 标准数据列表
	 * @param updateSupport 是否更新支持，如果已存在，则进行更新数据
	 * @param operName 操作用户
	 * @return 结果
	 */
    String importNcStandard(List<NcStandard> ncStandardList, boolean updateSupport, String operName);

	/**
	 * 导入标准数据
	 * @param sysUser operName 操作用户
	 * @param file Excel文件
	 * @param updateSupport updateSupport 是否更新支持，如果已存在，则进行更新数据
	 * @return
	 */
	AjaxResult importNcStandard(SysUser sysUser, String  importNcStandard, MultipartFile file, boolean updateSupport);

	/**根据已完成计划创建标准*/
    int createStandardByPlan(Long plandId,SysUser user);
}
