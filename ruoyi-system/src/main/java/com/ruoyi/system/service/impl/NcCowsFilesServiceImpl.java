package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.text.Convert;
import com.ruoyi.system.domain.NcCowsFiles;
import com.ruoyi.system.mapper.NcCowsFilesMapper;
import com.ruoyi.system.service.INcCowsFilesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 牛档案 服务层实现
 *
 * @author ruoyi
 * @date 2019-09-02
 */
@Service
public class NcCowsFilesServiceImpl implements INcCowsFilesService
{
	@Autowired
	private NcCowsFilesMapper ncCowsFilesMapper;

	/**
     * 查询牛档案信息
     *
     * @param cowId 牛档案ID
     * @return 牛档案信息
     */
    @Override
	public NcCowsFiles selectNcCowsFilesById(Integer cowId)
	{
	    return ncCowsFilesMapper.selectNcCowsFilesById(cowId);
	}

	/**
     * 查询牛档案列表
     *
     * @param ncCowsFiles 牛档案信息
     * @return 牛档案集合
     */
	@Override
	public List<NcCowsFiles> selectNcCowsFilesList(NcCowsFiles ncCowsFiles)
	{
	    return ncCowsFilesMapper.selectNcCowsFilesList(ncCowsFiles);
	}

    /**
     * 新增牛档案
     *
     * @param ncCowsFiles 牛档案信息
     * @return 结果
     */
	@Override
	public int insertNcCowsFiles(NcCowsFiles ncCowsFiles)
	{
	    return ncCowsFilesMapper.insertNcCowsFiles(ncCowsFiles);
	}

	/**
     * 修改牛档案
     *
     * @param ncCowsFiles 牛档案信息
     * @return 结果
     */
	@Override
	public int updateNcCowsFiles(NcCowsFiles ncCowsFiles)
	{
	    return ncCowsFilesMapper.updateNcCowsFiles(ncCowsFiles);
	}

	/**
     * 删除牛档案对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteNcCowsFilesByIds(String ids)
	{
		return ncCowsFilesMapper.deleteNcCowsFilesByIds(Convert.toStrArray(ids));
	}

	@Override
	public long selectCowIdByCowCode(String CowCode) {
		return ncCowsFilesMapper.selectCowIdByCowCode(CowCode);
	}

	@Override
	public List<String> selectAllCowCode() {


		return ncCowsFilesMapper.selectAllCowCode();
	}

	@Override
	public NcCowsFiles selectCowByCowCode(String CowCode) {
		return ncCowsFilesMapper.selectCowByCowCode(CowCode);	}

	/**
	 * 查询所有牛
	 * @return
	 */
	@Override
	public List<NcCowsFiles> selectNcCowsFilesAll() {
		return ncCowsFilesMapper.selectNcCowsFilesAll();
	}

	@Override
	public Integer selectCowCodeOne(String CowCode){
		return ncCowsFilesMapper.selectCowCodeOne(CowCode);
	}
}
