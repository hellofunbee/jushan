package com.ruoyi.system.service.impl;

import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.NcInputMilk;
import com.ruoyi.system.domain.NcOrder;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.mapper.NcInputMilkMapper;
import com.ruoyi.system.service.INcInputMilkService;
import com.ruoyi.system.service.INcOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 牛奶入库 服务层实现
 *
 * @author ruoyi
 * @date 2019-09-11
 */
@Service
public class NcInputMilkServiceImpl implements INcInputMilkService
{
	@Autowired
	private NcInputMilkMapper ncInputMilkMapper;

	@Autowired
	private INcOrderService ncOrderService;

	/**
     * 查询牛奶入库信息
     *
     * @param inputId 牛奶入库ID
     * @return 牛奶入库信息
     */
    @Override
	public NcInputMilk selectNcInputMilkById(Integer inputId)
	{
	    return ncInputMilkMapper.selectNcInputMilkById(inputId);
	}

	/**
     * 查询牛奶入库列表
     *
     * @param ncInputMilk 牛奶入库信息
     * @return 牛奶入库集合
     */
	@Override
	public List<NcInputMilk> selectNcInputMilkList(NcInputMilk ncInputMilk)
	{
	    return ncInputMilkMapper.selectNcInputMilkList(ncInputMilk);
	}

    /**
     * 新增牛奶入库
     *
     * @param ncInputMilk 牛奶入库信息
     * @return 结果
     */
	@Override
	public int insertNcInputMilk(NcInputMilk ncInputMilk)
	{
	    return ncInputMilkMapper.insertNcInputMilk(ncInputMilk);
	}

	/**
     * 修改牛奶入库
     *
     * @param ncInputMilk 牛奶入库信息
     * @return 结果
     */
	@Override
	public int updateNcInputMilk(NcInputMilk ncInputMilk)
	{
	    return ncInputMilkMapper.updateNcInputMilk(ncInputMilk);
	}

	/**
     * 删除牛奶入库对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteNcInputMilkByIds(String ids)
	{
		return ncInputMilkMapper.deleteNcInputMilkByIds(Convert.toStrArray(ids));
	}

	/**
	 * 生成牛奶入库单
	 * @param ids
	 * @param user
	 * @return
	 */
	@Override
	public int createOrderByIds(String ids, SysUser user) {
		NcOrder ncOrder = new NcOrder();
		//创建牛奶入库订单
		if (StringUtils.isNotEmpty(ids)) {
			String orderCode= DateUtils.parseDateToStr("yyyyMMddHHmmss", new Date())+"xxx";//单号
			ncOrder.setOrderCode(orderCode);
			ncOrder.setOrderType(Constants.ORDER_MILK_INPUT);
			ncOrder.setOrderTime(new Date());
			ncOrder.setCreateTime(new Date());
			ncOrder.setCreateBy(user.getUserName());

			ncOrderService.insertNcOrder(ncOrder);

			String[] idArr = Convert.toStrArray(ids);
			for (String id : idArr) {
				NcInputMilk ncInputMilk = ncInputMilkMapper.selectNcInputMilkById(Integer.valueOf(id));
				ncInputMilk.setOrderId(String.valueOf(ncOrder.getOrderId()));
				ncInputMilk.setInputType(2);
				ncInputMilkMapper.updateNcInputMilk(ncInputMilk);
			}
			return 1;
		}else {
			return 0;
		}


	}

}
