package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.NcOutPutMapper;
import com.ruoyi.system.domain.NcOutPut;
import com.ruoyi.system.service.INcOutPutService;
import com.ruoyi.common.core.text.Convert;

/**
 * 出库登记 服务层实现
 * 
 * @author ruoyi
 * @date 2019-09-16
 */
@Service
public class NcOutPutServiceImpl implements INcOutPutService 
{
	@Autowired
	private NcOutPutMapper ncOutPutMapper;

	/**
     * 查询出库登记信息
     * 
     * @param outPutId 出库登记ID
     * @return 出库登记信息
     */
    @Override
	public NcOutPut selectNcOutPutById(Long outPutId)
	{
	    return ncOutPutMapper.selectNcOutPutById(outPutId);
	}
	
	/**
     * 查询出库登记列表
     * 
     * @param ncOutPut 出库登记信息
     * @return 出库登记集合
     */
	@Override
	public List<NcOutPut> selectNcOutPutList(NcOutPut ncOutPut)
	{
	    return ncOutPutMapper.selectNcOutPutList(ncOutPut);
	}
	
    /**
     * 新增出库登记
     * 
     * @param ncOutPut 出库登记信息
     * @return 结果
     */
	@Override
	public int insertNcOutPut(NcOutPut ncOutPut)
	{
	    return ncOutPutMapper.insertNcOutPut(ncOutPut);
	}
	
	/**
     * 修改出库登记
     * 
     * @param ncOutPut 出库登记信息
     * @return 结果
     */
	@Override
	public int updateNcOutPut(NcOutPut ncOutPut)
	{
	    return ncOutPutMapper.updateNcOutPut(ncOutPut);
	}

	/**
     * 删除出库登记对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteNcOutPutByIds(String ids)
	{
		return ncOutPutMapper.deleteNcOutPutByIds(Convert.toStrArray(ids));
	}
	
}
