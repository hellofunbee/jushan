package com.ruoyi.system.service;

import com.ruoyi.system.domain.NcStage;
import java.util.List;

/**
 * 生产（育苗）阶段 服务层
 * 
 * @author ruoyi
 * @date 2019-07-28
 */
public interface INcStageService 
{
	/**
     * 查询生产（育苗）阶段信息
     * 
     * @param stageId 生产（育苗）阶段ID
     * @return 生产（育苗）阶段信息
     */
	public NcStage selectNcStageById(Long stageId);
	
	/**
     * 查询生产（育苗）阶段列表
     * 
     * @param ncStage 生产（育苗）阶段信息
     * @return 生产（育苗）阶段集合
     */
	public List<NcStage> selectNcStageList(NcStage ncStage);
	
	/**
     * 新增生产（育苗）阶段
     * 
     * @param ncStage 生产（育苗）阶段信息
     * @return 结果
     */
	public int insertNcStage(NcStage ncStage);
	
	/**
     * 修改生产（育苗）阶段
     * 
     * @param ncStage 生产（育苗）阶段信息
     * @return 结果
     */
	public int updateNcStage(NcStage ncStage);
		
	/**
     * 删除生产（育苗）阶段信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcStageByIds(String ids);
	
}
