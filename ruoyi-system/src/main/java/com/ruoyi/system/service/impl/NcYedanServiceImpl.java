package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.exception.BusinessException;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.SysUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.NcYedanMapper;
import com.ruoyi.system.domain.NcYedan;
import com.ruoyi.system.service.INcYedanService;
import com.ruoyi.common.core.text.Convert;

/**
 * 液氮 服务层实现
 * 
 * @author ruoyi
 * @date 2019-09-02
 */
@Service
public class NcYedanServiceImpl implements INcYedanService 
{
	@Autowired
	private NcYedanMapper ncYedanMapper;
	private static final Logger log = LoggerFactory.getLogger(NcYedanServiceImpl.class);

	/**
     * 查询液氮信息
     * 
     * @param yedanId 液氮ID
     * @return 液氮信息
     */
    @Override
	public NcYedan selectNcYedanById(Long yedanId)
	{
	    return ncYedanMapper.selectNcYedanById(yedanId);
	}
	
	/**
     * 查询液氮列表
     * 
     * @param ncYedan 液氮信息
     * @return 液氮集合
     */
	@Override
	public List<NcYedan> selectNcYedanList(NcYedan ncYedan)
	{
	    return ncYedanMapper.selectNcYedanList(ncYedan);
	}
	
    /**
     * 新增液氮
     * 
     * @param ncYedan 液氮信息
     * @return 结果
     */
	@Override
	public int insertNcYedan(NcYedan ncYedan, SysUser user)
	{

		//校验数量

		//经手人（当前用户）
		ncYedan.setCreateBy(user.getLoginName());
		//设置库存
		ncYedan.setStockAmout(ncYedan.getInputAmount());
		return ncYedanMapper.insertNcYedan(ncYedan);
	}


	/**
     * 修改液氮
     * 
     * @param ncYedan 液氮信息
     * @return 结果
     */
	@Override
	public int updateNcYedan(NcYedan ncYedan)
	{
	    return ncYedanMapper.updateNcYedan(ncYedan);
	}

	/**
     * 删除液氮对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteNcYedanByIds(String ids)
	{
		return ncYedanMapper.deleteNcYedanByIds(Convert.toStrArray(ids));
	}

	@Override
	public String importYeDan(List<NcYedan> yedanList, Boolean isUpdateSupport, SysUser sysUser) {
		if (StringUtils.isNull(yedanList) || yedanList.size() == 0) {
			throw new BusinessException("导入液氮数据不能为空！");
		}
		int successNum = 0;
		int failureNum = 0;
		StringBuilder successMsg = new StringBuilder();
		StringBuilder failureMsg = new StringBuilder();
		for (NcYedan yedan : yedanList) {
			try {
				// 验证是否存在这个用户
				this.insertNcYedan(yedan,sysUser);
				successNum++;
				successMsg.append("导入成功");
			} catch (Exception e) {
				failureNum++;
				String msg = "导入失败：";
				failureMsg.append(msg + e.getMessage());
				log.error(msg, e);
			}
		}
		if (failureNum > 0) {
			failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
			throw new BusinessException(failureMsg.toString());
		} else {
			successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
		}
		return successMsg.toString();
	}




	
}
