package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.NcStageMirrorMapper;
import com.ruoyi.system.domain.NcStageMirror;
import com.ruoyi.system.service.INcStageMirrorService;
import com.ruoyi.common.core.text.Convert;

/**
 * 生产（育苗）阶段 镜像 服务层实现
 * 
 * @author ruoyi
 * @date 2019-09-02
 */
@Service
public class NcStageMirrorServiceImpl implements INcStageMirrorService 
{
	@Autowired
	private NcStageMirrorMapper ncStageMirrorMapper;

	/**
     * 查询生产（育苗）阶段 镜像信息
     * 
     * @param stageMirrorId 生产（育苗）阶段 镜像ID
     * @return 生产（育苗）阶段 镜像信息
     */
    @Override
	public NcStageMirror selectNcStageMirrorById(Long stageMirrorId)
	{
	    return ncStageMirrorMapper.selectNcStageMirrorById(stageMirrorId);
	}
	
	/**
     * 查询生产（育苗）阶段 镜像列表
     * 
     * @param ncStageMirror 生产（育苗）阶段 镜像信息
     * @return 生产（育苗）阶段 镜像集合
     */
	@Override
	public List<NcStageMirror> selectNcStageMirrorList(NcStageMirror ncStageMirror)
	{
	    return ncStageMirrorMapper.selectNcStageMirrorList(ncStageMirror);
	}
	
    /**
     * 新增生产（育苗）阶段 镜像
     * 
     * @param ncStageMirror 生产（育苗）阶段 镜像信息
     * @return 结果
     */
	@Override
	public int insertNcStageMirror(NcStageMirror ncStageMirror)
	{
	    return ncStageMirrorMapper.insertNcStageMirror(ncStageMirror);
	}
	
	/**
     * 修改生产（育苗）阶段 镜像
     * 
     * @param ncStageMirror 生产（育苗）阶段 镜像信息
     * @return 结果
     */
	@Override
	public int updateNcStageMirror(NcStageMirror ncStageMirror)
	{
	    return ncStageMirrorMapper.updateNcStageMirror(ncStageMirror);
	}

	/**
     * 删除生产（育苗）阶段 镜像对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteNcStageMirrorByIds(String ids)
	{
		return ncStageMirrorMapper.deleteNcStageMirrorByIds(Convert.toStrArray(ids));
	}
	
}
