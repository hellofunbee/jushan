package com.ruoyi.system.service;

import com.ruoyi.system.domain.NcZaisha;
import java.util.List;

/**
 * 宰杀 服务层
 * 
 * @author ruoyi
 * @date 2019-09-04
 */
public interface INcZaishaService 
{
	/**
     * 查询宰杀信息
     * 
     * @param zaishaId 宰杀ID
     * @return 宰杀信息
     */
	public NcZaisha selectNcZaishaById(Long zaishaId);
	
	/**
     * 查询宰杀列表
     * 
     * @param ncZaisha 宰杀信息
     * @return 宰杀集合
     */
	public List<NcZaisha> selectNcZaishaList(NcZaisha ncZaisha);
	
	/**
     * 新增宰杀
     * 
     * @param ncZaisha 宰杀信息
     * @return 结果
     */
	public int insertNcZaisha(NcZaisha ncZaisha);
	
	/**
     * 修改宰杀
     * 
     * @param ncZaisha 宰杀信息
     * @return 结果
     */
	public int updateNcZaisha(NcZaisha ncZaisha);
		
	/**
     * 删除宰杀信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcZaishaByIds(String ids);
	
}
