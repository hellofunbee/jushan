package com.ruoyi.system.service.impl;

import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.NcUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.NcOrder;
import com.ruoyi.system.domain.NcZhibao;
import com.ruoyi.system.domain.NcZhibaoDrugs;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.mapper.NcZhibaoDrugsMapper;
import com.ruoyi.system.mapper.NcZhibaoMapper;
import com.ruoyi.system.service.INcCaiService;
import com.ruoyi.system.service.INcOrderCfService;
import com.ruoyi.system.service.INcOrderService;
import com.ruoyi.system.service.INcZhibaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * 植保 服务层实现
 * 
 * @author ruoyi
 * @date 2019-08-28
 */
@Service
public class NcZhibaoServiceImpl implements INcZhibaoService 
{
	@Autowired
	private NcZhibaoMapper ncZhibaoMapper;
	@Autowired
	private INcOrderService orderService;
	@Autowired
	private NcZhibaoDrugsMapper ncZhibaoDrugsMapper;
	@Autowired
	private INcOrderCfService orderCfService;
	@Autowired
	private INcCaiService caiService;


	/**
     * 查询植保信息
     * 
     * @param zhibaoId 植保ID
     * @return 植保信息
     */
    @Override
	public NcZhibao selectNcZhibaoById(Long zhibaoId)
	{
	    return ncZhibaoMapper.selectNcZhibaoById(zhibaoId);
	}
	
	/**
     * 查询植保列表
     * 
     * @param ncZhibao 植保信息
     * @return 植保集合
     */
	@Override
	public List<NcZhibao> selectNcZhibaoList(NcZhibao ncZhibao)
	{
	    return ncZhibaoMapper.selectNcZhibaoList(ncZhibao);
	}
	
    /**
     * 新增植保
     * 
     * @param ncZhibao 植保信息
     * @return 结果
     */
	@Override
	public int insertNcZhibao(NcZhibao ncZhibao)
	{
	    return ncZhibaoMapper.insertNcZhibao(ncZhibao);
	}
	
	/**
     * 修改植保
     * 
     * @param ncZhibao 植保信息
     * @return 结果
     */
	@Override
	public int updateNcZhibao(NcZhibao ncZhibao)
	{

		return ncZhibaoMapper.updateNcZhibao(ncZhibao);
	}

	/**
     * 删除植保对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteNcZhibaoByIds(String ids)
	{
		return ncZhibaoMapper.deleteNcZhibaoByIds(Convert.toStrArray(ids));
	}

	@Override
	@DataScope(deptAlias = "dept")
	public List<NcZhibao> selectNcZhibaoJiluList(NcZhibao ncZhibao) {
		return ncZhibaoMapper.selectNcZhibaoJiluList(ncZhibao);
	}
	@Override
	public int createOrderByIds(String ids, SysUser user) {
		NcOrder o = new NcOrder();

		if (StringUtils.isNotEmpty(ids)) {
			/*创建植保订单*/
			o.setOrderCode(NcUtils.getOrderCode());
			o.setOrderType(Constants.ORDER_ZHIBAO);
			o.setOrderTime(new Date());
			o.setCreateTime(new Date());
			o.setCreateBy(user.getUserName());
			orderService.insertNcOrder(o);

			System.out.println("----------orderId-------:" + o.getOrderId());
			String[] idArr = Convert.toStrArray(ids);
			for (String id : idArr) {
				NcZhibao zhibao = selectNcZhibaoById(Long.parseLong(id));
				zhibao.setOrderId(o.getOrderId());
				zhibao.setConformStatus(3);
				updateNcZhibao(zhibao);
			}

			return 1;
		} else {
			return 0;
		}
	}

	@Override
	public int updateZhibaoLog(NcZhibao ncZhibao) {
		return ncZhibaoMapper.updateZhibaoLog(ncZhibao);
	}

	@Override
	@Transactional
	public int insertZhibaoJilu(NcZhibao ncZhibao) {
		NcZhibao o=new NcZhibao();
		o.setCauses(ncZhibao.getCauses());//发病原因
		o.setProposal(ncZhibao.getProposal());//植保意见
		o.setRemark(ncZhibao.getRemark());//备注
		o.setSymptom(ncZhibao.getSymptom());//病症
		o.setSourceType(2);//新增
		o.setConformStatus(1);//已确认
		o.setPlanId(ncZhibao.getPlanId());
		int zhibaoResult=ncZhibaoMapper.insertNcZhibao(o);
		NcZhibaoDrugs drug=new NcZhibaoDrugs();
		drug.setZhibaoId(o.getZhibaoId());
		drug.setDrugCode(ncZhibao.getDrugCode());//药品批次号
		drug.setDrugName(ncZhibao.getDrugName());//药品名称
		drug.setExecuteType(ncZhibao.getExecuteType());//施药方式
		drug.setUnit(ncZhibao.getUnit());
		drug.setDrugAmount(ncZhibao.getDrugAmount());//施药量
		int zhibaoDrugsResult=ncZhibaoDrugsMapper.insertNcZhibaoDrugs(drug);
		if(zhibaoDrugsResult == 1 && zhibaoResult == 1){
			return 1;
		}
		else{
			return 0;
		}

	}

	@Override
	public Long selectChartAmount(NcZhibao ncZhibao) {
		return ncZhibaoMapper.selectChartAmount(ncZhibao);
	}

	@Override
	public List<NcZhibao> selectzhibaoDetail(NcZhibao ncZhibao) {
		return ncZhibaoMapper.selectzhibaoDetail(ncZhibao);
	}

	public List<Map> selectCountByTime(Integer spanType, String beginTime, String endTime,Integer zhibaoType) {

		Date s = null;
		Date e = null;
		Map se = orderCfService.calTimeSpan(s, e, beginTime, endTime, spanType);
		s = (Date) se.get("s");
		e = (Date) se.get("e");

		return getSumByDate(spanType, s, e,zhibaoType);
	}
	private List<Map> getSumByDate(Integer spanType, Date s, Date e,Integer zhibaoType) {
		List<Map> result = new ArrayList<>();
		if (spanType == 1) {
			while (e.getTime() >= s.getTime()) {
				String dateSpan = DateUtils.parseDateToStr("yyyy", s);

				Map m = new HashMap();
				m.put("x", dateSpan);
				m.put("count", getSumByDate(dateSpan, spanType,zhibaoType));
				result.add(m);

				s = DateUtils.addYears(s, 1);
			}
		} else if (spanType == 2) {
			while (e.getTime() >= s.getTime()) {
				String dateSpan = DateUtils.parseDateToStr("yyyy-MM", s);
				Map m = new HashMap();
				m.put("x", dateSpan);
				m.put("count", getSumByDate(dateSpan, spanType,zhibaoType));
				result.add(m);
				s = DateUtils.addMonths(s, 1);
			}
		} else if (spanType == 3) {
			while (e.getTime() >= s.getTime()) {
				String dateSpan = DateUtils.parseDateToStr("yyyy-MM-dd", s);
				Map m = new HashMap();
				m.put("x", dateSpan);
				m.put("count", getSumByDate(dateSpan, spanType,zhibaoType));
				result.add(m);
				s = DateUtils.addDays(s, 1);
			}
		}
		return result;
	}
	private Long getSumByDate(String dateSpan, Integer dataSpan,Integer zhibaoType) {
		NcZhibao zhibao = new NcZhibao();
		if (dataSpan == null || dataSpan == 2) {
			zhibao.getParams().put("dateSpanMonth", dateSpan);
		} else if (dataSpan == 1) {
			zhibao.getParams().put("dateSpanYear", dateSpan);
		} else if (dataSpan == 3) {
			zhibao.getParams().put("dateSpanDay", dateSpan);
		}
		zhibao.getParams().put("ancestors",zhibaoType);
		return ncZhibaoMapper.selectChartAmount(zhibao);
	}

}

