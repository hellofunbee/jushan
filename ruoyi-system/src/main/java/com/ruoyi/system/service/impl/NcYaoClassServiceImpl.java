package com.ruoyi.system.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.domain.Ztree;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.NcYaoClass;
import com.ruoyi.system.domain.SysRole;
import com.ruoyi.system.mapper.NcYaoClassMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.NcYaoClassMapper;
import com.ruoyi.system.domain.NcYaoClass;
import com.ruoyi.system.service.INcYaoClassService;
import com.ruoyi.common.core.text.Convert;
import org.springframework.transaction.annotation.Transactional;

/**
 * 药品类别 服务层实现
 * 
 * @author ruoyi
 * @date 2019-08-29
 */
@Service
public class NcYaoClassServiceImpl implements INcYaoClassService 
{
	@Autowired
	private NcYaoClassMapper yaoMapper;


	@Override
	public List<NcYaoClass> selectYaoClassList(NcYaoClass yaoClass) {
		return yaoMapper.selectNcYaoClassList(yaoClass);
	}

	/**
	 * 查询药品管理树
	 *
	 * @param yaoClass 药品信息
	 * @return 所有药品信息
	 */
	@Override
	public List<Ztree> selectYaoClassTree(NcYaoClass yaoClass) {
		List<NcYaoClass> yaoClassList = yaoMapper.selectNcYaoClassList(yaoClass);
		List<Ztree> ztrees = initZtree(yaoClassList);
		return ztrees;
	}

	/**
	 * 根据角色ID查询药品（数据权限）
	 *
	 * @param role 角色对象
	 * @return 药品列表（数据权限）
	 */
	@Override
	public List<Ztree> roleYaoClassTreeData(SysRole role) {
		Long roleId = role.getRoleId();
		List<Ztree> ztrees = new ArrayList<Ztree>();
		List<NcYaoClass> yaoClassList = selectYaoClassList(new NcYaoClass());
		if (StringUtils.isNotNull(roleId)) {
			List<String> roleYaoClassList = yaoMapper.selectRoleYaoTree(roleId);
			ztrees = initZtree(yaoClassList, roleYaoClassList);
		} else {
			ztrees = initZtree(yaoClassList);
		}
		return ztrees;
	}

	/**
	 * 对象转药品树
	 *
	 * @param yaoClassList 药品列表
	 * @return 树结构列表
	 */
	public List<Ztree> initZtree(List<NcYaoClass> yaoClassList) {
		return initZtree(yaoClassList, null);
	}

	/**
	 * 对象转药品树
	 *
	 * @param yaoClassList     药品列表
	 * @param roleYaoClassList 角色已存在菜单列表
	 * @return 树结构列表
	 */
	public List<Ztree> initZtree(List<NcYaoClass> yaoClassList, List<String> roleYaoClassList) {

		List<Ztree> ztrees = new ArrayList<Ztree>();
		boolean isCheck = StringUtils.isNotNull(roleYaoClassList);
		for (NcYaoClass yaoClass : yaoClassList) {

			Ztree ztree = new Ztree();
			ztree.setId(yaoClass.getYaoId());
			ztree.setpId(yaoClass.getParentId());
			ztree.setName(yaoClass.getYaoName());
			ztree.setTitle(yaoClass.getYaoName());
			if (isCheck) {
				ztree.setChecked(roleYaoClassList.contains(yaoClass.getYaoId() + yaoClass.getYaoName()));
			}
			ztrees.add(ztree);

		}
		return ztrees;
	}

	/**
	 * 查询药品人数
	 *
	 * @param parentId 药品ID
	 * @return 结果
	 */
	@Override
	public int selectYaoClassCount(Long parentId) {
		NcYaoClass yaoClass = new NcYaoClass();
		yaoClass.setParentId(parentId);
		return yaoMapper.selectYaoCount(yaoClass);
	}

	/**
	 * 查询药品是否存在用户
	 *
	 * @param yaoClassId 药品ID
	 * @return 结果 true 存在 false 不存在
	 */
	@Override
	public boolean checkYaoClassExistUser(Long yaoClassId) {
		int result = yaoMapper.checkYaoExistUser(yaoClassId);
		return result > 0 ? true : false;
	}

	/**
	 * 删除药品管理信息
	 *
	 * @param yaoClassId 药品ID
	 * @return 结果
	 */
	@Override
	public int deleteYaoClassById(Long yaoClassId) {
		return yaoMapper.deleteYaoById(yaoClassId);
	}

	/**
	 * 新增保存药品信息
	 *
	 * @param yaoClass 药品信息
	 * @return 结果
	 */
	@Override
	public int insertYaoClass(NcYaoClass yaoClass) {
		NcYaoClass info = yaoMapper.selectYaoById(yaoClass.getParentId());
		// 如果父节点不为"正常"状态,则不允许新增子节点
        /*if (!UserConstants.DEPT_NORMAL.equals(info.getStatus())) {
            throw new BusinessException("药品停用，不允许新增");
        }*/
		yaoClass.setAncestors(info.getAncestors() + "," + yaoClass.getParentId());
		return yaoMapper.insertNcYaoClass(yaoClass);
	}

	/**
	 * 修改保存药品信息
	 *
	 * @param yaoClass 药品信息
	 * @return 结果
	 */
	@Override
	@Transactional
	public int updateYaoClass(NcYaoClass yaoClass) {
		NcYaoClass newParentYaoClass = yaoMapper.selectYaoById(yaoClass.getParentId());
		NcYaoClass oldYaoClass = selectYaoClassById(yaoClass.getYaoId());
		if (StringUtils.isNotNull(newParentYaoClass) && StringUtils.isNotNull(oldYaoClass)) {
			String newAncestors = newParentYaoClass.getAncestors() + "," + newParentYaoClass.getYaoId();
			String oldAncestors = oldYaoClass.getAncestors();
			yaoClass.setAncestors(newAncestors);
			updateYaoClassChildren(yaoClass.getYaoId(), newAncestors, oldAncestors);
		}
		int result = yaoMapper.updateNcYaoClass(yaoClass);
        /*if (UserConstants.DEPT_NORMAL.equals(yaoClass.getStatus())) {
            // 如果该药品是启用状态，则启用该药品的所有上级药品
            updateParentYaoClassStatus(yaoClass);
        }*/
		return result;
	}

	/**
	 * 修改该药品的父级药品状态
	 *
	 * @param yaoClass 当前药品
	 */
	private void updateParentYaoClassStatus(NcYaoClass yaoClass) {
		String updateBy = yaoClass.getUpdateBy();
		yaoClass = yaoMapper.selectYaoById(yaoClass.getYaoId());
		yaoClass.setUpdateBy(updateBy);
		yaoMapper.updateYaoStatus(yaoClass);
	}

	/**
	 * 修改子元素关系
	 *
	 * @param yaoClassId        被修改的药品ID
	 * @param newAncestors 新的父ID集合
	 * @param oldAncestors 旧的父ID集合
	 */
	public void updateYaoClassChildren(Long yaoClassId, String newAncestors, String oldAncestors) {
		List<NcYaoClass> children = yaoMapper.selectChildrenYaoById(yaoClassId);
		for (NcYaoClass child : children) {
			child.setAncestors(child.getAncestors().replace(oldAncestors, newAncestors));
		}
		if (children.size() > 0) {
			yaoMapper.updateYaoChildren(children);
		}
	}

	/**
	 * 根据药品ID查询信息
	 *
	 * @param yaoClassId 药品ID
	 * @return 药品信息
	 */
	@Override
	public NcYaoClass selectYaoClassById(Long yaoClassId) {
		return yaoMapper.selectYaoById(yaoClassId);
	}

	/**
	 * 校验药品名称是否唯一
	 *
	 * @param yaoClass 药品信息
	 * @return 结果
	 */
	@Override
	public String checkYaoClassNameUnique(NcYaoClass yaoClass) {
		Long yaoClassId = StringUtils.isNull(yaoClass.getYaoId()) ? -1L : yaoClass.getYaoId();
		NcYaoClass info = yaoMapper.checkYaoNameUnique(yaoClass.getYaoName(), yaoClass.getParentId());
		if (StringUtils.isNotNull(info) && info.getYaoId().longValue() != yaoClassId.longValue()) {
			return UserConstants.DEPT_NAME_NOT_UNIQUE;
		}
		return UserConstants.DEPT_NAME_UNIQUE;
	}


}
