package com.ruoyi.system.service;

import com.ruoyi.system.domain.NcCowsMilk;
import java.util.List;

/**
 * 产奶记录 服务层
 * 
 * @author ruoyi
 * @date 2019-09-06
 */
public interface INcCowsMilkService 
{
	/**
     * 查询产奶记录信息
     * 
     * @param cowsMilkId 产奶记录ID
     * @return 产奶记录信息
     */
	public NcCowsMilk selectNcCowsMilkById(Integer cowsMilkId);
	
	/**
     * 查询产奶记录列表
     * 
     * @param ncCowsMilk 产奶记录信息
     * @return 产奶记录集合
     */
	public List<NcCowsMilk> selectNcCowsMilkList(NcCowsMilk ncCowsMilk);
	
	/**
     * 新增产奶记录
     * 
     * @param ncCowsMilk 产奶记录信息
     * @return 结果
     */
	public int insertNcCowsMilk(NcCowsMilk ncCowsMilk);
	
	/**
     * 修改产奶记录
     * 
     * @param ncCowsMilk 产奶记录信息
     * @return 结果
     */
	public int updateNcCowsMilk(NcCowsMilk ncCowsMilk);
		
	/**
     * 删除产奶记录信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcCowsMilkByIds(String ids);
}
