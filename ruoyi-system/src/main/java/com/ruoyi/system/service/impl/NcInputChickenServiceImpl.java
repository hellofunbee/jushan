package com.ruoyi.system.service.impl;

import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.utils.NcUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.NcInputChicken;
import com.ruoyi.system.domain.NcOrder;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.mapper.NcInputChickenMapper;
import com.ruoyi.system.service.INcInputChickenService;
import com.ruoyi.system.service.INcOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 鸡入库 服务层实现
 * 
 * @author ruoyi
 * @date 2019-09-09
 */
@Service
public class NcInputChickenServiceImpl implements INcInputChickenService 
{
	@Autowired
	private NcInputChickenMapper ncInputChickenMapper;
	@Autowired
	private INcOrderService orderService;

	/**
     * 查询鸡入库信息
     * 
     * @param inputId 鸡入库ID
     * @return 鸡入库信息
     */
    @Override
	public NcInputChicken selectNcInputChickenById(Integer inputId)
	{
	    return ncInputChickenMapper.selectNcInputChickenById(inputId);
	}
	
	/**
     * 查询鸡入库列表
     * 
     * @param ncInputChicken 鸡入库信息
     * @return 鸡入库集合
     */
	@Override
	public List<NcInputChicken> selectNcInputChickenList(NcInputChicken ncInputChicken)
	{
	    return ncInputChickenMapper.selectNcInputChickenList(ncInputChicken);
	}
	
    /**
     * 新增鸡入库
     * 
     * @param ncInputChicken 鸡入库信息
     * @return 结果
     */
	@Override
	public int insertNcInputChicken(NcInputChicken ncInputChicken)
	{
	    return ncInputChickenMapper.insertNcInputChicken(ncInputChicken);
	}
	
	/**
     * 修改鸡入库
     * 
     * @param ncInputChicken 鸡入库信息
     * @return 结果
     */
	@Override
	public int updateNcInputChicken(NcInputChicken ncInputChicken)
	{
	    return ncInputChickenMapper.updateNcInputChicken(ncInputChicken);
	}

	/**
     * 删除鸡入库对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteNcInputChickenByIds(String ids)
	{
		return ncInputChickenMapper.deleteNcInputChickenByIds(Convert.toStrArray(ids));
	}

	@Override
	public int createOrderByIds(String ids, SysUser user) {
		NcOrder o = new NcOrder();

		if (StringUtils.isNotEmpty(ids)) {
			/*创建鸡入库订单*/
			o.setOrderCode(NcUtils.getOrderCode());
			o.setOrderType(Constants.ORDER_CHICKEN_INPUT);
			o.setOrderTime(new Date());
			o.setCreateTime(new Date());
			o.setCreateBy(user.getUserName());
			orderService.insertNcOrder(o);

			System.out.println("----------orderId-------:" + o.getOrderId());
			String[] idArr = Convert.toStrArray(ids);
			for (String id : idArr) {
				NcInputChicken inputChicken = ncInputChickenMapper.selectNcInputChickenById(Integer.valueOf(id));
				inputChicken.setOrderId(String.valueOf(o.getOrderId()));
				inputChicken.setInputStatus(Long.valueOf(2));
			ncInputChickenMapper.updateNcInputChicken(inputChicken);

			}

			return 1;
		} else {
			return 0;
		}
	}

	@Override
	public Integer chickenStackAmount() {
		return ncInputChickenMapper.chickenStackAmount();
	}


}
