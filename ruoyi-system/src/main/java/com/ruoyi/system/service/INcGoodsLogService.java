package com.ruoyi.system.service;

import com.ruoyi.system.domain.NcGoodsLog;
import java.util.List;

/**
 * 物资仓储出入记录 服务层
 *
 * @author ruoyi
 * @date 2019-08-06
 */
public interface INcGoodsLogService
{
	/**
     * 查询物资仓储出入记录信息
     *
     * @param goodsLogId 物资仓储出入记录ID
     * @return 物资仓储出入记录信息
     */
	public NcGoodsLog selectNcGoodsLogById(Long goodsLogId);

	public List<NcGoodsLog> selectNcGoodsLogByGId(Long goodsId);

	/**
     * 查询物资仓储出入记录列表
     *
     * @param ncGoodsLog 物资仓储出入记录信息
     * @return 物资仓储出入记录集合
     */
	public List<NcGoodsLog> selectNcGoodsLogList(NcGoodsLog ncGoodsLog);

	/**
     * 新增物资仓储出入记录
     *
     * @param ncGoodsLog 物资仓储出入记录信息
     * @return 结果
     */
	public int insertNcGoodsLog(NcGoodsLog ncGoodsLog);

	/**
     * 修改物资仓储出入记录
     *
     * @param ncGoodsLog 物资仓储出入记录信息
     * @return 结果
     */
	public int updateNcGoodsLog(NcGoodsLog ncGoodsLog);

	/**
     * 删除物资仓储出入记录信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcGoodsLogByIds(String ids);

}
