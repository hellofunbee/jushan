package com.ruoyi.system.service;

import com.ruoyi.common.core.domain.Ztree;
import com.ruoyi.system.domain.NcCai;
import com.ruoyi.system.domain.SysRole;

import java.util.List;

/**
 * 蔬菜管理 服务层
 * 
 * @author ruoyi
 */
public interface INcCaiService
{
    /**
     * 查询蔬菜管理数据
     * 
     * @param cai 蔬菜信息
     * @return 蔬菜信息集合
     */
    public List<NcCai> selectCaiList(NcCai cai);
    /**
     * 查询蔬菜管理数据 (三级)
     *
     * @param cai 蔬菜信息
     * @return 蔬菜信息集合
     */
    public List<NcCai> selectCaiListHasParent2(NcCai cai);

    /**
     * 查询蔬菜管理树
     * 
     * @param cai 蔬菜信息
     * @return 所有蔬菜信息
     */
    public List<Ztree> selectCaiTree(NcCai cai);

    /**
     * 根据角色ID查询菜单
     *
     * @param role 角色对象
     * @return 菜单列表
     */
    public List<Ztree> roleCaiTreeData(SysRole role);

    /**
     * 查询蔬菜人数
     * 
     * @param parentId 父蔬菜ID
     * @return 结果
     */
    public int selectCaiCount(Long parentId);

    /**
     * 查询蔬菜是否存在用户
     * 
     * @param caiId 蔬菜ID
     * @return 结果 true 存在 false 不存在
     */
    public boolean checkCaiExistUser(Long caiId);

    /**
     * 删除蔬菜管理信息
     * 
     * @param caiId 蔬菜ID
     * @return 结果
     */
    public int deleteCaiById(Long caiId);

    /**
     * 新增保存蔬菜信息
     * 
     * @param cai 蔬菜信息
     * @return 结果
     */
    public int insertCai(NcCai cai);

    /**
     * 修改保存蔬菜信息
     * 
     * @param cai 蔬菜信息
     * @return 结果
     */
    public int updateCai(NcCai cai);

    /**
     * 根据蔬菜ID查询信息
     * 
     * @param caiId 蔬菜ID
     * @return 蔬菜信息
     */
    public NcCai selectCaiById(Long caiId);

    /**
     * 校验蔬菜名称是否唯一
     * 
     * @param cai 蔬菜信息
     * @return 结果
     */
    public String checkCaiNameUnique(NcCai cai);


    List<NcCai> selectBigClass(NcCai cai);


    /**
     * 根据蔬菜名称查找蔬菜Id
     * @param caiName 蔬菜Name
     * @return
     */
    public Long  selectCaIdByCaiName(String caiName);


    List<NcCai> selectCaiByNames(NcCai cai);
}
