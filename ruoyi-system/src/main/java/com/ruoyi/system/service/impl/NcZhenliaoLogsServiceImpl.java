package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.NcZhenliaoLogs;
import com.ruoyi.system.mapper.NcZhenliaoLogsMapper;
import com.ruoyi.system.service.INcZhenliaoLogsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 诊疗单 服务层实现
 *
 * @author ruoyi
 * @date 2019-09-11
 */
@Service
public class NcZhenliaoLogsServiceImpl implements INcZhenliaoLogsService
{
	@Autowired
	private NcZhenliaoLogsMapper ncZhenliaoLogsMapper;

	/**
     * 查询诊疗单信息
     *
     * @param zlId 诊疗单ID
     * @return 诊疗单信息
     */
    @Override
	public NcZhenliaoLogs selectNcZhenliaoLogsById(Long zlId)
	{
		return ncZhenliaoLogsMapper.selectNcZhenliaoLogsById(zlId);
	}

	/**
     * 查询诊疗单列表
     *
     * @param ncZhenliaoLogs 诊疗单信息
     * @return 诊疗单集合
     */
	@Override
	public List<NcZhenliaoLogs> selectNcZhenliaoLogsList(NcZhenliaoLogs ncZhenliaoLogs)
	{
	    return ncZhenliaoLogsMapper.selectNcZhenliaoLogsList(ncZhenliaoLogs);
	}

    /**
     * 新增诊疗单
     *
     * @param ncZhenliaoLogs 诊疗单信息
     * @return 结果
     */
	@Override
	public int insertNcZhenliaoLogs(NcZhenliaoLogs ncZhenliaoLogs)
	{
	    return ncZhenliaoLogsMapper.insertNcZhenliaoLogs(ncZhenliaoLogs);
	}

	/**
     * 修改诊疗单
     *
     * @param ncZhenliaoLogs 诊疗单信息
     * @return 结果
     */
	@Override
	public int updateNcZhenliaoLogs(NcZhenliaoLogs ncZhenliaoLogs)
	{
	    return ncZhenliaoLogsMapper.updateNcZhenliaoLogs(ncZhenliaoLogs);
	}

	/**
     * 删除诊疗单对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteNcZhenliaoLogsByIds(String ids)
	{
		return ncZhenliaoLogsMapper.deleteNcZhenliaoLogsByIds(Convert.toStrArray(ids));
	}

	@Override
	public int updateZhenliao(String ids) {
		NcZhenliaoLogs o = new NcZhenliaoLogs();

		if (StringUtils.isNotEmpty(ids)) {
			String[] idArr = Convert.toStrArray(ids);
			for (String id : idArr) {
                NcZhenliaoLogs zhenliao=ncZhenliaoLogsMapper.selectNcZhenliaoLogsById(Long.valueOf(id));
                if(Long.valueOf(zhenliao.getZlStatus()) == 5){
                    NcZhenliaoLogs zhenliaoLog =selectNcZhenliaoLogsById(Long.parseLong(id));
                    zhenliaoLog.setZlId(Long.parseLong(id));
                    zhenliaoLog.setZlType(2);
                    zhenliaoLog.setZlStatus("1");
                    updateNcZhenliaoLogs(zhenliaoLog);
                }
			}

			return 1;
		} else {
			return 0;
		}
	}

}
