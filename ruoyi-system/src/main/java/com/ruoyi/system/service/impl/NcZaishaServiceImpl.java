package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.NcZaishaMapper;
import com.ruoyi.system.domain.NcZaisha;
import com.ruoyi.system.service.INcZaishaService;
import com.ruoyi.common.core.text.Convert;

/**
 * 宰杀 服务层实现
 * 
 * @author ruoyi
 * @date 2019-09-04
 */
@Service
public class NcZaishaServiceImpl implements INcZaishaService 
{
	@Autowired
	private NcZaishaMapper ncZaishaMapper;

	/**
     * 查询宰杀信息
     * 
     * @param zaishaId 宰杀ID
     * @return 宰杀信息
     */
    @Override
	public NcZaisha selectNcZaishaById(Long zaishaId)
	{
	    return ncZaishaMapper.selectNcZaishaById(zaishaId);
	}
	
	/**
     * 查询宰杀列表
     * 
     * @param ncZaisha 宰杀信息
     * @return 宰杀集合
     */
	@Override
	public List<NcZaisha> selectNcZaishaList(NcZaisha ncZaisha)
	{
	    return ncZaishaMapper.selectNcZaishaList(ncZaisha);
	}
	
    /**
     * 新增宰杀
     * 
     * @param ncZaisha 宰杀信息
     * @return 结果
     */
	@Override
	public int insertNcZaisha(NcZaisha ncZaisha)
	{
	    return ncZaishaMapper.insertNcZaisha(ncZaisha);
	}
	
	/**
     * 修改宰杀
     * 
     * @param ncZaisha 宰杀信息
     * @return 结果
     */
	@Override
	public int updateNcZaisha(NcZaisha ncZaisha)
	{
	    return ncZaishaMapper.updateNcZaisha(ncZaisha);
	}

	/**
     * 删除宰杀对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteNcZaishaByIds(String ids)
	{
		return ncZaishaMapper.deleteNcZaishaByIds(Convert.toStrArray(ids));
	}
	
}
