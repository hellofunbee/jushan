package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.NcStageMapper;
import com.ruoyi.system.domain.NcStage;
import com.ruoyi.system.service.INcStageService;
import com.ruoyi.common.core.text.Convert;

/**
 * 生产（育苗）阶段 服务层实现
 * 
 * @author ruoyi
 * @date 2019-07-28
 */
@Service
public class NcStageServiceImpl implements INcStageService 
{
	@Autowired
	private NcStageMapper ncStageMapper;

	/**
     * 查询生产（育苗）阶段信息
     * 
     * @param stageId 生产（育苗）阶段ID
     * @return 生产（育苗）阶段信息
     */
    @Override
	public NcStage selectNcStageById(Long stageId)
	{
	    return ncStageMapper.selectNcStageById(stageId);
	}
	
	/**
     * 查询生产（育苗）阶段列表
     * 
     * @param ncStage 生产（育苗）阶段信息
     * @return 生产（育苗）阶段集合
     */
	@Override
	public List<NcStage> selectNcStageList(NcStage ncStage)
	{
	    return ncStageMapper.selectNcStageList(ncStage);
	}
	
    /**
     * 新增生产（育苗）阶段
     * 
     * @param ncStage 生产（育苗）阶段信息
     * @return 结果
     */
	@Override
	public int insertNcStage(NcStage ncStage)
	{
	    return ncStageMapper.insertNcStage(ncStage);
	}
	
	/**
     * 修改生产（育苗）阶段
     * 
     * @param ncStage 生产（育苗）阶段信息
     * @return 结果
     */
	@Override
	public int updateNcStage(NcStage ncStage)
	{
	    return ncStageMapper.updateNcStage(ncStage);
	}

	/**
     * 删除生产（育苗）阶段对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteNcStageByIds(String ids)
	{
		return ncStageMapper.deleteNcStageByIds(Convert.toStrArray(ids));
	}
	
}
