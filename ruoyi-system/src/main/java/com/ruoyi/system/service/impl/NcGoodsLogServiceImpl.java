package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.text.Convert;
import com.ruoyi.system.domain.NcGoodsLog;
import com.ruoyi.system.mapper.NcGoodsLogMapper;
import com.ruoyi.system.service.INcGoodsLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 物资仓储出入记录 服务层实现
 *
 * @author ruoyi
 * @date 2019-08-06
 */
@Service
public class NcGoodsLogServiceImpl implements INcGoodsLogService
{
	@Autowired
	private NcGoodsLogMapper ncGoodsLogMapper;

	/**
     * 查询物资仓储出入记录信息
     *
     * @param goodsLogId 物资仓储出入记录ID
     * @return 物资仓储出入记录信息
     */
    @Override
	public NcGoodsLog selectNcGoodsLogById(Long goodsLogId)
	{
	    return ncGoodsLogMapper.selectNcGoodsLogById(goodsLogId);
	}

	@Override
	public List<NcGoodsLog> selectNcGoodsLogByGId(Long goodsId) {
		return ncGoodsLogMapper.selectNcGoodsLogByGId(goodsId);
	}

	/**
     * 查询物资仓储出入记录列表
     *
     * @param ncGoodsLog 物资仓储出入记录信息
     * @return 物资仓储出入记录集合
     */
	@Override
	public List<NcGoodsLog> selectNcGoodsLogList(NcGoodsLog ncGoodsLog)
	{
	    return ncGoodsLogMapper.selectNcGoodsLogList(ncGoodsLog);
	}

    /**
     * 新增物资仓储出入记录
     *
     * @param ncGoodsLog 物资仓储出入记录信息
     * @return 结果
     */
	@Override
	public int insertNcGoodsLog(NcGoodsLog ncGoodsLog)
	{
	    return ncGoodsLogMapper.insertNcGoodsLog(ncGoodsLog);
	}

	/**
     * 修改物资仓储出入记录
     *
     * @param ncGoodsLog 物资仓储出入记录信息
     * @return 结果
     */
	@Override
	public int updateNcGoodsLog(NcGoodsLog ncGoodsLog)
	{
	    return ncGoodsLogMapper.updateNcGoodsLog(ncGoodsLog);
	}

	/**
     * 删除物资仓储出入记录对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteNcGoodsLogByIds(String ids)
	{
		return ncGoodsLogMapper.deleteNcGoodsLogByIds(Convert.toStrArray(ids));
	}

}
