package com.ruoyi.system.service;

import com.ruoyi.system.domain.NcCai;
import com.ruoyi.system.domain.NcCheck;
import com.ruoyi.system.domain.SysUser;

import java.util.List;

/**
 * 检测 服务层
 * 
 * @author ruoyi
 * @date 2019-08-29
 */
public interface INcCheckService 
{
	/**
     * 查询检测信息
     * 
     * @param checkId 检测ID
     * @return 检测信息
     */
	public NcCheck selectNcCheckById(Long checkId);
	
	/**
     * 查询检测列表
     * 
     * @param ncCheck 检测信息
     * @return 检测集合
     */
	public List<NcCheck> selectNcCheckList(NcCheck ncCheck);
	
	/**
     * 新增检测
     * 
     * @param ncCheck 检测信息
     * @return 结果
     */
	public int insertNcCheck(NcCheck ncCheck);
	
	/**
     * 修改检测
     * 
     * @param ncCheck 检测信息
     * @return 结果
     */
	public int updateNcCheck(NcCheck ncCheck);
		
	/**
     * 删除检测信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcCheckByIds(String ids);

    int createOrderByIds(String ids, SysUser user);

    List<NcCai> getBigCaiClass(NcCheck check);
}
