package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.NcWorkMirrorMapper;
import com.ruoyi.system.domain.NcWorkMirror;
import com.ruoyi.system.service.INcWorkMirrorService;
import com.ruoyi.common.core.text.Convert;

/**
 * 标准农事镜像 服务层实现
 * 
 * @author ruoyi
 * @date 2019-09-02
 */
@Service
public class NcWorkMirrorServiceImpl implements INcWorkMirrorService 
{
	@Autowired
	private NcWorkMirrorMapper ncWorkMirrorMapper;

	/**
     * 查询标准农事镜像信息
     * 
     * @param workId 标准农事镜像ID
     * @return 标准农事镜像信息
     */
    @Override
	public NcWorkMirror selectNcWorkMirrorById(Long workId)
	{
	    return ncWorkMirrorMapper.selectNcWorkMirrorById(workId);
	}
	
	/**
     * 查询标准农事镜像列表
     * 
     * @param ncWorkMirror 标准农事镜像信息
     * @return 标准农事镜像集合
     */
	@Override
	public List<NcWorkMirror> selectNcWorkMirrorList(NcWorkMirror ncWorkMirror)
	{
	    return ncWorkMirrorMapper.selectNcWorkMirrorList(ncWorkMirror);
	}
	
    /**
     * 新增标准农事镜像
     * 
     * @param ncWorkMirror 标准农事镜像信息
     * @return 结果
     */
	@Override
	public int insertNcWorkMirror(NcWorkMirror ncWorkMirror)
	{
	    return ncWorkMirrorMapper.insertNcWorkMirror(ncWorkMirror);
	}
	
	/**
     * 修改标准农事镜像
     * 
     * @param ncWorkMirror 标准农事镜像信息
     * @return 结果
     */
	@Override
	public int updateNcWorkMirror(NcWorkMirror ncWorkMirror)
	{
	    return ncWorkMirrorMapper.updateNcWorkMirror(ncWorkMirror);
	}

	/**
     * 删除标准农事镜像对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteNcWorkMirrorByIds(String ids)
	{
		return ncWorkMirrorMapper.deleteNcWorkMirrorByIds(Convert.toStrArray(ids));
	}
	
}
