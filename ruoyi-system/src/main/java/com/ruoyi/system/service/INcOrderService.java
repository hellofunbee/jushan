package com.ruoyi.system.service;

import com.ruoyi.system.domain.NcOrder;
import java.util.List;

/**
 * 订单主（系统中的所有订单共用此） 服务层
 * 
 * @author ruoyi
 * @date 2019-07-31
 */
public interface INcOrderService 
{
	/**
     * 查询订单主（系统中的所有订单共用此）信息
     * 
     * @param orderId 订单主（系统中的所有订单共用此）ID
     * @return 订单主（系统中的所有订单共用此）信息
     */
	public NcOrder selectNcOrderById(Long orderId);
	
	/**
     * 查询订单主（系统中的所有订单共用此）列表
     * 
     * @param ncOrder 订单主（系统中的所有订单共用此）信息
     * @return 订单主（系统中的所有订单共用此）集合
     */
	public List<NcOrder> selectNcOrderList(NcOrder ncOrder);
	
	/**
     * 新增订单主（系统中的所有订单共用此）
     * 
     * @param ncOrder 订单主（系统中的所有订单共用此）信息
     * @return 结果
     */
	public int insertNcOrder(NcOrder ncOrder);
	
	/**
     * 修改订单主（系统中的所有订单共用此）
     * 
     * @param ncOrder 订单主（系统中的所有订单共用此）信息
     * @return 结果
     */
	public int updateNcOrder(NcOrder ncOrder);
		
	/**
     * 删除订单主（系统中的所有订单共用此）信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcOrderByIds(String ids);
	
}
