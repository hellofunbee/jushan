package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.system.domain.NcBullJingye;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.mapper.NcBullJingyeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.NcBullJingyeLogMapper;
import com.ruoyi.system.domain.NcBullJingyeLog;
import com.ruoyi.system.service.INcBullJingyeLogService;
import com.ruoyi.common.core.text.Convert;
import org.springframework.transaction.annotation.Transactional;

/**
 * 牛舍精液 服务层实现
 *
 * @author ruoyi
 * @date 2019-08-30
 */
@Service
public class NcBullJingyeLogServiceImpl implements INcBullJingyeLogService {
    @Autowired
    private NcBullJingyeLogMapper ncBullJingyeLogMapper;

    @Autowired
    private NcBullJingyeMapper ncBullJingyeMapper;

    /**
     * 查询牛舍精液信息
     *
     * @param jingyeLogId 牛舍精液ID
     * @return 牛舍精液信息
     */
    @Override
    public NcBullJingyeLog selectNcBullJingyeLogById(Long jingyeLogId) {
        return ncBullJingyeLogMapper.selectNcBullJingyeLogById(jingyeLogId);
    }

    /**
     * 查询牛舍精液列表
     *
     * @param ncBullJingyeLog 牛舍精液信息
     * @return 牛舍精液集合
     */
    @Override
    public List<NcBullJingyeLog> selectNcBullJingyeLogList(NcBullJingyeLog ncBullJingyeLog) {
        return ncBullJingyeLogMapper.selectNcBullJingyeLogList(ncBullJingyeLog);
    }

    /**
     * 新增牛舍精液
     *
     * @param ncBullJingyeLog 牛舍精液信息
     * @return 结果
     */
    @Override
    public int insertNcBullJingyeLog(NcBullJingyeLog ncBullJingyeLog) {


        return ncBullJingyeLogMapper.insertNcBullJingyeLog(ncBullJingyeLog);
    }

    /**
     * 修改牛舍精液
     *
     * @param ncBullJingyeLog 牛舍精液信息
     * @return 结果
     */
    @Override
    public int updateNcBullJingyeLog(NcBullJingyeLog ncBullJingyeLog) {
        return ncBullJingyeLogMapper.updateNcBullJingyeLog(ncBullJingyeLog);
    }

    /**
     * 删除牛舍精液对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteNcBullJingyeLogByIds(String ids) {
        return ncBullJingyeLogMapper.deleteNcBullJingyeLogByIds(Convert.toStrArray(ids));
    }

    @Transactional
    @Override
    public boolean checkOutBullJingye(NcBullJingyeLog ncBullJingyeLog, SysUser sysUser) {
        //经手人
        ncBullJingyeLog.setCreateBy(sysUser.getLoginName());

        NcBullJingye ncBullJingye = ncBullJingyeMapper.selectNcBullJingyeById(ncBullJingyeLog.getJingyeId());

        //现有库存数量
        Integer stockAmoutNow = ncBullJingye.getStockAmout();
        //出库数量
        Integer inputAmount = ncBullJingyeLog.getInputAmount();


        if (inputAmount > 0 && stockAmoutNow >= inputAmount) {
            //出库
            //减少库存
            ncBullJingye.setStockAmout(ncBullJingye.getStockAmout() - inputAmount);
            updateCowJingYeAndUpdateLog(ncBullJingyeLog, ncBullJingye);
            return true;
        }
        //数量不足 出库失败
        return false;
    }

    @Transactional
    void updateCowJingYeAndUpdateLog(NcBullJingyeLog ncBullJingyeLog, NcBullJingye ncBullJingye) {
        ncBullJingyeMapper.updateNcBullJingye(ncBullJingye);
        //添加出库记录
        ncBullJingyeLogMapper.insertNcBullJingyeLog(ncBullJingyeLog);
    }

}
