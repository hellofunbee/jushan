package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.NcUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.NcGoods;
import com.ruoyi.system.domain.NcZhibao;
import com.ruoyi.system.mapper.NcGoodsLogMapper;
import com.ruoyi.system.mapper.NcGoodsMapper;
import com.ruoyi.system.service.INcGoodsService;
import com.ruoyi.system.service.INcOrderCfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 物资仓储存 服务层实现
 *
 * @author ruoyi
 * @date 2019-08-05
 */
@Service
public class NcGoodsServiceImpl implements INcGoodsService
{
	@Autowired
	private NcGoodsMapper ncGoodsMapper;
	@Autowired
	private NcGoodsLogMapper ncGoodsLogMapper;

	@Autowired
	private INcOrderCfService orderCfService;

	/**
     * 查询物资仓储存信息
     *
     * @param goodsId 物资仓储存ID
     * @return 物资仓储存信息
     */
    @Override
	public NcGoods selectNcGoodsById(Long goodsId)
	{
	    return ncGoodsMapper.selectNcGoodsById(goodsId);
	}

	/**
     * 查询物资仓储存列表
     *
     * @param ncGoods 物资仓储存信息
     * @return 物资仓储存集合
     */
	@Override
	public List<NcGoods> selectNcGoodsList(NcGoods ncGoods)
	{
	    return ncGoodsMapper.selectNcGoodsList(ncGoods);
	}

    /**
     * 新增物资仓储存
     *
     * @param ncGoods 物资仓储存信息
     * @return 结果
     */
	@Override
	public int insertNcGoods(NcGoods ncGoods)
	{
		//新增物资前查看数据库是否有该物资 有(增加数量) 没有(新增数据)
		/*List<NcGoods> meterialIdList = ncGoodsMapper.selectNcGoodsBymeterialId(ncGoods.getMeterialId());
		if(meterialIdList.size() !=0 && meterialIdList != null ){
			//修改入库数量 input_amount
			int i = ncGoodsMapper.updateNcGoodsById(ncGoods.getInputAmount());
			return i;
		}else {
			return ncGoodsMapper.insertNcGoods(ncGoods);

		}*/

		return ncGoodsMapper.insertNcGoods(ncGoods);
	}

	/**
     * 修改物资仓储存
     *
     * @param ncGoods 物资仓储存信息
     * @return 结果
     */
	@Override
	public int updateNcGoods(NcGoods ncGoods)
	{
	    return ncGoodsMapper.updateNcGoods(ncGoods);
	}

	/**
     * 删除物资仓储存对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteNcGoodsByIds(String ids)
	{
		return ncGoodsMapper.deleteNcGoodsByIds(Convert.toStrArray(ids));
	}

	@Override
	public Long selectmeterialIdByName(String meterialName) {
		return ncGoodsMapper.selectmeterialIdByName(meterialName);
	}

	@Override
	public List<NcGoods> selectNcGoodsByMeterialId(String type, String beginTime, String endTime) {

		return  ncGoodsMapper.selectNcGoodsSortBymeterialId(type,beginTime,endTime).stream().filter(m -> m.getReceiver()!=null).collect(Collectors.toList());
	}

	@Override
	public List<Map> selectCountByTime(Integer spanType, String beginTime, String endTime, String metialType) {
		Date s = null;
		Date e = null;
		Map se = orderCfService.calTimeSpan(s, e, beginTime, endTime, spanType);
		s = (Date) se.get("s");
		e = (Date) se.get("e");
		return getSumByDate(spanType, s, e,metialType);
	}

	private List<Map> getSumByDate(Integer spanType, Date s, Date e,String metialType) {
		List<Map> result = new ArrayList<>();
		if (spanType == 1) {
			while (e.getTime() >= s.getTime()) {
				String dateSpan = DateUtils.parseDateToStr("yyyy", s);

				Map m = new HashMap();
				m.put("x", dateSpan);
				m.put("count", getSumByDate(dateSpan, spanType,metialType));
				result.add(m);

				s = DateUtils.addYears(s, 1);
			}
		} else if (spanType == 2) {
			while (e.getTime() >= s.getTime()) {
				String dateSpan = DateUtils.parseDateToStr("yyyy-MM", s);
				Map m = new HashMap();
				m.put("x", dateSpan);
				m.put("count", getSumByDate(dateSpan, spanType,metialType));
				result.add(m);
				s = DateUtils.addMonths(s, 1);
			}
		} else if (spanType == 3) {
			while (e.getTime() >= s.getTime()) {
				String dateSpan = DateUtils.parseDateToStr("yyyy-MM-dd", s);
				Map m = new HashMap();
				m.put("x", dateSpan);
				m.put("count", getSumByDate(dateSpan, spanType,metialType));
				result.add(m);
				s = DateUtils.addDays(s, 1);
			}
		}
		return result;
	}
	private Long getSumByDate(String dateSpan, Integer dataSpan,String metialType) {
		Map map = new HashMap();
		if (dataSpan == null || dataSpan == 2) {
			map.put("dateSpanMonth", dateSpan);
		} else if (dataSpan == 1) {
			map.put("dateSpanYear", dateSpan);
		} else if (dataSpan == 3) {
			map.put("dateSpanDay", dateSpan);
		}
		map.put("metialType",metialType);
		if("all".equals(metialType)){
			Long longs = ncGoodsLogMapper.selectNcGoodsLogListByData(map);
			return longs == null ? 0 : longs;
		}
		Long longs = ncGoodsLogMapper.selectNcGoodsLogListByDataAndMetar(map);
		return longs == null ? 0 : longs;
	}

}
