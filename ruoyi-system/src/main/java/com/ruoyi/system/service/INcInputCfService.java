package com.ruoyi.system.service;

import com.ruoyi.system.domain.NcInputCf;
import com.ruoyi.system.domain.SysUser;

import java.util.List;

/**
 * 菜房入库 服务层
 * 
 * @author ruoyi
 * @date 2019-08-30
 */
public interface INcInputCfService 
{
	/**
     * 查询菜房入库信息
     * 
     * @param inputId 菜房入库ID
     * @return 菜房入库信息
     */
	public NcInputCf selectNcInputCfById(Integer inputId);
	
	/**
     * 查询菜房入库列表
     * 
     * @param ncInputCf 菜房入库信息
     * @return 菜房入库集合
     */
	public List<NcInputCf> selectNcInputCfList(NcInputCf ncInputCf);
	
	/**
     * 新增菜房入库
     * 
     * @param ncInputCf 菜房入库信息
     * @return 结果
     */
	public int insertNcInputCf(NcInputCf ncInputCf);
	
	/**
     * 修改菜房入库
     * 
     * @param ncInputCf 菜房入库信息
     * @return 结果
     */
	public int updateNcInputCf(NcInputCf ncInputCf);
		
	/**
     * 删除菜房入库信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcInputCfByIds(String ids);

    int createInputOrderByIds(String ids, SysUser user,Integer caifangType);
	public double selectCFinPutScreen(NcInputCf inputCf);
}
