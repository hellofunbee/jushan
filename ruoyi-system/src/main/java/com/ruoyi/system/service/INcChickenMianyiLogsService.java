package com.ruoyi.system.service;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.system.domain.NcMianyiLogs;
import com.ruoyi.system.domain.SysUser;

import java.util.List;

/**
 * 鸡免疫记录 服务层
 * 
 * @author ruoyi
 * @date 2019-09-02
 */
public interface INcChickenMianyiLogsService
{
	/**
     * 查询免疫记录信息
     * 
     * @param myId 免疫记录ID
     * @return 免疫记录信息
     */
	public NcMianyiLogs selectNcMianyiLogsById(Long myId);
	
	/**
     * 查询免疫记录列表
     * 
     * @param ncMianyiLogs 免疫记录信息
     * @return 免疫记录集合
     */
	public List<NcMianyiLogs> selectNcMianyiLogsList(NcMianyiLogs ncMianyiLogs);
	
	/**
     * 新增免疫记录
     * 
     * @param ncMianyiLogs 免疫记录信息
     * @return 结果
     */
	public AjaxResult insertNcMianyiLogs(NcMianyiLogs ncMianyiLogs);
	
	/**
     * 修改免疫记录
     * 
     * @param ncMianyiLogs 免疫记录信息
     * @return 结果
     */
	public int updateNcMianyiLogs(NcMianyiLogs ncMianyiLogs);
		
	/**
     * 删除免疫记录信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcMianyiLogsByIds(String ids);

	/**
	 * 创建牛舍免疫单
	 * @param ids
	 * @param user
	 * @return
	 */
	int createZlOrderByIds(String ids, SysUser user);
}
