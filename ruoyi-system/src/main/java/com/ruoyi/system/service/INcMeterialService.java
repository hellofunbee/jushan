package com.ruoyi.system.service;

import com.ruoyi.common.core.domain.Ztree;
import com.ruoyi.system.domain.NcGoods;
import com.ruoyi.system.domain.NcMeterial;
import com.ruoyi.system.domain.NcMeterialForStandard;
import com.ruoyi.system.domain.SysRole;
import org.springframework.ui.ModelMap;

import java.util.List;

/**
 * 物资管理 服务层
 *
 * @author ruoyi
 */
public interface INcMeterialService
{
    /**
     * 查询物资管理数据
     *
     * @param meterial  物资信息
     * @return 物资信息集合
     */
    public List<NcMeterial > selectMeterialList(NcMeterial meterial );




    /**
     * 查询物资管理数据 携带物资类别(父级)
     *
     * @param meterial 物资信息
     * @return 物资信息集合
     */

    public List<NcMeterialForStandard> selectMeterialListHasParent(NcMeterialForStandard meterial);
    /**
     * 查询物资管理树
     *
     * @param meterial 物资信息
     * @return 所有物资信息
     */
    public List<Ztree> selectMeterialTree(NcMeterial meterial);

    /**
     * 根据角色ID查询菜单
     *
     * @param role 角色对象
     * @return 菜单列表
     */
    public List<Ztree> roleMeterialTreeData(SysRole role);

    /**
     * 查询物资人数
     *
     * @param parentId 父物资ID
     * @return 结果
     */
    public int selectMeterialCount(Long parentId);

    /**
     * 查询物资是否存在用户
     *
     * @param meterialId 物资ID
     * @return 结果 true 存在 false 不存在
     */
    public boolean checkMeterialExistUser(Long meterialId);

    /**
     * 删除物资管理信息
     *
     * @param meterialId 物资ID
     * @return 结果
     */
    public int deleteMeterialById(Long meterialId);

    /**
     * 新增保存物资信息
     *
     * @param meterial 物资信息
     * @return 结果
     */
    public int insertMeterial(NcMeterial meterial);

    /**
     * 修改保存物资信息
     *
     * @param meterial 物资信息
     * @return 结果
     */
    public int updateMeterial(NcMeterial meterial);

    /**
     * 根据物资ID查询信息
     *
     * @param meterialId 物资ID
     * @return 物资信息
     */
    public NcMeterial selectMeterialById(Long meterialId);

    /**
     * 校验物资名称是否唯一
     *
     * @param meterial 物资信息
     * @return 结果
     */
    public String checkMeterialNameUnique(NcMeterial meterial);


    public List<NcMeterial> selectMeterialIdByName(String meterialName);

    /**
     *
     * @param startTime
     * @param endTime
     * @return 前端的物资管理分类
     */
    Object frontSelectNcGoodsMeter(String startTime, String endTime);

    void SelectNcGoodsMeter(ModelMap mmap, NcGoods ncGoods);

    /**
     * 查询第一大类的物资信息
     * @param i
     * @return
     */
    List<NcMeterial> selectMeterialFirstSort(int i);
    List<NcMeterial> selectMeterialParentId(long parentId);
}
