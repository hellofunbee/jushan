package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.text.Convert;
import com.ruoyi.system.domain.NcWorkNotice;
import com.ruoyi.system.mapper.NcWorkNoticeMapper;
import com.ruoyi.system.service.INcWorkNoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 工作提醒 服务层实现
 *
 * @author ruoyi
 * @date 2019-09-04
 */
@Service
public class NcWorkNoticeServiceImpl implements INcWorkNoticeService
{
	@Autowired
	private NcWorkNoticeMapper ncWorkNoticeMapper;

	/**
     * 查询工作提醒信息
     *
     * @param workNoticeId 工作提醒ID
     * @return 工作提醒信息
     */
    @Override
	public NcWorkNotice selectNcWorkNoticeById(Long workNoticeId)
	{
	    return ncWorkNoticeMapper.selectNcWorkNoticeById(workNoticeId);
	}

	/**
     * 查询工作提醒列表
     *
     * @param ncWorkNotice 工作提醒信息
     * @return 工作提醒集合
     */
	@Override
	public List<NcWorkNotice> selectNcWorkNoticeList(NcWorkNotice ncWorkNotice)
	{
	    return ncWorkNoticeMapper.selectNcWorkNoticeList(ncWorkNotice);
	}

    /**
     * 新增工作提醒
     *
     * @param ncWorkNotice 工作提醒信息
     * @return 结果
     */
	@Override
	public int insertNcWorkNotice(NcWorkNotice ncWorkNotice)
	{
	    return ncWorkNoticeMapper.insertNcWorkNotice(ncWorkNotice);
	}

	/**
     * 修改工作提醒
     *
     * @param ncWorkNotice 工作提醒信息
     * @return 结果
     */
	@Override
	public int updateNcWorkNotice(NcWorkNotice ncWorkNotice)
	{
	    return ncWorkNoticeMapper.updateNcWorkNotice(ncWorkNotice);
	}

	/**
     * 删除工作提醒对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteNcWorkNoticeByIds(String ids)
	{
		return ncWorkNoticeMapper.deleteNcWorkNoticeByIds(Convert.toStrArray(ids));
	}

}
