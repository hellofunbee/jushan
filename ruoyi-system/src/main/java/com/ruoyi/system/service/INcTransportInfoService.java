package com.ruoyi.system.service;

import com.ruoyi.system.domain.NcTransportInfo;
import java.util.List;

/**
 * 菜房出库 服务层
 * 
 * @author ruoyi
 * @date 2019-09-05
 */
public interface INcTransportInfoService 
{
	/**
     * 查询菜房出库信息
     * 
     * @param transId 菜房出库ID
     * @return 菜房出库信息
     */
	public NcTransportInfo selectNcTransportInfoById(Integer transId);
	
	/**
     * 查询菜房出库列表
     * 
     * @param ncTransportInfo 菜房出库信息
     * @return 菜房出库集合
     */
	public List<NcTransportInfo> selectNcTransportInfoList(NcTransportInfo ncTransportInfo);
	
	/**
     * 新增菜房出库
     * 
     * @param ncTransportInfo 菜房出库信息
     * @return 结果
     */
	public int insertNcTransportInfo(NcTransportInfo ncTransportInfo);
	
	/**
     * 修改菜房出库
     * 
     * @param ncTransportInfo 菜房出库信息
     * @return 结果
     */
	public int updateNcTransportInfo(NcTransportInfo ncTransportInfo);
		
	/**
     * 删除菜房出库信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcTransportInfoByIds(String ids);
	
}
