package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.text.Convert;
import com.ruoyi.system.domain.NcFeed;
import com.ruoyi.system.mapper.NcFeedMapper;
import com.ruoyi.system.service.INcFeedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 饲料库存 服务层实现
 *
 * @author ruoyi
 * @date 2019-09-10
 */
@Service
public class NcFeedServiceImpl implements INcFeedService
{
	@Autowired
	private NcFeedMapper ncFeedMapper;

	/**
     * 查询饲料库存信息
     *
     * @param feedId 饲料库存ID
     * @return 饲料库存信息
     */
    @Override
	public NcFeed selectNcFeedById(Long feedId)
	{
	    return ncFeedMapper.selectNcFeedById(feedId);
	}

	/**
     * 查询饲料库存列表
     *
     * @param ncFeed 饲料库存信息
     * @return 饲料库存集合
     */
	@Override
	public List<NcFeed> selectNcFeedList(NcFeed ncFeed)
	{
	    return ncFeedMapper.selectNcFeedList(ncFeed);
	}

    /**
     * 新增饲料库存
     *
     * @param ncFeed 饲料库存信息
     * @return 结果
     */
	@Override
	public int insertNcFeed(NcFeed ncFeed)
	{
	    return ncFeedMapper.insertNcFeed(ncFeed);
	}

	/**
     * 修改饲料库存
     *
     * @param ncFeed 饲料库存信息
     * @return 结果
     */
	@Override
	public int updateNcFeed(NcFeed ncFeed)
	{
	    return ncFeedMapper.updateNcFeed(ncFeed);
	}

	/**
     * 删除饲料库存对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteNcFeedByIds(String ids)
	{
		return ncFeedMapper.deleteNcFeedByIds(Convert.toStrArray(ids));
	}

	@Override
	public Integer stockAmout(Integer feedType) {

		return ncFeedMapper.stockAmout(feedType);
	}

}
