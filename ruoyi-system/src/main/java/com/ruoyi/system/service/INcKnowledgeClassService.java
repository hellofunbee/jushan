package com.ruoyi.system.service;

import com.ruoyi.system.domain.NcKnowledgeClass;
import com.ruoyi.system.domain.SysUser;

import java.util.List;

/**
 * 知识课堂 服务层
 * 
 * @author ruoyi
 * @date 2019-09-10
 */
public interface INcKnowledgeClassService 
{
	/**
     * 查询知识课堂信息
     * 
     * @param kcId 知识课堂ID
     * @return 知识课堂信息
     */
	public NcKnowledgeClass selectNcKnowledgeClassById(Long kcId);
	
	/**
     * 查询知识课堂列表
     * 
     * @param ncKnowledgeClass 知识课堂信息
     * @return 知识课堂集合
     */
	public List<NcKnowledgeClass> selectNcKnowledgeClassList(NcKnowledgeClass ncKnowledgeClass);


	/**
	 * 查询知识课堂列表（前台分页）
	 *
	 * @param ncKnowledgeClass 知识课堂信息
	 * @return 知识课堂集合
	 */
	public List<NcKnowledgeClass> listNcKnowledgeClassList(NcKnowledgeClass ncKnowledgeClass);


	/**
	 * 查询首页四篇知识课堂
	 *
	 * @param kctypeId 知识课堂信息
	 * @return 知识课堂集合
	 */
	public List<NcKnowledgeClass> selectFourNcKnowledgeClassList(Long kctypeId);

	/**
     * 新增知识课堂
     *
     * @param ncKnowledgeClass 知识课堂信息
     * @return 结果
     */
	public int insertNcKnowledgeClass(NcKnowledgeClass ncKnowledgeClass, SysUser sysUserf);

	/**
     * 修改知识课堂
     *
     * @param ncKnowledgeClass 知识课堂信息
     * @return 结果
     */
	public int updateNcKnowledgeClass(NcKnowledgeClass ncKnowledgeClass, SysUser sysUser);
		
	/**
     * 删除知识课堂信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcKnowledgeClassByIds(String ids);
	
}
