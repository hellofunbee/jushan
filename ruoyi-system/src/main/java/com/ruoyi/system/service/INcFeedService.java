package com.ruoyi.system.service;

import com.ruoyi.system.domain.NcFeed;
import java.util.List;

/**
 * 饲料库存 服务层
 * 
 * @author ruoyi
 * @date 2019-09-10
 */
public interface INcFeedService 
{
	/**
     * 查询饲料库存信息
     * 
     * @param feedId 饲料库存ID
     * @return 饲料库存信息
     */
	public NcFeed selectNcFeedById(Long feedId);
	
	/**
     * 查询饲料库存列表
     * 
     * @param ncFeed 饲料库存信息
     * @return 饲料库存集合
     */
	public List<NcFeed> selectNcFeedList(NcFeed ncFeed);
	
	/**
     * 新增饲料库存
     * 
     * @param ncFeed 饲料库存信息
     * @return 结果
     */
	public int insertNcFeed(NcFeed ncFeed);
	
	/**
     * 修改饲料库存
     * 
     * @param ncFeed 饲料库存信息
     * @return 结果
     */
	public int updateNcFeed(NcFeed ncFeed);
		
	/**
     * 删除饲料库存信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcFeedByIds(String ids);
	//查询鸡的库存数量
	public Integer stockAmout(Integer feedType);
	
}
