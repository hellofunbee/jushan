package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.NcWorkLogMapper;
import com.ruoyi.system.domain.NcWorkLog;
import com.ruoyi.system.service.INcWorkLogService;
import com.ruoyi.common.core.text.Convert;

/**
 * 农事记录 服务层实现
 * 
 * @author ruoyi
 * @date 2019-09-01
 */
@Service
public class NcWorkLogServiceImpl implements INcWorkLogService 
{
	@Autowired
	private NcWorkLogMapper ncWorkLogMapper;

	/**
     * 查询农事记录信息
     * 
     * @param zaipeiId 农事记录ID
     * @return 农事记录信息
     */
    @Override
	public NcWorkLog selectNcWorkLogById(Long zaipeiId)
	{
	    return ncWorkLogMapper.selectNcWorkLogById(zaipeiId);
	}
	
	/**
     * 查询农事记录
     * 
     * @param ncWorkLog 农事记录信息
     * @return 农事记录集合
     */
	@Override
	public List<NcWorkLog> selectNcWorkLogList(NcWorkLog ncWorkLog)
	{
	    return ncWorkLogMapper.selectNcWorkLogList(ncWorkLog);
	}
	
    /**
     * 新增农事记录
     * 
     * @param ncWorkLog 农事记录信息
     * @return 结果
     */
	@Override
	public int insertNcWorkLog(NcWorkLog ncWorkLog)
	{
	    return ncWorkLogMapper.insertNcWorkLog(ncWorkLog);
	}
	
	/**
     * 修改农事记录
     * 
     * @param ncWorkLog 农事记录信息
     * @return 结果
     */
	@Override
	public int updateNcWorkLog(NcWorkLog ncWorkLog)
	{
	    return ncWorkLogMapper.updateNcWorkLog(ncWorkLog);
	}

	/**
     * 删除农事记录对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteNcWorkLogByIds(String ids)
	{
		return ncWorkLogMapper.deleteNcWorkLogByIds(Convert.toStrArray(ids));
	}
	
}
