package com.ruoyi.system.service;

import com.ruoyi.system.domain.MonthCensus;

public interface INcMonthCensusService {
    public MonthCensus lMonthStackAmount(MonthCensus monthCensus);
    public MonthCensus lZhiAmount(MonthCensus monthCensus);
    public MonthCensus monthAddAmount(MonthCensus monthCensus);
    public MonthCensus monthAddZhiAmount(MonthCensus monthCensus);
    public MonthCensus monthReduceZhiAmount(MonthCensus monthCensus);
    public MonthCensus monthReduceNeiAmount(MonthCensus monthCensus);
    public MonthCensus monthReduceSiAmount(MonthCensus monthCensus);
    public MonthCensus chickenAmount(MonthCensus monthCensus);
    public MonthCensus feedAmount(MonthCensus monthCensus);

}
