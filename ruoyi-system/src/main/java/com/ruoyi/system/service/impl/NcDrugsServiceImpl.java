package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.exception.BusinessException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.NcDrugs;
import com.ruoyi.system.domain.NcYedan;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.mapper.NcDrugsMapper;
import com.ruoyi.system.service.INcDrugsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 兽药库存 服务层实现
 *
 * @author ruoyi
 * @date 2019-09-02
 */
@Service
public class NcDrugsServiceImpl implements INcDrugsService
{
	@Autowired
	private NcDrugsMapper ncDrugsMapper;
	private static final Logger log = LoggerFactory.getLogger(NcYedanServiceImpl.class);

	/**
     * 查询兽药库存信息
     *
     * @param drugId 兽药库存ID
     * @return 兽药库存信息
     */
    @Override
	public NcDrugs selectNcDrugsById(Long drugId)
	{
	    return ncDrugsMapper.selectNcDrugsById(drugId);
	}

	/**
     * 查询兽药库存列表
     *
     * @param ncDrugs 兽药库存信息
     * @return 兽药库存集合
     */
	@Override
	public List<NcDrugs> selectNcDrugsList(NcDrugs ncDrugs)
	{
	    return ncDrugsMapper.selectNcDrugsList(ncDrugs);
	}

    /**
     * 新增兽药库存
     *
     * @param ncDrugs 兽药库checkOutDrugs存信息
     * @return 结果
     */
	@Override
	public int insertNcDrugs(NcDrugs ncDrugs, SysUser sysUser)
	{

		//药品批号 日期加经手人
		ncDrugs.setDrugCode(DateUtils.dateTime(ncDrugs.getCreateTime()) + "-" +  sysUser.getLoginName());
		//数量
		ncDrugs.setStockAmout(ncDrugs.getInputAmount());
		//经手人
		ncDrugs.setCreateBy(sysUser.getLoginName());
		//采购人
		ncDrugs.setPurchaser(ncDrugs.getPurchaser());


		return ncDrugsMapper.insertNcDrugs(ncDrugs);
	}

	/**
     * 修改兽药库存
     *
     * @param ncDrugs 兽药库存信息
     * @return 结果
     */
	@Override
	public int updateNcDrugs(NcDrugs ncDrugs)
	{
	    return ncDrugsMapper.updateNcDrugs(ncDrugs);
	}

	/**
     * 删除兽药库存对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	@Override
	public int deleteNcDrugsByIds(String ids)
	{
		return ncDrugsMapper.deleteNcDrugsByIds(Convert.toStrArray(ids));
	}

	@Override
	public List<NcDrugs> selectNcDrugsChildList(Long yaoId) {
		return ncDrugsMapper.selectNcDrugsChildList(yaoId);
	}


	/*@Override
	public String importDrugs(List<NcDrugs> drugList, Boolean isUpdateSupport, SysUser sysUser) {
		if (StringUtils.isNull(drugList) || drugList.size() == 0) {
			throw new BusinessException("导入兽药数据不能为空！");
		}
		int successNum = 0;
		int failureNum = 0;
		StringBuilder successMsg = new StringBuilder();
		StringBuilder failureMsg = new StringBuilder();
		for (NcDrugs drug : drugList) {
			try {
				// 验证是否存在这个用户
				drug.setDrugCode(drug.getCreateTime() + drug.getPurchaser());
				this.insertNcDrugs(drug,sysUser);
				successNum++;
				successMsg.append("导入成功");
			} catch (Exception e) {
				failureNum++;
				String msg = "导入失败：";
				failureMsg.append(msg + e.getMessage());
				log.error(msg, e);
			}
		}
		if (failureNum > 0) {
			failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
			throw new BusinessException(failureMsg.toString());
		} else {
			successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
		}
		return successMsg.toString();
	}
*/

}
