package com.ruoyi.system.service;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.system.domain.*;
import org.springframework.ui.ModelMap;
import com.ruoyi.system.domain.querydata.ShowNcStandard;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * 计划 服务层
 *
 * @author ruoyi
 * @date 2019-07-31
 */
public interface INcPlanService {
    /**
     * 查询计划信息
     *
     * @param planId 计划ID
     * @return 计划信息
     */
    NcPlan selectNcPlanById(Long planId);

    /**
     * 查询计划列表
     *
     * @param ncPlan 计划信息
     * @return 计划集合
     */
    List<NcPlan> selectNcPlanList(NcPlan ncPlan);

    /**
     * 查询计划列表 (无用户权限限制)
     * @param plan
     * @return
     */
    List<NcPlan> selectNcPlanListTask(NcPlan plan);


    /**
     * 查询计划列表 导出育苗计划
     *
     * @param ncPlan 计划信息
     * @return 计划集合
     */
    List<NcPlanExprot1> selectNcPlanExprot1(NcPlan ncPlan);

    /**
     * 查询计划列表 导出生产计划
     *
     * @param ncPlan 计划信息
     * @return 计划集合
     */
    List<NcPlanExprot2> selectNcPlanExprot2(NcPlan ncPlan);

    /**
     * 新增计划
     *
     * @param ncPlan 计划信息
     * @return 结果
     */
    int insertNcPlan(NcPlan ncPlan);

    /**
     * 修改计划
     *
     * @param ncPlan 计划信息
     * @return 结果
     */
    int updateNcPlan(NcPlan ncPlan);

    /**
     * 删除计划信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    int deleteNcPlanByIds(String ids);

    NcPlan fillValues(NcPlan ncPlan);

    int createNcPlanByIds(String ids, SysUser user, Integer ORDER_YUMIAO);

    List<Map> selectSumByStatus(NcPlan plan);

    List<NcCai> getBigCaiClass(NcPlan plan);

    Map getStatusCount(NcPlan plan);


    void planDetail(Long planId, String planType, ModelMap mmap);


    List<NcPlan> selectNcPlanListByDeptId(NcPlan ncPlan);

    List<ShowNcStandard> showPlan(int planType);


    int showPlanNum(NcPlan planType);

    void matchStandard(NcPlan ncPlan);


    /***
     *
     * @param ncPlanQUery
     * @return
     */
    List<NcPlan> queryCropNameCnInPlan(NcPlan ncPlanQUery);

    /**
     *   根据作物查询计划集合
     * @return
     */
    List<NcPlan> queryPlanByCropNameCn(String team);

    AjaxResult importNcPlan(SysUser sysUser, Integer planType, MultipartFile file, boolean updateSupport);


    /**
     * 屏保 日光温室种植任务
     * @param ncPlan
     * @return
     */
    List<NcPlan> showPlanLandType4(NcPlan ncPlan);


}
