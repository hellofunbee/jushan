package com.ruoyi.system.service;

import com.ruoyi.system.domain.NcYedan;
import com.ruoyi.system.domain.SysUser;

import java.util.List;

/**
 * 液氮 服务层
 * 
 * @author ruoyi
 * @date 2019-09-02
 */
public interface INcYedanService 
{
	/**
     * 查询液氮信息
     * 
     * @param yedanId 液氮ID
     * @return 液氮信息
     */
	public NcYedan selectNcYedanById(Long yedanId);
	
	/**
     * 查询液氮列表
     * 
     * @param ncYedan 液氮信息
     * @return 液氮集合
     */
	public List<NcYedan> selectNcYedanList(NcYedan ncYedan);
	
	/**
     * 新增液氮
     * 
     * @param ncYedan 液氮信息
     * @return 结果
     */
	public int insertNcYedan(NcYedan ncYedan, SysUser user);
	
	/**
     * 修改液氮
     * 
     * @param ncYedan 液氮信息
     * @return 结果
     */
	public int updateNcYedan(NcYedan ncYedan);
		
	/**
     * 删除液氮信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcYedanByIds(String ids);



	/**
	 * 导入液氮数据
	 *
	 * @param yedanList 液氮数据列表
	 * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
	 * @return 结果
	 */
	public String importYeDan(List<NcYedan> yedanList, Boolean isUpdateSupport, SysUser sysUser);
	
}
