package com.ruoyi.system.service;

import com.ruoyi.system.domain.NcCowsFiles;

import java.util.List;

/**
 * 牛档案 服务层
 *
 * @author ruoyi
 * @date 2019-09-02
 */
public interface INcCowsFilesService
{
	/**
     * 查询牛档案信息
     *
     * @param cowId 牛档案ID
     * @return 牛档案信息
     */
	public NcCowsFiles selectNcCowsFilesById(Integer cowId);

	/**
     * 查询牛档案列表
     *
     * @param ncCowsFiles 牛档案信息
     * @return 牛档案集合
     */
	public List<NcCowsFiles> selectNcCowsFilesList(NcCowsFiles ncCowsFiles);

	/**
     * 新增牛档案
     *
     * @param ncCowsFiles 牛档案信息
     * @return 结果
     */
	public int insertNcCowsFiles(NcCowsFiles ncCowsFiles);

	/**
     * 修改牛档案
     *
     * @param ncCowsFiles 牛档案信息
     * @return 结果
     */
	public int updateNcCowsFiles(NcCowsFiles ncCowsFiles);

	/**
     * 删除牛档案信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcCowsFilesByIds(String ids);


	/**
	 * 根据牛耳号查询牛id
	 * @param CowCode 牛耳号
	 * @return
	 */
	public long selectCowIdByCowCode(String CowCode);


	/**
	 * 查询所有牛耳号
	 * @return
	 */
	public List<String> selectAllCowCode();



	/**
	 * 根据牛耳号查询牛
	 * @param CowCode 牛耳号
	 * @return
	 */
	public NcCowsFiles selectCowByCowCode(String CowCode);







	/**
	 * 查询所有牛
	 */
	public List<NcCowsFiles> selectNcCowsFilesAll();

	/**
	 * 校验耳号唯一
	 */
	Integer selectCowCodeOne(String CowCode);

}
