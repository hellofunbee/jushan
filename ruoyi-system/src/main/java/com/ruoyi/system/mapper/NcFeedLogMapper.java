package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.NcFeedLog;
import java.util.List;	

/**
 * 饲料出入记录 数据层
 * 
 * @author ruoyi
 * @date 2019-09-10
 */
public interface NcFeedLogMapper 
{
	/**
     * 查询饲料出入记录信息
     * 
     * @param feedLogId 饲料出入记录ID
     * @return 饲料出入记录信息
     */
	public NcFeedLog selectNcFeedLogById(Long feedLogId);
	
	/**
     * 查询饲料出入记录列表
     * 
     * @param ncFeedLog 饲料出入记录信息
     * @return 饲料出入记录集合
     */
	public List<NcFeedLog> selectNcFeedLogList(NcFeedLog ncFeedLog);
	
	/**
     * 新增饲料出入记录
     * 
     * @param ncFeedLog 饲料出入记录信息
     * @return 结果
     */
	public int insertNcFeedLog(NcFeedLog ncFeedLog);
	
	/**
     * 修改饲料出入记录
     * 
     * @param ncFeedLog 饲料出入记录信息
     * @return 结果
     */
	public int updateNcFeedLog(NcFeedLog ncFeedLog);
	
	/**
     * 删除饲料出入记录
     * 
     * @param feedLogId 饲料出入记录ID
     * @return 结果
     */
	public int deleteNcFeedLogById(Long feedLogId);
	
	/**
     * 批量删除饲料出入记录
     * 
     * @param feedLogIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcFeedLogByIds(String[] feedLogIds);
	public NcFeedLog selectNcFeedLogAmount(NcFeedLog ncFeedLog);
	public Double selectNcFeedLogMonthAmount(NcFeedLog ncFeedLog);
	public NcFeedLog selectNcFeedLogAmountCows(NcFeedLog ncFeedLog);

	Double selectNcFeedLogMonthAmountCows(NcFeedLog ncFeedLog);
	double selectNcFeedLogScreen(NcFeedLog ncFeedLog);
	public double selectNcFeedAmount(NcFeedLog ncFeedLog);
	public double selectNcCowFeedAmount(NcFeedLog ncFeedLog);
}