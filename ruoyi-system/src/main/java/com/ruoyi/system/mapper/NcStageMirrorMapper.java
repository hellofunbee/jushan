package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.NcStageMirror;
import java.util.List;	

/**
 * 生产（育苗）阶段 镜像 数据层
 * 
 * @author ruoyi
 * @date 2019-09-02
 */
public interface NcStageMirrorMapper 
{
	/**
     * 查询生产（育苗）阶段 镜像信息
     * 
     * @param stageMirrorId 生产（育苗）阶段 镜像ID
     * @return 生产（育苗）阶段 镜像信息
     */
	public NcStageMirror selectNcStageMirrorById(Long stageMirrorId);
	
	/**
     * 查询生产（育苗）阶段 镜像列表
     * 
     * @param ncStageMirror 生产（育苗）阶段 镜像信息
     * @return 生产（育苗）阶段 镜像集合
     */
	public List<NcStageMirror> selectNcStageMirrorList(NcStageMirror ncStageMirror);
	
	/**
     * 新增生产（育苗）阶段 镜像
     * 
     * @param ncStageMirror 生产（育苗）阶段 镜像信息
     * @return 结果
     */
	public int insertNcStageMirror(NcStageMirror ncStageMirror);
	
	/**
     * 修改生产（育苗）阶段 镜像
     * 
     * @param ncStageMirror 生产（育苗）阶段 镜像信息
     * @return 结果
     */
	public int updateNcStageMirror(NcStageMirror ncStageMirror);
	
	/**
     * 删除生产（育苗）阶段 镜像
     * 
     * @param stageMirrorId 生产（育苗）阶段 镜像ID
     * @return 结果
     */
	public int deleteNcStageMirrorById(Long stageMirrorId);
	
	/**
     * 批量删除生产（育苗）阶段 镜像
     * 
     * @param stageMirrorIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcStageMirrorByIds(String[] stageMirrorIds);
	
}