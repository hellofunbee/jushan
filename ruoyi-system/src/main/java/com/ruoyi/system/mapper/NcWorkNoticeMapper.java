package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.NcWorkNotice;
import java.util.List;	

/**
 * 工作提醒 数据层
 * 
 * @author ruoyi
 * @date 2019-09-04
 */
public interface NcWorkNoticeMapper 
{
	/**
     * 查询工作提醒信息
     * 
     * @param workNoticeId 工作提醒ID
     * @return 工作提醒信息
     */
	public NcWorkNotice selectNcWorkNoticeById(Long workNoticeId);
	
	/**
     * 查询工作提醒列表
     * 
     * @param ncWorkNotice 工作提醒信息
     * @return 工作提醒集合
     */
	public List<NcWorkNotice> selectNcWorkNoticeList(NcWorkNotice ncWorkNotice);
	
	/**
     * 新增工作提醒
     * 
     * @param ncWorkNotice 工作提醒信息
     * @return 结果
     */
	public int insertNcWorkNotice(NcWorkNotice ncWorkNotice);
	
	/**
     * 修改工作提醒
     * 
     * @param ncWorkNotice 工作提醒信息
     * @return 结果
     */
	public int updateNcWorkNotice(NcWorkNotice ncWorkNotice);
	
	/**
     * 删除工作提醒
     * 
     * @param workNoticeId 工作提醒ID
     * @return 结果
     */
	public int deleteNcWorkNoticeById(Long workNoticeId);
	
	/**
     * 批量删除工作提醒
     * 
     * @param workNoticeIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcWorkNoticeByIds(String[] workNoticeIds);
	
}