package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.NcPengInfo;
import java.util.List;	

/**
 * 计划大棚 数据层
 * 
 * @author ruoyi
 * @date 2019-09-08
 */
public interface NcPengInfoMapper 
{
	/**
     * 查询计划大棚信息
     * 
     * @param pengInfoId 计划大棚ID
     * @return 计划大棚信息
     */
	public NcPengInfo selectNcPengInfoById(Long pengInfoId);
	
	/**
     * 查询计划大棚列表
     * 
     * @param ncPengInfo 计划大棚信息
     * @return 计划大棚集合
     */
	public List<NcPengInfo> selectNcPengInfoList(NcPengInfo ncPengInfo);
	
	/**
     * 新增计划大棚
     * 
     * @param ncPengInfo 计划大棚信息
     * @return 结果
     */
	public int insertNcPengInfo(NcPengInfo ncPengInfo);
	
	/**
     * 修改计划大棚
     * 
     * @param ncPengInfo 计划大棚信息
     * @return 结果
     */
	public int updateNcPengInfo(NcPengInfo ncPengInfo);
	
	/**
     * 删除计划大棚
     * 
     * @param pengInfoId 计划大棚ID
     * @return 结果
     */
	public int deleteNcPengInfoById(Long pengInfoId);
	
	/**
     * 批量删除计划大棚
     * 
     * @param pengInfoIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcPengInfoByIds(String[] pengInfoIds);

    Integer getPlaningCount(Long deptId);
}