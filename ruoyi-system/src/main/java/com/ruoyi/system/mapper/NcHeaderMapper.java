package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.NcHeader;
import java.util.List;	

/**
 * 轮播图 数据层
 * 
 * @author ruoyi
 * @date 2019-09-15
 */
public interface NcHeaderMapper 
{
	/**
     * 查询轮播图信息
     * 
     * @param hId 轮播图ID
     * @return 轮播图信息
     */
	public NcHeader selectNcHeaderById(Integer hId);
	
	/**
     * 查询轮播图列表
     * 
     * @param ncHeader 轮播图信息
     * @return 轮播图集合
     */
	public List<NcHeader> selectNcHeaderList(NcHeader ncHeader);
	
	/**
     * 新增轮播图
     * 
     * @param ncHeader 轮播图信息
     * @return 结果
     */
	public int insertNcHeader(NcHeader ncHeader);
	
	/**
     * 修改轮播图
     * 
     * @param ncHeader 轮播图信息
     * @return 结果
     */
	public int updateNcHeader(NcHeader ncHeader);
	
	/**
     * 删除轮播图
     * 
     * @param hId 轮播图ID
     * @return 结果
     */
	public int deleteNcHeaderById(Integer hId);
	
	/**
     * 批量删除轮播图
     * 
     * @param hIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcHeaderByIds(String[] hIds);
	
}