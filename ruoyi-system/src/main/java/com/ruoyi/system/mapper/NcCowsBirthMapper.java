package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.NcCowsBirth;
import java.util.List;

/**
 * 产犊记录 数据层
 *
 * @author ruoyi
 * @date 2019-09-02
 */
public interface NcCowsBirthMapper
{
	/**
     * 查询产犊记录信息
     *
     * @param cowId 产犊记录ID
     * @return 产犊记录信息
     */
	public NcCowsBirth selectNcCowsBirthById(Integer cowId);

	public NcCowsBirth selectNcCowsBirthByBId(Integer birthId);

	/**
     * 查询产犊记录列表
     *
     * @param ncCowsBirth 产犊记录信息
     * @return 产犊记录集合
     */
	public List<NcCowsBirth> selectNcCowsBirthList(NcCowsBirth ncCowsBirth);

	/**
     * 新增产犊记录
     *
     * @param ncCowsBirth 产犊记录信息
     * @return 结果
     */
	public int insertNcCowsBirth(NcCowsBirth ncCowsBirth);

	/**
     * 修改产犊记录
     *
     * @param ncCowsBirth 产犊记录信息
     * @return 结果
     */
	public int updateNcCowsBirth(NcCowsBirth ncCowsBirth);

	/**
     * 删除产犊记录
     *
     * @param cowId 产犊记录ID
     * @return 结果
     */
	public int deleteNcCowsBirthById(Integer cowId);

	/**
     * 批量删除产犊记录
     *
     * @param birthId 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcCowsBirthByIds(String[] birthId);

	/**
	 * 查询所有犊牛耳号
	 * @return
	 */
    List<String> selectAllChildCode();

	/**
	 * 根据牛耳号查找牛
	 * @param childCode 牛耳号
	 * @return
	 */
	NcCowsBirth selectCowByChildCode(String childCode);

	/**
	 * 校验耳号唯一
	 */
	Integer selectCowCodeOne(String childCode);

}