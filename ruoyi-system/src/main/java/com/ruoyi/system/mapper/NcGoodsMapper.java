package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.NcGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 物资仓储存 数据层
 *
 * @author ruoyi
 * @date 2019-08-05
 */
public interface NcGoodsMapper
{
	/**
     * 查询物资仓储存信息
     *
     * @param goodsId 物资仓储存ID
     * @return 物资仓储存信息
     */
	public NcGoods selectNcGoodsById(Long goodsId);

	/**
	 * 通过物资id查询列表
	 * @param meterialId 物资ID
	 */
	public List<NcGoods> selectNcGoodsBymeterialId(Long meterialId);


	public Long selectmeterialIdByName(String meterialName);

	/**
     * 查询物资仓储存列表
     *
     * @param ncGoods 物资仓储存信息
     * @return 物资仓储存集合
     */
	public List<NcGoods> selectNcGoodsList(NcGoods ncGoods);

	/**
     * 新增物资仓储存
     *
     * @param ncGoods 物资仓储存信息
     * @return 结果
     */
	public int insertNcGoods(NcGoods ncGoods);

	/**
     * 修改物资仓储存
     *
     * @param ncGoods 物资仓储存信息
     * @return 结果
     */
	public int updateNcGoods(NcGoods ncGoods);

	/**
     * 删除物资仓储存
     *
     * @param goodsId 物资仓储存ID
     * @return 结果
     */
	public int deleteNcGoodsById(Long goodsId);

	/**
     * 批量删除物资仓储存
     *
     * @param goodsIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcGoodsByIds(String[] goodsIds);

	/**
	 * 根据meterid和时间 查询出第一类的物资列表
	 * @param type
	 * @param beginTime
	 * @param endTime
	 * @return
	 */
	List<NcGoods> selectNcGoodsSortBymeterialId(@Param("meterialId") String type, @Param("beginTime") String beginTime, @Param("endTime") String endTime);
}