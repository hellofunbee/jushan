package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.NcZhibaoDrugs;
import java.util.List;

/**
 * 植保用药 数据层
 *
 * @author ruoyi
 * @date 2019-09-06
 */
public interface NcZhibaoDrugsMapper
{
	/**
	 * 查询植保用药信息
	 *
	 * @param zhibaoDrugsId 植保用药ID
	 * @return 植保用药信息
	 */
	public NcZhibaoDrugs selectNcZhibaoDrugsById(Long zhibaoDrugsId);

	/**
	 * 查询植保用药列表
	 *
	 * @param ncZhibaoDrugs 植保用药信息
	 * @return 植保用药集合
	 */
	public List<NcZhibaoDrugs> selectNcZhibaoDrugsList(NcZhibaoDrugs ncZhibaoDrugs);

	/**
	 * 新增植保用药
	 *
	 * @param ncZhibaoDrugs 植保用药信息
	 * @return 结果
	 */
	public int insertNcZhibaoDrugs(NcZhibaoDrugs ncZhibaoDrugs);

	/**
	 * 修改植保用药
	 *
	 * @param ncZhibaoDrugs 植保用药信息
	 * @return 结果
	 */
	public int updateNcZhibaoDrugs(NcZhibaoDrugs ncZhibaoDrugs);

	/**
	 * 删除植保用药
	 *
	 * @param zhibaoDrugsId 植保用药ID
	 * @return 结果
	 */
	public int deleteNcZhibaoDrugsById(Long zhibaoDrugsId);

	/**
	 * 批量删除植保用药
	 *
	 * @param zhibaoDrugsIds 需要删除的数据ID
	 * @return 结果
	 */
	public int deleteNcZhibaoDrugsByIds(String[] zhibaoDrugsIds);

}