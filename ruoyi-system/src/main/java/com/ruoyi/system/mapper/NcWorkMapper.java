package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.NcWork;
import java.util.List;	

/**
 * 标准农事 数据层
 * 
 * @author ruoyi
 * @date 2019-07-28
 */
public interface NcWorkMapper 
{
	/**
     * 查询标准农事信息
     * 
     * @param workId 标准农事ID
     * @return 标准农事信息
     */
	public NcWork selectNcWorkById(Long workId);
	
	/**
     * 查询标准农事列表
     * 
     * @param ncWork 标准农事信息
     * @return 标准农事集合
     */
	public List<NcWork> selectNcWorkList(NcWork ncWork);
	
	/**
     * 新增标准农事
     * 
     * @param ncWork 标准农事信息
     * @return 结果
     */
	public int insertNcWork(NcWork ncWork);
	
	/**
     * 修改标准农事
     * 
     * @param ncWork 标准农事信息
     * @return 结果
     */
	public int updateNcWork(NcWork ncWork);
	
	/**
     * 删除标准农事
     * 
     * @param workId 标准农事ID
     * @return 结果
     */
	public int deleteNcWorkById(Long workId);
	
	/**
     * 批量删除标准农事
     * 
     * @param workIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcWorkByIds(String[] workIds);
	
}