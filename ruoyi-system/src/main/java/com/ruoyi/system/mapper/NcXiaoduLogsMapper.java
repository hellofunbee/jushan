package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.NcXiaoduLogs;
import java.util.List;	

/**
 * 消毒记录（植保） 数据层
 * 
 * @author ruoyi
 * @date 2019-08-30
 */
public interface NcXiaoduLogsMapper 
{
	/**
     * 查询消毒记录（植保）信息
     * 
     * @param xdId 消毒记录（植保）ID
     * @return 消毒记录（植保）信息
     */
	public NcXiaoduLogs selectNcXiaoduLogsById(Long xdId);
	
	/**
     * 查询消毒记录（植保）列表
     * 
     * @param ncXiaoduLogs 消毒记录（植保）信息
     * @return 消毒记录（植保）集合
     */
	public List<NcXiaoduLogs> selectNcXiaoduLogsList(NcXiaoduLogs ncXiaoduLogs);
	
	/**
     * 新增消毒记录（植保）
     * 
     * @param ncXiaoduLogs 消毒记录（植保）信息
     * @return 结果
     */
	public int insertNcXiaoduLogs(NcXiaoduLogs ncXiaoduLogs);
	
	/**
     * 修改消毒记录（植保）
     * 
     * @param ncXiaoduLogs 消毒记录（植保）信息
     * @return 结果
     */
	public int updateNcXiaoduLogs(NcXiaoduLogs ncXiaoduLogs);
	
	/**
     * 删除消毒记录（植保）
     * 
     * @param xdId 消毒记录（植保）ID
     * @return 结果
     */
	public int deleteNcXiaoduLogsById(Long xdId);
	
	/**
     * 批量删除消毒记录（植保）
     * 
     * @param xdIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcXiaoduLogsByIds(String[] xdIds);
	public NcXiaoduLogs selectNcXiaoduLogsAmount(NcXiaoduLogs ncXiaoduLogs);
	
}