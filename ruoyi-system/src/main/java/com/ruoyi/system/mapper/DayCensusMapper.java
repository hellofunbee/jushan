package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.DayCensus;

import java.util.List;

public interface DayCensusMapper {
    //存栏数量
    public DayCensus cunAmountList(DayCensus ncChickenDayCensus);
    //入购数量
    public DayCensus inputAmountList(DayCensus ncChickenDayCensus);
    //内供大只鸡数量
    public DayCensus outputAmountBigList(DayCensus ncChickenDayCensus);
    //内供小只鸡数量
    public DayCensus outputAmountLittleList(DayCensus ncChickenDayCensus);
    //内销鸡数量
    public DayCensus neiXiaoList(DayCensus ncChickenDayCensus);
    //主饲料数量
    public DayCensus zFeedList(DayCensus ncChickenDayCensus);
    //辅饲料数量
    public DayCensus fFeedList(DayCensus ncChickenDayCensus);
    //死亡数量
    public DayCensus dieList(DayCensus ncChickenDayCensus);
    //死亡大只鸡数量
    public DayCensus dieBig(DayCensus ncChickenDayCensus);
    //死亡小只鸡数量
    public DayCensus dieSmall(DayCensus ncChickenDayCensus);

}
