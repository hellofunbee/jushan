package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.NcFeed;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 饲料库存 数据层
 * 
 * @author ruoyi
 * @date 2019-09-10
 */
public interface NcFeedMapper 
{
	/**
     * 查询饲料库存信息
     * 
     * @param feedId 饲料库存ID
     * @return 饲料库存信息
     */
	public NcFeed selectNcFeedById(Long feedId);
	
	/**
     * 查询饲料库存列表
     * 
     * @param ncFeed 饲料库存信息
     * @return 饲料库存集合
     */
	public List<NcFeed> selectNcFeedList(NcFeed ncFeed);
	
	/**
     * 新增饲料库存
     * 
     * @param ncFeed 饲料库存信息
     * @return 结果
     */
	public int insertNcFeed(NcFeed ncFeed);
	
	/**
     * 修改饲料库存
     * 
     * @param ncFeed 饲料库存信息
     * @return 结果
     */
	public int updateNcFeed(NcFeed ncFeed);
	
	/**
     * 删除饲料库存
     * 
     * @param feedId 饲料库存ID
     * @return 结果
     */
	public int deleteNcFeedById(Long feedId);
	
	/**
     * 批量删除饲料库存
     * 
     * @param feedIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcFeedByIds(String[] feedIds);
	//查询鸡的库存数量
	public Integer stockAmout(@Param("feedType") Integer feedType);
	
}