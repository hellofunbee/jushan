package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.NcCai;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 蔬菜管理 数据层
 * 
 * @author ruoyi
 */
public interface NcCaiMapper
{
    /**
     * 查询蔬菜人数
     * 
     * @param cai 蔬菜信息
     * @return 结果
     */
    public int selectCaiCount(NcCai cai);

    /**
     * 查询蔬菜是否存在用户
     * 
     * @param caiId 蔬菜ID
     * @return 结果
     */
    public int checkCaiExistUser(Long caiId);

    /**
     * 查询蔬菜管理数据
     * 
     * @param cai 蔬菜信息
     * @return 蔬菜信息集合
     */
    public List<NcCai> selectCaiList(NcCai cai);

    /**
     * 查询蔬菜管理数据 三级分类
     *
     * @param cai 蔬菜信息
     * @return 蔬菜信息集合(三级)
     */
    public List<NcCai> selectCaiListHasParent2(NcCai cai);

    /**
     * 删除蔬菜管理信息
     * 
     * @param caiId 蔬菜ID
     * @return 结果
     */
    public int deleteCaiById(Long caiId);

    /**
     * 新增蔬菜信息
     * 
     * @param cai 蔬菜信息
     * @return 结果
     */
    public int insertCai(NcCai cai);

    /**
     * 修改蔬菜信息
     * 
     * @param cai 蔬菜信息
     * @return 结果
     */
    public int updateCai(NcCai cai);

    /**
     * 修改子元素关系
     * 
     * @param cais 子元素
     * @return 结果
     */
    public int updateCaiChildren(@Param("cais") List<NcCai> cais);

    /**
     * 根据蔬菜ID查询信息
     * 
     * @param caiId 蔬菜ID
     * @return 蔬菜信息
     */
    public NcCai selectCaiById(Long caiId);

    /**
     * 校验蔬菜名称是否唯一
     * 
     * @param caiName 蔬菜名称
     * @param parentId 父蔬菜ID
     * @return 结果
     */
    public NcCai checkCaiNameUnique(@Param("caiName") String caiName, @Param("parentId") Long parentId);

    /**
     * 根据角色ID查询蔬菜
     *
     * @param roleId 角色ID
     * @return 蔬菜列表
     */
    public List<String> selectRoleCaiTree(Long roleId);

    /**
     * 修改所在蔬菜的父级蔬菜状态
     * 
     * @param cai 蔬菜
     */
    public void updateCaiStatus(NcCai cai);

    /**
     * 根据ID查询所有子蔬菜
     * @param caiId 蔬菜ID
     * @return 蔬菜列表
     */
    public List<NcCai> selectChildrenCaiById(Long caiId);


    /**
     * 根据蔬菜名称查找蔬菜Id
     * @param caiName 蔬菜Name
     * @return
     */
    public Long  selectCaIdByCaiName(String caiName);


    List<NcCai> selectCaiByNames(NcCai cai);
}
