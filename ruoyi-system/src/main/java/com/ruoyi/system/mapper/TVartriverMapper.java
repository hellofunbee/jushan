package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.TVartriver;
import java.util.List;	

/**
 * 传感器数数据 数据层
 * 
 * @author ruoyi
 * @date 2019-10-28
 */
public interface TVartriverMapper 
{
	/**
     * 查询传感器数数据信息
     * 
     * @param id 传感器数数据ID
     * @return 传感器数数据信息
     */
	public TVartriver selectTVartriverById(Integer id);
	
	/**
     * 查询传感器数数据列表
     * 
     * @param tVartriver 传感器数数据信息
     * @return 传感器数数据集合
     */
	public List<TVartriver> selectTVartriverList(TVartriver tVartriver);
	
	/**
     * 新增传感器数数据
     * 
     * @param tVartriver 传感器数数据信息
     * @return 结果
     */
	public int insertTVartriver(TVartriver tVartriver);
	
	/**
     * 修改传感器数数据
     * 
     * @param tVartriver 传感器数数据信息
     * @return 结果
     */
	public int updateTVartriver(TVartriver tVartriver);
	
	/**
     * 删除传感器数数据
     * 
     * @param id 传感器数数据ID
     * @return 结果
     */
	public int deleteTVartriverById(Integer id);
	
	/**
     * 批量删除传感器数数据
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteTVartriverByIds(String[] ids);
	
}