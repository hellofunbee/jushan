package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.NcZhibao;
import java.util.List;	

/**
 * 植保 数据层
 * 
 * @author ruoyi
 * @date 2019-08-28
 */
public interface NcZhibaoMapper 
{
	/**
     * 查询植保信息
     * 
     * @param zhibaoId 植保ID
     * @return 植保信息
     */
	public NcZhibao selectNcZhibaoById(Long zhibaoId);
	
	/**
     * 查询植保列表
     * 
     * @param ncZhibao 植保信息
     * @return 植保集合
     */
	public List<NcZhibao> selectNcZhibaoList(NcZhibao ncZhibao);
	
	/**
     * 新增植保
     * 
     * @param ncZhibao 植保信息
     * @return 结果
     */
	public int insertNcZhibao(NcZhibao ncZhibao);
	
	/**
     * 修改植保
     * 
     * @param ncZhibao 植保信息
     * @return 结果
     */
	public int updateNcZhibao(NcZhibao ncZhibao);
	
	/**
     * 删除植保
     * 
     * @param zhibaoId 植保ID
     * @return 结果
     */
	public int deleteNcZhibaoById(Long zhibaoId);
	
	/**
     * 批量删除植保
     * 
     * @param zhibaoIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcZhibaoByIds(String[] zhibaoIds);
	/**
	 * 查询植保记录列表
	 *
	 * @param ncZhibao 植保信息
	 * @return 结果
	 */
	public List<NcZhibao> selectNcZhibaoJiluList(NcZhibao ncZhibao);
	//修改植保记录
	public int updateZhibaoLog(NcZhibao ncZhibao);
	public Long selectChartAmount(NcZhibao ncZhibao);
	//植保详情接口
	public List<NcZhibao> selectzhibaoDetail(NcZhibao ncZhibao);
}