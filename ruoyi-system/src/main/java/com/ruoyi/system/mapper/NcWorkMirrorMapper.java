package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.NcWorkMirror;
import java.util.List;	

/**
 * 标准农事镜像 数据层
 * 
 * @author ruoyi
 * @date 2019-09-02
 */
public interface NcWorkMirrorMapper 
{
	/**
     * 查询标准农事镜像信息
     * 
     * @param workId 标准农事镜像ID
     * @return 标准农事镜像信息
     */
	public NcWorkMirror selectNcWorkMirrorById(Long workId);
	
	/**
     * 查询标准农事镜像列表
     * 
     * @param ncWorkMirror 标准农事镜像信息
     * @return 标准农事镜像集合
     */
	public List<NcWorkMirror> selectNcWorkMirrorList(NcWorkMirror ncWorkMirror);
	
	/**
     * 新增标准农事镜像
     * 
     * @param ncWorkMirror 标准农事镜像信息
     * @return 结果
     */
	public int insertNcWorkMirror(NcWorkMirror ncWorkMirror);
	
	/**
     * 修改标准农事镜像
     * 
     * @param ncWorkMirror 标准农事镜像信息
     * @return 结果
     */
	public int updateNcWorkMirror(NcWorkMirror ncWorkMirror);
	
	/**
     * 删除标准农事镜像
     * 
     * @param workId 标准农事镜像ID
     * @return 结果
     */
	public int deleteNcWorkMirrorById(Long workId);
	
	/**
     * 批量删除标准农事镜像
     * 
     * @param workIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcWorkMirrorByIds(String[] workIds);
	
}