package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.NcGasDuty;
import java.util.List;

/**
 * 锅炉房值班记录 数据层
 *
 * @author ruoyi
 * @date 2019-07-31
 */
public interface NcGasDutyMapper
{
	/**
     * 查询锅炉房值班记录信息
     *
     * @param dutyId 锅炉房值班记录ID
     * @return 锅炉房值班记录信息
     */
	public NcGasDuty selectNcGasDutyById(Long dutyId);

	/**
	 *查询所有锅炉值班信息
	 */
	public List<NcGasDuty> selectNcGasDutyAll();

	/**
     * 查询锅炉房值班记录列表
     *
     * @param ncGasDuty 锅炉房值班记录信息
     * @return 锅炉房值班记录集合
     */
	public List<NcGasDuty> selectNcGasDutyList(NcGasDuty ncGasDuty);

	/**
     * 新增锅炉房值班记录
     *
     * @param ncGasDuty 锅炉房值班记录信息
     * @return 结果
     */
	public int insertNcGasDuty(NcGasDuty ncGasDuty);

	/**
     * 修改锅炉房值班记录
     *
     * @param ncGasDuty 锅炉房值班记录信息
     * @return 结果
     */
	public int updateNcGasDuty(NcGasDuty ncGasDuty);

	/**
     * 删除锅炉房值班记录
     *
     * @param dutyId 锅炉房值班记录ID
     * @return 结果
     */
	public int deleteNcGasDutyById(Long dutyId);

	/**
     * 批量删除锅炉房值班记录
     *
     * @param dutyIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcGasDutyByIds(String[] dutyIds);

}