package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.TPoint;

import java.util.List;
import java.util.Map;

/**
 * 物联网节点 数据层
 * 
 * @author ruoyi
 * @date 2019-09-19
 */
public interface TPointMapper 
{
	/**
     * 查询物联网节点信息
     * 
     * @param tpId 物联网节点ID
     * @return 物联网节点信息
     */
	public TPoint selectTPointById(Integer tpId);
	
	/**
     * 查询物联网节点列表
     * 
     * @param tPoint 物联网节点信息
     * @return 物联网节点集合
     */
	public List<TPoint> selectTPointList(TPoint tPoint);
	
	/**
     * 新增物联网节点
     * 
     * @param tPoint 物联网节点信息
     * @return 结果
     */
	public int insertTPoint(TPoint tPoint);
	
	/**
     * 修改物联网节点
     * 
     * @param tPoint 物联网节点信息
     * @return 结果
     */
	public int updateTPoint(TPoint tPoint);
	
	/**
     * 删除物联网节点
     * 
     * @param tpId 物联网节点ID
     * @return 结果
     */
	public int deleteTPointById(Long tpId);
	
	/**
     * 批量删除物联网节点
     * 
     * @param tpIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteTPointByIds(String[] tpIds);


	int checkPointExistDevice(Long tpId);

    TPoint selectTPointByUuid(String uuid);

    int updateTPointByUuid(TPoint uuid);

    void deleteTPointByUuid(String uuid);

    List<Map> selectDevices(Long tpPid);
}