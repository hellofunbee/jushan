package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.NcYedan;
import java.util.List;	

/**
 * 液氮 数据层
 * 
 * @author ruoyi
 * @date 2019-09-02
 */
public interface NcYedanMapper 
{
	/**
     * 查询液氮信息
     * 
     * @param yedanId 液氮ID
     * @return 液氮信息
     */
	public NcYedan selectNcYedanById(Long yedanId);
	
	/**
     * 查询液氮列表
     * 
     * @param ncYedan 液氮信息
     * @return 液氮集合
     */
	public List<NcYedan> selectNcYedanList(NcYedan ncYedan);
	
	/**
     * 新增液氮
     * 
     * @param ncYedan 液氮信息
     * @return 结果
     */
	public int insertNcYedan(NcYedan ncYedan);
	
	/**
     * 修改液氮
     * 
     * @param ncYedan 液氮信息
     * @return 结果
     */
	public int updateNcYedan(NcYedan ncYedan);
	
	/**
     * 删除液氮
     * 
     * @param yedanId 液氮ID
     * @return 结果
     */
	public int deleteNcYedanById(Long yedanId);
	
	/**
     * 批量删除液氮
     * 
     * @param yedanIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcYedanByIds(String[] yedanIds);
	
}