package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.NcKnowledgeClassType;
import java.util.List;

/**
 * 知识课堂类别 数据层
 *
 * @author ruoyi
 * @date 2019-09-10
 */
public interface NcKnowledgeClassTypeMapper {
    /**
     * 查询知识课堂类别信息
     *
     * @param kctypeId 知识课堂类别ID
     * @return 知识课堂类别信息
     */
	NcKnowledgeClassType selectNcKnowledgeClassTypeById(Long kctypeId);

    /**
     * 查询知识课堂类别列表
     *
     * @param ncKnowledgeClassType 知识课堂类别信息
     * @return 知识课堂类别集合
     */
	List<NcKnowledgeClassType> selectNcKnowledgeClassTypeList(NcKnowledgeClassType ncKnowledgeClassType);


	/**
	 * 查询知识课堂二级类别列表
	 *
	 * @return 知识课堂类别集合
	 */
	List<NcKnowledgeClassType> listSecondLevelNcKnowledgeClassTypeList();

    /**
     * 新增知识课堂类别
     *
     * @param ncKnowledgeClassType 知识课堂类别信息
     * @return 结果
     */
	int insertNcKnowledgeClassType(NcKnowledgeClassType ncKnowledgeClassType);

    /**
     * 修改知识课堂类别
     *
     * @param ncKnowledgeClassType 知识课堂类别信息
     * @return 结果
     */
	int updateNcKnowledgeClassType(NcKnowledgeClassType ncKnowledgeClassType);

    /**
     * 删除知识课堂类别
     *
     * @param kctypeId 知识课堂类别ID
     * @return 结果
     */
	int deleteNcKnowledgeClassTypeById(Long kctypeId);

    /**
     * 批量删除知识课堂类别
     *
     * @param kctypeIds 需要删除的数据ID
     * @return 结果
     */
	int deleteNcKnowledgeClassTypeByIds(String[] kctypeIds);

    /**
     * 根据id查询该类子类别个数
     *
     * @param ncKnowledgeClassType
     * @return
     */
    int selectKCTypeClassCount(NcKnowledgeClassType ncKnowledgeClassType);

    /**
     * 根据id查询是否有该分类下的知识课堂
     *
     * @param kctypeId idd
     * @return
     */
    int checkKCClassExistKnowClass(Long kctypeId);


	/**
	 * 根据父id 查询
	 * @param parentId
	 * @return
	 */
	List<NcKnowledgeClassType> selectNCTypeByPid(Long parentId);
}