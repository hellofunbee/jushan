package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.NcInputCf;
import java.util.List;	

/**
 * 菜房入库 数据层
 * 
 * @author ruoyi
 * @date 2019-08-30
 */
public interface NcInputCfMapper 
{
	/**
     * 查询菜房入库信息
     * 
     * @param inputId 菜房入库ID
     * @return 菜房入库信息
     */
	public NcInputCf selectNcInputCfById(Integer inputId);
	
	/**
     * 查询菜房入库列表
     * 
     * @param ncInputCf 菜房入库信息
     * @return 菜房入库集合
     */
	public List<NcInputCf> selectNcInputCfList(NcInputCf ncInputCf);
	
	/**
     * 新增菜房入库
     * 
     * @param ncInputCf 菜房入库信息
     * @return 结果
     */
	public int insertNcInputCf(NcInputCf ncInputCf);
	
	/**
     * 修改菜房入库
     * 
     * @param ncInputCf 菜房入库信息
     * @return 结果
     */
	public int updateNcInputCf(NcInputCf ncInputCf);
	
	/**
     * 删除菜房入库
     * 
     * @param inputId 菜房入库ID
     * @return 结果
     */
	public int deleteNcInputCfById(Integer inputId);
	
	/**
     * 批量删除菜房入库
     * 
     * @param inputIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcInputCfByIds(String[] inputIds);
	public double selectCFinPutScreen(NcInputCf inputCf);
	
}