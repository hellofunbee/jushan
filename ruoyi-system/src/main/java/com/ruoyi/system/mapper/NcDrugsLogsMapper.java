package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.NcDrugsLogs;
import java.util.List;	

/**
 * 兽药出库记录 数据层
 * 
 * @author ruoyi
 * @date 2019-09-02
 */
public interface NcDrugsLogsMapper 
{
	/**
     * 查询兽药出库记录信息
     * 
     * @param drugLogId 兽药出库记录ID
     * @return 兽药出库记录信息
     */
	public NcDrugsLogs selectNcDrugsLogsById(Long drugLogId);
	
	/**
     * 查询兽药出库记录列表
     * 
     * @param ncDrugsLogs 兽药出库记录信息
     * @return 兽药出库记录集合
     */
	public List<NcDrugsLogs> selectNcDrugsLogsList(NcDrugsLogs ncDrugsLogs);
	
	/**
     * 新增兽药出库记录
     * 
     * @param ncDrugsLogs 兽药出库记录信息
     * @return 结果
     */
	public int insertNcDrugsLogs(NcDrugsLogs ncDrugsLogs);
	
	/**
     * 修改兽药出库记录
     * 
     * @param ncDrugsLogs 兽药出库记录信息
     * @return 结果
     */
	public int updateNcDrugsLogs(NcDrugsLogs ncDrugsLogs);
	
	/**
     * 删除兽药出库记录
     * 
     * @param drugLogId 兽药出库记录ID
     * @return 结果
     */
	public int deleteNcDrugsLogsById(Long drugLogId);
	
	/**
     * 批量删除兽药出库记录
     * 
     * @param drugLogIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcDrugsLogsByIds(String[] drugLogIds);
	
}