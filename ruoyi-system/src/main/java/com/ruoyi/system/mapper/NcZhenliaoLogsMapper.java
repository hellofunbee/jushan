package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.NcZhenliaoLogs;

import java.util.List;

/**
 * 诊疗单 数据层
 * 
 * @author ruoyi
 * @date 2019-08-28
 */
public interface NcZhenliaoLogsMapper 
{
	/**f
     * 查询诊疗单信息
     * 
     * @param zlId 诊疗单ID
     * @return 诊疗单信息
     */
	public NcZhenliaoLogs selectNcZhenliaoLogsById(Long zlId);
	
	/**
     * 查询诊疗单列表
     * 
     * @param ncZhenliaoLogs 诊疗单信息
     * @return 诊疗单集合
     */
	public List<NcZhenliaoLogs> selectNcZhenliaoLogsList(NcZhenliaoLogs ncZhenliaoLogs);



	/**
	 * 查询诊疗单列表(确认用药)
	 *
	 * @param ncZhenliaoLogs 诊疗单信息
	 * @return 诊疗单集合
	 */
	public List<NcZhenliaoLogs> selectNcZhenliaoLogsListConfirm(NcZhenliaoLogs ncZhenliaoLogs);
	
	/**
     * 新增诊疗单
     * 
     * @param ncZhenliaoLogs 诊疗单信息
     * @return 结果
     */
	public int insertNcZhenliaoLogs(NcZhenliaoLogs ncZhenliaoLogs);
	
	/**
     * 修改诊疗单
     * 
     * @param ncZhenliaoLogs 诊疗单信息
     * @return 结果
     */
	public int updateNcZhenliaoLogs(NcZhenliaoLogs ncZhenliaoLogs);
	
	/**
     * 删除诊疗单
     * 
     * @param zlId 诊疗单ID
     * @return 结果
     */
	public int deleteNcZhenliaoLogsById(Long zlId);
	
	/**
     * 批量删除诊疗单
     * 
     * @param zlIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcZhenliaoLogsByIds(String[] zlIds);
	
}