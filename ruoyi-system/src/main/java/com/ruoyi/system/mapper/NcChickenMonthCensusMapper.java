package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.NcChickenMonthCensus;
import java.util.List;	

/**
 * 鸡舍月报 数据层
 * 
 * @author ruoyi
 * @date 2019-09-19
 */
public interface NcChickenMonthCensusMapper 
{
	/**
     * 查询鸡舍月报信息
     * 
     * @param cenusId 鸡舍月报ID
     * @return 鸡舍月报信息
     */
	public NcChickenMonthCensus selectNcChickenMonthCensusById(Long cenusId);
	
	/**
     * 查询鸡舍月报列表
     * 
     * @param ncChickenMonthCensus 鸡舍月报信息
     * @return 鸡舍月报集合
     */
	public List<NcChickenMonthCensus> selectNcChickenMonthCensusList(NcChickenMonthCensus ncChickenMonthCensus);
	
	/**
     * 新增鸡舍月报
     * 
     * @param ncChickenMonthCensus 鸡舍月报信息
     * @return 结果
     */
	public int insertNcChickenMonthCensus(NcChickenMonthCensus ncChickenMonthCensus);
	
	/**
     * 修改鸡舍月报
     * 
     * @param ncChickenMonthCensus 鸡舍月报信息
     * @return 结果
     */
	public int updateNcChickenMonthCensus(NcChickenMonthCensus ncChickenMonthCensus);
	
	/**
     * 删除鸡舍月报
     * 
     * @param cenusId 鸡舍月报ID
     * @return 结果
     */
	public int deleteNcChickenMonthCensusById(Long cenusId);
	
	/**
     * 批量删除鸡舍月报
     * 
     * @param cenusIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcChickenMonthCensusByIds(String[] cenusIds);
	
}