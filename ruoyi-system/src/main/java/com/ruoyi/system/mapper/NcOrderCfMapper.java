package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.NcOrderCf;

import java.util.List;
import java.util.Map;

/**
 * 菜房订单 数据层
 * 
 * @author ruoyi
 * @date 2019-08-29
 */
public interface NcOrderCfMapper 
{
	/**
     * 查询菜房订单信息
     * 
     * @param cforderId 菜房订单ID
     * @return 菜房订单信息
     */
	public NcOrderCf selectNcOrderCfById(Long cforderId);
	
	/**
     * 查询菜房订单列表
     * 
     * @param ncOrderCf 菜房订单信息
     * @return 菜房订单集合
     */
	public List<NcOrderCf> selectNcOrderCfList(NcOrderCf ncOrderCf);
	
	/**
     * 新增菜房订单
     * 
     * @param ncOrderCf 菜房订单信息
     * @return 结果
     */
	public int insertNcOrderCf(NcOrderCf ncOrderCf);
	
	/**
     * 修改菜房订单
     * 
     * @param ncOrderCf 菜房订单信息
     * @return 结果
     */
	public int updateNcOrderCf(NcOrderCf ncOrderCf);
	
	/**
     * 删除菜房订单
     * 
     * @param cforderId 菜房订单ID
     * @return 结果
     */
	public int deleteNcOrderCfById(Long cforderId);
	
	/**
     * 批量删除菜房订单
     * 
     * @param cforderIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcOrderCfByIds(String[] cforderIds);

    List<Map> selectSumByStatus(NcOrderCf orderCf);


	Integer selectSum(NcOrderCf order);
	Integer caiScreen(NcOrderCf order);
}