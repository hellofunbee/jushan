package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.NcChickenDayCensus;
import java.util.List;	

/**
 * 鸡日报 数据层
 * 
 * @author ruoyi
 * @date 2019-09-19
 */
public interface NcChickenDayCensusMapper 
{
	/**
     * 查询鸡日报信息
     * 
     * @param censusId 鸡日报ID
     * @return 鸡日报信息
     */
	public NcChickenDayCensus selectNcChickenDayCensusById(Long censusId);
	
	/**
     * 查询鸡日报列表
     * 
     * @param ncChickenDayCensus 鸡日报信息
     * @return 鸡日报集合
     */
	public List<NcChickenDayCensus> selectNcChickenDayCensusList(NcChickenDayCensus ncChickenDayCensus);
	
	/**
     * 新增鸡日报
     * 
     * @param ncChickenDayCensus 鸡日报信息
     * @return 结果
     */
	public int insertNcChickenDayCensus(NcChickenDayCensus ncChickenDayCensus);
	
	/**
     * 修改鸡日报
     * 
     * @param ncChickenDayCensus 鸡日报信息
     * @return 结果
     */
	public int updateNcChickenDayCensus(NcChickenDayCensus ncChickenDayCensus);
	
	/**
     * 删除鸡日报
     * 
     * @param censusId 鸡日报ID
     * @return 结果
     */
	public int deleteNcChickenDayCensusById(Long censusId);
	
	/**
     * 批量删除鸡日报
     * 
     * @param censusIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcChickenDayCensusByIds(String[] censusIds);
	
}