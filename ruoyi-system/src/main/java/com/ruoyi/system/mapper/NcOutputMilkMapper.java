package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.NcOutputMilk;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 牛奶出库 数据层
 *
 * @author ruoyi
 * @date 2019-09-11
 */
public interface NcOutputMilkMapper
{
	/**
     * 查询牛奶出库信息
     *
     * @param outputId 牛奶出库ID
     * @return 牛奶出库信息
     */
	public NcOutputMilk selectNcOutputMilkById(Long outputId);

	/**
     * 查询牛奶出库列表
     *
     * @param ncOutputMilk 牛奶出库信息
     * @return 牛奶出库集合
     */
	public List<NcOutputMilk> selectNcOutputMilkList(NcOutputMilk ncOutputMilk);

	/**
     * 新增牛奶出库
     *
     * @param ncOutputMilk 牛奶出库信息
     * @return 结果
     */
	public int insertNcOutputMilk(NcOutputMilk ncOutputMilk);

	/**
     * 修改牛奶出库
     *
     * @param ncOutputMilk 牛奶出库信息
     * @return 结果
     */
	public int updateNcOutputMilk(NcOutputMilk ncOutputMilk);

	/**
     * 删除牛奶出库
     *
     * @param outputId 牛奶出库ID
     * @return 结果
     */
	public int deleteNcOutputMilkById(Long outputId);

	/**
     * 批量删除牛奶出库
     *
     * @param outputIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcOutputMilkByIds(String[] outputIds);

    List<NcOutputMilk> selectMilkTotalByOutPutType(@Param("outputType") int parseInt, @Param("beginTime") String startTime, @Param("endTime") String endTime);

	Double selectMilkTotalByMonth(@Param("beginTime") String last12Month, @Param("outputType") String output);

    List<NcOutputMilk> selectMilkOrderInfoByOrderId(@Param("orderId") Long orderId);

    Double selectMilkTotalByMonthRange(@Param("beginTime") String beginTime, @Param("endTime") String endTime);

	public Double selectNcOutputMonthAmount(NcOutputMilk ncOutputMilk);
    Double selectMilkTotalByMonthRange(@Param("beginTime") String beginTime, @Param("endTime") String endTime, @Param("outputType") String output);


	Double selectMilkTotalByDays(@Param("days") String s, @Param("outputType") String output);

	List<NcOutputMilk> selectMilkOrderInfoByOutputType(@Param("outputType") Integer outputType);
	public double selectNcCowsMilkScreen(NcOutputMilk ncOutputMilk);
	public double selectMilkInfoByDays(NcOutputMilk ncOutputMilk);
}