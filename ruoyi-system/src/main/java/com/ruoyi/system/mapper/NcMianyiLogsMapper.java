package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.NcMianyiLogs;
import java.util.List;	

/**
 * 免疫记录 数据层
 * 
 * @author ruoyi
 * @date 2019-09-02
 */
public interface NcMianyiLogsMapper 
{
	/**
     * 查询免疫记录信息
     * 
     * @param myId 免疫记录ID
     * @return 免疫记录信息
     */
	public NcMianyiLogs selectNcMianyiLogsById(Long myId);
	
	/**
     * 查询免疫记录列表
     * 
     * @param ncMianyiLogs 免疫记录信息
     * @return 免疫记录集合
     */
	public List<NcMianyiLogs> selectNcMianyiLogsList(NcMianyiLogs ncMianyiLogs);
	
	/**
     * 新增免疫记录
     * 
     * @param ncMianyiLogs 免疫记录信息
     * @return 结果
     */
	public int insertNcMianyiLogs(NcMianyiLogs ncMianyiLogs);
	
	/**
     * 修改免疫记录
     * 
     * @param ncMianyiLogs 免疫记录信息
     * @return 结果
     */
	public int updateNcMianyiLogs(NcMianyiLogs ncMianyiLogs);
	
	/**
     * 删除免疫记录
     * 
     * @param myId 免疫记录ID
     * @return 结果
     */
	public int deleteNcMianyiLogsById(Long myId);
	
	/**
     * 批量删除免疫记录
     * 
     * @param myIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcMianyiLogsByIds(String[] myIds);
	
}