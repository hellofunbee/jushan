package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.NcYedanLog;
import java.util.List;	

/**
 * 液氮出库 数据层
 * 
 * @author ruoyi
 * @date 2019-09-02
 */
public interface NcYedanLogMapper 
{
	/**
     * 查询液氮出库信息
     * 
     * @param yedanLogId 液氮出库ID
     * @return 液氮出库信息
     */
	public NcYedanLog selectNcYedanLogById(Long yedanLogId);
	
	/**
     * 查询液氮出库列表
     * 
     * @param ncYedanLog 液氮出库信息
     * @return 液氮出库集合
     */
	public List<NcYedanLog> selectNcYedanLogList(NcYedanLog ncYedanLog);
	
	/**
     * 新增液氮出库
     * 
     * @param ncYedanLog 液氮出库信息
     * @return 结果
     */
	public int insertNcYedanLog(NcYedanLog ncYedanLog);
	
	/**
     * 修改液氮出库
     * 
     * @param ncYedanLog 液氮出库信息
     * @return 结果
     */
	public int updateNcYedanLog(NcYedanLog ncYedanLog);
	
	/**
     * 删除液氮出库
     * 
     * @param yedanLogId 液氮出库ID
     * @return 结果
     */
	public int deleteNcYedanLogById(Long yedanLogId);
	
	/**
     * 批量删除液氮出库
     * 
     * @param yedanLogIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcYedanLogByIds(String[] yedanLogIds);
	
}