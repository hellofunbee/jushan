package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.NcGoodsLog;

import java.util.List;
import java.util.Map;

/**
 * 物资仓储出入记录 数据层
 *
 * @author ruoyi
 * @date 2019-08-06
 */
public interface NcGoodsLogMapper
{
	/**
     * 查询物资仓储出入记录信息
     *
     * @param goodsLogId 物资仓储出入记录ID
     * @return 物资仓储出入记录信息
     */
	public NcGoodsLog selectNcGoodsLogById(Long goodsLogId);

	public List<NcGoodsLog> selectNcGoodsLogByGId(Long goodsId);

	/**
     * 查询物资仓储出入记录列表
     *
     * @param ncGoodsLog 物资仓储出入记录信息
     * @return 物资仓储出入记录集合
     */
	public List<NcGoodsLog> selectNcGoodsLogList(NcGoodsLog ncGoodsLog);

	/**
     * 新增物资仓储出入记录
     *
     * @param ncGoodsLog 物资仓储出入记录信息
     * @return 结果
     */
	public int insertNcGoodsLog(NcGoodsLog ncGoodsLog);

	/**
     * 修改物资仓储出入记录
     *
     * @param ncGoodsLog 物资仓储出入记录信息
     * @return 结果
     */
	public int updateNcGoodsLog(NcGoodsLog ncGoodsLog);

	/**
     * 删除物资仓储出入记录
     *
     * @param goodsLogId 物资仓储出入记录ID
     * @return 结果
     */
	public int deleteNcGoodsLogById(Long goodsLogId);

	/**
     * 批量删除物资仓储出入记录
     *
     * @param goodsLogIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcGoodsLogByIds(String[] goodsLogIds);

    Long selectNcGoodsLogListByDataAndMetar(Map map);

    Long selectNcGoodsLogListByData(Map map);
}