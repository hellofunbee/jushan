package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.NcOutputChicken;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 鸡出库 数据层
 * 
 * @author ruoyi
 * @date 2019-09-09
 */
public interface NcOutputChickenMapper 
{
	/**
     * 查询鸡出库信息
     * 
     * @param outputId 鸡出库ID
     * @return 鸡出库信息
     */
	public NcOutputChicken selectNcOutputChickenById(Integer outputId);
	
	/**
     * 查询鸡出库列表
     * 
     * @param ncOutputChicken 鸡出库信息
     * @return 鸡出库集合
     */
	public List<NcOutputChicken> selectNcOutputChickenList(NcOutputChicken ncOutputChicken);
	
	/**
     * 新增鸡出库
     * 
     * @param ncOutputChicken 鸡出库信息
     * @return 结果
     */
	public int insertNcOutputChicken(NcOutputChicken ncOutputChicken);
	
	/**
     * 修改鸡出库
     * 
     * @param ncOutputChicken 鸡出库信息
     * @return 结果
     */
	public int updateNcOutputChicken(NcOutputChicken ncOutputChicken);
	
	/**
     * 删除鸡出库
     * 
     * @param outputId 鸡出库ID
     * @return 结果
     */
	public int deleteNcOutputChickenById(Integer outputId);
	
	/**
     * 批量删除鸡出库
     * 
     * @param outputIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcOutputChickenByIds(String[] outputIds);
	//查询出库数量
	public double selectNcOutputAmount(NcOutputChicken ncOutputChicken);
	public double selectNcOutputMonthAmount(NcOutputChicken ncOutputChicken);
	Double selectChickenInfoByMonthRange(NcOutputChicken ncOutputChicken);


    Double selectChickenByMonthRange(@Param("beginTime") String beginTime, @Param("endTime") String endTime, @Param("outputType") String output);

    Double selectChickenTotalByMonth(@Param("beginTime") String s,@Param("outputType") String output);

	Double selectChkicenTotalByDays(@Param("day") String s,@Param("outputType") String output);
}