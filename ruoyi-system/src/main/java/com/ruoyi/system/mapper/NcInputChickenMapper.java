package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.NcInputChicken;
import java.util.List;	

/**
 * 鸡入库 数据层
 * 
 * @author ruoyi
 * @date 2019-09-09
 */
public interface NcInputChickenMapper 
{
	/**
     * 查询鸡入库信息
     * 
     * @param inputId 鸡入库ID
     * @return 鸡入库信息
     */
	public NcInputChicken selectNcInputChickenById(Integer inputId);
	
	/**
     * 查询鸡入库列表
     * 
     * @param ncInputChicken 鸡入库信息
     * @return 鸡入库集合
     */
	public List<NcInputChicken> selectNcInputChickenList(NcInputChicken ncInputChicken);
	
	/**
     * 新增鸡入库
     * 
     * @param ncInputChicken 鸡入库信息
     * @return 结果
     */
	public int insertNcInputChicken(NcInputChicken ncInputChicken);
	
	/**
     * 修改鸡入库
     * 
     * @param ncInputChicken 鸡入库信息
     * @return 结果
     */
	public int updateNcInputChicken(NcInputChicken ncInputChicken);
	
	/**
     * 删除鸡入库
     * 
     * @param inputId 鸡入库ID
     * @return 结果
     */
	public int deleteNcInputChickenById(Integer inputId);
	
	/**
     * 批量删除鸡入库
     * 
     * @param inputIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcInputChickenByIds(String[] inputIds);
	//鸡的库存
	public Integer chickenStackAmount();
	
}