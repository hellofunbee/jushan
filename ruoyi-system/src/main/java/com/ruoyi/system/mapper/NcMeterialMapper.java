package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.NcMeterial;
import com.ruoyi.system.domain.NcMeterialForStandard;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 物资管理 数据层
 *
 * @author ruoyi
 */
public interface NcMeterialMapper
{
    /**
     * 查询物资人数
     *
     * @param meterial 物资信息
     * @return 结果
     */
    public int selectMeterialCount(NcMeterial meterial);

    /**
     * 查询物资是否存在用户
     *
     * @param meterialId 物资ID
     * @return 结果
     */
    public int checkMeterialExistUser(Long meterialId);

    /**
     * 查询物资管理数据
     *
     * @param meterial 物资信息
     * @return 物资信息集合
     */
    public List<NcMeterial> selectMeterialList(NcMeterial meterial);

    /**
     * 查询物资管理数据 携带物资类别(父级)
     *
     * @param meterial 物资信息
     * @return 物资信息集合
     */
    public List<NcMeterialForStandard> selectMeterialListHasParent(NcMeterialForStandard meterial);


    /**
     * 删除物资管理信息
     *
     * @param meterialId 物资ID
     * @return 结果
     */
    public int deleteMeterialById(Long meterialId);

    /**
     * 新增物资信息
     *
     * @param meterial 物资信息
     * @return 结果
     */
    public int insertMeterial(NcMeterial meterial);

    /**
     * 修改物资信息
     *
     * @param meterial 物资信息
     * @return 结果
     */
    public int updateMeterial(NcMeterial meterial);

    /**
     * 修改子元素关系
     *
     * @param meterials 子元素
     * @return 结果
     */
    public int updateMeterialChildren(@Param("meterials") List<NcMeterial> meterials);

    /**
     * 根据物资ID查询信息
     *
     * @param meterialId 物资ID
     * @return 物资信息
     */
    public NcMeterial selectMeterialById(Long meterialId);

    /**
     * 校验物资名称是否唯一
     *
     * @param meterialName 物资名称
     * @param parentId 父物资ID
     * @return 结果
     */
    public NcMeterial checkMeterialNameUnique(@Param("meterialName") String meterialName, @Param("parentId") Long parentId);

    /**
     * 根据角色ID查询物资
     *
     * @param roleId 角色ID
     * @return 物资列表
     */
    public List<String> selectRoleMeterialTree(Long roleId);

    /**
     * 修改所在物资的父级物资状态
     *
     * @param meterial 物资
     */
    public void updateMeterialStatus(NcMeterial meterial);

    /**
     * 根据ID查询所有子物资
     * @param meterialId 物资ID
     * @return 物资列表
     */
    public List<NcMeterial> selectChildrenMeterialById(Long meterialId);

    public List<NcMeterial> selectMeterialIdByName(String meterialName);

    List<NcMeterial> frontSelectNcGoodsMeter(@Param("startTime") String startTime, @Param("endTime") String endTime);

    List<NcMeterial> selectMeterialFirstSort(@Param("parentId") int i);

    Integer countGoodsByMeterID(Map map);
}
