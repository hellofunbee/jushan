package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.NcInputMilk;
import java.util.List;	

/**
 * 牛奶入库 数据层
 * 
 * @author ruoyi
 * @date 2019-09-11
 */
public interface NcInputMilkMapper 
{
	/**
     * 查询牛奶入库信息
     * 
     * @param inputId 牛奶入库ID
     * @return 牛奶入库信息
     */
	public NcInputMilk selectNcInputMilkById(Integer inputId);
	
	/**
     * 查询牛奶入库列表
     * 
     * @param ncInputMilk 牛奶入库信息
     * @return 牛奶入库集合
     */
	public List<NcInputMilk> selectNcInputMilkList(NcInputMilk ncInputMilk);
	
	/**
     * 新增牛奶入库
     * 
     * @param ncInputMilk 牛奶入库信息
     * @return 结果
     */
	public int insertNcInputMilk(NcInputMilk ncInputMilk);
	
	/**
     * 修改牛奶入库
     * 
     * @param ncInputMilk 牛奶入库信息
     * @return 结果
     */
	public int updateNcInputMilk(NcInputMilk ncInputMilk);
	
	/**
     * 删除牛奶入库
     * 
     * @param inputId 牛奶入库ID
     * @return 结果
     */
	public int deleteNcInputMilkById(Integer inputId);
	
	/**
     * 批量删除牛奶入库
     * 
     * @param inputIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcInputMilkByIds(String[] inputIds);
	
}