package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.NcOrderCf;
import com.ruoyi.system.domain.NcOuputCf;
import org.apache.ibatis.annotations.Param;


import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * 菜房出库 数据层
 * 
 * @author ruoyi
 * @date 2019-08-30
 */
public interface NcOuputCfMapper 
{
	/**
     * 查询菜房出库信息
     * 
     * @param outputId 菜房出库ID
     * @return 菜房出库信息
     */
	public NcOuputCf selectNcOuputCfById(Integer outputId);
	
	/**
     * 查询菜房出库列表
     * 
     * @param ncOuputCf 菜房出库信息
     * @return 菜房出库集合
     */
	public List<NcOuputCf> selectNcOuputCfList(NcOuputCf ncOuputCf);
	
	/**
     * 新增菜房出库
     * 
     * @param ncOuputCf 菜房出库信息
     * @return 结果
     */
	public int insertNcOuputCf(NcOuputCf ncOuputCf);
	
	/**
     * 修改菜房出库
     * 
     * @param ncOuputCf 菜房出库信息
     * @return 结果
     */
	public int updateNcOuputCf(NcOuputCf ncOuputCf);
	
	/**
     * 删除菜房出库
     * 
     * @param outputId 菜房出库ID
     * @return 结果
     */
	public int deleteNcOuputCfById(Integer outputId);
	
	/**
     * 批量删除菜房出库
     * 
     * @param outputIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcOuputCfByIds(String[] outputIds);

	Double selectSum(NcOrderCf order);

	List<Map> selectSumByStatus(NcOrderCf cf);

    Double selectCfByMonthRange(@Param("beginTime") String beginTime, @Param("endTime") String endTime,@Param("outputType") String output);

	/**
	 * 根据计划id查询菜房订单id查询菜房出库数量
	 * @param planId 计划id
	 * @return
	 */
	BigDecimal queryOutAmoutByPlanId(Long planId );

    Double selectCfTotalByMonth(@Param("beginTime") String s,@Param("outputType") String output);

	Double selectCfTotalByDays(@Param("day") String s,@Param("outputType") String output);
	Double selectCFoutPutScreen(NcOuputCf ncOuputCf);
	public List<NcOuputCf> avgCFoutPutScreen(NcOuputCf ncOuputCf);
	public double selectCfInfo(NcOuputCf ncOuputCf);
}