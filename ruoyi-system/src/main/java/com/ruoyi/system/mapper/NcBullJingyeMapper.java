package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.NcBullJingye;
import java.util.List;	

/**
 * 牛舍精液 数据层
 * 
 * @author ruoyi
 * @date 2019-08-30
 */
public interface NcBullJingyeMapper 
{
	/**
     * 查询牛舍精液信息
     * 
     * @param jingyeId 牛舍精液ID
     * @return 牛舍精液信息
     */
	public NcBullJingye selectNcBullJingyeById(Integer jingyeId);
	
	/**
     * 查询牛舍精液列表
     * 
     * @param ncBullJingye 牛舍精液信息
     * @return 牛舍精液集合
     */
	public List<NcBullJingye> selectNcBullJingyeList(NcBullJingye ncBullJingye);
	
	/**
     * 新增牛舍精液
     * 
     * @param ncBullJingye 牛舍精液信息
     * @return 结果
     */
	public int insertNcBullJingye(NcBullJingye ncBullJingye);
	
	/**
     * 修改牛舍精液
     * 
     * @param ncBullJingye 牛舍精液信息
     * @return 结果
     */
	public int updateNcBullJingye(NcBullJingye ncBullJingye);
	
	/**
     * 删除牛舍精液
     * 
     * @param jingyeId 牛舍精液ID
     * @return 结果
     */
	public int deleteNcBullJingyeById(Integer jingyeId);
	
	/**
     * 批量删除牛舍精液
     * 
     * @param jingyeIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcBullJingyeByIds(String[] jingyeIds);
	
}