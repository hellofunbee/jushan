package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.NcCai;
import com.ruoyi.system.domain.NcYaoClass;
import com.ruoyi.system.domain.NcZhibao;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 药品类别 数据层
 * 
 * @author ruoyi
 * @date 2019-08-29
 */
public interface NcYaoClassMapper 
{/**
 * 查询药品人数
 *
 * @param yao 药品信息
 * @return 结果
 */
public int selectYaoCount(NcYaoClass yao);

	/**
	 * 查询药品是否存在用户
	 *
	 * @param yaoId 药品ID
	 * @return 结果
	 */
	public int checkYaoExistUser(Long yaoId);

	/**
	 * 查询药品管理数据
	 *
	 * @param yao 药品信息
	 * @return 药品信息集合
	 */
	public List<NcYaoClass> selectNcYaoClassList(NcYaoClass yao);

	/**
	 * 删除药品管理信息
	 *
	 * @param yaoId 药品ID
	 * @return 结果
	 */
	public int deleteYaoById(Long yaoId);

	/**
	 * 新增药品信息
	 *
	 * @param yao 药品信息
	 * @return 结果
	 */
	public int insertNcYaoClass(NcYaoClass yao);

	/**
	 * 修改药品信息
	 *
	 * @param yao 药品信息
	 * @return 结果
	 */
	public int updateNcYaoClass(NcYaoClass yao);

	/**
	 * 修改子元素关系
	 *
	 * @param yaos 子元素
	 * @return 结果
	 */
	public int updateYaoChildren(@Param("yaos") List<NcYaoClass> yaos);

	/**
	 * 根据药品ID查询信息
	 *
	 * @param yaoId 药品ID
	 * @return 药品信息
	 */
	public NcYaoClass selectYaoById(Long yaoId);

	/**
	 * 校验药品名称是否唯一
	 *
	 * @param yaoName 药品名称
	 * @param parentId 父药品ID
	 * @return 结果
	 */
	public NcYaoClass checkYaoNameUnique(@Param("yaoName") String yaoName, @Param("parentId") Long parentId);

	/**
	 * 根据角色ID查询药品
	 *
	 * @param roleId 角色ID
	 * @return 药品列表
	 */
	public List<String> selectRoleYaoTree(Long roleId);

	/**
	 * 修改所在药品的父级药品状态
	 *
	 * @param yao 药品
	 */
	public void updateYaoStatus(NcYaoClass yao);

	/**
	 * 根据ID查询所有子药品
	 * @param yaoId 药品ID
	 * @return 药品列表
	 */
	public List<NcYaoClass> selectChildrenYaoById(Long yaoId);

}