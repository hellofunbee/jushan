package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.NcDrugs;
import java.util.List;	

/**
 * 兽药库存 数据层
 * 
 * @author ruoyi
 * @date 2019-09-02
 */
public interface NcDrugsMapper 
{
	/**
     * 查询兽药库存信息
     * 
     * @param drugId 兽药库存ID
     * @return 兽药库存信息
     */
	public NcDrugs selectNcDrugsById(Long drugId);
	
	/**
     * 查询兽药库存列表
     * 
     * @param ncDrugs 兽药库存信息
     * @return 兽药库存集合
     */
	public List<NcDrugs> selectNcDrugsList(NcDrugs ncDrugs);
	
	/**
     * 新增兽药库存
     * 
     * @param ncDrugs 兽药库存信息
     * @return 结果
     */
	public int insertNcDrugs(NcDrugs ncDrugs);
	
	/**
     * 修改兽药库存
     * 
     * @param ncDrugs 兽药库存信息
     * @return 结果
     */
	public int updateNcDrugs(NcDrugs ncDrugs);
	
	/**
     * 删除兽药库存
     * 
     * @param drugId 兽药库存ID
     * @return 结果
     */
	public int deleteNcDrugsById(Long drugId);
	
	/**
     * 批量删除兽药库存
     * 
     * @param drugIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcDrugsByIds(String[] drugIds);

	/**
	 *根据药品id 查询子级药品
	 * @param yaoId 药品id
	 * @return
	 */
	public  List<NcDrugs> selectNcDrugsChildList(Long yaoId);
	
}