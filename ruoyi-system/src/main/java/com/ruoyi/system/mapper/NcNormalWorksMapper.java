package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.NcNormalWorks;
import java.util.List;	

/**
 * 农事记录 数据层
 * 
 * @author ruoyi
 * @date 2019-07-31
 */
public interface NcNormalWorksMapper 
{
	/**
     * 查询农事记录信息
     * 
     * @param nWorkId 农事记录ID
     * @return 农事记录信息
     */
	public NcNormalWorks selectNcNormalWorksById(Long nWorkId);
	
	/**
     * 查询农事记录列表
     * 
     * @param ncNormalWorks 农事记录信息
     * @return 农事记录集合
     */
	public List<NcNormalWorks> selectNcNormalWorksList(NcNormalWorks ncNormalWorks);
	
	/**
     * 新增农事记录
     * 
     * @param ncNormalWorks 农事记录信息
     * @return 结果
     */
	public int insertNcNormalWorks(NcNormalWorks ncNormalWorks);
	
	/**
     * 修改农事记录
     * 
     * @param ncNormalWorks 农事记录信息
     * @return 结果
     */
	public int updateNcNormalWorks(NcNormalWorks ncNormalWorks);
	
	/**
     * 删除农事记录
     * 
     * @param nWorkId 农事记录ID
     * @return 结果
     */
	public int deleteNcNormalWorksById(Long nWorkId);
	
	/**
     * 批量删除农事记录
     * 
     * @param nWorkIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcNormalWorksByIds(String[] nWorkIds);
	
}