package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.NcXunshiLogs;
import java.util.List;	

/**
 * 免疫记录 数据层
 * 
 * @author ruoyi
 * @date 2019-09-02
 */
public interface NcXunshiLogsMapper 
{
	/**
     * 查询免疫记录信息
     * 
     * @param xsId 免疫记录ID
     * @return 免疫记录信息
     */
	public NcXunshiLogs selectNcXunshiLogsById(Long xsId);
	
	/**
     * 查询免疫记录列表
     * 
     * @param ncXunshiLogs 免疫记录信息
     * @return 免疫记录集合
     */
	public List<NcXunshiLogs> selectNcXunshiLogsList(NcXunshiLogs ncXunshiLogs);
	
	/**
     * 新增免疫记录
     * 
     * @param ncXunshiLogs 免疫记录信息
     * @return 结果
     */
	public int insertNcXunshiLogs(NcXunshiLogs ncXunshiLogs);
	
	/**
     * 修改免疫记录
     * 
     * @param ncXunshiLogs 免疫记录信息
     * @return 结果
     */
	public int updateNcXunshiLogs(NcXunshiLogs ncXunshiLogs);
	
	/**
     * 删除免疫记录
     * 
     * @param xsId 免疫记录ID
     * @return 结果
     */
	public int deleteNcXunshiLogsById(Long xsId);
	
	/**
     * 批量删除免疫记录
     * 
     * @param xsIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcXunshiLogsByIds(String[] xsIds);
	
}