package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.NcGasLog;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 气值 数据层
 *
 * @author ruoyi
 * @date 2019-07-31
 */
public interface NcGasLogMapper
{
	/**
     * 查询气值信息
     *
     * @param gasLogId 气值ID
     * @return 气值信息
     */
	public NcGasLog selectNcGasLogById(Long gasLogId);

	/**
     * 查询气值列表
     *
     * @param ncGasLog 气值信息
     * @return 气值集合
     */
	public List<NcGasLog> selectNcGasLogList(NcGasLog ncGasLog);

	/**
     * 新增气值
     *
     * @param ncGasLog 气值信息
     * @return 结果
     */
	public int insertNcGasLog(NcGasLog ncGasLog);

	/**
     * 修改气值
     *
     * @param ncGasLog 气值信息
     * @return 结果
     */
	public int updateNcGasLog(NcGasLog ncGasLog);

	/**
     * 删除气值
     *
     * @param gasLogId 气值ID
     * @return 结果
     */
	public int deleteNcGasLogById(Long gasLogId);

	/**
     * 批量删除气值
     *
     * @param gasLogIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcGasLogByIds(String[] gasLogIds);

	/**
	 * 前端查询用气量曲线图
	 * @return
	 */
	public List<NcGasLog>  selectNcGasLogMonthFront();

	List<NcGasLog> frontGasLogByMonth(@Param("createTime") String createTime);


    Double selectGasValByMonth(@Param("month") String last12Month);

	public Double selectGasAmount(NcGasLog ncGasLog);

	Double selectGasMonthAmount(NcGasLog ncGasLog);

	Double selectGasMonth(NcGasLog ncGasLog);
	Double selectGasMonthMin(NcGasLog ncGasLog);

	List<NcGasLog> selectGasMonthList(NcGasLog ncGasLog);


	/**
	 * 找到时间区间的头一个值
	 */
	Double seleGasTop(NcGasLog ncGasLog);

	/**
	 * 找到时间区间的末一个值
	 */
	Double seleGasLow(NcGasLog ncGasLog);

	String seleTimeLow(NcGasLog ncGasLog);
	double seleGasLowScreen(NcGasLog ncGasLog);
}