package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.NcPlan;
import com.ruoyi.system.domain.NcPlanExprot1;
import com.ruoyi.system.domain.querydata.ShowNcStandard;

import java.util.List;
import java.util.Map;

/**
 * 计划 数据层
 *
 * @author ruoyi
 * @date 2019-07-31
 */
public interface NcPlanMapper {
    /**
     * 查询计划信息
     *
     * @param planId 计划ID
     * @return 计划信息
     */
	NcPlan selectNcPlanById(Long planId);

    /**
     * 查询计划列表
     *
     * @param ncPlan 计划信息
     * @return 计划集合
     */
	List<NcPlan> selectNcPlanList(NcPlan ncPlan);


    /**
     * 查询计划列表(无权限限制)
     *
     * @param ncPlan 计划信息
     * @return 计划集合
     */
    List<NcPlan> selectNcPlanListTask(NcPlan ncPlan);

    /**
     * 查询计划列表(无status =1 无鸡蛋牛奶)
     *
     *
     * @param ncPlan 计划信息
     * @return 计划集合
     */
    List<NcPlan> showNcPlanList(NcPlan ncPlan);


    /**
     * 新增计划
     *
     * @param ncPlan 计划信息
     * @return 结果
     */
	int insertNcPlan(NcPlan ncPlan);

    /**
     * 修改计划
     *
     * @param ncPlan 计划信息
     * @return 结果
     */
	int updateNcPlan(NcPlan ncPlan);

    /**
     * 删除计划
     *
     * @param planId 计划ID
     * @return 结果
     */
	int deleteNcPlanById(Long planId);

    /**
     * 批量删除计划
     *
     * @param planIds 需要删除的数据ID
     * @return 结果
     */
	int deleteNcPlanByIds(String[] planIds);

    List<Map> selectSumByStatus(NcPlan ncPlan);

    List<NcPlan> selectNcPlanListByDeptId(NcPlan ncPlan);

    List<ShowNcStandard> showPlan(int planType);

    int showPlanNum(NcPlan planType);

    /**
     * 查询计划中的所有作物类别
     * @return
     */
    List<NcPlan> queryCropNameCnInPlan(NcPlan ncPlanQUery);

    /**
     *   根据班组查询计划集合
     * @return
     */
    List<NcPlan> queryPlanByCropNameCn(String team);

    /**
     * 屏保 日光温室种植任务
     * @param ncPlan
     * @return
     */
    List<NcPlan> showPlanLandType4(NcPlan ncPlan);

}