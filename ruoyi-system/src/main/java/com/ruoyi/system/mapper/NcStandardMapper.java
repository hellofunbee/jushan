package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.NcPlan;
import com.ruoyi.system.domain.NcStandard;
import com.ruoyi.system.domain.SysUser;
import com.ruoyi.system.domain.querydata.ShowNcStandard;

import java.util.List;
import java.util.Map;

/**
 * 标准主 数据层
 *
 * @author ruoyi
 * @date 2019-07-28
 */
public interface NcStandardMapper {
    /**
     * 查询标准主信息
     *
     * @param standardId 标准主ID
     * @return 标准主信息
     */
	NcStandard selectNcStandardById(Long standardId);

    /**
     * 查询标准主列表
     *
     * @param ncStandard 标准主信息
     * @return 标准主集合
     */
	List<NcStandard> selectNcStandardList(NcStandard ncStandard);

    /**
     * 新增标准主
     *
     * @param ncStandard 标准主信息
     * @return 结果
     */
	int insertNcStandard(NcStandard ncStandard);

    /**
     * 修改标准主
     *
     * @param ncStandard 标准主信息
     * @return 结果
     */
	int updateNcStandard(NcStandard ncStandard);

    /**
     * 删除标准主
     *
     * @param standardId 标准主ID
     * @return 结果
     */
	int deleteNcStandardById(Long standardId);

    /**
     * 批量删除标准主
     *
     * @param standardIds 需要删除的数据ID
     * @return 结果
     */
	int deleteNcStandardByIds(String[] standardIds);

    List<NcStandard> selectNcStandardListByIds(String[] toStrArray);


    /**
     * 根据类型中作物的类型查找标准
     *
     * @return
     */
    List<ShowNcStandard> showNcStandard(String standardType);

    List<Map> selectSum(NcStandard ncStandard);


    /**
     * 通过标准名查询标准
     *
     * @param standardName 标准名
     * @return 标准信息
     */
    public NcStandard selectStandardByStandardName(String standardName);
}