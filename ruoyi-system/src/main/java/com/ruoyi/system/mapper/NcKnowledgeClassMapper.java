package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.NcKnowledgeClass;
import java.util.List;

/**
 * 知识课堂 数据层
 * 
 * @author ruoyi
 * @date 2019-09-10
 */
public interface NcKnowledgeClassMapper 
{
	/**
     * 查询知识课堂信息
     * 
     * @param kcId 知识课堂ID
     * @return 知识课堂信息
     */
	public NcKnowledgeClass selectNcKnowledgeClassById(Long kcId);
	
	/**
     * 查询知识课堂列表
     * 
     * @param ncKnowledgeClass 知识课堂信息
     * @return 知识课堂集合
     */
	public List<NcKnowledgeClass> selectNcKnowledgeClassList(NcKnowledgeClass ncKnowledgeClass);

	/**
	 * 查询知识课堂列表(前台分页)
	 *
	 * @param ncKnowledgeClassType 知识课堂类别信息
	 * @return 知识课堂类别集合
	 */
	List<NcKnowledgeClass> listNcKnowledgeClassList(NcKnowledgeClass ncKnowledgeClassType);
	
	/**
     * 新增知识课堂
     * 
     * @param ncKnowledgeClass 知识课堂信息
     * @return 结果
     */
	public int insertNcKnowledgeClass(NcKnowledgeClass ncKnowledgeClass);
	
	/**
     * 修改知识课堂
     * 
     * @param ncKnowledgeClass 知识课堂信息
     * @return 结果
     */
	public int updateNcKnowledgeClass(NcKnowledgeClass ncKnowledgeClass);
	
	/**
     * 删除知识课堂
     * 
     * @param kcId 知识课堂ID
     * @return 结果
     */
	public int deleteNcKnowledgeClassById(Long kcId);
	
	/**
     * 批量删除知识课堂
     * 
     * @param kcIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcKnowledgeClassByIds(String[] kcIds);

    List<NcKnowledgeClass> selectFourNcKnowledgeClassList(Long kctypeId);

}