package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.NcCheck;

import java.util.List;

/**
 * 检测 数据层
 * 
 * @author ruoyi
 * @date 2019-08-29
 */
public interface NcCheckMapper 
{
	/**
     * 查询检测信息
     * 
     * @param checkId 检测ID
     * @return 检测信息
     */
	public NcCheck selectNcCheckById(Long checkId);
	
	/**
     * 查询检测列表
     * 
     * @param ncCheck 检测信息
     * @return 检测集合
     */
	public List<NcCheck> selectNcCheckList(NcCheck ncCheck);
	
	/**
     * 新增检测
     * 
     * @param ncCheck 检测信息
     * @return 结果
     */
	public int insertNcCheck(NcCheck ncCheck);
	
	/**
     * 修改检测
     * 
     * @param ncCheck 检测信息
     * @return 结果
     */
	public int updateNcCheck(NcCheck ncCheck);
	
	/**
     * 删除检测
     * 
     * @param checkId 检测ID
     * @return 结果
     */
	public int deleteNcCheckById(Long checkId);
	
	/**
     * 批量删除检测
     * 
     * @param checkIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcCheckByIds(String[] checkIds);

    int selectSum(NcCheck check);
}