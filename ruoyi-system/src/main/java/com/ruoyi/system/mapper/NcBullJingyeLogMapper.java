package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.NcBullJingyeLog;
import java.util.List;	

/**
 * 牛舍精液 数据层
 * 
 * @author ruoyi
 * @date 2019-08-30
 */
public interface NcBullJingyeLogMapper 
{
	/**
     * 查询牛舍精液信息
     * 
     * @param jingyeLogId 牛舍精液ID
     * @return 牛舍精液信息
     */
	public NcBullJingyeLog selectNcBullJingyeLogById(Long jingyeLogId);
	
	/**
     * 查询牛舍精液列表
     * 
     * @param ncBullJingyeLog 牛舍精液信息
     * @return 牛舍精液集合
     */
	public List<NcBullJingyeLog> selectNcBullJingyeLogList(NcBullJingyeLog ncBullJingyeLog);
	
	/**
     * 新增牛舍精液
     * 
     * @param ncBullJingyeLog 牛舍精液信息
     * @return 结果
     */
	public int insertNcBullJingyeLog(NcBullJingyeLog ncBullJingyeLog);
	
	/**
     * 修改牛舍精液
     * 
     * @param ncBullJingyeLog 牛舍精液信息
     * @return 结果
     */
	public int updateNcBullJingyeLog(NcBullJingyeLog ncBullJingyeLog);
	
	/**
     * 删除牛舍精液
     * 
     * @param jingyeLogId 牛舍精液ID
     * @return 结果
     */
	public int deleteNcBullJingyeLogById(Long jingyeLogId);
	
	/**
     * 批量删除牛舍精液
     * 
     * @param jingyeLogIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteNcBullJingyeLogByIds(String[] jingyeLogIds);
	
}