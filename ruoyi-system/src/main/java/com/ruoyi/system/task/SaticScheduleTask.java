package com.ruoyi.system.task;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.NcUtils;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Component
@Configuration
@EnableScheduling
public class SaticScheduleTask {

    @Autowired
    private INcWorkMirrorService workMirrorService;
    @Autowired
    private INcStageMirrorService stageService;

    @Autowired
    private INcPlanService planService;

    @Autowired
    private INcStandardService standardService;

    @Autowired
    private INcStageService ncStageService;

    @Autowired
    private INcWorkService workService;
    @Autowired
    private DayCensusService service;
    @Autowired
    private INcChickenDayCensusService ncChickenDayCensusService;
    @Autowired
    private INcMonthCensusService monthCensusService;
    @Autowired
    private INcChickenMonthCensusService chickenMonthCensusService;


    /*把进入当前阶段的计划的第一阶段状态改为-状态 3 -->2 */
    @Scheduled(cron = "0/30 * * * * ?")
    //@Scheduled(fixedRate=5000)
    private void startPlan() {
        String today = DateUtils.parseDateToStr("yyyy-MM-dd", new Date());
        NcPlan plan = new NcPlan();
        plan.setStatus("2");//已生计划单 但尚未完成的计划
//        plan.getParams().put("today", today);
        List<NcPlan> plans = planService.selectNcPlanListTask(plan);
        matchStandard(plans);

        for (NcPlan p : plans) {
            //标准
            NcStandard standard = standardService.selectNcStandardById(p.getStandardId());

            if (standard != null) {
                if (NcUtils.differentDaysByDate(new Date(), p.getPlanTime()) >= 0) {

                    NcStageMirror st = new NcStageMirror();
                    st.setPlanId(p.getPlanId());
                    st.setStageStatus("3");//尚未进入阶段的

//                    st.setOrderNum(1);//TODO 必须是一，录入数据时要注意
                    List<NcStageMirror> stages = stageService.selectNcStageMirrorList(st);

//                    for (NcStageMirror m : stages) {
                    if (stages != null && stages.size() > 0) {
                        NcStageMirror m = stages.get(0);
                        //第一阶段还没开始的开始
                        if ("3".equals(m.getStageStatus())) {
                            System.out.println("*********进入第一阶段***********:" + m.getStageName());
                            m.setStageStatus("2");//进入第一阶段
                            p.setStatus("3");//在执行
                            planService.updateNcPlan(p);
                            stageService.updateNcStageMirror(m);
                        }
                    }
//                    }
                }

            }
        }

    }

    /*同步镜像表（副本）--针对匹配到标准但是尚未生成标准副本的计划*/
    @Scheduled(cron = "0/30 * * * * ?")
    private void asycMirror() {
        String today = DateUtils.parseDateToStr("yyyy-MM-dd", new Date());
        /*step1 获取当前所有的进入生产的计划*/
        NcPlan plan = new NcPlan();
        plan.setStatus("2");//已生计划单 但尚未完成的计划
//        plan.getParams().put("today", today);
        List<NcPlan> plans = planService.selectNcPlanListTask(plan);


        /*step2 根据从计划获取的镜像标准id获取标准 及标准下的阶段 和工作*/

        for (NcPlan p : plans) {
            NcStandard standard = standardService.selectNcStandardById(p.getStandardId());
            if (standard != null) {

                NcStageMirror st = new NcStageMirror();
                st.setPlanId(p.getPlanId());
                List<NcStageMirror> stages = stageService.selectNcStageMirrorList(st);

                //如果镜像表里面没有阶段数据则插入
                if (stages == null || stages.size() == 0) {
                    inserMirror(standard, p);
                }
            }
        }
    }

    /**
     * 把标准的阶段 以及工作插入到镜像表中  可以在工作过程中 允许 不断修改
     *
     * @param standard
     * @param p
     * @return
     */
    private void inserMirror(NcStandard standard, NcPlan p) {
        NcStage stage = new NcStage();
        stage.setStandardId(standard.getStandardId());

        System.out.println("--------------------_____-------------------------------");
        //插入阶段
        List<NcStage> ncStages = ncStageService.selectNcStageList(stage);
        for (NcStage s : ncStages) {
            NcStageMirror m = new NcStageMirror(s, p.getPlanId());
            m.setStageStatus("3");//未进入
            stageService.insertNcStageMirror(m);
        }

        //插入工作
        NcWork w = new NcWork();
        w.setStandardId(standard.getStandardId());
        List<NcWork> works = workService.selectNcWorkList(w);
        for (NcWork work : works) {
            NcWorkMirror m = new NcWorkMirror(work, p.getPlanId());
            m.setWorkStatus("2");
            workMirrorService.insertNcWorkMirror(m);
        }

    }

    //鸡舍的日报表
    //*把进入当前阶段的计划的第一阶段状态改为-状态 3 -->2 *//**//*

   @Scheduled(cron = "0 0 0/1 * * ?")
    private void ncChickenDayCensus() {
        DayCensus c = new DayCensus();
        Date t = new Date();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        c.setDateTime(df.format(t));
        DayCensus cunAmountList = service.cunAmountList(c);
        //入购数量
        DayCensus inputAmountList = service.inputAmountList(c);
        //内供大只鸡数量
        DayCensus outputAmountBigList = service.outputAmountBigList(c);
        //内供小只鸡数量
        DayCensus outputAmountLittleList = service.outputAmountLittleList(c);
        //内销鸡数量
        DayCensus neiXiaoList = service.neiXiaoList(c);
        //主饲料数量
        DayCensus zFeedList = service.zFeedList(c);
        //辅饲料数量
        DayCensus fFeedList = service.fFeedList(c);
        //死亡数量
        DayCensus dieList = service.dieList(c);
        //死亡小只鸡数量
        DayCensus dieSmall = service.dieSmall(c);
        //死亡大只鸡数量
        DayCensus dieBig = service.dieBig(c);

        NcChickenDayCensus chickenDayCensus = new NcChickenDayCensus();
        chickenDayCensus.setCunAmount(Long.valueOf(cunAmountList.getCunAmount()));
        chickenDayCensus.setInputAmount(Long.valueOf(inputAmountList.getInputAmount()));
        chickenDayCensus.setOutputAmountBig(Long.valueOf(outputAmountBigList.getOutputAmountBig()));
        chickenDayCensus.setOutputAmountLittle(Long.valueOf(outputAmountLittleList.getOutputAmountLittle()));
        chickenDayCensus.setNeiXiao(Long.valueOf(neiXiaoList.getNeiXiao()));
        chickenDayCensus.setZFeed(Long.valueOf(zFeedList.getzFeed()));
        chickenDayCensus.setFFeed(Long.valueOf(fFeedList.getfFeed()));
        chickenDayCensus.setDie(Long.valueOf(dieList.getDie()));
        chickenDayCensus.setDieSmall(Long.valueOf(dieSmall.getDieSmall()));
        chickenDayCensus.setDieBig(Long.valueOf(dieBig.getDieBig()));
        NcChickenDayCensus c1 = new NcChickenDayCensus();
        c1.setDateTime(df.format(t));
        chickenDayCensus.setDateTime(df.format(t));
        if (ncChickenDayCensusService.selectNcChickenDayCensusList(c1).size() == 0) {
            ncChickenDayCensusService.insertNcChickenDayCensus(chickenDayCensus);

        } else {
            chickenDayCensus.setCensusId(ncChickenDayCensusService.selectNcChickenDayCensusList(c1).get(0).getCensusId());
            ncChickenDayCensusService.updateNcChickenDayCensus(chickenDayCensus);
        }

    }

    //鸡舍的月报表
    /*把进入当前阶段的计划的第一阶段状态改为-状态 3 -->2 */
    @Scheduled(cron = "0 0 0 0/1 * ?")
    private void ncChickenMonthCensus() {
        //获取上个月时间
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM");
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date); // 设置为当前时间
        calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 1); // 设置为上一个月
        date = calendar.getTime();
        String dateStr = dateFormat.format(date);
        //当前月份
        Date t = new Date();
        //时间格式为上个月的对象
        MonthCensus census = new MonthCensus();
        census.setDateTime(dateStr);
        //时间格式为本月的对象
        MonthCensus now = new MonthCensus();
        now.setDateTime(dateFormat.format(t));
        //鸡舍
        MonthCensus lZhiAmount = monthCensusService.lMonthStackAmount(census);//上月鸡库存
        MonthCensus monthAddZhiAmount = monthCensusService.monthAddAmount(now);//本月增加鸡数量
        MonthCensus monthReduceZhiAmount = monthCensusService.monthReduceZhiAmount(now);//直供数量
        MonthCensus monthReduceNeiAmount = monthCensusService.monthReduceNeiAmount(now);//内销数量
        MonthCensus monthReducesiAmount=monthCensusService.monthReduceSiAmount(now);//死亡数量
        MonthCensus chickenAmount = monthCensusService.chickenAmount(now);//盘存
        NcChickenMonthCensus chickenMonthCensus = new NcChickenMonthCensus();
        if (lZhiAmount.getlMonthStack() == null) {
            chickenMonthCensus.setLastChickenStack(Long.valueOf(0));
        } else {
            chickenMonthCensus.setLastChickenStack(Long.valueOf(lZhiAmount.getlMonthStack()));
        }

        if (monthAddZhiAmount.getMonthAdd() == null) {
            chickenMonthCensus.setChickenStack(Long.valueOf(0));
        } else {
            chickenMonthCensus.setChickenStack(Long.valueOf(monthAddZhiAmount.getMonthAdd()));
        }

        if (monthReduceZhiAmount.getMonthReduceZhi() == null) {
            chickenMonthCensus.setZhiChicken(Long.valueOf(0));
        } else {
            chickenMonthCensus.setZhiChicken(Long.valueOf(monthReduceZhiAmount.getMonthReduceZhi()));
        }

        if (monthReduceNeiAmount.getMonthReduceNei() == null) {
            chickenMonthCensus.setNeiChicken(Long.valueOf(0));
        } else {
            chickenMonthCensus.setNeiChicken(Long.valueOf(monthReduceNeiAmount.getMonthReduceNei()));
        }

        if (monthReducesiAmount.getMonthreduceSi() == null) {
            chickenMonthCensus.setSiChicken(Long.valueOf(0));
        } else {
            chickenMonthCensus.setSiChicken(Long.valueOf(monthReducesiAmount.getMonthreduceSi()));
        }
        chickenMonthCensus.setStack(BigDecimal.valueOf(chickenAmount.getSumAmount()));
        chickenMonthCensus.setType(1);

        Date t1 = new Date();
        NcChickenMonthCensus chicken1 = new NcChickenMonthCensus();
        chicken1.setDataTime(dateFormat.format(t1));
        chickenMonthCensus.setDataTime(dateFormat.format(t1));
        if (chickenMonthCensusService.selectNcChickenMonthCensusList(chicken1).size() == 0) {
            chickenMonthCensusService.insertNcChickenMonthCensus(chickenMonthCensus);

        } else {
            chickenMonthCensus.setCenusId(chickenMonthCensusService.selectNcChickenMonthCensusList(chicken1).get(0).getCenusId());
            chickenMonthCensusService.updateNcChickenMonthCensus(chickenMonthCensus);
        }
        //饲料
        MonthCensus lMonthStackAmount = monthCensusService.lZhiAmount(census);//上月饲料库存
        MonthCensus monthAddAmount = monthCensusService.monthAddZhiAmount(now);//本月增加饲料数量
        MonthCensus feedAmount = monthCensusService.feedAmount(now);//盘存
        NcChickenMonthCensus chicken = new NcChickenMonthCensus();
        chicken.setLastFeedStack(BigDecimal.valueOf(lMonthStackAmount.getlZhi()));
        chicken.setFeedStack(BigDecimal.valueOf(monthAddAmount.getMonthAddZhi()));

        chicken.setStack(BigDecimal.valueOf(feedAmount.getSumAmount()));

        chicken.setType(2);
        chicken.setDataTime(dateFormat.format(t));
        if (chickenMonthCensusService.selectNcChickenMonthCensusList(chicken1).size() == 2) {
            chicken.setCenusId(chickenMonthCensusService.selectNcChickenMonthCensusList(chicken1).get(1).getCenusId());
            chickenMonthCensusService.updateNcChickenMonthCensus(chicken);
        } else {
            chickenMonthCensusService.insertNcChickenMonthCensus(chicken);
        }
    }

    /*匹配标准*/
    private void matchStandard(List<NcPlan> plans) {

        for (NcPlan ncPlan : plans) {
            if (ncPlan.getStandardId() == null) {
                //匹配标准
                planService.matchStandard(ncPlan);
                planService.updateNcPlan(ncPlan);
            }
        }


    }


}